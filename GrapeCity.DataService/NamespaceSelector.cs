﻿using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;

namespace GrapeCity.DataService
{
    public class NamespaceSelector : IActionSelector
    {
        IActionSelector _selector;
        public NamespaceSelector(IActionSelector selector)
        {
            _selector = selector;
        }

        public ActionDescriptor SelectBestCandidate(RouteContext context, IReadOnlyList<ActionDescriptor> candidates)
        {
            return _selector.SelectBestCandidate(context, candidates);
        }

        public IReadOnlyList<ActionDescriptor> SelectCandidates(RouteContext context)
        {
            if(context == null)
            {
                return null;
            }
            var actionDescriptoers = _selector.SelectCandidates(context);

            List<ActionDescriptor> descriptors = new List<ActionDescriptor>();
            foreach (var action in actionDescriptoers)
            {
                if (action is ControllerActionDescriptor controllerAction)
                {
                    if(string.CompareOrdinal(controllerAction.ControllerName, "Metadata") == 0)
                    {
                        descriptors.Add(action);
                        continue;
                    }
                    if (controllerAction.ControllerTypeInfo.BaseType?.Name == "ODataController")
                    {
                        var pathArray = context.HttpContext.Request.Path.Value.Split('/');
                        var currentNamespace = controllerAction.ControllerTypeInfo.Namespace.ToUpperInvariant();
                        var requestNamespace = context.HttpContext.Request.Path.Value.Split('/')[1];
                        // You can implement version control here, code below will filter the not match request.
                        var requestFullNamespace = $"GrapeCity.DataService.{requestNamespace}.Controllers.{pathArray[2]}".ToUpperInvariant();
                        if (string.CompareOrdinal(currentNamespace, requestFullNamespace) == 0)
                        {
                            descriptors.Add(action);
                        }
                    }
                }
            }
            return descriptors;
        }
    }
}
