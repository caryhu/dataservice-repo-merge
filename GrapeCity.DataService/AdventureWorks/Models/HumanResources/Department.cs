﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GrapeCity.DataService.AdventureWorks.Models
{
    /// <summary>
    /// Lookup table containing the departments within the Adventure Works Cycles company.
    /// </summary>
    [Table("HumanResources_Department", Schema = "AW")]
    public partial class Department
    {
        public Department()
        {
            EmployeeDepartmentHistories = new HashSet<EmployeeDepartmentHistory>();
        }

        [Key]
        [Column("DepartmentID")]
        /// <summary>
        /// Primary key for Department records.
        /// </summary>
        public short DepartmentId { get; set; }
        [Required]
        [StringLength(50)]
        /// <summary>
        /// Name of the department.
        /// </summary>
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        /// <summary>
        /// Name of the group to which the department belongs.
        /// </summary>
        public string GroupName { get; set; }
        [Column(TypeName = "date")]
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        [InverseProperty(nameof(EmployeeDepartmentHistory.Department))]
        public virtual ICollection<EmployeeDepartmentHistory> EmployeeDepartmentHistories { get; set; }
    }
}