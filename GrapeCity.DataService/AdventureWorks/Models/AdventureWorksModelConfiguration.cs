﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Models
{
    public class AdventureWorksModelConfiguration : IModelConfiguration
    {
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            #region Human Resources
            builder.EntitySet<Department>("Departments");
            builder.EntitySet<Employee>("Employees");
            builder.EntitySet<EmployeePayHistory>("EmployeePayHistories");
            builder.EntityType<EmployeePayHistory>().HasKey(t => new
            {
                t.BusinessEntityId,
                t.RateChangeDate
            });
            builder.EntitySet<EmployeeDepartmentHistory>("EmployeeDepartmentHistories");
            builder.EntityType<EmployeeDepartmentHistory>().HasKey(t => new
            {
                t.BusinessEntityId,
                t.DepartmentId,
                t.ShiftId,
                t.StartDate
            });
            builder.EntitySet<JobCandidate>("JobCandidates");
            builder.EntitySet<Shift>("Shifts");
            #endregion

            #region Person
            builder.EntitySet<Address>("Addresses");
            builder.EntitySet<AddressType>("AddressTypes");
            builder.EntitySet<BusinessEntity>("BusinessEntities");
            builder.EntitySet<BusinessEntityAddress>("BusinessEntityAddresses");
            builder.EntitySet<BusinessEntityContact>("BusinessEntityContacts");
            builder.EntitySet<ContactType>("ContactTypes");
            builder.EntitySet<CountryRegion>("CountryRegions");
            builder.EntitySet<EmailAddress>("EmailAddresses");
            builder.EntitySet<PersonPhone>("PersonPhones");
            builder.EntitySet<PhoneNumberType>("PhoneNumberTypes");
            builder.EntitySet<Person>("Persons");
            builder.EntitySet<StateProvince>("StateProvinces");
            #endregion

            #region Production
            builder.EntitySet<BillOfMaterial>("BillOfMaterials");
            builder.EntitySet<Culture>("Cultures");
            builder.EntitySet<Illustration>("Illustrations");
            builder.EntitySet<Location>("Locations");
            builder.EntitySet<ProductCategory>("ProductCategories");
            builder.EntitySet<ProductCostHistory>("ProductCostHistories");
            builder.EntityType<ProductCostHistory>().HasKey(t => new
            {
                t.ProductId,
                t.StartDate
            });
            builder.EntitySet<ProductDescription>("ProductDescriptions");
            builder.EntitySet<ProductInventory>("ProductInventories");
            builder.EntitySet<ProductListPriceHistory>("ProductListPriceHistories");
            builder.EntityType<ProductListPriceHistory>().HasKey(t => new
            {
                t.ProductId,
                t.StartDate
            });
            builder.EntitySet<ProductModelIllustration>("ProductModelIllustrations");
            builder.EntitySet<ProductModel>("ProductModels");
            builder.EntitySet<ProductPhoto>("ProductPhotos");
            builder.EntitySet<ProductReview>("ProductReviews");
            builder.EntitySet<Product>("Products");
            builder.EntitySet<ProductSubcategory>("ProductSubcategories");
            builder.EntitySet<ScrapReason>("ScrapReasons");
            builder.EntitySet<TransactionHistory>("TransactionHistories");
            builder.EntitySet<TransactionHistoryArchive>("TransactionHistoryArchives");
            builder.EntitySet<UnitMeasure>("UnitMeasures");
            builder.EntitySet<WorkOrder>("WorkOrders");
            builder.EntitySet<WorkOrderRouting>("WorkOrderRoutings");
            #endregion

            #region Purchasing
            builder.EntitySet<ProductVendor>("ProductVendors");
            builder.EntitySet<PurchaseOrderDetail>("PurchaseOrderDetails");
            builder.EntitySet<PurchaseOrderHeader>("PurchaseOrderHeaders");
            builder.EntitySet<ShipMethod>("ShipMethods");
            builder.EntitySet<Vendor>("Vendors");
            #endregion

            #region Sales
            builder.EntitySet<CreditCard>("CreditCards");
            builder.EntitySet<Currency>("Currencies");
            builder.EntitySet<CurrencyRate>("CurrencyRates");
            builder.EntitySet<Customer>("Customers");
            builder.EntitySet<SalesOrderHeader>("SalesOrderHeaders");
            builder.EntitySet<SalesOrderDetail>("SalesOrderDetails");
            builder.EntitySet<SalesPerson>("SalesPersons");
            builder.EntitySet<SalesPersonQuotaHistory>("SalesPersonQuotaHistories");
            builder.EntitySet<SalesReason>("SalesReasons");
            builder.EntitySet<SalesTaxRate>("SalesTaxRates");
            builder.EntitySet<SalesTerritory>("SalesTerritories");
            builder.EntitySet<SalesTerritoryHistory>("SalesTerritoryHistories");
            builder.EntitySet<ShoppingCartItem>("ShoppingCartItems");
            builder.EntitySet<SpecialOffer>("SpecialOffers");
            builder.EntitySet<Store>("Stores");
            #endregion
        }
    }
}
