﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GrapeCity.DataService.AdventureWorks.Models
{
    [Table("Person_Address", Schema = "AW")]
    /// <summary>
    /// Street address information for customers, employees, and vendors.
    /// </summary>
    public partial class Address
    {
        public Address()
        {
            BusinessEntityAddresses = new HashSet<BusinessEntityAddress>();
            SalesOrdersBilledTo = new HashSet<SalesOrderHeader>();
            SalesOrdersShippedTo = new HashSet<SalesOrderHeader>();
        }

        [Key]
        [Column("AddressID")]
        /// <summary>
        /// Primary key for Address records.
        /// </summary>
        public int AddressId { get; set; }
       
        [Required]
        [StringLength(60)]
        /// <summary>
        /// First street address line.
        /// </summary>
        public string AddressLine1 { get; set; }
     
        [StringLength(60)]
        /// <summary>
        /// Second street address line.
        /// </summary>
        public string AddressLine2 { get; set; }
     
        [Required]
        [StringLength(30)]
        /// <summary>
        /// Name of the city.
        /// </summary>
        public string City { get; set; }
        [Column("StateProvinceID")]
        /// <summary>
        /// Unique identification number for the state or province. Foreign key to StateProvince table.
        /// </summary>
        public int StateProvinceId { get; set; }
        [Required]
        [StringLength(15)]
        /// <summary>
        /// Postal code for the street address.
        /// </summary>
        public string PostalCode { get; set; }
        [Column("rowguid")]
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public Guid Rowguid { get; set; }
        [Column(TypeName = "date")]
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        [ForeignKey(nameof(StateProvinceId))]
        [InverseProperty(nameof(Models.StateProvince.Addresses))]
        public virtual StateProvince StateProvince { get; set; }

        [InverseProperty(nameof(BusinessEntityAddress.Address))]
        public virtual ICollection<BusinessEntityAddress> BusinessEntityAddresses { get; set; }
        
        [InverseProperty(nameof(SalesOrderHeader.BillToAddress))]
        public virtual ICollection<SalesOrderHeader> SalesOrdersBilledTo { get; set; }
        
        [InverseProperty(nameof(SalesOrderHeader.ShipToAddress))]
        public virtual ICollection<SalesOrderHeader> SalesOrdersShippedTo { get; set; }
    }
}