﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GrapeCity.DataService.AdventureWorks.Models
{
    [Table("Sales_SalesOrderDetail", Schema = "AW")]
    /// <summary>
    /// Individual products associated with a specific sales order. See SalesOrderHeader.
    /// </summary>
    public partial class SalesOrderDetail
    {
        [Key]
        [Column("SalesOrderID")]
        /// <summary>
        /// Primary key. Foreign key to SalesOrderHeader.SalesOrderID.
        /// </summary>
        public int SalesOrderId { get; set; }
        [Key]
        [Column("SalesOrderDetailID")]
        /// <summary>
        /// Primary key. One incremental unique number per product sold.
        /// </summary>
        public int SalesOrderDetailId { get; set; }
        [StringLength(25)]
        /// <summary>
        /// Shipment tracking number supplied by the shipper.
        /// </summary>
        public string CarrierTrackingNumber { get; set; }
        /// <summary>
        /// Quantity ordered per product.
        /// </summary>
        public short OrderQty { get; set; }
        [Column("ProductID")]
        /// <summary>
        /// Product sold to customer. Foreign key to Product.ProductID.
        /// </summary>
        public int ProductId { get; set; }
        [Column("SpecialOfferID")]
        /// <summary>
        /// Promotional code. Foreign key to SpecialOffer.SpecialOfferID.
        /// </summary>
        public int SpecialOfferId { get; set; }
        [Column(TypeName = "money")]
        /// <summary>
        /// Selling price of a single product.
        /// </summary>
        public decimal UnitPrice { get; set; }
        [Column(TypeName = "money")]
        /// <summary>
        /// Discount amount.
        /// </summary>
        public decimal UnitPriceDiscount { get; set; }
        [Column(TypeName = "numeric(38, 6)")]
        /// <summary>
        /// Per product subtotal. Computed as UnitPrice * (1 - UnitPriceDiscount) * OrderQty.
        /// </summary>
        public decimal LineTotal { get; set; }
        [Column("rowguid")]
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public Guid Rowguid { get; set; }
        [Column(TypeName = "date")]
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        [ForeignKey(nameof(SalesOrderId))]
        [InverseProperty(nameof(SalesOrderHeader.SalesOrderDetails))]
        public virtual SalesOrderHeader SalesOrder { get; set; }
        [ForeignKey("SpecialOfferId,ProductId")]
        [InverseProperty(nameof(Models.SpecialOfferProduct.SalesOrderDetails))]
        public virtual SpecialOfferProduct SpecialOfferProduct { get; set; }
    }
}