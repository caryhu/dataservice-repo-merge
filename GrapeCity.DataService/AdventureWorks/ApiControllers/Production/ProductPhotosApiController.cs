﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productPhotos")]
    [ApiController]
    public class ProductPhotosApiController : ControllerBase
    {
        private readonly IProductPhotoService _productPhotoService;

        public ProductPhotosApiController(IProductPhotoService productPhotoService)
        {
            _productPhotoService = productPhotoService;
        }

        // GET: api/productPhotos/1
        [HttpGet("{photoId}")]
        public ActionResult<ProductPhotoDTO> GetProductPhoto(int photoId)
        {
            var photo = _productPhotoService.GetProductPhoto(photoId).FirstOrDefault();

            if(photo == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProdPhotoDto(photo);
        }

        // GET: api/productPhotos
        [HttpGet]
        public IQueryable<ProductPhotoDTO> GetProductPhotos()
        {
            return _productPhotoService.GetProductPhotos().Select(ProductionDtoConverter.AsProdPhotoDto);
        }
    }
}
