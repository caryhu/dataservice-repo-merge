﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productReviews")]
    [ApiController]
    public class ProductReviewsApiController : ControllerBase
    {
        private readonly IProductReviewService _productReviewService;

        public ProductReviewsApiController(IProductReviewService productReviewService)
        {
            _productReviewService = productReviewService;
        }

        // GET: api/productReviews/1/product
        [HttpGet("{reviewId}/product")]
        public ActionResult<ProductDTO> GetProduct(int reviewId)
        {
            var product = _productReviewService.GetProduct(reviewId).FirstOrDefault();

            if(product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        // GET: api/productReviews/1
        [HttpGet("{reviewId}")]
        public ActionResult<ProductReviewDTO> GetProductReview(int reviewId)
        {
            var review = _productReviewService.GetProductReview(reviewId).FirstOrDefault();

            if (review == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProdReviewDto(review);
        }

        // GET: api/productReviews
        [HttpGet]
        public IQueryable<ProductReviewDTO> GetProductReviews()
        {
            return _productReviewService.GetProductReviews().Select(ProductionDtoConverter.AsProdReviewDto);
        }
    }
}
