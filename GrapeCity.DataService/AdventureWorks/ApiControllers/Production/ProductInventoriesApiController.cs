﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productInventories")]
    [ApiController]
    public class ProductInventoriesApiController : ControllerBase
    {
        private readonly IProductInventoryService _productInventoryService;

        public ProductInventoriesApiController(IProductInventoryService productInventoryService)
        {
            _productInventoryService = productInventoryService;
        }

        // GET: api/productInventories/location?productId=5&locationId=1
        [HttpGet("location")]
        public ActionResult<LocationDTO> GetLocation([FromQuery] int productId,[FromQuery] short locationId)
        {
            var location = _productInventoryService.GetLocation(productId, locationId).FirstOrDefault();

            if(location == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToLocationDto(location);
        }

        // GET: api/productInventories/product?productId=5&locationId=1
        [HttpGet("product")]
        public ActionResult<ProductDTO> GetProduct([FromQuery] int productId, [FromQuery] short locationId)
        {
            var product = _productInventoryService.GetProduct(productId, locationId).FirstOrDefault();

            if (product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        // GET: api/productInventories
        // GET: api/productInventories?productId=5&locationId=1
        [HttpGet]
        public IQueryable<ProductInventoryDTO> GetProductInventories([FromQuery] int? productId, [FromQuery] short? locationId)
        {
            return _productInventoryService.GetProductInventories(productId, locationId).Select(ProductionDtoConverter.AsProductInventoryDTO);
        }
    }
}
