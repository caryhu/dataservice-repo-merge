﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productCategories")]
    [ApiController]
    public class ProductCategoriesApiController : ControllerBase
    {
        private readonly IProductCategoryService _productCategoryService;

        public ProductCategoriesApiController(IProductCategoryService productCategoryService)
        {
            _productCategoryService = productCategoryService;
        }

        // GET: api/productCategories
        [HttpGet]
        public IQueryable<ProductCategoryDTO> GetProductCategories()
        {
            return _productCategoryService.GetProductCategories().Select(ProductionDtoConverter.AsProductCategoryDto);
        }

        // GET: api/productCategories/6
        [HttpGet("{categoryId}")]
        public ActionResult<ProductCategoryDTO> GetProductCategory(int categoryId)
        {
            var category = _productCategoryService.GetProductCategory(categoryId).FirstOrDefault();

            if(category == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductCategoryDto(category);
        }

        // GET: api/productCategories/6/products
        [HttpGet("{categoryId}/products")]
        public IQueryable<ProductDTO> GetProducts(int categoryId)
        {
            return _productCategoryService.GetProducts(categoryId).Select(ProductionDtoConverter.AsProductDto);
        }

        // GET: api/productCategories/7/subCategories
        [HttpGet("{categoryId}/subCategories")]
        public IQueryable<ProductSubcategoryDTO> GetProductSubcategories(int categoryId)
        {
            return _productCategoryService.GetProductSubcategories(categoryId).Select(ProductionDtoConverter.AsProductSubcategoryDto);
        }

        // GET: api/productCategories/5/subCategories/1/products
        [HttpGet("{categoryId}/subCategories/{subcategoryId}/products")]
        public IQueryable<ProductDTO> GetSubcategoriesProduct(int categoryId, int subcategoryId)
        {
            return _productCategoryService.GetSubcategoriesProduct(categoryId, subcategoryId).Select(ProductionDtoConverter.AsProductDto);
        }
    }
}