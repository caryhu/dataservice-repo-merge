﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/cultures")]
    [ApiController]
    public class CulturesApiController : ControllerBase
    {
        private readonly ICultureService _cultureService;

        public CulturesApiController(ICultureService cultureService)
        {
            _cultureService = cultureService;
        }

        // GET: api/cultures/en
        [HttpGet("{cultureId}")]
        public ActionResult<CultureDTO> GetCulture(string cultureId)
        {
            var culture = _cultureService.GetCulture(cultureId).FirstOrDefault();

            if(culture == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToCultureDto(culture);
        }

        // GET: api/cultures
        [HttpGet]
        public IQueryable<CultureDTO> GetCultures()
        {
            return _cultureService.GetCultures().Select(ProductionDtoConverter.AsCultureDto);
        }

        // GET: api/cultures/default
        [HttpGet("default")]
        public ActionResult<CultureDTO> GetInvariantCulture()
        {
            var invariantCulture = _cultureService.GetInvariantCulture().FirstOrDefault();

            if (invariantCulture == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToCultureDto(invariantCulture);
        }

        // GET: api/cultures/en/productModelDescriptions
        [HttpGet("{cultureId}/productModelDescriptions")]
        public IQueryable<ProductDescriptionDTO> GetProductModelDescriptions(string cultureId)
        {
            return _cultureService.GetProductModelDescriptions(cultureId).
                Select(ProductionDtoConverter.AsProductDescriptionDto);
        }
    }
}
