﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/unitMeasures")]
    [ApiController]
    public class UnitMeasuresApiController : ControllerBase
    {
        private readonly IUnitMeasureService _unitMeasureService;

        public UnitMeasuresApiController(IUnitMeasureService unitMeasureService)
        {
            _unitMeasureService = unitMeasureService;
        }

        [HttpGet("{measureCode}/billOfMaterials")]
        public IQueryable<BillOfMaterialDTO> GetBillOfMaterials(string measureCode)
        {
            return _unitMeasureService.GetBillOfMaterials(measureCode).Select(ProductionDtoConverter.AsBillOfMaterialDto);
        }

        [HttpGet("{measureCode}/productSizeUnits")]
        public IQueryable<UnitMeasureDTO> GetProductSizeUnits(string measureCode)
        {
            return _unitMeasureService.GetProductSizeUnits(measureCode).Select(ProductionDtoConverter.AsUnitMeasureDto);
        }

        [HttpGet("{measureCode}/productVendors")]
        public IQueryable<ProductVendorDTO> GetProductVendors(string measureCode)
        {
            return _unitMeasureService.GetProductVendors(measureCode).Select(PurchaseDtoConverter.AsProductVendorDto);
        }

        [HttpGet("{measureCode}/productWeightUnits")]
        public IQueryable<UnitMeasureDTO> GetProductWeightUnits(string measureCode)
        {
            return _unitMeasureService.GetProductWeightUnits(measureCode).Select(ProductionDtoConverter.AsUnitMeasureDto);
        }

        [HttpGet("{measureCode}")]
        public ActionResult<UnitMeasureDTO> GetUnitMeasure(string measureCode)
        {
            var unitMeasure = _unitMeasureService.GetUnitMeasure(measureCode).FirstOrDefault();

            if(unitMeasure == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToUnitMeasureDto(unitMeasure);
        }

        [HttpGet]
        public IQueryable<UnitMeasureDTO> GetUnitMeasures()
        {
            return _unitMeasureService.GetUnitMeasures().Select(ProductionDtoConverter.AsUnitMeasureDto);
        }
    }
}
