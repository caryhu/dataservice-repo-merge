﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productDescriptions")]
    [ApiController]
    public class ProductDescriptionsApiController : ControllerBase
    {
        private readonly IProductDescriptionService _productDescriptionService;

        public ProductDescriptionsApiController(IProductDescriptionService productDescriptionService)
        {
            _productDescriptionService = productDescriptionService;
        }

        // GET : api/productDescriptions/1
        [HttpGet("{descriptionId}")]
        public ActionResult<ProductDescriptionDTO> GetProductDescription(int descriptionId)
        {
            var description = _productDescriptionService.GetProductDescription(descriptionId).FirstOrDefault();

            if(description == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDescriptionDto(description);
        }

        // GET : api/productDescriptions
        [HttpGet]
        public IQueryable<ProductDescriptionDTO> GetProductDescriptions()
        {
            return _productDescriptionService.GetProductDescriptions().Select(ProductionDtoConverter.AsProductDescriptionDto);
        }
    }
}
