﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productSubcategories")]
    [ApiController]
    public class ProductSubcategoriesApiController : ControllerBase
    {
        private readonly IProductSubcategoryService _productSubcategoryService;

        public ProductSubcategoriesApiController(IProductSubcategoryService productSubcategoryService)
        {
            _productSubcategoryService = productSubcategoryService;
        }

        [HttpGet("{subcategoryId}/category")]
        public ActionResult<ProductCategoryDTO> GetCategory(int subcategoryId)
        {
            var category = _productSubcategoryService.GetCategory(subcategoryId).FirstOrDefault();

            if (category == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductCategoryDto(category);
        }

        [HttpGet("{subcategoryId}/products")]
        public IQueryable<ProductDTO> GetProducts(int subcategoryId)
        {
            return _productSubcategoryService.GetProducts(subcategoryId).Select(ProductionDtoConverter.AsProductDto);
        }

        [HttpGet]
        public IQueryable<ProductSubcategoryDTO> GetProductSubcategories()
        {
            return _productSubcategoryService.GetProductSubcategories().Select(ProductionDtoConverter.AsProductSubcategoryDto);
        }

        [HttpGet("{subcategoryId}")]
        public ActionResult<ProductSubcategoryDTO> GetProductSubcategory(int subcategoryId)
        {
            var subcategory = _productSubcategoryService.GetProductSubcategory(subcategoryId).FirstOrDefault();

            if(subcategory == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductSubcateogryDto(subcategory);
        }
    }
}
