﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/workOrders")]
    [ApiController]
    public class WorkOrdersApiController : ControllerBase
    {
        private readonly IWorkOrderService _workOrderService;

        public WorkOrdersApiController(IWorkOrderService workOrderService)
        {
            _workOrderService = workOrderService;
        }

        // GET: api/workOrders/1/product
        [HttpGet("{workOrderId}/product")]
        public ActionResult<ProductDTO> GetProduct(int workOrderId)
        {
            var product = _workOrderService.GetProduct(workOrderId).FirstOrDefault();

            if(product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        // GET: api/workOrders/91/scrapReason
        [HttpGet("{workOrderId}/scrapReason")]
        public ActionResult<ScrapReasonDTO> GetScrapReason(int workOrderId)
        {
            var scrapReason = _workOrderService.GetScrapReason(workOrderId).FirstOrDefault();

            if (scrapReason == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToScrapReasonDto(scrapReason);
        }

        // GET: api/workOrders/1
        [HttpGet("{workOrderId}")]
        public ActionResult<WorkOrderDTO> GetWorkOrder(int workOrderId)
        {
            var workOrder = _workOrderService.GetWorkOrder(workOrderId).FirstOrDefault();

            if (workOrder == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToWorkOrderDto(workOrder);
        }

        // GET: api/workOrders/13/workOrderRoutings
        [HttpGet("{workOrderId}/workOrderRoutings")]
        public IQueryable<WorkOrderRoutingDTO> GetWorkOrderRoutings(int workOrderId)
        {
            return _workOrderService.GetWorkOrderRoutings(workOrderId).Select(ProductionDtoConverter.AsWorkOrderRouting);
        }


        /// <summary>
        /// Returns Paginated Work Orders data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Work Orders data</returns>
        [HttpGet]
        public IQueryable<WorkOrderDTO> GetWorkOrders([FromQuery]PagingParameters pagingParameters)
        {
            var source = _workOrderService.GetWorkOrders();
            var response = source.CreatePagedResponse(pagingParameters, ProductionDtoConverter.AsWorkOrderDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }
    }
}
