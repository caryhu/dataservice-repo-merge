﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productPriceHistories")]
    [ApiController]
    public class ProductListPriceHistoriesApiController : ControllerBase
    {
        private readonly IProductListPriceHistoryService _productListPriceHistoryService;

        public ProductListPriceHistoriesApiController(IProductListPriceHistoryService productListPriceHistoryService)
        {
            _productListPriceHistoryService = productListPriceHistoryService;
        }

        // GET: api/productPriceHistories/707/product
        [HttpGet("{productId}/product")]
        public ActionResult<ProductDTO> GetProduct(int productId)
        {
            var product = _productListPriceHistoryService.GetProduct(productId).FirstOrDefault();

            if(product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        // GET: api/productPriceHistories
        [HttpGet]
        public IQueryable<ProductListPriceHistoryDTO> GetProductListPriceHistories()
        {
            return _productListPriceHistoryService.GetProductListPriceHistories().Select(ProductionDtoConverter.AsProdListPriceHistoryDto);
        }

        /// <summary>
        ///     Gets ProductPriceHistories by productId and startDate
        /// </summary>
        /// <param name="productId">
        ///     Id of product for which product price histories will be fetched.      
        /// </param>
        /// <param name="startDate">
        ///      This date will be used to fetch all product price histories where startdate value matches or is greater than the provided value.
        /// </param>
        /// <returns></returns>
        // GET: api/productPriceHistories/707
        // GET: api/productPriceHistories/707?startDate=2011/5/31
        [HttpGet("{productId}")]
        public IQueryable<ProductListPriceHistoryDTO> GetProductListPriceHistories(int productId, DateTime? startDate)
        {
            return _productListPriceHistoryService.GetProductListPriceHistories(productId, startDate).Select(ProductionDtoConverter.AsProdListPriceHistoryDto);
        }
    }
}
