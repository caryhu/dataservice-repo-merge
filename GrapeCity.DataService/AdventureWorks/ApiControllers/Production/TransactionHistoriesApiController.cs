﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/transactionHistories")]
    [ApiController]
    public class TransactionHistoriesApiController : ControllerBase
    {
        private readonly ITransactionHistoryService _transactionHistoryService;

        public TransactionHistoriesApiController(ITransactionHistoryService transactionHistoryService)
        {
            _transactionHistoryService = transactionHistoryService;
        }

        // GET: api/transactionHistories/1/product
        [HttpGet("{transactionId}/product")]
        public ActionResult<ProductDTO> GetProduct(int transactionId)
        {
            var product = _transactionHistoryService.GetProduct(transactionId).FirstOrDefault();

            if(product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        /// <summary>
        /// Returns Paginated Transaction History data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Transaction History data</returns>
        [HttpGet]
        public IQueryable<TransactionDTO> GetTransactionHistories([FromQuery]PagingParameters pagingParameters)
        {
            var source = _transactionHistoryService.GetTransactionHistories();
            var response = source.CreatePagedResponse(pagingParameters, ProductionDtoConverter.AsTransactionHistoryDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        // GET: api/transactionHistories/1
        [HttpGet("{transactionId}")]
        public ActionResult<TransactionDTO> GetTransactionHistory(int transactionId)
        {
            var transaction = _transactionHistoryService.GetTransactionHistory(transactionId).FirstOrDefault();

            if (transaction == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToTransactionHistoryDto(transaction);
        }
    }
}
