﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/workOrderRoutings")]
    [ApiController]
    public class WorkOrderRoutingsApiController : ControllerBase
    {
        private readonly IWorkOrderRoutingService _workOrderRoutingService;

        public WorkOrderRoutingsApiController(IWorkOrderRoutingService workOrderRoutingService)
        {
            _workOrderRoutingService = workOrderRoutingService;
        }

        // GET: api/workOrderRoutings/location?workOrderId=13&productId=747&operationSequence=1
        [HttpGet("location")]
        public ActionResult<LocationDTO> GetLocation(int workOrderId, int productId, short operationSequence)
        {
            var location = _workOrderRoutingService.GetLocation(workOrderId, productId, operationSequence).FirstOrDefault();

            if (location == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToLocationDto(location);
        }

        // GET: api/workOrderRoutings/workOrder?workOrderId=13&productId=747&operationSequence=1
        [HttpGet("workOrder")]
        public ActionResult<WorkOrderDTO> GetWorkOrder(int workOrderId, int productId, short operationSequence)
        {
            var workorder = _workOrderRoutingService.GetWorkOrder(workOrderId, productId, operationSequence).FirstOrDefault();

            if(workorder == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToWorkOrderDto(workorder);
        }

        /// <summary>
        /// Returns Paginated Work Order Routing data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="workOrderId"></param>
        /// <param name="productId"></param>
        /// <param name="operationSequence"></param>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Work Order Routing  data</returns>
        [HttpGet]
        public IQueryable<WorkOrderRoutingDTO> GetWorkOrderRoutings(int? workOrderId, int? productId, short? operationSequence, [FromQuery] PagingParameters pagingParameters)
        {
            var source = _workOrderRoutingService.GetWorkOrderRoutings(workOrderId, productId, operationSequence);
            var response = source.CreatePagedResponse(pagingParameters, ProductionDtoConverter.AsWorkOrderRouting);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }
    }
}
