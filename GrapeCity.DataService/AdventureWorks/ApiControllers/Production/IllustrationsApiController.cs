﻿using System.Linq;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/illustrations")]
    [ApiController]
    public class IllustrationsApiController : ControllerBase
    {
        private readonly IIllustrationService _illustrationService;

        public IllustrationsApiController(IIllustrationService illustrationService)
        {
            _illustrationService = illustrationService;
        }

        // GET: api/illustrations/5
        [HttpGet("{illustrationId}")]
        public ActionResult<IllustrationDTO> GetIllustration(int illustrationId)
        {
            var illustration = _illustrationService.GetIllustration(illustrationId).FirstOrDefault();

            if (illustration == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToIllustrationDTo(illustration);
        }

        // GET: api/illustrations
        [HttpGet]
        public IQueryable<IllustrationDTO> GetIllustrations()
        {
            return _illustrationService.GetIllustrations().Select(ProductionDtoConverter.AsIllustrationDto);
        }

        // GET: api/illustrations/5/productModelIllustrations
        [HttpGet("{illustrationId}/productModelIllustrations")]
        public IQueryable<ProductModelIllustrationDTO> GetProductModelIllustrations(int illustrationId)
        {
            return _illustrationService.GetProductModelIllustrations(illustrationId).Select(ProductionDtoConverter.AsProdModelIllustrationDto);
        }
    }
}