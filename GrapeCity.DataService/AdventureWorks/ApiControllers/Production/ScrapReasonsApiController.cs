﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/scrapReasons")]
    [ApiController]
    public class ScrapReasonsApiController : ControllerBase
    {
        private readonly IScrapReasonService _scrapReasonService;

        public ScrapReasonsApiController(IScrapReasonService scrapReasonService)
        {
            _scrapReasonService = scrapReasonService;
        }
        
        // GET: api/scrapReasons/2
        [HttpGet("{scrapReasonId}")]
        public ActionResult<ScrapReasonDTO> GetScrapReason(short scrapReasonId)
        {
            var scrapReason = _scrapReasonService.GetScrapReason(scrapReasonId).FirstOrDefault();

            if(scrapReason == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToScrapReasonDto(scrapReason);
        }

        // GET: api/scrapReasons
        [HttpGet]
        public IQueryable<ScrapReasonDTO> GetScrapReasons()
        {
            return _scrapReasonService.GetScrapReasons().Select(ProductionDtoConverter.AsScrapReasonDto);
        }

        // GET: api/scrapReasons/2/workOrders
        [HttpGet("{scrapReasonId}/workOrders")]
        public IQueryable<WorkOrderDTO> GetWorkOrders(short scrapReasonId)
        {
            return _scrapReasonService.GetWorkOrders(scrapReasonId).Select(ProductionDtoConverter.AsWorkOrderDto);
        }
    }
}
