﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/billOfMaterials")]
    [ApiController]
    public class BillOfMaterialsApiController : ControllerBase
    {
        private readonly IBillOfMaterialService _billOfMaterialService;

        public BillOfMaterialsApiController(IBillOfMaterialService billOfMaterialService)
        {
            _billOfMaterialService = billOfMaterialService;
        }

        // GET: api/billOfMaterials/4
        [HttpGet("{billId}")]
        public ActionResult<BillOfMaterialDTO> GetBillOfMaterial(int billId)
        {
            var billOfMaterial = _billOfMaterialService.GetBillOfMaterial(billId).FirstOrDefault();

            if(billOfMaterial == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToBillOfMaterialDto(billOfMaterial);
        }

        /// <summary>
        /// Returns Paginated Bill of materials data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Bill of materials data</returns>
        [HttpGet]
        public IQueryable<BillOfMaterialDTO> GetBillOfMaterials([FromQuery] PagingParameters pagingParameters)
        {
            var source = _billOfMaterialService.GetBillOfMaterials();
            var response = source.CreatePagedResponse(pagingParameters, ProductionDtoConverter.AsBillOfMaterialDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        // GET: api/billOfMaterials/4/component
        [HttpGet("{billId}/component")]
        public ActionResult<ProductDTO> GetComponent(int billId)
        {
            var component = _billOfMaterialService.GetComponent(billId).FirstOrDefault();

            if (component == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(component);
        }

        // GET: api/billOfMaterials/4/parentProduct
        [HttpGet("{billId}/parentProduct")]
        public ActionResult<ProductDTO> GetParentProduct(int billId)
        {
            var parentProduct = _billOfMaterialService.GetParentProduct(billId).FirstOrDefault();

            if (parentProduct == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(parentProduct);
        }

        // GET: api/billOfMaterials/4/measureUnit
        [HttpGet("{billId}/measureUnit")]
        public ActionResult<UnitMeasureDTO> GetUnitMeasure(int billId)
        {
            var unitMeasure = _billOfMaterialService.GetUnitMeasure(billId).FirstOrDefault();

            if (unitMeasure == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToUnitMeasureDto(unitMeasure);
        }
    }
}
