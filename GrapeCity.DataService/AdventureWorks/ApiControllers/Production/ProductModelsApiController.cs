﻿using System.Linq;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productModels")]
    [ApiController]
    public class ProductModelsApiController : ControllerBase
    {
        private readonly IProductModelService _productModelService;

        public ProductModelsApiController(IProductModelService productModelService)
        {
            _productModelService = productModelService;
        }

        // GET: api/productModels/1
        [HttpGet("{modelId}")]
        public ActionResult<ProductModelDTO> GetProductModel(int modelId)
        {
            var model = _productModelService.GetProductModel(modelId).FirstOrDefault();

            if(model == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductModelDto(model);
        }

        // GET: api/productModels/1/descriptions
        [HttpGet("{modelId}/descriptions")]
        public IQueryable<ProductDescriptionDTO> GetProductModelDescriptions(int modelId)
        {
            return _productModelService.GetProductModelDescriptions(modelId).Select(ProductionDtoConverter.AsProductDescriptionDto);
        }

        // GET: api/productModels/1/descriptions/en
        [HttpGet("{modelId}/descriptions/{cultureId}")]
        public ActionResult<ProductDescriptionDTO> GetProductModelDescription(int modelId, string cultureId)
        {
            var description = _productModelService.GetProductModelDescription(modelId, cultureId).FirstOrDefault();

            if(description == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDescriptionDto(description);
        }

        // GET: api/productModels
        [HttpGet]
        public IQueryable<ProductModelDTO> GetProductModels()
        {
            return _productModelService.GetProductModels().Select(ProductionDtoConverter.AsProductModelDto);
        }
    }
}