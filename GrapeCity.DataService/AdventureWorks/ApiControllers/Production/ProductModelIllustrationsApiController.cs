﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productModelIllustrations")]
    [ApiController]
    public class ProductModelIllustrationsApiController : ControllerBase
    {
        private readonly IProductModelIllustrationService _productModelIllustrationService;

        public ProductModelIllustrationsApiController(IProductModelIllustrationService productModelIllustrationService)
        {
            _productModelIllustrationService = productModelIllustrationService;
        }

        // GET: api/productModelIllustrations/illustration?modelId=2&illustrationId=1
        [HttpGet("illustration")]
        public ActionResult<IllustrationDTO> GetIllustration(int modelId, int illustrationId)
        {
            var illustration = _productModelIllustrationService.GetIllustration(modelId, illustrationId).FirstOrDefault();

            if(illustration == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToIllustrationDTo(illustration);
        }

        // GET: api/productModelIllustrations/model?modelId=2&illustrationId=1
        [HttpGet("model")]
        public ActionResult<ProductModelDTO> GetProductModel(int modelId, int illustrationId)
        {
            var model = _productModelIllustrationService.GetProductModel(modelId, illustrationId).FirstOrDefault();

            if (model == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductModelDto(model);
        }

        // GET: api/productModelIllustrations
        [HttpGet]
        public IQueryable<ProductModelIllustrationDTO> GetProductModelIllustrations()
        {
            return _productModelIllustrationService.GetProductModelIllustrations().Select(ProductionDtoConverter.AsProdModelIllustrationDto);
        }
    }
}
