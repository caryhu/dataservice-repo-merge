﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/transactionHistoryArchives")]
    [ApiController]
    public class TransactionHistoryArchivesApiController : ControllerBase
    {
        private readonly ITransactionHistoryArchiveService _transactionHistoryArchiveService;

        public TransactionHistoryArchivesApiController(ITransactionHistoryArchiveService transactionHistoryArchiveService)
        {
            _transactionHistoryArchiveService = transactionHistoryArchiveService;
        }

        // GET: api/transactionHistoryArchives/125
        [HttpGet("{transactionId}")]
        public ActionResult<TransactionDTO> GetTransactionHistoryArchive(int transactionId)
        {
            var transaction = _transactionHistoryArchiveService.GetTransactionHistoryArchive(transactionId).FirstOrDefault();

            if(transaction == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToTransactionHistoryArchDto(transaction);
        }

        /// <summary>
        /// Returns Paginated Transaction History Archives data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Transaction History Archives data</returns>
        [HttpGet]
        public IQueryable<TransactionDTO> GetTransactionHistoryArchives([FromQuery]PagingParameters pagingParameters)
        {
            var source = _transactionHistoryArchiveService.GetTransactionHistoryArchives();
            var response = source.CreatePagedResponse(pagingParameters, ProductionDtoConverter.AsTransactionHistoryArchDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }
    }
}
