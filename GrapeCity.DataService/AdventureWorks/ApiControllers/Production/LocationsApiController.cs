﻿using System.Linq;
using System.Linq.Expressions;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/locations")]
    [ApiController]
    public class LocationsApiController : ControllerBase
    {
        private readonly ILocationService _locationService;

        public LocationsApiController(ILocationService locationService)
        {
            _locationService = locationService;
        }

        // GET: api/locations/1
        [HttpGet("{locationId}")]
        public ActionResult<LocationDTO> GetLocation(short locationId)
        {
            var location = _locationService.GetLocation(locationId).FirstOrDefault();

            if (location == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToLocationDto(location);
        }

        // GET: api/locations
        [HttpGet]
        public IQueryable<LocationDTO> GetLocations()
        {
            return _locationService.GetLocations().Select(ProductionDtoConverter.AsLocationDto);
        }

        // Get: api/locations/5/productInventories
        [HttpGet("{locationId}/productInventories")]
        public IQueryable<ProductInventoryDTO> GetProductInventories(short locationId)
        {
            return _locationService.GetProductInventories(locationId).ToList().AsQueryable()
                .Select(ProductionDtoConverter.AsProductInventoryDTO);
        }

        // Get: api/locations/10/workOrderRoutings
        [HttpGet("{locationId}/workOrderRoutings")]
        public IQueryable<WorkOrderRoutingDTO> GetWorkOrderRoutings(short locationId)
        {
            return _locationService.GetWorkOrderRoutings(locationId).Select(ProductionDtoConverter.AsWorkOrderRouting);
        }
    }
}