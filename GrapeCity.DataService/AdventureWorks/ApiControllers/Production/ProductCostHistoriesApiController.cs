﻿using System;
using System.Linq;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productCostHistories")]
    [ApiController]
    public class ProductCostHistoriesApiController : ControllerBase
    {
        private readonly IProductCostHistoryService _productCostHistoryService;

        public ProductCostHistoriesApiController(IProductCostHistoryService productCostHistoryService)
        {
            _productCostHistoryService = productCostHistoryService;
        }

        // GET: api/productCostHistories
        [HttpGet]
        public IQueryable<ProductCostHistoryDTO> GetProductCostHistories()
        {
            return _productCostHistoryService.GetProductCostHistories().Select(ProductionDtoConverter.AsProductCostHistoryDto);
        }

        /// <summary>
        ///     Gets ProductCostHistories by productId and startDate.
        /// </summary>
        /// <param name="productId">
        ///     Id of product for which product cost histories will be fetched.
        /// </param>
        /// <param name="startDate">
        ///     This date will be used to fetch all product cost histories where startdate value matches or is greater than the provided value.
        /// </param>
        /// <returns></returns>
        // GET: api/productCostHistories/707?startDate=2011/05/31
        [HttpGet("{productId}")]
        public IQueryable<ProductCostHistoryDTO> GetProductCostHistories(int productId,[FromQuery] DateTime? startDate)
        {
            return _productCostHistoryService.GetProductCostHistories(productId, startDate).Select(ProductionDtoConverter.AsProductCostHistoryDto);
        }

        // GET: api/productCostHistories/708/product
        [HttpGet("{productId}/product")]
        public ActionResult<ProductDTO> GetProduct(int productId)
        {
            var product = _productCostHistoryService.GetProduct(productId).FirstOrDefault();

            if(product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }
    }
}