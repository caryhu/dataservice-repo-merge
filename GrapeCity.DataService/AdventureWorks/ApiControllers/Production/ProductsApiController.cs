﻿using System.Linq;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/products")]
    [ApiController]
    public class ProductsApiController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsApiController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: api/products/5
        [HttpGet("{productId:int}")]
        public ActionResult<ProductDTO> GetProduct(int productId)
        {
            var product = _productService.GetProduct(productId).FirstOrDefault();

            if(product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        // GET: api/products/AR-5381
        [HttpGet("{productNumber}")]
        public ActionResult<ProductDTO> GetProduct(string productNumber)
        {
            var product = _productService.GetProduct(productNumber).FirstOrDefault();

            if (product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        // GET: api/products/5/productAssemblies
        [HttpGet("{productId}/productAssemblies")]
        public IQueryable<ProductDTO> GetProductAssemblies(int productId)
        {
            return _productService.GetProductAssemblies(productId).Select(ProductionDtoConverter.AsProductDto);
        }

        // GET: api/products/5/components
        [HttpGet("{productId}/components")]
        public IQueryable<ProductDTO> GetProductComponents(int productId)
        {
            return _productService.GetProductComponents(productId).Select(ProductionDtoConverter.AsProductDto);
        }

        // GET: api/products/5/costHistories
        [HttpGet("{productId}/costHistories")]
        public IQueryable<ProductCostHistoryDTO> GetProductCostHistories(int productId)
        {
            return _productService.GetProductCostHistories(productId).Select(ProductionDtoConverter.AsProductCostHistoryDto);
        }

        // GET: api/products/5/details
        [HttpGet("{productId}/details")]
        public ActionResult<ProductDetailDTO> GetProductDetails(int productId)
        {
            var productDetails = _productService.GetProductDetails(productId).FirstOrDefault();

            if (productDetails == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDetailDto(productDetails);
        }

        // GET: api/products/5/inventories
        [HttpGet("{productId}/inventories")]
        public IQueryable<ProductInventoryDTO> GetProductInventories(int productId)
        {
            return _productService.GetProductInventories(productId).Select(ProductionDtoConverter.AsProductInventoryDTO);
        }

        // GET: api/products/5/model
        [HttpGet("{productId}/model")]
        public ActionResult<ProductModelDTO> GetProductModel(int productId)
        {
            var productModel = _productService.GetProductModel(productId).FirstOrDefault();

            if (productModel == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductModelDto(productModel);
        }

        // GET: api/products/5/offers
        [HttpGet("{productId}/offers")]
        public IQueryable<SpecialOfferDTO> GetProductOffers(int productId)
        {
            return _productService.GetProductOffers(productId).Select(SalesDtoConverter.AsSpecialOfferDto);
        }

        // GET: api/products/5/photos
        [HttpGet("{productId}/photos")]
        public IQueryable<ProductPhotoDTO> GetProductPhotos(int productId)
        {
            return _productService.GetProductPhotos(productId).Select(ProductionDtoConverter.AsProdPhotoDto);
        }

        // GET: api/products/5/priceHistories
        [HttpGet("{productId}/priceHistories")]
        public IQueryable<ProductListPriceHistoryDTO> GetProductPriceHistories(int productId)
        {
            return _productService.GetProductPriceHistories(productId).Select(ProductionDtoConverter.AsProdListPriceHistoryDto);
        }

        // GET: api/products/5/reviews
        [HttpGet("{productId}/reviews")]
        public IQueryable<ProductReviewDTO> GetProductReviews(int productId)
        {
            return _productService.GetProductReviews(productId).Select(ProductionDtoConverter.AsProdReviewDto);
        }

        [HttpGet]
        public IQueryable<ProductDTO> GetProducts()
        {
            return _productService.GetProducts().Select(ProductionDtoConverter.AsProductDto);
        }

        // GET: api/products/5/sizeUnit
        [HttpGet("{productId}/sizeUnit")]
        public ActionResult<UnitMeasureDTO> GetProductSizeUnit(int productId)
        {
            var sizeUnit = _productService.GetProductSizeUnit(productId).FirstOrDefault();
            
            if(sizeUnit == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToUnitMeasureDto(sizeUnit);
        }

        // GET: api/products/5/subCategory
        [HttpGet("{productId}/subCategory")]
        public ActionResult<ProductSubcategoryDTO> GetProductSubcategory(int productId)
        {
            var subcategory = _productService.GetProductSubcategory(productId).FirstOrDefault();

            if (subcategory == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductSubcateogryDto(subcategory);
        }

        // GET: api/products/5/transactions
        [HttpGet("{productId}/transactions")]
        public IQueryable<TransactionDTO> GetProductTransactions(int productId)
        {
            return _productService.GetProductTransactions(productId).Select(ProductionDtoConverter.AsTransactionHistoryDto);
        }

        // GET: api/products/5/vendors
        [HttpGet("{productId}/vendors")]
        public IQueryable<ProductVendorDTO> GetProductVendors(int productId)
        {
            return _productService.GetProductVendors(productId).Select(PurchaseDtoConverter.AsProductVendorDto);
        }

        // GET: api/products/5/weightUnit
        [HttpGet("{productId}/weightUnit")]
        public ActionResult<UnitMeasureDTO> GetProductWeightUnit(int productId)
        {
            var weightUnit = _productService.GetProductWeightUnit(productId).FirstOrDefault();

            if (weightUnit == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToUnitMeasureDto(weightUnit);
        }

        // GET: api/products/5/workOrders
        [HttpGet("{productId}/workOrders")]
        public IQueryable<WorkOrderDTO> GetWorkOrders(int productId)
        {
            return _productService.GetWorkOrders(productId).Select(ProductionDtoConverter.AsWorkOrderDto);
        }
    }
}