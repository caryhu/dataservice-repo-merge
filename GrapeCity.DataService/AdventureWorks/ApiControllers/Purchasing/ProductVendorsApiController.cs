﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/productVendors")]
    [ApiController]
    public class ProductVendorsApiController : ControllerBase
    {
        private readonly IProductVendorService _productVendorService;
        public ProductVendorsApiController(IProductVendorService productVendorService)
        {
            _productVendorService = productVendorService;
        }

        [HttpGet]
        public IQueryable<ProductVendorDTO> GetProductVendor(int? productId, int? vendorId)
        {
            return _productVendorService.GetProductVendors(productId, vendorId).
                Select(PurchaseDtoConverter.AsProductVendorDto);
        }

        [HttpGet("product")]
        public ActionResult<ProductDTO> GetProduct(int productId, int vendorId)
        {
            var product = _productVendorService.GetProduct(productId, vendorId).
                Select(ProductionDtoConverter.AsProductDto).FirstOrDefault();

            if (product == null)
                return NotFound();
            return product;
        }

        [HttpGet("vendor")]
        public ActionResult<VendorDTO> GetVendor(int productId, int vendorId)
        {
            var vendor = _productVendorService.GetVendor(productId, vendorId)
                .Select(PurchaseDtoConverter.AsVendorDto).FirstOrDefault();

            if (vendor == null)
                return NotFound();
            return vendor;
        }
        [HttpGet("{productId}/vendors")]
        public IQueryable<VendorDTO> GetVendorsForProduct(int productId)
        {
            return _productVendorService.GetVendorsForProduct(productId).Select(PurchaseDtoConverter.AsVendorDto);
        }

        [HttpGet("{vendorId}/products")]
        public IQueryable<ProductDTO> GetProductsFromVendor(int vendorId)
        {
            return _productVendorService.GetProductsFromVendor(vendorId).Select(ProductionDtoConverter.AsProductDto);
        }

        [HttpGet("measureUnit")]
        public ActionResult<UnitMeasureDTO> GetProductVendorUnitMeasure(int productId, int vendorId)
        {
            var unitMeasure = _productVendorService.GetProductVendorUnitMeasure(productId, vendorId)
                .Select(ProductionDtoConverter.AsUnitMeasureDto).FirstOrDefault();

            if (unitMeasure == null)
                return NotFound();
            return unitMeasure;
        }
    }
}