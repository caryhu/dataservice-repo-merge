﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/vendors")]
    [ApiController]
    public class VendorsApiController : ControllerBase
    {
        IVendorService _vendorService;
        public VendorsApiController(IVendorService vendorService)
        {
            _vendorService = vendorService;
        }

        [HttpGet]
        public IQueryable<VendorDTO> GetVendors()
        {
            return _vendorService.GetVendors().Select(PurchaseDtoConverter.AsVendorDto);
        }

        [HttpGet("{id}")]
        public ActionResult<VendorDTO> GetVendor(int id)
        {
            var vendor = _vendorService.GetVendor(id).Select(PurchaseDtoConverter.AsVendorDto).FirstOrDefault();

            if (vendor == null)
                return NotFound();
            return vendor;
        }

        [HttpGet("{id}/details")] 
        public ActionResult<VendorDetailDTO> GetVendorDetails(int id)
        {
            var vendor = _vendorService.GetVendor(id).FirstOrDefault();

            if (vendor == null)
                return NotFound();
            return PurchaseDtoConverter.ConvertToVendorDetailDto(vendor);
        }

        [HttpGet("{id}/entityDetails")]
        public ActionResult<BusinessEntityDetailDTO> GetEntityDetails(int id)
        {
            var entity = _vendorService.GetBusinessEntity(id).FirstOrDefault();

            if (entity == null)
                return NotFound();
            return PersonDtoConverter.ConvertToBusinessEntityDetailDto(entity);
        }

        [HttpGet("{id}/productVendors")]
        public IQueryable<ProductVendorDTO> GetProductVendors(int id)
        {
            return _vendorService.GetProductVendors(id).Select(PurchaseDtoConverter.AsProductVendorDto);
        }

        [HttpGet("{id}/purchaseOrders")]
        public IQueryable<PurchaseOrderHeaderDTO> GetPurchaseOrders(int id)
        {
            return _vendorService.GetPurchaseOrders(id).Select(PurchaseDtoConverter.AsPurchaseOrderHeaderDto);
        }
    }
}