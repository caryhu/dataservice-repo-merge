﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/shippingMethods")]
    [ApiController]
    public class ShipMethodsApiController : ControllerBase
    {
        readonly IShipMethodService _shipMethodService;
        public ShipMethodsApiController(IShipMethodService shipMethodService)
        {
            _shipMethodService = shipMethodService;
        }

        [HttpGet]
        public IQueryable<ShipMethodDTO> GetShippingMethods()
        {
            return _shipMethodService.GetShipMethods().Select(PurchaseDtoConverter.AsShipMethodDto);
        }

        [HttpGet("{id}")]
        public ActionResult<ShipMethodDTO> GetShippingMethod(int id)
        {
            var shippingMethod = _shipMethodService.GetShipMethod(id).
                Select(PurchaseDtoConverter.AsShipMethodDto).FirstOrDefault();

            if (shippingMethod == null)
                return NotFound();
            return shippingMethod;
        }

        [HttpGet("{id}/purchaseOrders")]
        public IQueryable<PurchaseOrderHeaderDTO> GetPurchaseOrders(int id)
        {
            return _shipMethodService.GetPurchaseOrders(id).Select(PurchaseDtoConverter.AsPurchaseOrderHeaderDto);
        }

        [HttpGet("{id}/salesOrders")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrders(int id)
        {
            return _shipMethodService.GetSalesOrders(id).Select(SalesDtoConverter.AsSalesOrderHeaderDto);
        }
    }
}