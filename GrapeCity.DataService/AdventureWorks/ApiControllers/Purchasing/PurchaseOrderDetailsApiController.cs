﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/purchaseOrderDetails")]
    [ApiController]
    public class PurchaseOrderDetailsApiController : ControllerBase
    {
        readonly IPurchaseOrderDetailService _purchaseOrderDetailService;
        public PurchaseOrderDetailsApiController(IPurchaseOrderDetailService purchaseOrderDetailService)
        {
            _purchaseOrderDetailService = purchaseOrderDetailService;
        }

        [HttpGet]
        public IQueryable<PurchaseOrderDetailDTO> GetPurchaseOrderDetails()
        {
            return _purchaseOrderDetailService.GetPurchaseOrderDetails().Select(PurchaseDtoConverter.AsPurchaseOrderDetailDto);
        }

        [HttpGet("{detailId}")]
        public ActionResult<PurchaseOrderDetailDTO> GetPurchaseorderDetail(int detailId)
        {
            var orderDetail = _purchaseOrderDetailService.GetPurchaseOrderDetail(detailId).
                Select(PurchaseDtoConverter.AsPurchaseOrderDetailDto).FirstOrDefault();

            if (orderDetail == null)
                return NotFound();
            return orderDetail;
        }

        [HttpGet("{detailId}/purchaseOrder")]
        public ActionResult<PurchaseOrderHeaderDTO> GetPurchaseOrder(int detailId)
        {
            var order = _purchaseOrderDetailService.GetPurchaseOrder(detailId).
                Select(PurchaseDtoConverter.AsPurchaseOrderHeaderDto).FirstOrDefault();

            if (order == null)
                return NotFound();
            return order;
        }

        [HttpGet("{detailId}/product")]
        public ActionResult<ProductDTO> GetOrderedProduct(int detailId)
        {
            var orderedProd = _purchaseOrderDetailService.GetOrderedProduct(detailId).
                Select(ProductionDtoConverter.AsProductDto).FirstOrDefault();

            if (orderedProd == null)
                return NotFound();
            return orderedProd;
        }
    }
}