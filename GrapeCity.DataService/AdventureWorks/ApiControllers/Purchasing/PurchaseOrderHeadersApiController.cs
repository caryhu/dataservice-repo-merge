﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/purchaseOrders")]
    [ApiController]
    public class PurchaseOrderHeadersApiController : ControllerBase
    {
        IPurchaseOrderHeaderService _purchaseOrderService;
        public PurchaseOrderHeadersApiController(IPurchaseOrderHeaderService purchaseOrderService)
        {
            _purchaseOrderService = purchaseOrderService;
        }

        /// <summary>
        /// Returns Paginated Purchase Orders data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Purchase Orders data</returns>
        [HttpGet]
        public IQueryable<PurchaseOrderHeaderDTO> GetPurchaseOrders([FromQuery] PagingParameters pagingParameters)
        {
            var source = _purchaseOrderService.GetPurchaseOrderHeaders();
            var response = source.CreatePagedResponse(pagingParameters, PurchaseDtoConverter.AsPurchaseOrderHeaderDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        [HttpGet("{id}")]
        public ActionResult<PurchaseOrderHeaderDTO> GetPurchaseOrder(int id)
        {
            var purchaseOrder = _purchaseOrderService.GetPurchaseOrderHeader(id).
                Select(PurchaseDtoConverter.AsPurchaseOrderHeaderDto).FirstOrDefault();

            if (purchaseOrder == null)
                return NotFound();
            return purchaseOrder;
        }

        [HttpGet("{id}/creator")]
        public ActionResult<EmployeeDTO> GetPurchaseOrderCreator(int id)
        {
            var creator = _purchaseOrderService.GetPurchaseOrderCreator(id).
                Select(HRDtoConverter.AsEmployeeDto).FirstOrDefault();

            if (creator == null)
                return NotFound();
            return creator;
        }

        [HttpGet("{id}/shippingDetail")]
        public ActionResult<ShipMethodDTO> GetPurchaseOrderShippingDetail(int id)
        {
            var detail = _purchaseOrderService.GetPurchaseOrderShippingDetail(id).
                Select(PurchaseDtoConverter.AsShipMethodDto).FirstOrDefault();

            if (detail == null)
                return NotFound();
            return detail;
        }

        [HttpGet("{id}/vendor")]
        public ActionResult<VendorDTO> GetPurchaseOrderVendor(int id)
        {
            var vendor = _purchaseOrderService.GetPurchaseOrderVendor(id).
                Select(PurchaseDtoConverter.AsVendorDto).FirstOrDefault();

            if (vendor == null)
                return NotFound();
            return vendor;
        }
        [HttpGet("{id}/details")]
        public IQueryable<PurchaseOrderDetailDTO> GetPurchaseOrderDetails(int id)
        {
            return _purchaseOrderService.GetPurchaseOrderDetails(id).Select(PurchaseDtoConverter.AsPurchaseOrderDetailDto);
        }
    }
}