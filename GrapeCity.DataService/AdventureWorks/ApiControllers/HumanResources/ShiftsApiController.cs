﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/shifts")]
    [ApiController]
    public class ShiftsApiController : ControllerBase
    {
        private readonly IShiftService _shiftService;

        public ShiftsApiController(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        // GET: api/shifts
        [HttpGet]
        public IQueryable<ShiftDTO> GetShifts()
        {
            return _shiftService.GetShifts().Select(HRDtoConverter.AsShiftDTO);
        }

        // GET: api/shifts/1
        [HttpGet("{shiftId}")]
        public ActionResult<ShiftDTO> GetShift(byte shiftId)
        {
            var shift = _shiftService.GetShift(shiftId).FirstOrDefault();

            if(shift == null)
            {
                return NotFound();
            }

            return HRDtoConverter.ConvertToShiftDTO(shift);
        }
    }
}
