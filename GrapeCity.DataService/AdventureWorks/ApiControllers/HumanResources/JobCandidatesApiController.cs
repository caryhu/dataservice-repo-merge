﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/jobCandidates")]
    [ApiController]
    public class JobCandidatesApiController : ControllerBase
    {
        private readonly IJobCandidateService _candidateService;

        public JobCandidatesApiController(IJobCandidateService candidateService)
        {
            _candidateService = candidateService;
        }

        // GET: api/jobCandidates
        [HttpGet]
        public IQueryable<JobCandidateDTO> GetJobCandidates()
        {
            return _candidateService.GetJobCandidates().Select(HRDtoConverter.AsJobCandidateDTO);
        }

        // GET: api/jobCandidates/1
        [HttpGet("{candidateId}")]
        public ActionResult<JobCandidateDTO> GetJobCandidate(int candidateId)
        {
            var candidate = _candidateService.GetJobCandidate(candidateId).FirstOrDefault();

            if(candidate == null)
            {
                return NotFound();
            }

            return HRDtoConverter.ConvertToJobCandidateDTO(candidate);
        }

        // GET: api/jobCandidates/4/employee
        [HttpGet("{candidateId}/employee")]
        public ActionResult<EmployeeDTO> GetEmployee(int candidateId)
        {
            var employee = _candidateService.GetEmployee(candidateId).FirstOrDefault();

            if (employee == null)
            {
                return NotFound();
            }

            return HRDtoConverter.ConvertToEmployeeDto(employee);
        }
    }
}
