﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.DAL;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/departments")]
    [ApiController]
    public class DepartmentsApiController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentsApiController(IDepartmentService deptartmentService)
        {
            _departmentService = deptartmentService;
        }

        // GET: api/Departments
        [HttpGet]
         public IQueryable<DepartmentDTO> GetDepartments()
        {
            return _departmentService.GetDepartments()
                .Select(HRDtoConverter.AsDepartmentDto);
        }

        // GET: api/Departments/5
        [HttpGet("{id}")]
        public ActionResult<DepartmentDTO> GetDepartment(short id)
        {
            var department = _departmentService.GetDepartment(id).FirstOrDefault();

            if (department == null)
            {
                return NotFound();
            }

            return HRDtoConverter.ConvertToDepartmentDto(department);
        }

        // GET: api/Departments/5/employees
        [HttpGet("{id}/employees")]
        public IQueryable<EmployeeDTO> GetEmployees(short id)
        {
            return _departmentService.GetEmployees(id)
                .Select(HRDtoConverter.AsEmployeeDto);
        }
    }
}
