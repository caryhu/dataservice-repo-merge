﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/employees")]
    [ApiController]
    public class EmployeesApiController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesApiController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        // GET: api/Employees
        [HttpGet]
        public IQueryable<EmployeeDTO> GetEmployees()
        {
            return _employeeService.GetEmployees().Select(HRDtoConverter.AsEmployeeDto);
        }

        // GET: api/Employees/2
        [HttpGet("{employeeId}")]
        public ActionResult<EmployeeDTO> GetEmployee(int employeeId)
        {
            var employee = _employeeService.GetEmployee(employeeId).FirstOrDefault();

            if(employee == null)
            {
                return NotFound();
            }

            return HRDtoConverter.ConvertToEmployeeDto(employee);
        }

        // GET: api/Employees/2/departmentHistories/1
        [HttpGet("{employeeId}/departmentHistories/{departmentId}")]
        public ActionResult<EmployeeDepartmentHistoryDTO> GetEmployeeDepartmentHistory(int employeeId, short departmentId)
        {
            var deptHistory = _employeeService.GetEmployeeDepartmentHistory(employeeId, departmentId).FirstOrDefault();

            if(deptHistory == null)
            {
                return NotFound();
            }

            return HRDtoConverter.ConvertToEmployeeDepartmentHistoryDTO(deptHistory);
        }

        // GET: api/Employees/2/departmentHistories
        [HttpGet("{employeeId}/departmentHistories")]
        public IQueryable<EmployeeDepartmentHistoryDTO> GetEmployeeDepartmentHistories(int employeeId)
        {
            return _employeeService.GetEmployeeDepartmentHistories(employeeId).Select(HRDtoConverter.AsEmployeeDepartmentHistoryDto);
        }

        // GET: api/Employees/2/payHistories
        [HttpGet("{employeeId}/payHistories")]
        public IQueryable<EmployeePayHistoryDTO> GetEmployeePayHistories(int employeeId)
        {
            return _employeeService.GetEmployeePayHistories(employeeId).Select(HRDtoConverter.AsEmployeePayHistoryDto);
        }

        // GET: api/Employees/2/jobApplications
        [HttpGet("{employeeId}/jobApplications")]
        public IQueryable<JobCandidateDTO> GetEmployeeJobApplications(int employeeId)
        {
            return _employeeService.GetEmployeeJobApplications(employeeId).Select(HRDtoConverter.AsJobCandidateDTO);
        }

        // GET: api/Employees/2/personalDetails
        [HttpGet("{employeeId}/personalDetails")]
        public ActionResult<PersonDTO> GetEmployeePersonalDetails(int employeeId)
        {
            var personalDetails = _employeeService.GetEmployeePersonalDetails(employeeId).FirstOrDefault();

            if (personalDetails == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPersonDto(personalDetails);
        }

        // GET: api/Employees/2/purchaseOrders
        [HttpGet("{employeeId}/purchaseOrders")]
        public IQueryable<PurchaseOrderHeaderDTO> GetEmployeePurchaseOrders(int employeeId)
        {
            return _employeeService.GetEmployeePurchaseOrders(employeeId).Select(PurchaseDtoConverter.AsPurchaseOrderHeaderDto);
        }

        // GET: api/Employees/279/sales
        [HttpGet("{employeeId}/sales")]
        public IQueryable<SalesOrderHeaderDTO> GetSales(int employeeId)
        {
            return _employeeService.GetSales(employeeId).Select(SalesDtoConverter.AsSalesOrderHeaderDto);
        }
    }
}
