﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/creditCards")]
    [ApiController]
    public class CreditCardsApiController : ControllerBase
    {
        readonly ICreditCardService _crediCardService;

        public CreditCardsApiController(ICreditCardService creditCardService)
        {
            _crediCardService = creditCardService;
        }

        /// <summary>
        /// Returns Paginated Credit cards data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Credit cards data</returns>
        [HttpGet]
        public IQueryable<CreditCardDTO> GetCreditCards([FromQuery] PagingParameters pagingParameters)
        {
            var source = _crediCardService.GetCreditCards();
            var response = source.CreatePagedResponse(pagingParameters, SalesDtoConverter.AsCreditCardDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        [HttpGet("{id}")]
        public ActionResult<CreditCardDTO> GetCreditCard(int id)
        {
            var creditCard = _crediCardService.GetCreditCard(id).FirstOrDefault();

            if (creditCard == null)
                return NotFound();
            return SalesDtoConverter.ConvertToCreditCardDto(creditCard);
        }

        [HttpGet("{id}/persons")]
        public IQueryable<PersonDTO> GetPersonsHavingCreditCard(int id)
        {
            return _crediCardService.GetPersonsHavingCreditCard(id).Select(PersonDtoConverter.AsPersonDto);
        }

        [HttpGet("{id}/salesOrders")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrdersPlacedWithCreditCard(int id)
        {
            return _crediCardService.GetSalesOrdersPlacedWithCreditCard(id).ToList().
                Select(x => SalesDtoConverter.ConvertToSalesOrderHeaderDto(x)).AsQueryable();
        }
    }
}