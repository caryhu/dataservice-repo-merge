﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesTaxRates")]
    [ApiController]
    public class SalesTaxRatesApiController : ControllerBase
    {
        private readonly ISalesTaxRateService _salesTaxRateService;

        public SalesTaxRatesApiController(ISalesTaxRateService salesTaxRateService)
        {
            _salesTaxRateService = salesTaxRateService;
        }

        // GET: api/salesTaxRates/1
        [HttpGet("{taxRateId}")]
        public ActionResult<SalesTaxRateDTO> GetSalesTaxRate(int taxRateId)
        {
            var taxRate = _salesTaxRateService.GetSalesTaxRate(taxRateId).FirstOrDefault();

            if(taxRate == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToSalesTaxRateDto(taxRate);
        }

        // GET: api/salesTaxRates
        [HttpGet]
        public IQueryable<SalesTaxRateDTO> GetSalesTaxRates()
        {
            return _salesTaxRateService.GetSalesTaxRates().Select(SalesDtoConverter.AsSalesTaxRateDto);
        }

        // GET: api/salesTaxRates/1/province
        [HttpGet("{taxRateId}/province")]
        public ActionResult<StateProvinceDTO> GetStateProvince(int taxRateId)
        {
            var state = _salesTaxRateService.GetStateProvince(taxRateId).FirstOrDefault();

            if (state == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToStateProvinceDto(state);
        }
    }
}
