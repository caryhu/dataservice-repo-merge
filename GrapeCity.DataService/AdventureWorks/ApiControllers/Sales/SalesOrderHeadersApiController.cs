﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesOrders")]
    [ApiController]
    public class SalesOrderHeadersApiController : ControllerBase
    {
        private readonly ISalesOrderHeaderService _salesOrderHeaderService;

        public SalesOrderHeadersApiController(ISalesOrderHeaderService salesOrderHeaderService)
        {
            _salesOrderHeaderService = salesOrderHeaderService;
        }

        /// <summary>
        /// Returns Paginated Sales Order data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Sales Order data</returns>
        [HttpGet]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrders([FromQuery] PagingParameters pagingParameters)
        {
            var source = _salesOrderHeaderService.GetSalesOrders();
            var response = source.CreatePagedResponse(pagingParameters, SalesDtoConverter.AsSalesOrderHeaderDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        [HttpGet("{id}")]
        public ActionResult<SalesOrderHeaderDTO> GetSalesOrder(int id)
        {
            var order = _salesOrderHeaderService.GetSalesOrder(id).FirstOrDefault();

            if (order == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesOrderHeaderDto(order);
        }

        [HttpGet("{id}/details")]
        public ActionResult<SalesOrderHeaderDetailDTO> GetSalesOrderDetail(int id)
        {
            var order = _salesOrderHeaderService.GetSalesOrder(id).FirstOrDefault();

            if (order == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesOrderHeaderDetailDto(order);
        }

        [HttpGet("{id}/billedToAddress")]
        public ActionResult<AddressDTO> GetBillingAddress(int id)
        {
            var address = _salesOrderHeaderService.GetBillingAddress(id).FirstOrDefault();

            if (address == null)
                return NotFound();
            return PersonDtoConverter.ConvertToAddressDto(address);

        }

        [HttpGet("{id}/shippedToAddress")]
        public ActionResult<AddressDTO> GetShippingAddress(int id)
        {
            var address = _salesOrderHeaderService.GetShippingAddress(id).FirstOrDefault();

            if (address == null)
                return NotFound();
            return PersonDtoConverter.ConvertToAddressDto(address);
        }

        [HttpGet("{id}/creditCard")]
        public ActionResult<CreditCardDTO> GetCreditCardUsedForOrder(int id)
        {
            var creditCard = _salesOrderHeaderService.GetCreditCardUsedForOrder(id).FirstOrDefault();

            if (creditCard == null)
                return NotFound();
            return SalesDtoConverter.ConvertToCreditCardDto(creditCard);

        }

        [HttpGet("{id}/currencyRate")]
        public ActionResult<CurrencyRateDTO> GetCurrencyRateAppliedOnOrder(int id)
        {
            var currencyRate = _salesOrderHeaderService.GetCurrencyRateAppliedOnOrder(id).FirstOrDefault();

            if (currencyRate == null)
                return NotFound();
            return SalesDtoConverter.ConvertToCurrencyRateDto(currencyRate);

        }

        [HttpGet("{id}/customer")]
        public ActionResult<CustomerDTO> GetCustomerOfOrder(int id)
        {
            var customer = _salesOrderHeaderService.GetCustomerOfOrder(id).FirstOrDefault();

            if (customer == null)
                return NotFound();
            return SalesDtoConverter.ConvertToCustomerDto(customer);
        }

        [HttpGet("{id}/salesPerson")]
        public ActionResult<SalesPersonDTO> GetSalesPerson(int id)
        {
            var salesPerson = _salesOrderHeaderService.GetSalesPerson(id).FirstOrDefault();

            if (salesPerson == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesPersonDto(salesPerson);
        }

        [HttpGet("{id}/shippingMethod")]
        public ActionResult<ShipMethodDTO> GetShippingMethod(int id)
        {
            var shipMethod = _salesOrderHeaderService.GetShippingMethod(id).FirstOrDefault();

            if (shipMethod == null)
                return NotFound();
            return PurchaseDtoConverter.ConvertToShipMethodDto(shipMethod);
        }

        [HttpGet("{id}/territory")]
        public ActionResult<SalesTerritoryDTO> GetSalesOrderTerritory(int id)
        {
            var territory = _salesOrderHeaderService.GetSalesOrderTerritory(id).FirstOrDefault();

            if (territory == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesTerritoryDto(territory);
        }

        [HttpGet("{id}/orderDetails")]
        public IQueryable<SalesOrderDetailDTO> GetSalesOrderDetails(int id)
        {
            return _salesOrderHeaderService.GetSalesOrderDetails(id).Select(SalesDtoConverter.AsSalesOrderDetailDto);
        }

        [HttpGet("{id}/salesReasons")]
        public IQueryable<SalesReasonDTO> GetSalesReasons(int id)
        {
            return _salesOrderHeaderService.GetSalesReasons(id).Select(SalesDtoConverter.AsSalesReasonDto);
        }
    }
}
