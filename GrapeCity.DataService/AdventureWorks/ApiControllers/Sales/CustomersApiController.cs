﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/customers")]
    [ApiController]
    public class CustomersApiController : ControllerBase
    {
        readonly ICustomerService _customerService;

        public CustomersApiController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Returns Paginated Customers data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Customers data</returns>
        [HttpGet]
        public IQueryable<CustomerDTO> GetCustomers([FromQuery] PagingParameters pagingParameters) 
        {
            var source = _customerService.GetCustomers();
            var response = source.CreatePagedResponse(pagingParameters, SalesDtoConverter.AsCustomerDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        [HttpGet("{id}")]
        public ActionResult<CustomerDTO> GetCustomer(int id)
        {
            var customer = _customerService.GetCustomer(id).FirstOrDefault();

            if (customer == null)
                return NotFound();
            return SalesDtoConverter.ConvertToCustomerDto(customer);
        }

        [HttpGet("{id}/details")]
        public ActionResult<CustomerDetailDTO> GetCustomerDetails(int id)
        {
            var customer = _customerService.GetCustomer(id).FirstOrDefault();

            if (customer == null)
                return NotFound();
            return SalesDtoConverter.ConvertToCustomerDetailDto(customer);
        }

        [HttpGet("{id}/person")]
        public ActionResult<PersonDTO> GetCustomerPersonDetails(int id)
        {
            var person = _customerService.GetCustomerPersonDetails(id).FirstOrDefault();

            if (person == null)
                return NotFound();
            return PersonDtoConverter.ConvertToPersonDto(person);
        }

        [HttpGet("{id}/store")]
        public ActionResult<StoreDTO> GetCustomerStore(int id)
        {
            var store = _customerService.GetCustomerStore(id).FirstOrDefault();

            if (store == null)
                return NotFound();
            return SalesDtoConverter.ConvertToStoreDto(store);
        }

        [HttpGet("{id}/territory")]
        public ActionResult<SalesTerritoryDTO> GetCustomerTerritory(int id)
        {
            var territory = _customerService.GetCustomerTerritory(id).FirstOrDefault();

            if (territory == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesTerritoryDto(territory);
        }

        [HttpGet("{id}/salesOrders")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrdersPlacedByCustomer(int id)
        {
            return _customerService.GetSalesOrdersPlacedByCustomer(id).ToList().
                Select(x=>SalesDtoConverter.ConvertToSalesOrderHeaderDto(x)).AsQueryable();
        }
    }
}