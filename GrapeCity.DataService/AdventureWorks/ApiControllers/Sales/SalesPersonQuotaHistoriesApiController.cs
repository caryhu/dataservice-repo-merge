﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesPersonQuotaHistories")]
    [ApiController]
    public class SalesPersonQuotaHistoriesApiController : ControllerBase
    {
        private readonly ISalesPersonQuotaHistoryService _salesPersonQuotaHistoryService;

        public SalesPersonQuotaHistoriesApiController(ISalesPersonQuotaHistoryService salesPersonQuotaHistoryService)
        {
            _salesPersonQuotaHistoryService = salesPersonQuotaHistoryService;
        }

        /// <summary>
        ///     Gets the SalesPersonQuotaHistories by salesPersonId and quotaDate
        /// </summary>
        /// <param name="salesPersonId">
        ///     Id of sales person whose quota history will be fetched. 
        /// </param>
        /// <param name="quotaDate">
        ///     This date will be used to fetch sales person quota histories where quotaDate matched with the provided value. 
        /// </param>
        /// <returns></returns>
        // GET: api/salesPersonQuotaHistories?salesPersonId=274&quotaDate=2011-05-31
        // GET: api/salesPersonQuotaHistories
        [HttpGet]
        public IQueryable<SalesPersonQuotaHistoryDTO> GetSalesPersonQuotaHistories(int? salesPersonId, DateTime? quotaDate)
        {
            return _salesPersonQuotaHistoryService.GetSalesPersonQuotaHistories(salesPersonId, quotaDate).Select(SalesDtoConverter.AsSalesPersonQuotaHistoryDto);
        }

        // GET: api/salesPersonQuotaHistories/274
        [HttpGet("{salesPersonId}")]
        public IQueryable<SalesPersonQuotaHistoryDTO> GetSalesPersonQuotaHistories(int salesPersonId)
        {
            return _salesPersonQuotaHistoryService.GetSalesPersonQuotaHistories(salesPersonId, null).Select(SalesDtoConverter.AsSalesPersonQuotaHistoryDto);
        }
    }
}
