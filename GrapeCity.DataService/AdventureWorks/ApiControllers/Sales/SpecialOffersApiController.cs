﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/specialOffers")]
    [ApiController]
    public class SpecialOffersApiController : ControllerBase
    {
        private readonly ISpecialOfferService _specialOfferService;

        public SpecialOffersApiController(ISpecialOfferService specialOfferService)
        {
            _specialOfferService = specialOfferService;
        }

        // GET: api/specialOffers/5/products
        [HttpGet("{offerId}/products")]
        public IQueryable<ProductDTO> GetProducts(int offerId)
        {
            return _specialOfferService.GetProducts(offerId).Select(ProductionDtoConverter.AsProductDto);
        }

        // GET: api/specialOffers/5
        [HttpGet("{offerId}")]
        public ActionResult<SpecialOfferDTO> GetSpecialOffer(int offerId)
        {
            var offer = _specialOfferService.GetSpecialOffer(offerId).FirstOrDefault();

            if(offer == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToSpecialOfferDto(offer);
        }

        // GET: api/specialOffers
        [HttpGet]
        public IQueryable<SpecialOfferDTO> GetSpecialOffers()
        {
            return _specialOfferService.GetSpecialOffers().Select(SalesDtoConverter.AsSpecialOfferDto);
        }
    }
}
