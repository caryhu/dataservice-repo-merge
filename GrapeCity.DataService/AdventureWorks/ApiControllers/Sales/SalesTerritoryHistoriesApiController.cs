﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesTerritoryHistories")]
    [ApiController]
    public class SalesTerritoryHistoriesApiController : ControllerBase
    {
        private readonly ISalesTerritoryHistoryService _salesTerritoryHistoryService;

        public SalesTerritoryHistoriesApiController(ISalesTerritoryHistoryService salesTerritoryHistoryService)
        {
            _salesTerritoryHistoryService = salesTerritoryHistoryService;
        }

        /// <summary>
        ///     Gets the SalesTerritoryHistories by salesPersonId, TerritoryId and startDate.
        /// </summary>
        /// <param name="salesPersonid">
        ///     Id of sales person for which sales territory histories will be fetched.
        /// </param>
        /// <param name="territoryId">
        ///     Id of sales territory for which histories will be fetched.
        /// </param>
        /// <param name="startDate">
        ///     This date will be used to fetch sales territory histories where startDate matches or is greater than the provided value.
        /// </param>
        /// <returns></returns>
        // GET: api/salesTerritoryHistories
        // GET: api/salesTerritoryHistories?salesPersonid=275&territoryId=3&startDate=2012-11-30
        [HttpGet]
        public IQueryable<SalesTerritoryHistoryDTO> GetSalesTerritoryHistories(int? salesPersonid, int? territoryId, DateTime? startDate)
        {
            return _salesTerritoryHistoryService.GetSalesTerritoryHistories(salesPersonid, territoryId, startDate).Select(SalesDtoConverter.AsSalesTerritoryHistoryDto).AsQueryable();
        }
    }
}
