﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/currencies")]
    [ApiController]
    public class CurrenciesApiController : ControllerBase
    {
        readonly ICurrencyService _currencyService;
        public CurrenciesApiController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        [HttpGet]
        public IQueryable<CurrencyDTO> GetCurrencies()
        {
            return _currencyService.GetCurrencies().Select(SalesDtoConverter.AsCurrencyDto);
        }

        [HttpGet("{code}")]
        public ActionResult<string> GetCurrency(string code)
        {
            var currency = _currencyService.GetCurrency(code).FirstOrDefault();

            if (currency == null)
                return NotFound();
            return currency.Name;
        }

        [HttpGet("{code}/countries")]
        public IQueryable<CountryRegionDTO> GetCountriesWithCurrency(string code)
        {
            return _currencyService.GetCountriesWithCourrency(code).Select(PersonDtoConverter.AsCountryRegionDto);
        }

        [HttpGet("{fromCurrencyCode}/conversionRates")]
        public IQueryable<CurrencyRateDTO> GetCurrnecyConversionRates(string fromCurrencyCode)
        {
            return _currencyService.GetCurrnecyConversionRates(fromCurrencyCode).ToList()
                .Select(x=>SalesDtoConverter.ConvertToCurrencyRateDto(x)).AsQueryable();
        }
    }
}