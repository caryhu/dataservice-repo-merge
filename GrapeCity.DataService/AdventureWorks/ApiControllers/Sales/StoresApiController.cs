﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/stores")]
    [ApiController]
    public class StoresApiController : ControllerBase
    {
        private readonly IStoreService _storeService;

        public StoresApiController(IStoreService storeService)
        {
            _storeService = storeService;
        }

        // GET: api/stores/292/businessEntity
        [HttpGet("{storeId}/businessEntity")]
        public ActionResult<BusinessEntityDetailDTO> GetBusinessEntity(int storeId)
        {
            var entity = _storeService.GetBusinessEntity(storeId).FirstOrDefault();

            if(entity == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToBusinessEntityDetailDto(entity);
        }

        // GET: api/stores/292/customers
        [HttpGet("{storeId}/customers")]
        public IQueryable<CustomerDTO> GetCustomers(int storeId)
        {
            return _storeService.GetCustomers(storeId).Select(SalesDtoConverter.AsCustomerDto);
        }

        // GET: api/stores/292/salesPerson
        [HttpGet("{storeId}/salesPerson")]
        public ActionResult<SalesPersonDTO> GetSalesPerson(int storeId)
        {
            var salesPerson = _storeService.GetSalesPerson(storeId).FirstOrDefault();

            if (salesPerson == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToSalesPersonDto(salesPerson);
        }

        // GET: api/stores/292
        [HttpGet("{storeId}")]
        public ActionResult<StoreDTO> GetStore(int storeId)
        {
            var store = _storeService.GetStore(storeId).FirstOrDefault();

            if (store == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToStoreDto(store);
        }

        // GET: api/stores
        [HttpGet]
        public IQueryable<StoreDTO> GetStores()
        {
            return _storeService.GetStores().Select(SalesDtoConverter.AsStoreDto);
        }
    }
}
