﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesReasons")]
    [ApiController]
    public class SalesReasonsApiController : ControllerBase
    {
        private readonly ISalesReasonService _salesReasonService;

        public SalesReasonsApiController(ISalesReasonService salesReasonService)
        {
            _salesReasonService = salesReasonService;
        }

        /// <summary>
        /// Returns Paginated Sales Order data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="salesReasonId"></param>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Sales Order data</returns>
        [HttpGet("{salesReasonId}/salesOrders")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrders(int salesReasonId, [FromQuery] PagingParameters pagingParameters)
        {
            var source = _salesReasonService.GetSalesOrders(salesReasonId);
            var response = source.CreatePagedResponse(pagingParameters, SalesDtoConverter.AsSalesOrderHeaderDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        // GET: api/salesReasons/5
        [HttpGet("{salesReasonId}")]
        public ActionResult<SalesReasonDTO> GetSalesReason(int salesReasonId)
        {
            var salesReason = _salesReasonService.GetSalesReason(salesReasonId).FirstOrDefault();

            if(salesReason == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToSalesReasonDto(salesReason);
        }

        // GET: api/salesReasons
        [HttpGet]
        public IQueryable<SalesReasonDTO> GetSalesReasons()
        {
            return _salesReasonService.GetSalesReasons().Select(SalesDtoConverter.AsSalesReasonDto);
        }
    }
}
