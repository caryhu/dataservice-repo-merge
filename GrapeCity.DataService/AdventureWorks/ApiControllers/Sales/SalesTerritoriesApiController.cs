﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesTerritories")]
    [ApiController]
    public class SalesTerritoriesApiController : ControllerBase
    {
        private readonly ISalesTerritoryService _salesTerritoryService;

        public SalesTerritoriesApiController(ISalesTerritoryService salesTerritoryService)
        {
            _salesTerritoryService = salesTerritoryService;
        }

        // GET: api/salesTerritories/5/country
        [HttpGet("{territoryId}/country")]
        public ActionResult<CountryRegionDTO> GetCountry(int territoryId)
        {
            var country = _salesTerritoryService.GetCountry(territoryId).FirstOrDefault();

            if(country == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToCountryRegionDto(country);
        }

        // GET: api/salesTerritories/5/customers
        [HttpGet("{territoryId}/customers")]
        public IQueryable<CustomerDTO> GetCustomers(int territoryId)
        {
            return _salesTerritoryService.GetCustomers(territoryId).Select(SalesDtoConverter.AsCustomerDto);
        }

        // GET: api/salesTerritories/5/salesOrders
        [HttpGet("{territoryId}/salesOrders")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrders(int territoryId)
        {
            return _salesTerritoryService.GetSalesOrders(territoryId).Select(SalesDtoConverter.AsSalesOrderHeaderDto);
        }

        // GET: api/salesTerritories/5/salesPeople
        [HttpGet("{territoryId}/salesPeople")]
        public IQueryable<SalesPersonDTO> GetSalesPeople(int territoryId)
        {
            return _salesTerritoryService.GetSalesPeople(territoryId).Select(SalesDtoConverter.AsSalesPersonDto);
        }

        // GET: api/salesTerritories
        [HttpGet]
        public IQueryable<SalesTerritoryDTO> GetSalesTerritories()
        {
            return _salesTerritoryService.GetSalesTerritories().Select(SalesDtoConverter.AsSalesTerritoryDto);
        }

        // GET: api/salesTerritories/5
        [HttpGet("{territoryId}")]
        public ActionResult<SalesTerritoryDTO> GetSalesTerritory(int territoryId)
        {
            var territory = _salesTerritoryService.GetSalesTerritory(territoryId).FirstOrDefault();

            if (territory == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToSalesTerritoryDto(territory);
        }

        // GET: api/salesTerritories/5/territoryHistories
        [HttpGet("{territoryId}/territoryHistories")]
        public IQueryable<SalesTerritoryHistoryDTO> GetSalesTerritoryHistories(int territoryId)
        {
            return _salesTerritoryService.GetSalesTerritoryHistories(territoryId).Select(SalesDtoConverter.AsSalesTerritoryHistoryDto);
        }

        // GET: api/salesTerritories/5/provinces

        [HttpGet("{territoryId}/provinces")]
        public IQueryable<StateProvinceDTO> GetStateProvinces(int territoryId)
        {
            return _salesTerritoryService.GetStateProvinces(territoryId).Select(PersonDtoConverter.AsStateProvinceDto);
        }
    }
}
