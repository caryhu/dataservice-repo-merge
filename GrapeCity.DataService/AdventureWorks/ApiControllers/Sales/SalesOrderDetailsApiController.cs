﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesOrderDetails")]
    [ApiController]
    public class SalesOrderDetailsApiController : ControllerBase
    {
        private readonly ISalesOrderDetailService _salesOrderDetailService;

        public SalesOrderDetailsApiController(ISalesOrderDetailService salesOrderDetailService)
        {
            _salesOrderDetailService = salesOrderDetailService;
        }

        /// <summary>
        /// Returns Paginated Sales Order Details. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Sales Order Details</returns>
        [HttpGet]
        public IQueryable<SalesOrderDetailDTO> GetSalesOrderDetails([FromQuery]PagingParameters pagingParameters)
        {
            var source = _salesOrderDetailService.GetSalesOrderDetails();
            var response = source.CreatePagedResponse(pagingParameters, SalesDtoConverter.AsSalesOrderDetailDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        [HttpGet("{id}")]
        public ActionResult<SalesOrderDetailDTO> GetSalesOrderDetail(int id)
        {
            var orderDetail = _salesOrderDetailService.GetSalesOrderDetail(id).FirstOrDefault();

            if (orderDetail == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesOrderDetailDto(orderDetail);
        }

        [HttpGet("{id}/order")]
        public ActionResult<SalesOrderHeaderDTO> GetSalesOrder(int id)
        {
            var order = _salesOrderDetailService.GetSalesOrder(id).FirstOrDefault();

            if (order == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesOrderHeaderDto(order);
        }

        [HttpGet("{id}/specialOffer")]
        public ActionResult<SpecialOfferDTO> GetSpecialOfferWithOrder(int id)
        {
            var offer = _salesOrderDetailService.GetSpecialOfferWithOrder(id).FirstOrDefault();

            if (offer == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSpecialOfferDto(offer);
        }
    }
}
