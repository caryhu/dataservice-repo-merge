﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/salesPersons")]
    [ApiController]
    public class SalesPersonsApiController : ControllerBase
    {
        private readonly ISalesPersonService _salesPersonService;

        public SalesPersonsApiController(ISalesPersonService salesPersonService)
        {
            _salesPersonService = salesPersonService;
        }

        [HttpGet]
        public IQueryable<SalesPersonDTO> GetSalesPersons()
        {
            return _salesPersonService.GetSalesPersons().Select(SalesDtoConverter.AsSalesPersonDto);
        }

        [HttpGet("{id}")]
        public ActionResult<SalesPersonDTO> GetSalesPerson(int id)
        {
            var salesPerson = _salesPersonService.GetSalesPerson(id).FirstOrDefault();

            if (salesPerson == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesPersonDto(salesPerson);
        }

        [HttpGet("{id}/employee")]
        public ActionResult<EmployeeDTO> GetEmployee(int id)
        {
            var employee = _salesPersonService.GetEmployee(id).FirstOrDefault();

            if (employee == null)
                return NotFound();
            return HRDtoConverter.ConvertToEmployeeDto(employee);
        }

        [HttpGet("{id}/territory")]
        public ActionResult<SalesTerritoryDTO> GetSalesTerritory(int id)
        {
            var territory = _salesPersonService.GetSalesTerritory(id).FirstOrDefault();

            if (territory == null)
                return NotFound();
            return SalesDtoConverter.ConvertToSalesTerritoryDto(territory);
        }

        [HttpGet("{id}/salesOrders")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrders(int id)
        {
            return _salesPersonService.GetSalesOrders(id).ToList().AsQueryable().Select(SalesDtoConverter.AsSalesOrderHeaderDto);
        }

        [HttpGet("{id}/quotaHistories")]
        public IQueryable<SalesPersonQuotaHistoryDTO> GetQuotaHistories(int id)
        {
            return _salesPersonService.GetQuotaHistories(id).Select(SalesDtoConverter.AsSalesPersonQuotaHistoryDto);
        }

        [HttpGet("{id}/territoryHistories")]
        public IQueryable<SalesTerritoryHistoryDTO> GetSalesTerritoryHistories(int id)
        {
            return _salesPersonService.GetSalesTerritoryHistories(id).Select(SalesDtoConverter.AsSalesTerritoryHistoryDto);
        }

        [HttpGet("{id}/stores")]
        public IQueryable<StoreDTO> GetStores(int id)
        {
            return _salesPersonService.GetStores(id).Select(SalesDtoConverter.AsStoreDto);
        }
    }
}
