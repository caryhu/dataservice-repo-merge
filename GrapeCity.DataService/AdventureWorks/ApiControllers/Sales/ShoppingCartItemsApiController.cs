﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/shoppingCartItems")]
    [ApiController]
    public class ShoppingCartItemsApiController : ControllerBase
    {
        private readonly IShoppingCartItemService _shoppingCartItemService;

        public ShoppingCartItemsApiController(IShoppingCartItemService shoppingCartItemService)
        {
            _shoppingCartItemService = shoppingCartItemService;
        }

        // GET: api/shoppingCartItems/2/product
        [HttpGet("{cartItemId}/product")]
        public ActionResult<ProductDTO> GetProduct(int cartItemId)
        {
            var product = _shoppingCartItemService.GetProduct(cartItemId).FirstOrDefault();

            if(product == null)
            {
                return NotFound();
            }

            return ProductionDtoConverter.ConvertToProductDto(product);
        }

        // GET: api/shoppingCartItems/cart/14951
        [HttpGet("cart/{cartId}")]
        public ActionResult<ShoppingCartItemDTO> GetShoppingCartItemByCartId(string cartId)
        {
            var cartItem = _shoppingCartItemService.GetShoppingCartItemByCartId(cartId).FirstOrDefault();

            if (cartItem == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToShoppingCartItemDto(cartItem);
        }

        // GET: api/shoppingCartItems/2
        [HttpGet("{cartItemId}")]
        public ActionResult<ShoppingCartItemDTO> GetShoppingCartItemByItemId(int cartItemId)
        {
            var cartItem = _shoppingCartItemService.GetShoppingCartItemByItemId(cartItemId).FirstOrDefault();

            if (cartItem == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToShoppingCartItemDto(cartItem);
        }

        // GET: api/shoppingCartItems
        [HttpGet]
        public IQueryable<ShoppingCartItemDTO> GetShoppingCartItems()
        {
            return _shoppingCartItemService.GetShoppingCartItems().Select(SalesDtoConverter.AsShoppingCartItemDto);
        }
    }
}
