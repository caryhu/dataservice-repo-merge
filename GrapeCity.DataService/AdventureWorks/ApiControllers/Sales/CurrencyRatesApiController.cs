﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/currencyRates")]
    [ApiController]
    public class CurrencyRatesApiController : ControllerBase
    {
        readonly ICurrencyRateService _currencyRateService;
        public CurrencyRatesApiController(ICurrencyRateService currencyRateService)
        {
            _currencyRateService = currencyRateService;
        }

        [HttpGet]
        public IQueryable<CurrencyRateDTO> GetCurrencyRates(string fromCode, string toCode)
        {
            return _currencyRateService.GetCurrencyRates(fromCode, toCode).Select(SalesDtoConverter.AsCurrencyRateDto);
        }

        [HttpGet("{id}")]
        public ActionResult<CurrencyRateDTO> GetCurrencyRate(int id)
        {
            var currencyRate = _currencyRateService.GetCurrencyRate(id).FirstOrDefault();

            if (currencyRate == null)
                return NotFound();
            return SalesDtoConverter.ConvertToCurrencyRateDto(currencyRate);
        }

        [HttpGet("{id}/salesOrders")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrdersWithCurrencyRates(int id)
        {
            return _currencyRateService.GetSalesOrdersWithCurrencyRate(id).ToList().
                Select(x => SalesDtoConverter.ConvertToSalesOrderHeaderDto(x)).AsQueryable();
        }
    }
}