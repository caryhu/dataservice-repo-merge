﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/addressTypes")]
    [ApiController]
    public class AddressTypesApiController : ControllerBase
    {
        private readonly IAddressTypeService _addressTypeService;

        public AddressTypesApiController(IAddressTypeService addressTypeService)
        {
            _addressTypeService = addressTypeService;
        }

        // GET: api/addresses/3
        [HttpGet("{addressTypeId}")]
        public ActionResult<AddressTypeDTO> GetAddressType(short addressTypeId)
        {
            var addressType = _addressTypeService.GetAddressType(addressTypeId).FirstOrDefault();

            if(addressType == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToAddressTypeDto(addressType);
        }

        // GET: api/addresses
        [HttpGet]
        public IQueryable<AddressTypeDTO> GetAddressTypes()
        {
            return _addressTypeService.GetAddressTypes().Select(PersonDtoConverter.AsAddressTypeDto);
        }
      
        // GET : api/addresses/4/businessEntityAddresses
        [HttpGet("{addressTypeId}/businessEntityAddresses")]
        public IQueryable<BusinessEntityAddressDTO> GetBusinessEntityAddresses(short addressTypeId)
        {
            return _addressTypeService.GetBusinessEntityAddresses(addressTypeId).Select(PersonDtoConverter.AsBusinessEntityAddressDto);
        }
    }
}
