﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/phoneNumberTypes")]
    [ApiController]
    public class PhoneNumberTypesApiController : ControllerBase
    {
        private readonly IPersonPhoneService _personPhoneService;

        public PhoneNumberTypesApiController(IPersonPhoneService personPhoneService)
        {
            _personPhoneService = personPhoneService;
        }

        // GET: api/phoneNumberTypes/1
        [HttpGet("{typeId}")]
        public ActionResult<PhoneNumberTypeDTO> GetPhoneNumberType(short typeId)
        {
            var phoneType = _personPhoneService.GetPhoneNumberType(typeId).FirstOrDefault();

            if(phoneType == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPhoneTypeDto(phoneType);
        }

        // GET: api/phoneNumberTypes
        [HttpGet]
        public IQueryable<PhoneNumberTypeDTO> GetPhoneNumberTypes()
        {
            return _personPhoneService.GetPhoneNumberTypes().Select(PersonDtoConverter.AsPhoneTypeDto);
        }
       
        // GET: api/phoneNumberTypes/1/phones
        [HttpGet("{typeId}/phones")]
        public IQueryable<PersonPhoneDTO> GetPersonPhones(short typeId)
        {
            return _personPhoneService.GetPersonPhones(typeId).Select(PersonDtoConverter.AsPersonPhoneDto);
        }
    }
}
