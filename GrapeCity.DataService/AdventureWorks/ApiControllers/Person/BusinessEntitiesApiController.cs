﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/businessEntity")]
    [ApiController]
    public class BusinessEntitiesApiController : ControllerBase
    {
        private readonly IBusinessEntityService _businessEntityService;

        public BusinessEntitiesApiController(IBusinessEntityService businessEntityService)
        {
            _businessEntityService = businessEntityService;
        }

        // GET: api/3/addresses
        [HttpGet("{entityId}/addresses")]
        public IQueryable<BusinessEntityAddressDTO> GetAddresses(int entityId)
        {
           return _businessEntityService.GetAddresses(entityId).Select(PersonDtoConverter.AsBusinessEntityAddressDto); ;
        }

        // GET: api/3/businessContacts
        [HttpGet("{entityId}/businessContacts")]
        public IQueryable<BusinessEntityContactDTO> GetContacts(int entityId)
        {
            return _businessEntityService.GetContacts(entityId).Select(PersonDtoConverter.AsBusinessEntityContactDto);
        }

        // GET: api/3/person
        [HttpGet("{entityId}/person")]
        public ActionResult<PersonDTO> GetPerson(int entityId)
        {
            var person = _businessEntityService.GetPerson(entityId).FirstOrDefault();

            if(person == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPersonDto(person);
        }

        // GET: api/3/store
        [HttpGet("{entityId}/store")]
        public ActionResult<StoreDTO> GetStore(int entityId)
        {
            var store = _businessEntityService.GetStore(entityId).FirstOrDefault();

            if (store == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToStoreDto(store);
        }

        // GET: api/3/vendor
        [HttpGet("{entityId}/vendor")]
        public ActionResult<VendorDTO> GetVendor(int entityId)
        {
            var vendor = _businessEntityService.GetVendor(entityId).FirstOrDefault();

            if (vendor == null)
            {
                return NotFound();
            }

            return PurchaseDtoConverter.ConvertToVendorDto(vendor);
        }
    }
}
