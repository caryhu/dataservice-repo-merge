﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/businessEntityAddresses")]
    [ApiController]
    public class BusinessEntityAddressesApiController : ControllerBase
    {
        private readonly IBusinessEntityService _businessEntityService;

        public BusinessEntityAddressesApiController(IBusinessEntityService businessEntityService)
        {
            _businessEntityService = businessEntityService;
        }

        /// <summary>
        /// Returns Paginated Business entity addresses data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Business entity addresses data</returns>
        [HttpGet]
        public IQueryable<BusinessEntityAddressDTO> GetBusinessEntityAddresses([FromQuery] PagingParameters pagingParameters)
        {
            var source = _businessEntityService.GetAddresses();
            var response = source.CreatePagedResponse(pagingParameters, PersonDtoConverter.AsBusinessEntityAddressDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }
    }
}
