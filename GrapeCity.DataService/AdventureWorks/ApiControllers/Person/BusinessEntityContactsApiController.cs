﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/businessEntityContacts")]
    [ApiController]
    public class BusinessEntityContactsApiController : ControllerBase
    {
        private readonly IBusinessEntityService _businessEntityService;

        public BusinessEntityContactsApiController(IBusinessEntityService businessEntityService)
        {
            _businessEntityService = businessEntityService;
        }

        // GET: api/businessEntityContacts
        [HttpGet]
        public IQueryable<BusinessEntityContactDTO> GetBusinessEntityContacts()
        {
            return _businessEntityService.GetContacts().Select(PersonDtoConverter.AsBusinessEntityContactDto);
        }
    }
}
