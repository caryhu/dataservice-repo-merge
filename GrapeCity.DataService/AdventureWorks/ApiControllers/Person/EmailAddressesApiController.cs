﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/emails")]
    [ApiController]
    public class EmailAddressesApiController : ControllerBase
    {
        private readonly IEmailAddressService _emailAddressService;

        public EmailAddressesApiController(IEmailAddressService emailAddressService)
        {
            _emailAddressService = emailAddressService;
        }

        /// <summary>
        /// Returns Paginated Email addresses data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Email addresses data</returns>
        [HttpGet]
        public IQueryable<EmailAddressDTO> GetEmailAddresses([FromQuery] PagingParameters pagingParameters)
        {
            var source = _emailAddressService.GetEmailAddresses();
            var response = source.CreatePagedResponse(pagingParameters, PersonDtoConverter.AsEmailAddressDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        // GET : api/emails/example@gmail.com/person
        [HttpGet("{email}/person")]
        public ActionResult<PersonDTO> GetPerson(string email)
        {
            var person = _emailAddressService.GetPerson(email).FirstOrDefault();

            if (person == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPersonDto(person);
        }
    }
}
