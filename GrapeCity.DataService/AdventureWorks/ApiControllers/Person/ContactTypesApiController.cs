﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/contactTypes")]
    [ApiController]
    public class ContactTypesApiController : ControllerBase
    {
        private readonly IContactTypeService _contactTypeService;

        public ContactTypesApiController(IContactTypeService contactTypeService)
        {
            _contactTypeService = contactTypeService;
        }

        // GET: api/contactTypes
        [HttpGet]
        public IQueryable<ContactTypeDTO> GetContactTypes()
        {
            return _contactTypeService.GetContactTypes().Select(PersonDtoConverter.AsContactTypeDto);
        }

        // GET: api/contactTypes/2
        [HttpGet("{typeId}")]
        public ActionResult<ContactTypeDTO> GetContactType(short typeId)
        {
            var contactType = _contactTypeService.GetContactType(typeId).FirstOrDefault();

            if(contactType == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToContactTypeDto(contactType);
        }

        // GET: api/contactTypes/2/businessContacts
        [HttpGet("{typeId}/businessContacts")]
        public IQueryable<BusinessEntityContactDTO> GetBusinessEntityContacts(short typeId)
        {
            return _contactTypeService.GetBusinessEntityContacts(typeId).Select(PersonDtoConverter.AsBusinessEntityContactDto);
        }
    }
}
