﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/addresses")]
    [ApiController]
    public class AddressesApiController : ControllerBase
    {
        private readonly IAddressService _addressService;

        public AddressesApiController(IAddressService addressService)
        {
            _addressService = addressService;
        }

        // GET: api/addresses/3
        [HttpGet("{addressId}")]
        public ActionResult<AddressDTO> GetAddress(int addressId)
        {
            var address = _addressService.GetAddress(addressId).FirstOrDefault();

            if (address == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToAddressDto(address);
        }

        /// <summary>
        /// Returns Paginated Addresses data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Addresses data</returns>
        [HttpGet]
        public IQueryable<AddressDTO> GetAddresses([FromQuery] PagingParameters pagingParameters)
        {
            var source = _addressService.GetAddresses();
            var response = source.CreatePagedResponse(pagingParameters, PersonDtoConverter.AsAddressDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        // GET: api/addresses/4/businessEntityAddresses
        [HttpGet("{addressId}/businessEntityAddresses")]
        public IQueryable<BusinessEntityAddressDTO> GetBusinessEntityAddresses(int addressId)
        {
            return _addressService.GetBusinessEntityAddresses(addressId).Select(PersonDtoConverter.AsBusinessEntityAddressDto);
        }

        // GET: api/addresses/4/province
        [HttpGet("{addressId}/province")]
        public IQueryable<StateProvinceDTO> GetProvince(int addressId)
        {
            return _addressService.GetProvince(addressId).Select(PersonDtoConverter.AsStateProvinceDto);
        }

        // GET: api/addresses/4/ordersBilledTo
        [HttpGet("{addressId}/ordersBilledTo")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrdersBilledToAddress(int addressId)
        {
            return _addressService.GetSalesOrdersBilledToAddress(addressId).Select(SalesDtoConverter.AsSalesOrderHeaderDto);
        }

        // GET: api/addresses/4/ordersShippedTo
        [HttpGet("{addressId}/ordersShippedTo")]
        public IQueryable<SalesOrderHeaderDTO> GetSalesOrdersShippedToAddress(int addressId)
        {
            return _addressService.GetSalesOrdersShippedToAddress(addressId).Select(SalesDtoConverter.AsSalesOrderHeaderDto);
        }
    }
}
