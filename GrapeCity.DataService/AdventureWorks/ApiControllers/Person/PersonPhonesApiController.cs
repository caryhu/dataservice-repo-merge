﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/phoneNumbers")]
    [ApiController]
    public class PersonPhonesApiController : ControllerBase
    {
        private readonly IPersonPhoneService _personPhoneService;

        public PersonPhonesApiController(IPersonPhoneService personPhoneService)
        {
            _personPhoneService = personPhoneService;
        }

        // GET: api/phoneNumbers/9999999999/person
        [HttpGet("{phoneNumber}/person")]
        public ActionResult<PersonDTO> GetPerson(string phoneNumber)
        {
           var person = _personPhoneService.GetPerson(phoneNumber).FirstOrDefault();

            if(person == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPersonDto(person);
        }

        /// <summary>
        /// Returns Paginated Phone numbers data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Phone numbers data</returns>
        [HttpGet]
        public IQueryable<PersonPhoneDTO> GetPhoneNumbers([FromQuery] PagingParameters pagingParameters)
        {
            var source = _personPhoneService.GetPhoneNumbers();
            var response = source.CreatePagedResponse(pagingParameters, PersonDtoConverter.AsPersonPhoneDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        // GET: api/phoneNumbers/9999999999/type
        [HttpGet("{phoneNumber}/type")]
        public ActionResult<PhoneNumberTypeDTO> GetPhoneNumberType(string phoneNumber)
        {
            var phoneNumberType = _personPhoneService.GetPhoneNumberType(phoneNumber).FirstOrDefault();

            if(phoneNumberType == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPhoneTypeDto(phoneNumberType);
        }
    }
}
