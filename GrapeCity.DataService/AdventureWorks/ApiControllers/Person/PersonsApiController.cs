﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using GrapeCity.DataService.AdventureWorks.Infra;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/persons")]
    [ApiController]
    public class PersonsApiController : ControllerBase
    {
        private readonly IPersonService _personService;

        public PersonsApiController(IPersonService personService)
        {
            _personService = personService;
        }

        // GET: api/persons/1/businessContacts
        [HttpGet("{personId}/businessContacts")]
        public IQueryable<BusinessEntityContactDTO> GetBusinessEntityContacts(int personId)
        {
            return _personService.GetBusinessEntityContacts(personId).Select(PersonDtoConverter.AsBusinessEntityContactDto);
        }

        // GET: api/persons/1/creditCards
        [HttpGet("{personId}/creditCards")]
        public IQueryable<CreditCardDTO> GetCreditCards(int personId)
        {
            return _personService.GetCreditCards(personId).Select(SalesDtoConverter.AsCreditCardDto);
        }

        // GET: api/persons/1/customers
        [HttpGet("{personId}/customers")]
        public IQueryable<CustomerDTO> GetCustomers(int personId)
        {
            return _personService.GetCustomers(personId).Select(SalesDtoConverter.AsCustomerDto);
        }

        // GET: api/persons/1/demographics
        [HttpGet("{personId}/demographics")]
        public IQueryable<Dictionary<string, string>> GetDemographicsData(int personId)
        {
           return _personService.GetDemographicsData(personId);
        }

        // GET: api/persons/4/emails
        [HttpGet("{personId}/emails")]
        public IQueryable<EmailAddressDTO> GetEmails(int personId)
        {
            return _personService.GetEmails(personId).Select(PersonDtoConverter.AsEmailAddressDto);
        }

        // GET: api/persons/3/employee
        [HttpGet("{personId}/employee")]
        public ActionResult<EmployeeDTO> GetEmployee(int personId)
        {
            var employee = _personService.GetEmployee(personId).FirstOrDefault();

            if (employee == null)
            {
                return NotFound();
            }

            return HRDtoConverter.ConvertToEmployeeDto(employee);
        }

        // GET: api/persons/1/password
        [HttpGet("{personId}/password")]
        public ActionResult<PasswordDTO> GetPassword(int personId)
        {
            var password = _personService.GetPassword(personId).FirstOrDefault();

            if (password == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPasswordDto(password);
        }

        // GET: api/persons/1
        [HttpGet("{personId}")]
        public ActionResult<PersonDTO> GetPerson(int personId)
        {
            var person = _personService.GetPerson(personId).FirstOrDefault();

            if (person == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToPersonDto(person);
        }

        /// <summary>
        /// Returns Paginated Persons data. Check 'X-Pagination' response header for paging details.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <returns>Returns Paginated Persons data</returns>
        [HttpGet]
        public IQueryable<PersonDTO> GetPersons([FromQuery] PagingParameters pagingParameters)
        {
            var source = _personService.GetPersons();
            var response = source.CreatePagedResponse(pagingParameters, PersonDtoConverter.AsPersonDto);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(response.MetaData));
            return response.Source;
        }

        // GET: api/persons/1/phones
        [HttpGet("{personId}/phones")]
        public IQueryable<PersonPhoneDTO> GetPhones(int personId)
        {
            return _personService.GetPhones(personId).Select(PersonDtoConverter.AsPersonPhoneDto);
        }

        // GET: api/persons/types
        [HttpGet("types")]
        public IQueryable<string> GetPersonTypes()
        {
            return _personService.GetPersonTypes();
        }
    }
}
