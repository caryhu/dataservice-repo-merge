﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/provinces")]
    [ApiController]
    public class StateProvincesApiController : ControllerBase
    {
        private readonly IStateProvinceService _stateProvinceService;

        public StateProvincesApiController(IStateProvinceService stateProvinceService)
        {
            _stateProvinceService =  stateProvinceService;
        }

        // GET : api/provinces/AB/addresses
        [HttpGet("{provinceCode}/addresses")]
        public IQueryable<AddressDTO> GetAddresses(string provinceCode)
        {
            return _stateProvinceService.GetAddresses(provinceCode).Select(PersonDtoConverter.AsAddressDto);
        }

        // GET : api/provinces/AB/country
        [HttpGet("{provinceCode}/country")]
        public ActionResult<CountryRegionDTO> GetCountry(string provinceCode)
        {
            var country = _stateProvinceService.GetCountry(provinceCode).FirstOrDefault();

            if(country == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToCountryRegionDto(country);
        }

        // GET : api/provinces/1
        [HttpGet("{provinceId:int}")]
        public ActionResult<StateProvinceDTO> GetStateProvince(int provinceId)
        {
            var state = _stateProvinceService.GetStateProvince(provinceId).FirstOrDefault();

            if (state == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToStateProvinceDto(state);
        }

        // GET : api/provinces/AB
        [HttpGet("{provinceCode}")]
        public ActionResult<StateProvinceDTO> GetStateProvince(string provinceCode)
        {
            var state = _stateProvinceService.GetStateProvince(provinceCode).FirstOrDefault();

            if (state == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToStateProvinceDto(state);
        }

        // GET : api/provinces/provinceNames
        [HttpGet("provinceNames")]
        public IQueryable<string> GetStateProvinceNames()
        {
            return _stateProvinceService.GetStateProvinceNames();
        }

        // GET : api/provinces
        [HttpGet]
        public IQueryable<StateProvinceDTO> GetStateProvinces()
        {
            return _stateProvinceService.GetStateProvinces().Select(PersonDtoConverter.AsStateProvinceDto);
        }

        // GET : api/provinces/AB/territory
        [HttpGet("{provinceCode}/territory")]
        public ActionResult<SalesTerritoryDTO> GetTerritory(string provinceCode)
        {
            var territory = _stateProvinceService.GetTerritory(provinceCode).FirstOrDefault();

            if (territory == null)
            {
                return NotFound();
            }

            return SalesDtoConverter.ConvertToSalesTerritoryDto(territory);
        }
    }
}
