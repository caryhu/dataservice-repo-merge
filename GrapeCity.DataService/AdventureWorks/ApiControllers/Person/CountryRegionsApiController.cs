﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.ApiControllers
{
    [Route("adventureworks/api/countries")]
    [ApiController]
    public class CountryRegionsApiController : ControllerBase
    {
        private readonly ICountryRegionService _countryRegionService;

        public CountryRegionsApiController(ICountryRegionService countryRegionService)
        {
            _countryRegionService = countryRegionService;
        }

        // GET: api/countries
        [HttpGet]
        public IQueryable<CountryRegionDTO> GetCountries()
        {
            return _countryRegionService.GetCountries().Select(PersonDtoConverter.AsCountryRegionDto);
        }

        // GET: api/countries/IN/details
        [HttpGet("{countryCode}/details")]
        public ActionResult<CountryRegionDTO> GetCountryDetails(string countryCode)
        {
            var country = _countryRegionService.GetCountryDetails(countryCode).FirstOrDefault();

            if(country == null)
            {
                return NotFound();
            }

            return PersonDtoConverter.ConvertToCountryRegionDto(country);
        }

        // GET: api/countries/IN
        [HttpGet("{countryCode}")]
        public ActionResult<string> GetCountryName(string countryCode)
        {
            string countryName = _countryRegionService.GetCountryName(countryCode).FirstOrDefault();

            if(countryName == null)
            {
                return NotFound();
            }

            return countryName;
        }

        // GET: api/countries/countryNames
        [HttpGet("countryNames")]
        public IQueryable<string> GetCountryNames()
        {
            return _countryRegionService.GetCountryNames();
        }

        // GET: api/countries/IN/currencies
        [HttpGet("{countryCode}/currencies")]
        public IQueryable<CurrencyDTO> GetCurrencies(string countryCode)
        {
            return _countryRegionService.GetCurrencies(countryCode).Select(SalesDtoConverter.AsCurrencyDto);
        }

        // GET: api/countries/IN/salesTerritories
        [HttpGet("{countryCode}/salesTerritories")]
        public IQueryable<SalesTerritoryDTO> GetSalesTerritories(string countryCode)
        {
            return _countryRegionService.GetSalesTerritories(countryCode).Select(SalesDtoConverter.AsSalesTerritoryDto);
        }

        // GET: api/countries/IN/stateProvinces
        [HttpGet("{countryCode}/stateProvinces")]
        public IQueryable<StateProvinceDTO> GetStateProvinces(string countryCode)
        {
            return _countryRegionService.GetStateProvinces(countryCode).Select(PersonDtoConverter.AsStateProvinceDto);
        }
    }
}
