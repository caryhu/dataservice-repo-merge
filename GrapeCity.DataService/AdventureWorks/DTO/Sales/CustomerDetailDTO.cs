﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public class CustomerDetailDTO
    {
        /// <summary>
        /// Primary key.
        /// </summary>
        public int CustomerId { get; set; }
        public PersonDTO Person { get; set; }
        public StoreDTO Store { get; set; }

        /// <summary>
        /// ID of the territory in which the customer is located. Foreign key to SalesTerritory.SalesTerritoryID.
        /// </summary>
        public string Territory { get; set; }
        /// <summary>
        /// Unique number identifying the customer assigned by the accounting system.
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        public ICollection<SalesOrderHeaderDTO> SalesOrderHeaders { get; set; }
    }
}
