﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public static class SalesDtoConverter
    {
        public static readonly Expression<Func<CreditCard, CreditCardDTO>> AsCreditCardDto = source =>
        new CreditCardDTO()
        {
            CreditCardId = source.CreditCardId,
            CardNumber = source.CardNumber,
            CardType = source.CardType,
            ExpiryMonth = source.ExpMonth,
            ExpiryYear = source.ExpYear,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<CreditCard, CreditCardDTO> ConvertToCreditCardDto = AsCreditCardDto.Compile();

        public static readonly Expression<Func<Currency, CurrencyDTO>> AsCurrencyDto = source =>
        new CurrencyDTO()
        {
            CurrencyCode = source.CurrencyCode,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Currency, CurrencyDTO> ConvertToCurrencyDto = AsCurrencyDto.Compile();

        public static readonly Expression<Func<CurrencyRate, CurrencyRateDTO>> AsCurrencyRateDto = sourc =>
        new CurrencyRateDTO()
        {
            CurrencyRateId = sourc.CurrencyRateId,
            FromCurrencyCode = sourc.FromCurrencyCode,
            ToCurrencyCode = sourc.ToCurrencyCode,
            AverageRate = sourc.AverageRate,
            CurrencyRateDate = sourc.CurrencyRateDate,
            EndOfDayRate = sourc.EndOfDayRate,
            ModifiedDate = sourc.ModifiedDate
        };
        public static readonly Func<CurrencyRate, CurrencyRateDTO> ConvertToCurrencyRateDto = AsCurrencyRateDto.Compile();

        public static readonly Expression<Func<Customer, CustomerDTO>> AsCustomerDto = source =>
        new CustomerDTO()
        {
            CustomerId = source.CustomerId,
            PersonId = source.PersonId,
            StoreId = source.StoreId,
            AccountNumber = source.AccountNumber,
            Territory = DataCompletionSource.Territory(source.Territory),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Customer, CustomerDTO> ConvertToCustomerDto = AsCustomerDto.Compile();

        public static readonly Expression<Func<Customer, CustomerDetailDTO>> AsCustomerDetailDto = source =>
        new CustomerDetailDTO()
        {
            CustomerId = source.CustomerId,
            Person = source.Person != null ? PersonDtoConverter.ConvertToPersonDto(source.Person) : null,
            Store = source.Store != null ? ConvertToStoreDto(source.Store) : null,
            Territory = DataCompletionSource.Territory(source.Territory),
            AccountNumber = source.AccountNumber,
            ModifiedDate = source.ModifiedDate,
            SalesOrderHeaders = source.SalesOrderHeaders.Select(o => ConvertToSalesOrderHeaderDto(o)).ToList()
        };
        public static readonly Func<Customer, CustomerDetailDTO> ConvertToCustomerDetailDto = AsCustomerDetailDto.Compile();

        public static readonly Expression<Func<SalesOrderDetail, SalesOrderDetailDTO>> AsSalesOrderDetailDto = source =>
        new SalesOrderDetailDTO()
        {
            SalesOrderDetailId = source.SalesOrderDetailId,
            SalesOrderId = source.SalesOrderId,
            ProductId = source.ProductId,
            ProductName = source.SpecialOfferProduct.Product.Name,
            OrderQuantity = source.OrderQty,
            UnitPrice = source.UnitPrice,
            UnitPriceDiscount = source.UnitPriceDiscount,
            LineTotal = source.LineTotal,
            SpecialOfferId = source.SpecialOfferId,
            CarrierTrackingNumber = source.CarrierTrackingNumber,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SalesOrderDetail, SalesOrderDetailDTO> ConvertToSalesOrderDetailDto = AsSalesOrderDetailDto.Compile();

        public static readonly Expression<Func<SalesOrderHeader, SalesOrderHeaderDTO>> AsSalesOrderHeaderDto = source =>
        new SalesOrderHeaderDTO()
        {
            SalesOrderId = source.SalesOrderId,
            RevisionNumber = source.RevisionNumber,
            OrderDate = source.OrderDate,
            DueDate = source.DueDate,
            ShipDate = source.ShipDate,
            Status = DataCompletionSource.SalesOrderStatus(source.Status),
            IsOrderedOnline = source.OnlineOrderFlag,
            SalesOrderNumber = source.SalesOrderNumber,
            PurchaseOrderNumber = source.PurchaseOrderNumber,
            AccountNumber = source.AccountNumber,
            CustomerId = source.CustomerId,
            SalesPersonId = source.SalesPersonId,
            Territory = DataCompletionSource.Territory(source.Territory),
            BillToAddress = DataCompletionSource.Address(source.BillToAddress),
            ShipToAddress = DataCompletionSource.Address(source.ShipToAddress),
            ShipMethod = source.ShipMethod.Name,
            CreditCardId = source.CreditCardId,
            CreditCardApprovalCode = source.CreditCardApprovalCode,
            CurrencyRateId = source.CurrencyRateId,
            SubTotal = source.SubTotal,
            TaxAmount = source.TaxAmt,
            Freight = source.Freight,
            TotalDue = source.TotalDue,
            Comment = source.Comment,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SalesOrderHeader, SalesOrderHeaderDTO> ConvertToSalesOrderHeaderDto = AsSalesOrderHeaderDto.Compile();

        public static readonly Expression<Func<SalesOrderHeader, SalesOrderHeaderDetailDTO>> AsSalesOrderHeaderDetailDto = source =>
        new SalesOrderHeaderDetailDTO()
        {
            SalesOrderId = source.SalesOrderId,
            RevisionNumber = source.RevisionNumber,
            OrderDate = source.OrderDate,
            DueDate = source.DueDate,
            ShipDate = source.ShipDate,
            Status = DataCompletionSource.SalesOrderStatus(source.Status),
            IsOrderedOnline = source.OnlineOrderFlag,
            SalesOrderNumber = source.SalesOrderNumber,
            PurchaseOrderNumber = source.PurchaseOrderNumber,
            AccountNumber = source.AccountNumber,
            Customer = ConvertToCustomerDto(source.Customer),
            SalesPerson = source.SalesPerson == null? null: ConvertToSalesPersonDto(source.SalesPerson),
            Territory = ConvertToSalesTerritoryDto(source.Territory),
            BillToAddress = PersonDtoConverter.ConvertToAddressDto(source.BillToAddress),
            ShipToAddress = PersonDtoConverter.ConvertToAddressDto(source.ShipToAddress),
            ShipMethod = PurchaseDtoConverter.ConvertToShipMethodDto(source.ShipMethod),
            CreditCard = ConvertToCreditCardDto(source.CreditCard),
            CreditCardApprovalCode = source.CreditCardApprovalCode,
            CurrencyRate = source.CurrencyRate == null? null: ConvertToCurrencyRateDto(source.CurrencyRate),
            SubTotal = source.SubTotal,            
            TaxAmount = source.TaxAmt,
            Freight = source.Freight,
            TotalDue = source.TotalDue,
            Comment = source.Comment,
            ModifiedDate = source.ModifiedDate,
            SalesOrderDetails = source.SalesOrderDetails.Select(o=>ConvertToSalesOrderDetailDto(o)).ToList(),
            SalesReasons = source.SalesOrderHeaderSalesReasons.Select(r=>ConvertToSalesReasonDto(r.SalesReason)).ToList()
        };
        public static readonly Func<SalesOrderHeader, SalesOrderHeaderDetailDTO> ConvertToSalesOrderHeaderDetailDto = AsSalesOrderHeaderDetailDto.Compile();

        public static readonly Expression<Func<SalesPerson, SalesPersonDTO>> AsSalesPersonDto = source =>
        new SalesPersonDTO()
        {
            SalesPersonId = source.BusinessEntityId,
            PercentCommision = source.CommissionPct,
            SalesLastYear = source.SalesLastYear,
            SalesQuota = source.SalesQuota,
            Bonus = source.Bonus,
            SalesYtd = source.SalesYtd,
            Territory = DataCompletionSource.Territory(source.Territory),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SalesPerson, SalesPersonDTO> ConvertToSalesPersonDto = AsSalesPersonDto.Compile();

        public static readonly Expression<Func<SalesPersonQuotaHistory, SalesPersonQuotaHistoryDTO>> AsSalesPersonQuotaHistoryDto =
            source => new SalesPersonQuotaHistoryDTO()
            {
                SalesPersonId = source.BusinessEntityId,
                SalesQuota = source.SalesQuota,
                QuotaDate = source.QuotaDate,
                ModifiedDate = source.ModifiedDate
            };
        public static readonly Func<SalesPersonQuotaHistory, SalesPersonQuotaHistoryDTO> ConvertToSalesPersonQuotaHistoryDto = AsSalesPersonQuotaHistoryDto.Compile();

        public static readonly Expression<Func<SalesReason, SalesReasonDTO>> AsSalesReasonDto = source =>
        new SalesReasonDTO()
        {
            SalesReasonId = source.SalesReasonId,
            Name = source.Name,
            ReasonType = source.ReasonType,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SalesReason, SalesReasonDTO> ConvertToSalesReasonDto = AsSalesReasonDto.Compile();

        public static readonly Expression<Func<SalesTaxRate, SalesTaxRateDTO>> AsSalesTaxRateDto = source =>
        new SalesTaxRateDTO()
        {
            SalesTaxRateId = source.SalesTaxRateId,
            Name = source.Name,
            TaxRate = source.TaxRate,
            TaxType = DataCompletionSource.TaxType(source.TaxType),
            StateProvince = PersonDtoConverter.ConvertToStateProvinceDto(source.StateProvince),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SalesTaxRate, SalesTaxRateDTO> ConvertToSalesTaxRateDto = AsSalesTaxRateDto.Compile();

        public static readonly Expression<Func<SalesTerritory, SalesTerritoryDTO>> AsSalesTerritoryDto = source =>
        new SalesTerritoryDTO()
        {
            TerritoryId = source.TerritoryId,
            Name = source.Name,
            CountryRegionCode = source.CountryRegionCode,
            Group = source.Group,
            SalesLastYear = source.SalesLastYear,
            SalesYtd = source.SalesYtd,
            CostLastYear = source.CostLastYear,
            CostYtd = source.CostYtd,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SalesTerritory, SalesTerritoryDTO> ConvertToSalesTerritoryDto = AsSalesTerritoryDto.Compile();

        public static readonly Expression<Func<SalesTerritoryHistory, SalesTerritoryHistoryDTO>> AsSalesTerritoryHistoryDto = source =>
        new SalesTerritoryHistoryDTO()
        {
            TerritoryId = source.TerritoryId,
            SalesPersonId = source.BusinessEntityId,
            StartDate = source.StartDate,
            EndDate = source.EndDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SalesTerritoryHistory, SalesTerritoryHistoryDTO> ConvertToSalesTerritoryHistoryDto = AsSalesTerritoryHistoryDto.Compile();

        public static readonly Expression<Func<ShoppingCartItem, ShoppingCartItemDTO>> AsShoppingCartItemDto = source =>
        new ShoppingCartItemDTO()
        {
            ShoppingCartId = source.ShoppingCartId,
            ShoppingCartItemId = source.ShoppingCartItemId,
            ProductId = source.ProductId,
            Quantity = source.Quantity,
            DateCreated = source.DateCreated,
            ModifiedDate = source.ModifiedDate,
        };
        public static readonly Func<ShoppingCartItem, ShoppingCartItemDTO> ConvertToShoppingCartItemDto = AsShoppingCartItemDto.Compile();

        public static readonly Expression<Func<SpecialOffer, SpecialOfferDTO>> AsSpecialOfferDto = source =>
        new SpecialOfferDTO()
        {
            SpecialOfferId = source.SpecialOfferId,
            Type = source.Type,
            Category = source.Category,
            Description = source.Description,
            PercentDiscount = source.DiscountPercentage,
            MinimumQuantity = source.MinimumQuantity,
            MaximumQuantity = source.MaximumQuantity,
            StartDate = source.StartDate,
            EndDate = source.EndDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<SpecialOffer, SpecialOfferDTO> ConvertToSpecialOfferDto = AsSpecialOfferDto.Compile();

        public static readonly Expression<Func<Store, StoreDTO>> AsStoreDto = source =>
        new StoreDTO()
        {
            StoreId = source.BusinessEntityId,
            SalesPersonId = source.SalesPersonId,
            Name = source.Name,
            Demographics = DataCompletionSource.Demographics(source.Demographics),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Store, StoreDTO> ConvertToStoreDto = AsStoreDto.Compile();
    }
}
