﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public static class HRDtoConverter
    {
        public static readonly Expression<Func<Department, DepartmentDTO>> AsDepartmentDto = source =>
        new DepartmentDTO()
        {
            DepartmentId = source.DepartmentId,
            Name = source.Name,
            GroupName = source.GroupName,
            ModifiedDate = source.ModifiedDate
        };

        public static readonly Func<Department, DepartmentDTO> ConvertToDepartmentDto = AsDepartmentDto.Compile();

        public static readonly Expression<Func<EmployeePayHistory, EmployeePayHistoryDTO>> AsEmployeePayHistoryDto = source =>
        new EmployeePayHistoryDTO()
        {
            EmployeeId = source.BusinessEntityId,
            Rate = source.Rate,
            PayFrequency = DataCompletionSource.PayFrequency(source.PayFrequency),
            RateChangeDate = source.RateChangeDate,
            ModifiedDate = source.ModifiedDate
        };

        public static readonly Func<EmployeePayHistory, EmployeePayHistoryDTO> ConvertToEmployeePayHistoryDto = AsEmployeePayHistoryDto.Compile();

        public static readonly Expression<Func<EmployeeDepartmentHistory, EmployeeDepartmentHistoryDTO>> AsEmployeeDepartmentHistoryDto =
            source =>
            new EmployeeDepartmentHistoryDTO()
            {
                Department = source.Department.Name,
                DepartmentGroup = source.Department.GroupName,
                Shift = source.Shift.Name,
                EmployeeId = source.BusinessEntityId,
                StartDate = source.StartDate,
                EndDate = source.EndDate
            };

        public static readonly Func<EmployeeDepartmentHistory, EmployeeDepartmentHistoryDTO> ConvertToEmployeeDepartmentHistoryDTO =
            AsEmployeeDepartmentHistoryDto.Compile();

        public static readonly Expression<Func<Employee, EmployeeDTO>> AsEmployeeDto = source =>
        new EmployeeDTO()
        {
            EmployeeId = source.BusinessEntityId,
            BirthDate = source.BirthDate,
            IsActive = source.CurrentFlag,
            Gender = DataCompletionSource.Gender(source.Gender),
            HireDate = source.HireDate,
            JobTitle = source.JobTitle,
            LoginId = source.LoginId,
            MaritalStatus = DataCompletionSource.MaritalStatus(source.MaritalStatus),
            NationalIdnumber = source.NationalIdnumber,
            OrganizationLevel = source.OrganizationLevel,
            IsSalaried = source.SalariedFlag,
            SickLeaveHours = source.SickLeaveHours,
            VacationHours = source.VacationHours
        };

        public static readonly Func<Employee, EmployeeDTO> ConvertToEmployeeDto = AsEmployeeDto.Compile();

        public static readonly Expression<Func<JobCandidate, JobCandidateDTO>> AsJobCandidateDTO = source =>
        new JobCandidateDTO()
        {
            EmployeeId = source.BusinessEntityId,
            JobCandidateId = source.JobCandidateId,
            Resume = source.Resume
        };

        public static readonly Func<JobCandidate, JobCandidateDTO> ConvertToJobCandidateDTO = AsJobCandidateDTO.Compile();

        public static readonly Expression<Func<Shift, ShiftDTO>> AsShiftDTO = source =>
        new ShiftDTO()
        {
            ShiftId = source.ShiftId,
            Name = source.Name,
            StartTime = source.StartTime.ToString(@"hh\:mm"),
            EndTime = source.EndTime.ToString(@"hh\:mm")
        };

        public static readonly Func<Shift, ShiftDTO> ConvertToShiftDTO = AsShiftDTO.Compile();
    }
}
