﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public class ProductModelDescriptionCultureDTO
    {
        public string CultureId { get; set; }
        public int ProductDescriptionId { get; set; }
        public string ProductDescription { get; set; }
        public int ProductModelId { get; set; }
        public string ProductModel { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
