﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    /// <summary>
    /// Lookup table containing the languages in which some AdventureWorks data is stored.
    /// </summary>
    public partial class CultureDTO
    {
        /// <summary>
        /// Primary key for Culture records.
        /// </summary>
        public string CultureId { get; set; }
        /// <summary>
        /// Culture description.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}