﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public static class ProductionDtoConverter
    {
        public static readonly Expression<Func<BillOfMaterial, BillOfMaterialDTO>> AsBillOfMaterialDto = source =>
        new BillOfMaterialDTO()
        {
            BillOfMaterialsId = source.BillOfMaterialsId,
            ProductAssemblyId = source.ProductAssemblyId,
            ComponentId = source.ComponentId,
            MeasurementUnit = source.QuantityUnit.Name,
            StartDate = source.StartDate,
            EndDate = source.EndDate,
            Bomlevel = source.Bomlevel,
            PerAssemblyQuantity = source.PerAssemblyQty,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<BillOfMaterial, BillOfMaterialDTO> ConvertToBillOfMaterialDto = AsBillOfMaterialDto.Compile();

        public static readonly Expression<Func<Culture, CultureDTO>> AsCultureDto = source =>
        new CultureDTO()
        {
            CultureId = source.CultureId.Trim(),
            Name = source.Name.Trim(),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Culture, CultureDTO> ConvertToCultureDto = AsCultureDto.Compile();

        public static readonly Expression<Func<Illustration, IllustrationDTO>> AsIllustrationDto = source =>
        new IllustrationDTO()
        {
            IllustrationId = source.IllustrationId,
            Diagram = source.Diagram,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Illustration, IllustrationDTO> ConvertToIllustrationDTo = AsIllustrationDto.Compile();

        public static readonly Expression<Func<Location, LocationDTO>> AsLocationDto = source =>
        new LocationDTO()
        {
            LocationId = source.LocationId,
            Name = source.Name,
            Availability = source.Availability,
            CostRate = source.CostRate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Location, LocationDTO> ConvertToLocationDto = AsLocationDto.Compile();

        public static readonly Expression<Func<ProductCategory, ProductCategoryDTO>> AsProductCategoryDto = source =>
        new ProductCategoryDTO()
        {
            ProductCategoryId = source.ProductCategoryId,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductCategory, ProductCategoryDTO> ConvertToProductCategoryDto = AsProductCategoryDto.Compile();

        public static readonly Expression<Func<ProductSubcategory, ProductSubcategoryDTO>> AsProductSubcategoryDto = source =>
        new ProductSubcategoryDTO()
        {
            ProductSubcategoryId = source.ProductSubcategoryId,
            Category = source.ProductCategory.Name,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductSubcategory, ProductSubcategoryDTO> ConvertToProductSubcateogryDto = AsProductSubcategoryDto.Compile();

        public static readonly Expression<Func<ProductCostHistory, ProductCostHistoryDTO>> AsProductCostHistoryDto = source =>
        new ProductCostHistoryDTO()
        {
            ProductId = source.ProductId,
            StartDate = source.StartDate,
            EndDate = source.EndDate,
            StandardCost = source.StandardCost,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductCostHistory, ProductCostHistoryDTO> ConvertToProductCostHistoryDto = AsProductCostHistoryDto.Compile();

        public static readonly Expression<Func<ProductDescription, ProductDescriptionDTO>> AsProductDescriptionDto = source =>
        new ProductDescriptionDTO()
        {
            ProductDescriptionId = source.ProductDescriptionId,
            Description = source.Description,
            CultureCode = source.ProductModelProductDescriptionCultures.First(x=>x.ProductDescriptionId == source.ProductDescriptionId).CultureId.Trim(),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductDescription, ProductDescriptionDTO> ConvertToProductDescriptionDto = AsProductDescriptionDto.Compile();

        public static readonly Expression<Func<Product, ProductDTO>> AsProductDto = source =>
        new ProductDTO()
        {
            ProductId = source.ProductId,
            Name = source.Name,
            ProductNumber = source.ProductNumber,
            IsManufactured = source.MakeFlag,
            IsSaleable = source.FinishedGoodsFlag,
            Color = source.Color,
            SafetyStockLevel = source.SafetyStockLevel,
            ReorderPoint = source.ReorderPoint,
            StandardCost = source.StandardCost,
            ListPrice = source.ListPrice,
            Size = source.Size,
            SizeUnit = source.UnitOfSize != null ? source.UnitOfSize.Name : null,
            Weight = source.Weight,
            WeightUnit = source.UnitOfSize != null ? source.UnitOfWeight.Name : null,
            DaysToManufacture = source.DaysToManufacture,
            ProductLine = DataCompletionSource.ProductLine(source.ProductLine),
            Class = DataCompletionSource.ProductClass(source.Class),
            Style = DataCompletionSource.ProductStyle(source.Style),
            Category = source.ProductSubcategory != null ? source.ProductSubcategory.ProductCategory.Name : null,
            Subcategory = source.ProductSubcategory != null ? source.ProductSubcategory.Name : null,
            Model = source.ProductModel != null ? source.ProductModel.Name : null,
            SellStartDate = source.SellStartDate,
            SellEndDate = source.SellEndDate,
            DiscontinuedDate = source.DiscontinuedDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Product, ProductDTO> ConvertToProductDto = AsProductDto.Compile();

        public static readonly Expression<Func<Product, ProductDetailDTO>> AsProductDetailDto = source =>
         new ProductDetailDTO()
         {
             ProductId = source.ProductId,
             Name = source.Name,
             ProductNumber = source.ProductNumber,
             IsManufactured = source.MakeFlag,
             IsSaleable = source.FinishedGoodsFlag,
             Color = source.Color,
             SafetyStockLevel = source.SafetyStockLevel,
             ReorderPoint = source.ReorderPoint,
             StandardCost = source.StandardCost,
             ListPrice = source.ListPrice,
             Size = source.Size,
             SizeUnit = source.UnitOfSize != null ? source.UnitOfSize.Name : null,
             Weight = source.Weight,
             WeightUnit = source.UnitOfWeight != null ? source.UnitOfWeight.Name : null,
             DaysToManufacture = source.DaysToManufacture,
             ProductLine = source.ProductLine != null ? DataCompletionSource.ProductLine(source.ProductLine) : null,
             Class = source.Class != null ? DataCompletionSource.ProductClass(source.Class): null,
             Style = source.Style != null ? DataCompletionSource.ProductStyle(source.Style): null,
             Category = source.ProductSubcategory != null ? source.ProductSubcategory.ProductCategory.Name : null,
             Subcategory = source.ProductSubcategory != null ? source.ProductSubcategory.Name : null,
             Model = source.ProductModel != null ? source.ProductModel.Name : null,
             ModelInfo = source.ProductModel != null ? ConvertToProductModelDto(source.ProductModel) : null,
             SellStartDate = source.SellStartDate,
             SellEndDate = source.SellEndDate,
             DiscontinuedDate = source.DiscontinuedDate,
             ModifiedDate = source.ModifiedDate,
             BillOfMaterialComponents = source.BillOfMaterialComponents.Select(b=>ConvertToBillOfMaterialDto(b)).ToList(),
             BillOfMaterialProductAssemblies = source.BillOfMaterialProductAssemblies.Select(b=>ConvertToBillOfMaterialDto(b)).ToList(),
             ProductCostHistories = source.ProductCostHistories.Select(c=>ConvertToProductCostHistoryDto(c)).ToList(),
             ProductInventories = source.ProductInventories.Select(i=>ConvertToProductInventoryDto(i)).ToList(),
             ProductListPriceHistories = source.ProductListPriceHistories.Select(p=>ConvertToProdListHistoryDto(p)).ToList(),
             ProductPhotos = source.ProductProductPhotos.Select(p=>ConvertToProdPhotoDto(p.ProductPhoto)).ToList(),
             ProductReviews = source.ProductReviews.Select(r=>ConvertToProdReviewDto(r)).ToList(),
             ProductVendors = source.ProductVendors.Select(v=> PurchaseDtoConverter.ConvertToProductVendorDto(v)).ToList(),
             WorkOrders = source.WorkOrders.Select(o => ConvertToWorkOrderDto(o)).ToList(),
             PurchaseOrderDetails = source.PurchaseOrderDetails.Select(o=>PurchaseDtoConverter.ConvertToPurchaseOrderDetailDto(o)).ToList(),
             ShoppingCartItems = source.ShoppingCartItems.Select(i=>SalesDtoConverter.ConvertToShoppingCartItemDto(i)).ToList(),
             TransactionHistories = source.TransactionHistories.Select(t=>ConvertToTransactionHistoryDto(t)).ToList(),
             SpecialOffers = source.SpecialOfferProducts.Select(o=>SalesDtoConverter.ConvertToSpecialOfferDto(o.SpecialOffer)).ToList()
         };
        public static readonly Func<Product, ProductDetailDTO> ConvertToProductDetailDto = AsProductDetailDto.Compile();

        public static readonly Expression<Func<ProductInventory, ProductInventoryDTO>> AsProductInventoryDTO = source =>
        new ProductInventoryDTO()
        {
            ProductId = source.ProductId,
            Location = ConvertToLocationDto(source.Location),
            Shelf = source.Shelf,
            Bin = source.Bin,
            Quantity = source.Quantity,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductInventory, ProductInventoryDTO> ConvertToProductInventoryDto = AsProductInventoryDTO.Compile();

        public static readonly Expression<Func<ProductListPriceHistory, ProductListPriceHistoryDTO>> AsProdListPriceHistoryDto = source =>
        new ProductListPriceHistoryDTO()
        {
            ProductId = source.ProductId,
            ListPrice = source.ListPrice,
            StartDate = source.StartDate,
            EndDate = source.EndDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductListPriceHistory, ProductListPriceHistoryDTO> ConvertToProdListHistoryDto = AsProdListPriceHistoryDto.Compile();

        public static readonly Expression<Func<ProductModelProductDescriptionCulture, ProductModelDescriptionCultureDTO>> AsProductModelDescriptionCultureDto =
            source => new ProductModelDescriptionCultureDTO()
            {
                CultureId = source.CultureId,
                ProductDescriptionId = source.ProductDescriptionId,
                ProductDescription = source.ProductDescription.Description,
                ProductModelId = source.ProductModelId,
                ProductModel = source.ProductModel.Name,
                ModifiedDate = source.ModifiedDate
            };
        public static readonly Func<ProductModelProductDescriptionCulture, ProductModelDescriptionCultureDTO> ConvertToProductModelDescCultureDto = AsProductModelDescriptionCultureDto.Compile();

        public static readonly Expression<Func<ProductModel, ProductModelDTO>> AsProductModelDto = source =>
        new ProductModelDTO()
        {
            ProductModelId = source.ProductModelId,
            Name = source.Name,
            Instructions = source.Instructions,
            CatalogDescription = source.CatalogDescription,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductModel, ProductModelDTO> ConvertToProductModelDto = AsProductModelDto.Compile();

        public static readonly Expression<Func<ProductModelIllustration, ProductModelIllustrationDTO>> AsProdModelIllustrationDto =
            source => new ProductModelIllustrationDTO()
            {
                ProductModelId =source.ProductModelId,
                IllustrationId = source.IllustrationId,
                ModifiedDate = source.ModifiedDate
            };
        public static readonly Func<ProductModelIllustration, ProductModelIllustrationDTO> ConvertToProdModelIllustrationDto = AsProdModelIllustrationDto.Compile();

        public static readonly Expression<Func<ProductPhoto, ProductPhotoDTO>> AsProdPhotoDto = source =>
        new ProductPhotoDTO()
        {
            ProductPhotoId = source.ProductPhotoId,
            ThumbnailPhotoFileName = source.ThumbnailPhotoFileName,
            ThumbNailPhoto = Convert.ToBase64String(source.ThumbNailPhoto),
            LargePhotoFileName = source.LargePhotoFileName,
            LargePhoto = Convert.ToBase64String(source.LargePhoto),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductPhoto, ProductPhotoDTO> ConvertToProdPhotoDto = AsProdPhotoDto.Compile();

        public static readonly Expression<Func<ProductReview, ProductReviewDTO>> AsProdReviewDto = source =>
        new ProductReviewDTO()
        {
            ProductReviewId = source.ProductReviewId,
            ProductId = source.ProductId,
            ReviewerName = source.ReviewerName,
            EmailAddress = source.EmailAddress,
            Comments = source.Comments,
            Rating = source.Rating,
            ReviewDate = source.ReviewDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductReview, ProductReviewDTO> ConvertToProdReviewDto = AsProdReviewDto.Compile();

        public static readonly Expression<Func<ScrapReason, ScrapReasonDTO>> AsScrapReasonDto = source =>
        new ScrapReasonDTO()
        {
            ScrapReasonId = source.ScrapReasonId,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ScrapReason, ScrapReasonDTO> ConvertToScrapReasonDto = AsScrapReasonDto.Compile();

        public static readonly Expression<Func<TransactionHistory, TransactionDTO>> AsTransactionHistoryDto = source =>
        new TransactionDTO()
        {
            ProductId = source.ProductId,
            ReferenceOrderId = source.ReferenceOrderId,
            ReferenceOrderLineId = source.ReferenceOrderLineId,
            TransactionId = source.TransactionId,
            TransactionType = DataCompletionSource.TransactionType(source.TransactionType),
            Quantity = source.Quantity,
            ActualCost = source.ActualCost,
            TransactionDate = source.TransactionDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<TransactionHistory, TransactionDTO> ConvertToTransactionHistoryDto = AsTransactionHistoryDto.Compile();

        public static readonly Expression<Func<TransactionHistoryArchive, TransactionDTO>> AsTransactionHistoryArchDto = source =>
        new TransactionDTO()
        {
            ProductId = source.ProductId,
            ReferenceOrderId = source.ReferenceOrderId,
            ReferenceOrderLineId = source.ReferenceOrderLineId,
            TransactionId = source.TransactionId,
            TransactionType = DataCompletionSource.TransactionType(source.TransactionType),
            Quantity = source.Quantity,
            ActualCost = source.ActualCost,
            TransactionDate = source.TransactionDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<TransactionHistoryArchive, TransactionDTO> ConvertToTransactionHistoryArchDto = AsTransactionHistoryArchDto.Compile();

        public static readonly Expression<Func<UnitMeasure, UnitMeasureDTO>> AsUnitMeasureDto = source =>
        new UnitMeasureDTO()
        {
            UnitMeasureCode = source.UnitMeasureCode,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<UnitMeasure, UnitMeasureDTO> ConvertToUnitMeasureDto = AsUnitMeasureDto.Compile();

        public static readonly Expression<Func<WorkOrder, WorkOrderDTO>> AsWorkOrderDto = source =>
        new WorkOrderDTO()
        {
            WorkOrderId = source.WorkOrderId,
            ProductId = source.ProductId,
            StockedQuantity = source.StockedQty,
            OrderQuantity = source.OrderQty,
            ScrappedQuantity = source.ScrappedQty,
            StartDate = source.StartDate,
            DueDate = source.DueDate,
            EndDate = source.EndDate,
            ScrapReason = source.ScrapReason != null ?  source.ScrapReason.Name : null,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<WorkOrder, WorkOrderDTO> ConvertToWorkOrderDto = AsWorkOrderDto.Compile();

        public static readonly Expression<Func<WorkOrderRouting, WorkOrderRoutingDTO>> AsWorkOrderRouting = source =>
        new WorkOrderRoutingDTO()
        {
            WorkOrderId = source.WorkOrderId,
            ProductId = source.ProductId,
            OperationSequence = source.OperationSequence,
            Location = source.Location.Name,
            PlannedCost = source.PlannedCost,
            ActualCost = source.ActualCost,
            ScheduledStartDate = source.ScheduledStartDate,
            ActualStartDate = source.ActualStartDate,
            ScheduledEndDate = source.ScheduledEndDate,
            ActualEndDate = source.ActualEndDate,
            ActualResourceHours = source.ActualResourceHrs,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<WorkOrderRouting, WorkOrderRoutingDTO> ConvertToWorkOrderRouting = AsWorkOrderRouting.Compile();
    }
}
