﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    /// <summary>
    /// Human beings involved with AdventureWorks: employees, customer contacts, and vendor contacts.
    /// </summary>
    public partial class PersonDetailDTO : PersonDTO
    {             
        public PersonDetailDTO()
        {
            BusinessEntityContacts = new HashSet<BusinessEntityContactDTO>();
            Customers = new HashSet<CustomerDTO>();
            EmailAddresses = new HashSet<EmailAddressDTO>();
            PersonCreditCards = new HashSet<CreditCardDTO>();
            PersonPhones = new HashSet<PersonPhoneDTO>();
        }
        public EmployeeDTO Employee { get; set; }
        public PasswordDTO Password { get; set; }
        public ICollection<BusinessEntityContactDTO> BusinessEntityContacts { get; set; }
        public ICollection<CustomerDTO> Customers { get; set; }
        public ICollection<EmailAddressDTO> EmailAddresses { get; set; }
        public ICollection<CreditCardDTO> PersonCreditCards { get; set; }
        public ICollection<PersonPhoneDTO> PersonPhones { get; set; }
    }
}