﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public static class PersonDtoConverter
    {
        public static readonly Expression<Func<Address, AddressDTO>> AsAddressDto = source =>
        new AddressDTO()
        {
            AddressId = source.AddressId,
            AddressLine1 = source.AddressLine1,
            AddressLine2 = source.AddressLine2,
            City = source.City,
            State = source.StateProvince.Name,
            Country = source.StateProvince.Country.Name,
            PostalCode = source.PostalCode,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Address, AddressDTO> ConvertToAddressDto = AsAddressDto.Compile();

        public static readonly Expression<Func<AddressType, AddressTypeDTO>> AsAddressTypeDto = source =>
        new AddressTypeDTO()
        {
            AddressTypeId = source.AddressTypeId,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<AddressType, AddressTypeDTO> ConvertToAddressTypeDto = AsAddressTypeDto.Compile();

        public static readonly Expression<Func<BusinessEntityAddress, BusinessEntityAddressDTO>> AsBusinessEntityAddressDto = 
        source =>
        new BusinessEntityAddressDTO()
        {
            BusinessEntityId = source.BusinessEntityId,
            Address = DataCompletionSource.Address(source.Address),
            AddressType = source.AddressType.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<BusinessEntityAddress, BusinessEntityAddressDTO> ConvertToBusinessEnityAddressDto =
            AsBusinessEntityAddressDto.Compile();

        public static readonly Expression<Func<BusinessEntityContact, BusinessEntityContactDTO>> AsBusinessEntityContactDto =
            source =>
            new BusinessEntityContactDTO()
            {
                BusinessEntityId = source.BusinessEntityId,
                PersonId = source.PersonId,
                ContactType = source.ContactType.Name,
                ModifiedDate = source.ModifiedDate,
            };
        public static readonly Func<BusinessEntityContact, BusinessEntityContactDTO> ConvertToBusinessEntityContactDto
            = AsBusinessEntityContactDto.Compile();

        public static readonly Expression<Func<ContactType, ContactTypeDTO>> AsContactTypeDto = source =>
        new ContactTypeDTO()
        {
            ContactTypeId = source.ContactTypeId,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ContactType, ContactTypeDTO> ConvertToContactTypeDto = AsContactTypeDto.Compile();

        public static readonly Expression<Func<BusinessEntity, BusinessEntityDetailDTO>> AsBusinessEntityDetailDto = source =>
        new BusinessEntityDetailDTO()
        {
            BusinessEntityID = source.BusinessEntityId,
            Addresses = source.BusinessEntityAddresses.Select(x => ConvertToBusinessEnityAddressDto(x)).ToList(),
            Contacts = source.BusinessEntityContacts.Select(x => ConvertToBusinessEntityContactDto(x)).ToList()
        };
        public static readonly Func<BusinessEntity, BusinessEntityDetailDTO> ConvertToBusinessEntityDetailDto = AsBusinessEntityDetailDto.Compile();

        public static readonly Expression<Func<CountryRegion, CountryRegionDTO>> AsCountryRegionDto = source =>
        new CountryRegionDTO()
        {
            CountryRegionCode = source.CountryRegionCode,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<CountryRegion, CountryRegionDTO> ConvertToCountryRegionDto = AsCountryRegionDto.Compile();

        public static readonly Expression<Func<EmailAddress, EmailAddressDTO>> AsEmailAddressDto = source =>
        new EmailAddressDTO()
        {
            PersonId = source.BusinessEntityId,
            EmailAddressId = source.EmailAddressId,
            EmailAddress = source.Email,
            ModifiedDate = source.ModifiedDate,
        };
        public static readonly Func<EmailAddress, EmailAddressDTO> ConvertToEmailAddressDto = AsEmailAddressDto.Compile();

        public static readonly Expression<Func<Password, PasswordDTO>> AsPasswordDto = source =>
        new PasswordDTO()
        {
            PersonId = source.Person.BusinessEntityId,
            PasswordHash = source.PasswordHash,
            PasswordSalt = source.PasswordSalt,
            ModifiedDate = source.ModifiedDate,
        };
        public static readonly Func<Password, PasswordDTO> ConvertToPasswordDto = AsPasswordDto.Compile();

        public static readonly Expression<Func<Person, PersonDTO>> AsPersonDto = source =>
        new PersonDTO
        {
            PersonId = source.BusinessEntityId,
            PersonType = DataCompletionSource.PersonType(source.PersonType),
            Title = source.Title,
            FirstName = source.FirstName,
            MiddleName = source.MiddleName,
            LastName = source.LastName,
            NameStyle = DataCompletionSource.NameStyle(source.NameStyle),
            Suffix = source.Suffix,
            EmailPromotion = DataCompletionSource.EmailPromotion(source.EmailPromotion),
            AdditionalContactInfo = source.AdditionalContactInfo,
            Demographics = DataCompletionSource.Demographics(source.Demographics),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Person, PersonDTO> ConvertToPersonDto = AsPersonDto.Compile();

        public static readonly Expression<Func<Person, PersonDetailDTO>> AsPersonDetailDto = source =>
        new PersonDetailDTO()
        {
            PersonId = source.BusinessEntityId,
            PersonType = DataCompletionSource.PersonType(source.PersonType),
            Title = source.Title,
            FirstName = source.FirstName,
            MiddleName = source.MiddleName,
            LastName = source.LastName,
            NameStyle = DataCompletionSource.NameStyle(source.NameStyle),
            Suffix = source.Suffix,
            EmailPromotion = DataCompletionSource.EmailPromotion(source.EmailPromotion),
            AdditionalContactInfo = source.AdditionalContactInfo,
            Demographics = DataCompletionSource.Demographics(source.Demographics),
            ModifiedDate = source.ModifiedDate,
            Employee = HRDtoConverter.ConvertToEmployeeDto(source.Employee),
            EmailAddresses = source.EmailAddresses.Select(e=> ConvertToEmailAddressDto(e)).ToList(),
            Password = ConvertToPasswordDto(source.Password),
            BusinessEntityContacts = source.BusinessEntityContacts.Select(e=>ConvertToBusinessEntityContactDto(e)).ToList(),
            Customers = source.Customers.Select(c=>SalesDtoConverter.ConvertToCustomerDto(c)).ToList(),
            PersonPhones = source.PersonPhones.Select(p=>ConvertToPersonPhoneDto(p)).ToList(),
            PersonCreditCards = source.PersonCreditCards.Select(c=>SalesDtoConverter.ConvertToCreditCardDto(c.CreditCard)).ToList(),
        };
        public static readonly Func<Person, PersonDetailDTO> ConvertToPersonDetailDto = AsPersonDetailDto.Compile();

        public static readonly Expression<Func<PersonPhone, PersonPhoneDTO>> AsPersonPhoneDto = source =>
        new PersonPhoneDTO()
        {
            PersonId = source.Person.BusinessEntityId,
            PhoneNumber = source.PhoneNumber,
            PhoneNumberType = source.PhoneNumberType.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<PersonPhone, PersonPhoneDTO> ConvertToPersonPhoneDto = AsPersonPhoneDto.Compile();

        public static readonly Expression<Func<PhoneNumberType, PhoneNumberTypeDTO>> AsPhoneTypeDto = source =>
        new PhoneNumberTypeDTO()
        {
            PhoneNumberTypeId = source.PhoneNumberTypeId,
            Name = source.Name,
            ModifiedDate = source.ModifiedDate,
        };
        public static readonly Func<PhoneNumberType, PhoneNumberTypeDTO> ConvertToPhoneTypeDto = AsPhoneTypeDto.Compile();

        public static readonly Expression<Func<StateProvince, StateProvinceDTO>> AsStateProvinceDto = source =>
        new StateProvinceDTO()
        {
            StateProvinceId = source.StateProvinceId,
            StateProvinceCode = source.StateProvinceCode,
            Name = source.Name,
            StateProvinceCodeUnavailable = source.IsOnlyStateProvinceFlag,
            Country = source.Country.Name,
            Territory = DataCompletionSource.Territory(source.Territory),
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<StateProvince, StateProvinceDTO> ConvertToStateProvinceDto = AsStateProvinceDto.Compile();
    }
}
