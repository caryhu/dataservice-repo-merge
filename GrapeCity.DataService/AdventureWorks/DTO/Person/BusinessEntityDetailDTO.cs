﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public class BusinessEntityDetailDTO
    {
        public int BusinessEntityID { get; set; }
        public ICollection<BusinessEntityAddressDTO> Addresses { get; set; }
        public ICollection<BusinessEntityContactDTO> Contacts { get; set; }
    }
}
