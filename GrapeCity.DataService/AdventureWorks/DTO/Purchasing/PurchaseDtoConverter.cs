﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public static class PurchaseDtoConverter
    {
        public static readonly Expression<Func<ProductVendor, ProductVendorDTO>> AsProductVendorDto = source =>
        new ProductVendorDTO
        {
            ProductId = source.ProductId,
            VendorId = source.BusinessEntityId,
            AverageLeadTime = source.AverageLeadTime,
            StandardPrice = source.StandardPrice,
            LastReceiptCost = source.LastReceiptCost,
            LastReceiptDate = source.LastReceiptDate,
            MinOrderQuantity = source.MinOrderQty,
            MaxOrderQuantity = source.MaxOrderQty,
            OnOrderQuantity = source.OnOrderQty,
            MeasurementUnit = source.UnitOfMeasure.Name,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ProductVendor, ProductVendorDTO> ConvertToProductVendorDto = AsProductVendorDto.Compile();

        public static readonly Expression<Func<PurchaseOrderHeader, PurchaseOrderHeaderDTO>> AsPurchaseOrderHeaderDto = source =>
        new PurchaseOrderHeaderDTO()
        {
            PurchaseOrderId = source.PurchaseOrderId,
            EmployeeId = source.EmployeeId,
            RevisionNumber = source.RevisionNumber,
            Vendor = source.Vendor.Name,
            Status = DataCompletionSource.PurchaseOrderStatus(source.Status),
            OrderDate = source.OrderDate,
            ShipDate = source.ShipDate,
            ShipMethod = source.ShipMethod.Name,
            SubTotal = source.SubTotal,
            TaxAmount = source.TaxAmt,
            Freight = source.Freight,
            TotalDue = source.TotalDue,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<PurchaseOrderHeader, PurchaseOrderHeaderDTO> ConvertToPurchaseOrderHeaderDto = AsPurchaseOrderHeaderDto.Compile();

        public static readonly Expression<Func<PurchaseOrderHeader, PurchaseOrderHeaderDetailDTO>> AsPurchaseOrderHeaderDetailDto = source =>
        new PurchaseOrderHeaderDetailDTO()
        {
            PurchaseOrderId = source.PurchaseOrderId,
            Employee = HRDtoConverter.ConvertToEmployeeDto(source.Employee),
            RevisionNumber = source.RevisionNumber,
            Vendor = ConvertToVendorDto(source.Vendor),
            Status = DataCompletionSource.PurchaseOrderStatus(source.Status),
            OrderDate = source.OrderDate,
            ShipDate = source.ShipDate,
            ShipMethod = ConvertToShipMethodDto(source.ShipMethod),
            SubTotal = source.SubTotal,
            TaxAmount = source.TaxAmt,
            Freight = source.Freight,
            TotalDue = source.TotalDue,
            ModifiedDate = source.ModifiedDate,
            PurchaseOrderDetails = source.PurchaseOrderDetails.Select(o => ConvertToPurchaseOrderDetailDto(o)).ToList()
        };

        public static readonly Expression<Func<PurchaseOrderDetail, PurchaseOrderDetailDTO>> AsPurchaseOrderDetailDto = source =>
        new PurchaseOrderDetailDTO()
        {
            PurchaseOrderDetailId = source.PurchaseOrderDetailId,
            PurchaseOrderId = source.PurchaseOrderId,
            ProductId = source.ProductId,
            OrderQuantity = source.OrderQty,
            StockedQuantity = source.StockedQty,
            ReceivedQuantity = source.ReceivedQty,
            RejectedQuantity = source.RejectedQty,
            UnitPrice = source.UnitPrice,
            LineTotal = source.LineTotal,
            DueDate = source.DueDate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<PurchaseOrderDetail, PurchaseOrderDetailDTO> ConvertToPurchaseOrderDetailDto = AsPurchaseOrderDetailDto.Compile();

        public static readonly Expression<Func<ShipMethod, ShipMethodDTO>> AsShipMethodDto = source =>
        new ShipMethodDTO()
        {
            ShipMethodId = source.ShipMethodId,
            Name = source.Name,
            ShipBase = source.ShipBase,
            ShipRate = source.ShipRate,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<ShipMethod, ShipMethodDTO> ConvertToShipMethodDto = AsShipMethodDto.Compile();

        public static readonly Expression<Func<Vendor, VendorDTO>> AsVendorDto = source =>
        new VendorDTO()
        {
            VendorId = source.BusinessEntityId,
            Name = source.Name,
            AccountNumber = source.AccountNumber,
            IsActive = source.ActiveFlag,
            IsPreferred = source.PreferredVendorStatus,
            CreditRating = DataCompletionSource.Rating(source.CreditRating),
            PurchasingWebServiceUrl = source.PurchasingWebServiceUrl,
            ModifiedDate = source.ModifiedDate
        };
        public static readonly Func<Vendor, VendorDTO> ConvertToVendorDto = AsVendorDto.Compile();

        public static readonly Expression<Func<Vendor, VendorDetailDTO>> AsVendorDetailDto = source =>
        new VendorDetailDTO()
        {
            VendorId = source.BusinessEntityId,
            Name = source.Name,
            AccountNumber = source.AccountNumber,
            IsActive = source.ActiveFlag,
            IsPreferred = source.PreferredVendorStatus,
            CreditRating = DataCompletionSource.Rating(source.CreditRating),
            PurchasingWebServiceUrl = source.PurchasingWebServiceUrl,
            ModifiedDate = source.ModifiedDate,
            BusinessEntity = PersonDtoConverter.ConvertToBusinessEntityDetailDto(source.BusinessEntity),
            ProductVendors = source.ProductVendors.Select(x=>ConvertToProductVendorDto(x)).ToList(),
            PurchaseOrders = source.PurchaseOrderHeaders.Select(x=>ConvertToPurchaseOrderHeaderDto(x)).ToList()
        };
        public static readonly Func<Vendor, VendorDetailDTO> ConvertToVendorDetailDto = AsVendorDetailDto.Compile();
    }
}
