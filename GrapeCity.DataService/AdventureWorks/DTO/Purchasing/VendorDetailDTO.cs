﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DTO
{
    public class VendorDetailDTO : VendorDTO
    {
        public BusinessEntityDetailDTO BusinessEntity { get; set; }
        public ICollection<PurchaseOrderHeaderDTO> PurchaseOrders { get; set; }
        public ICollection<ProductVendorDTO> ProductVendors { get; set; }
    }
}
