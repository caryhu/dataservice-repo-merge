﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{

    public class EmployeePayHistoriesController : ODataController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeePayHistoriesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [EnableQuery]
        public IQueryable<EmployeePayHistory> Get()
        {
            return _employeeService.GetEmployeePayHistories();
        }

        [EnableQuery]
        public SingleResult<EmployeePayHistory> Get([FromODataUri] int keyBusinessEntityId, DateTime keyRateChangeDate)
        {
            return SingleResult.Create(_employeeService.GetEmployeePayHistories(keyBusinessEntityId, keyRateChangeDate));
        }

        [EnableQuery]
        public SingleResult<Employee> GetBusinessEntity([FromODataUri] int keyBusinessEntityId, DateTime keyRateChangeDate)
        {
            return SingleResult.Create(_employeeService.GetEmployeeFromPayHistories(keyBusinessEntityId, keyRateChangeDate));
        }
    }
}
