﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class JobCandidatesController : ODataController
    {
        private readonly IJobCandidateService _candidateService;

        public JobCandidatesController(IJobCandidateService candidateService)
        {
            _candidateService = candidateService;
        }

        [EnableQuery]
        public IQueryable<JobCandidate> Get()
        {
            return _candidateService.GetJobCandidates();
        }

        [EnableQuery]
        public SingleResult<JobCandidate> Get([FromODataUri] int key)
        {
           return SingleResult.Create(_candidateService.GetJobCandidate(key));
        }

        [EnableQuery]
        public SingleResult<Employee> GetBusinessEntity([FromODataUri] int key)
        {
            return SingleResult.Create(_candidateService.GetEmployee(key));
        }
    }
}
