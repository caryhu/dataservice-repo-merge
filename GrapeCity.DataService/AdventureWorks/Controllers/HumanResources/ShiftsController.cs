﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class ShiftsController : ODataController
    {
        private readonly IShiftService _shiftService;

        public ShiftsController(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        [EnableQuery]
        public IQueryable<Shift> Get()
        {
            return _shiftService.GetShifts();
        }

        [EnableQuery]
        public SingleResult<Shift> Get([FromODataUri] byte key)
        {
            return SingleResult.Create(_shiftService.GetShift(key));
        }

        [EnableQuery]
        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories([FromODataUri] byte key)
        {
            return _shiftService.GetEmployeeDepartmentHistories(key);
        }
    }
}
