﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    public class DepartmentsController : ODataController
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentsController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [EnableQuery]
        public IQueryable<Department> Get()
        {
            return _departmentService.GetDepartments();
        }

        [EnableQuery]
        public SingleResult<Department> Get([FromODataUri] short key)
        {
            return SingleResult.Create(_departmentService.GetDepartment(key));
        }

        [EnableQuery]
        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories([FromODataUri] short key)
        {
            return _departmentService.GetEmployeeDepartmentHistories(key);
        }
    }
}