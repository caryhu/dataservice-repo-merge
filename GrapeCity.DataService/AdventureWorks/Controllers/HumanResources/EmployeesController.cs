﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    public class EmployeesController : ODataController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [EnableQuery]
        public IQueryable<Employee> Get()
        {
            return _employeeService.GetEmployees();
        }

        [EnableQuery]
        public SingleResult<Employee> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_employeeService.GetEmployee(key));
        }

        [EnableQuery]
        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories([FromODataUri] int key)
        {
            return _employeeService.GetEmployeeDepartmentHistories(key);
        }

        [EnableQuery]
        public IQueryable<EmployeePayHistory> GetEmployeePayHistories([FromODataUri] int key)
        {
            return _employeeService.GetEmployeePayHistories(key);
        }

        [EnableQuery]
        public IQueryable<JobCandidate> GetJobCandidates([FromODataUri] int key)
        {
            return _employeeService.GetEmployeeJobApplications(key);
        }

        [EnableQuery]
        public SingleResult<Models.Person> GetBusinessEntity([FromODataUri] int key)
        {
            return SingleResult.Create(_employeeService.GetEmployeePersonalDetails(key));
        }

        [EnableQuery]
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrderHeaders([FromODataUri] int key)
        {
            return _employeeService.GetEmployeePurchaseOrders(key);
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesPerson([FromODataUri] int key)
        {
            return _employeeService.GetSales(key);
        }
    }
}
