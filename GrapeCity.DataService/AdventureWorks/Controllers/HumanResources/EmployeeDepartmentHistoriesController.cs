﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    public class EmployeeDepartmentHistoriesController : ODataController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeDepartmentHistoriesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [EnableQuery]
        public IQueryable<EmployeeDepartmentHistory> Get()
        {
            return _employeeService.GetEmployeeDepartmentHistories();
        }

        [EnableQuery]
        public SingleResult<EmployeeDepartmentHistory> Get([FromODataUri] int keyBusinessEntityId, [FromODataUri] short keyDepartmentId, [FromODataUri] byte keyShiftId, DateTime keyStartDate)
        {
            return SingleResult.Create(_employeeService.GetEmployeeDepartmentHistory(keyBusinessEntityId, keyDepartmentId, keyShiftId, keyStartDate));
        }

        [EnableQuery]
        public SingleResult<Department> GetDepartment([FromODataUri] int keyBusinessEntityId, [FromODataUri] short keyDepartmentId, [FromODataUri] byte keyShiftId, DateTime keyStartDate)
        {
            return SingleResult.Create(_employeeService.GetDepartmentFromDepartmentHistories(keyBusinessEntityId, keyDepartmentId, keyShiftId, keyStartDate));
        }

        [EnableQuery]
        public SingleResult<Shift> GetShift([FromODataUri] int keyBusinessEntityId, [FromODataUri] short keyDepartmentId, [FromODataUri] byte keyShiftId, DateTime keyStartDate)
        {
            return SingleResult.Create(_employeeService.GetShiftFromDepartmentHistories(keyBusinessEntityId, keyDepartmentId, keyShiftId, keyStartDate));
        }

        [EnableQuery]
        public SingleResult<Employee> GetBusinessEntity([FromODataUri] int keyBusinessEntityId, [FromODataUri] short keyDepartmentId, [FromODataUri] byte keyShiftId, DateTime keyStartDate)
        {
            return SingleResult.Create(_employeeService.GetEmployeeFromDepartmentHistories(keyBusinessEntityId, keyDepartmentId, keyShiftId, keyStartDate));
        }
    }
}
