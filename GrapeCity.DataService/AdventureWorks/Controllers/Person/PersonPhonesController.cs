﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class PersonPhonesController : ODataController
    {
        private readonly IPersonPhoneService _personPhoneService;

        public PersonPhonesController(IPersonPhoneService personPhoneService)
        {
            _personPhoneService = personPhoneService;
        }

        [EnableQuery]
        public SingleResult<Person> GetPerson([FromODataUri] int keyBusinessEntityId, string keyPhoneNumber, [FromODataUri] int keyPhoneNumberTypeId)
        {
            return SingleResult.Create(_personPhoneService.GetPerson(keyBusinessEntityId, keyPhoneNumber, keyPhoneNumberTypeId));
        }

        [EnableQuery]
        public IQueryable<PersonPhone> Get()
        {
            return _personPhoneService.GetPhoneNumbers();
        }

        [EnableQuery]
        public SingleResult<PhoneNumberType> GetPhoneNumberType([FromODataUri] int keyBusinessEntityId, string keyPhoneNumber, [FromODataUri] int keyPhoneNumberTypeId)
        {
            return SingleResult.Create(_personPhoneService.GetPhoneNumberType(keyBusinessEntityId, keyPhoneNumber, keyPhoneNumberTypeId));
        }
    }
}
