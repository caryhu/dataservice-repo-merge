﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class PhoneNumberTypesController : ODataController
    {
        private readonly IPersonPhoneService _personPhoneService;

        public PhoneNumberTypesController(IPersonPhoneService personPhoneService)
        {
            _personPhoneService = personPhoneService;
        }

        [EnableQuery]
        public SingleResult<PhoneNumberType> Get([FromODataUri] short key)
        {
            return SingleResult.Create(_personPhoneService.GetPhoneNumberType(key));
        }

        [EnableQuery]
        public IQueryable<PhoneNumberType> Get()
        {
            return _personPhoneService.GetPhoneNumberTypes();
        }

        [EnableQuery]
        public IQueryable<PersonPhone> GetPersonPhones([FromODataUri] short key)
        {
            return _personPhoneService.GetPersonPhones(key);
        }
    }
}
