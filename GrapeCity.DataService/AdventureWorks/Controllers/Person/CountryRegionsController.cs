﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class CountryRegionsController : ODataController
    {
        private readonly ICountryRegionService _countryRegionService;

        public CountryRegionsController(ICountryRegionService countryRegionService)
        {
            _countryRegionService = countryRegionService;
        }

        [EnableQuery]
        public IQueryable<CountryRegion> Get()
        {
            return _countryRegionService.GetCountries();
        }

        [EnableQuery]
        public SingleResult<CountryRegion> Get([FromODataUri] string key)
        {
            return SingleResult.Create(_countryRegionService.GetCountryDetails(key));
        }

        [EnableQuery]
        public SingleResult<string> GetName([FromODataUri] string key)
        {
            return SingleResult.Create(_countryRegionService.GetCountryName(key));
        }

        [EnableQuery]
        public IQueryable<Currency> GetCountryRegionCurrencies([FromODataUri] string key)
        {
            return _countryRegionService.GetCurrencies(key);
        }

        [EnableQuery]
        public IQueryable<SalesTerritory> GetSalesTerritories([FromODataUri] string key)
        {
            return _countryRegionService.GetSalesTerritories(key);
        }

        [EnableQuery]
        public IQueryable<StateProvince> GetStateProvinces([FromODataUri] string key)
        {
            return _countryRegionService.GetStateProvinces(key);
        }
    }
}
