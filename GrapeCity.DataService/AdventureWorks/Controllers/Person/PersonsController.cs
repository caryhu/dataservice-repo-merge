﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class PersonsController : ODataController
    {
        private readonly IPersonService _personService;

        public PersonsController(IPersonService personService)
        {
            _personService = personService;
        }

        [EnableQuery]
        public IQueryable<BusinessEntityContact> GetBusinessEntityContacts([FromODataUri] int key)
        {
            return _personService.GetBusinessEntityContacts(key);
        }

        [EnableQuery]
        public IQueryable<CreditCard> GetPersonCreditCards([FromODataUri] int key)
        {
            return _personService.GetCreditCards(key);
        }

        [EnableQuery]
        public IQueryable<Customer> GetCustomers([FromODataUri] int key)
        {
            return _personService.GetCustomers(key);
        }

        [EnableQuery]
        public IQueryable<Dictionary<string, string>> GetDemographics([FromODataUri] int key)
        {
            return _personService.GetDemographicsData(key);
        }

        [EnableQuery]
        public IQueryable<EmailAddress> GetEmailAddresses([FromODataUri] int key)
        {
            return _personService.GetEmails(key);
        }

        [EnableQuery]
        public SingleResult<Employee> GetEmployee([FromODataUri] int key)
        {
            return SingleResult.Create(_personService.GetEmployee(key));
        }

        [EnableQuery]
        public SingleResult<Password> GetPassword([FromODataUri] int key)
        {
            return SingleResult.Create(_personService.GetPassword(key));
        }

        [EnableQuery]
        public SingleResult<Person> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_personService.GetPerson(key));
        }

        [EnableQuery]
        public IQueryable<Person> Get()
        {
            return _personService.GetPersons();
        }

        [EnableQuery]
        public IQueryable<PersonPhone> GetPersonPhones([FromODataUri] int key)
        {
            return _personService.GetPhones(key);
        }
    }
}
