﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class BusinessEntitiesController : ODataController
    {
        private readonly IBusinessEntityService _businessEntityService;

        public BusinessEntitiesController(IBusinessEntityService businessEntityService)
        {
            _businessEntityService = businessEntityService;
        }

        [EnableQuery]
        public IQueryable<BusinessEntityAddress> GetBusinessEntityAddresses([FromODataUri] int key)
        {
            return _businessEntityService.GetAddresses(key);
        }

        [EnableQuery]
        public IQueryable<BusinessEntityContact> GetBusinessEntityContacts([FromODataUri] int key)
        {
            return _businessEntityService.GetContacts(key);
        }

        [EnableQuery]
        public SingleResult<Models.Person> GetPerson([FromODataUri] int key)
        {
            return SingleResult.Create(_businessEntityService.GetPerson(key));
        }

        [EnableQuery]
        public SingleResult<Store> GetStore([FromODataUri] int key)
        {
            return SingleResult.Create(_businessEntityService.GetStore(key));
        }

        [EnableQuery]
        public SingleResult<Vendor> GetVendor([FromODataUri] int key)
        {
            return SingleResult.Create(_businessEntityService.GetVendor(key));
        }
    }
}
