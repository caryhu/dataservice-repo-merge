﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class BusinessEntityContactsController : ODataController
    {
        private readonly IBusinessEntityService _businessEntityService;

        public BusinessEntityContactsController(IBusinessEntityService businessEntityService)
        {
            _businessEntityService = businessEntityService;
        }

        [EnableQuery]
        public IQueryable<BusinessEntityContact> Get()
        {
            return _businessEntityService.GetContacts();
        }
    }
}
