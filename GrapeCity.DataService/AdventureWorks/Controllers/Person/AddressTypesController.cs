﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class AddressTypesController : ODataController
    {
        private readonly IAddressTypeService _addressTypeService;

        public AddressTypesController(IAddressTypeService addressTypeService)
        {
            _addressTypeService = addressTypeService;
        }

        [EnableQuery]
        public IQueryable<AddressType> Get()
        {
            return _addressTypeService.GetAddressTypes();
        }

        [EnableQuery]
        public SingleResult<AddressType> Get([FromODataUri] short key)
        {
            return SingleResult.Create(_addressTypeService.GetAddressType(key));
        }

        [EnableQuery]
        public IQueryable<BusinessEntityAddress> GetBusinessEntityAddresses([FromODataUri] short key)
        {
            return _addressTypeService.GetBusinessEntityAddresses(key);
        }
    }
}
