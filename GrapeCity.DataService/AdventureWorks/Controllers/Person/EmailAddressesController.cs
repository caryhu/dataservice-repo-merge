﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class EmailAddressesController : ODataController
    {
        private readonly IEmailAddressService _emailAddressService;

        public EmailAddressesController(IEmailAddressService emailAddressService)
        {
            _emailAddressService = emailAddressService;
        }

        [EnableQuery]
        public IQueryable<EmailAddress> Get()
        {
            return _emailAddressService.GetEmailAddresses();
        }

        [EnableQuery]
        public SingleResult<Person> GetPerson([FromODataUri] int keyBusinessEntityId, [FromODataUri] int keyEmailAddressId)
        {
            return SingleResult.Create(_emailAddressService.GetPerson(keyBusinessEntityId, keyEmailAddressId));
        }
    }
}
