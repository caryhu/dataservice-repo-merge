﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class AddressesController : ODataController
    {
        private readonly IAddressService _addressService;

        public AddressesController(IAddressService addressService)
        {
            _addressService = addressService;
        }

        [EnableQuery]
        public SingleResult<Address> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_addressService.GetAddress(key));
        }

        [EnableQuery]
        public IQueryable<Address> Get()
        {
            return _addressService.GetAddresses();
        }

        [EnableQuery]
        public IQueryable<BusinessEntityAddress> GetBusinessEntityAddresses([FromODataUri] int key)
        {
            return _addressService.GetBusinessEntityAddresses(key);
        }

        [EnableQuery]
        public IQueryable<StateProvince> GetStateProvince([FromODataUri] int key)
        {
            return _addressService.GetProvince(key);
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrdersBilledTo([FromODataUri] int key)
        {
            return _addressService.GetSalesOrdersBilledToAddress(key);
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrdersShippedTo([FromODataUri] int key)
        {
            return _addressService.GetSalesOrdersShippedToAddress(key);
        }
    }
}
