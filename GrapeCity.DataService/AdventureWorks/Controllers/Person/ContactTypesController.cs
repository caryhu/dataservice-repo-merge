﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class ContactTypesController : ODataController
    {
        private readonly IContactTypeService _contactTypeService;

        public ContactTypesController(IContactTypeService contactTypeService)
        {
            _contactTypeService = contactTypeService;
        }

        [EnableQuery]
        public IQueryable<ContactType> Get()
        {
            return _contactTypeService.GetContactTypes();
        }

        [EnableQuery]
        public SingleResult<ContactType> Get([FromODataUri] short key)
        {
            return SingleResult.Create(_contactTypeService.GetContactType(key));
        }

        [EnableQuery]
        public IQueryable<BusinessEntityContact> GetBusinessEntityContacts([FromODataUri] short key)
        {
            return _contactTypeService.GetBusinessEntityContacts(key);
        }
    }
}
