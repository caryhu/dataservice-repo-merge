﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class StateProvincesController : ODataController
    {
        private readonly IStateProvinceService _stateProvinceService;

        public StateProvincesController(IStateProvinceService stateProvinceService)
        {
            _stateProvinceService = stateProvinceService;
        }

        [EnableQuery]
        public IQueryable<Address> GetAddresses([FromODataUri] int key)
        {
            return _stateProvinceService.GetAddresses(key);
        }

        [EnableQuery]
        public SingleResult<CountryRegion> GetCountry([FromODataUri] int key)
        {
            return SingleResult.Create(_stateProvinceService.GetCountry(key));
        }

        [EnableQuery]
        public SingleResult<StateProvince> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_stateProvinceService.GetStateProvince(key));
        }

        [EnableQuery]
        public IQueryable<StateProvince> Get()
        {
            return _stateProvinceService.GetStateProvinces();
        }

        [EnableQuery]
        public SingleResult<SalesTerritory> GetTerritory([FromODataUri] int key)
        {
            return SingleResult.Create(_stateProvinceService.GetTerritory(key));
        }
    }
}
