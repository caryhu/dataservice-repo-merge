﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class BillOfMaterialsController : ODataController
    {
        private readonly IBillOfMaterialService _billOfMaterialService;

        public BillOfMaterialsController(IBillOfMaterialService billOfMaterialService)
        {
            _billOfMaterialService = billOfMaterialService;
        }

        [EnableQuery]
        public IQueryable<BillOfMaterial> Get()
        {
            return _billOfMaterialService.GetBillOfMaterials();
        }

        [EnableQuery]
        public SingleResult<BillOfMaterial> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_billOfMaterialService.GetBillOfMaterial(key));
        }

        [EnableQuery]
        public SingleResult<Product> GetProductAssembly([FromODataUri] int key)
        {
            return SingleResult.Create(_billOfMaterialService.GetParentProduct(key));
        }

        [EnableQuery]
        public SingleResult<Product> GetComponent([FromODataUri] int key)
        {
            return SingleResult.Create(_billOfMaterialService.GetComponent(key));
        }

        [EnableQuery]
        public SingleResult<UnitMeasure> GetQuantityUnit([FromODataUri] int key)
        {
            return SingleResult.Create(_billOfMaterialService.GetUnitMeasure(key));
        }
    }
}
