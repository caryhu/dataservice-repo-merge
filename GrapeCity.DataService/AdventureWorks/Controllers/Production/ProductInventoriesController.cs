﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductInventoriesController : ODataController
    {
        private readonly IProductInventoryService _productInventoryService;

        public ProductInventoriesController(IProductInventoryService productInventoryService)
        {
            _productInventoryService = productInventoryService;
        }

        [EnableQuery]
        public SingleResult<Location> GetLocation([FromODataUri] short keyLocationId, [FromODataUri] int keyProductId)
        {
            return SingleResult.Create(_productInventoryService.GetLocation(keyProductId, keyLocationId));
        }

        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] short keyLocationId, [FromODataUri] int keyProductId)
        {
            return SingleResult.Create(_productInventoryService.GetProduct(keyProductId, keyLocationId));
        }

        [EnableQuery]
        public SingleResult<ProductInventory> Get([FromODataUri] short? keyLocationId, [FromODataUri] int? keyProductId)
        {
            return SingleResult.Create(_productInventoryService.GetProductInventories(keyProductId, keyLocationId));
        }

        [EnableQuery]
        public IQueryable<ProductInventory> Get()
        {
            return _productInventoryService.GetProductInventories(null, null);
        }
    }
}
