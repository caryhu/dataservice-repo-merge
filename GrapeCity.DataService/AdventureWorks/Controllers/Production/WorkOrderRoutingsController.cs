﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class WorkOrderRoutingsController : ODataController
    {
        private readonly IWorkOrderRoutingService _workOrderRoutingService;

        public WorkOrderRoutingsController(IWorkOrderRoutingService workOrderRoutingService)
        {
            _workOrderRoutingService = workOrderRoutingService;
        }

        [EnableQuery]
        public SingleResult<Location> GetLocation([FromODataUri] short keyOperationSequence, [FromODataUri] int keyProductId, [FromODataUri] int keyWorkOrderId)
        {
            return SingleResult.Create(_workOrderRoutingService.GetLocation(keyWorkOrderId, keyProductId, keyOperationSequence));
        }

        [EnableQuery]
        public SingleResult<WorkOrder> GetWorkOrder([FromODataUri] short keyOperationSequence, [FromODataUri] int keyProductId, [FromODataUri] int keyWorkOrderId)
        {
            return SingleResult.Create(_workOrderRoutingService.GetWorkOrder(keyWorkOrderId, keyProductId, keyOperationSequence));
        }

        [EnableQuery]
        public SingleResult<WorkOrderRouting> Get([FromODataUri] short keyOperationSequence, [FromODataUri] int keyProductId, [FromODataUri] int keyWorkOrderId)
        {
            return SingleResult.Create(_workOrderRoutingService.GetWorkOrderRoutings(keyWorkOrderId, keyProductId, keyOperationSequence));
        }

        [EnableQuery]
        public IQueryable<WorkOrderRouting> Get()
        {
            return _workOrderRoutingService.GetWorkOrderRoutings(null, null, null);
        }
    }
}
