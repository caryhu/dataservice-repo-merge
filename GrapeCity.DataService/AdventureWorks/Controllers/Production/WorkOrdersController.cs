﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class WorkOrdersController : ODataController
    {
        private readonly IWorkOrderService _workOrderService;

        public WorkOrdersController(IWorkOrderService workOrderService)
        {
            _workOrderService = workOrderService;
        }

        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] int key)
        {
            return SingleResult.Create(_workOrderService.GetProduct(key));
        }

        [EnableQuery]
        public SingleResult<ScrapReason> GetScrapReason([FromODataUri] int key)
        {
            return SingleResult.Create(_workOrderService.GetScrapReason(key));
        }

        [EnableQuery]
        public SingleResult<WorkOrder> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_workOrderService.GetWorkOrder(key));
        }

        [EnableQuery]
        public IQueryable<WorkOrderRouting> GetWorkOrderRoutings([FromODataUri] int key)
        {
            return _workOrderService.GetWorkOrderRoutings(key);
        }

        [EnableQuery]
        public IQueryable<WorkOrder> Get()
        {
            return _workOrderService.GetWorkOrders();
        }
    }
}
