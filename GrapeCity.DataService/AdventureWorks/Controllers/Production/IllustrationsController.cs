﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class IllustrationsController : ODataController
    {
        private readonly IIllustrationService _illustrationService;

        public IllustrationsController(IIllustrationService illustrationService)
        {
            _illustrationService = illustrationService;
        }

        [EnableQuery]
        public IQueryable<Illustration> Get()
        {
            return _illustrationService.GetIllustrations();
        }

        [EnableQuery]
        public SingleResult<Illustration> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_illustrationService.GetIllustration(key));
        }

        [EnableQuery]
        public IQueryable<ProductModelIllustration> GetProductModelIllustrations([FromODataUri] int key)
        {
            return _illustrationService.GetProductModelIllustrations(key);
        }
    }
}
