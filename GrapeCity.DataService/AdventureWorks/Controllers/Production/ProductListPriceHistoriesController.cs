﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductListPriceHistoriesController : ODataController
    {
        private readonly IProductListPriceHistoryService _productListPriceHistoryService;

        public ProductListPriceHistoriesController(IProductListPriceHistoryService productListPriceHistoryService)
        {
            _productListPriceHistoryService = productListPriceHistoryService;
        }

        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] int keyProductId, DateTime keyStartDate)
        {
            return SingleResult.Create(_productListPriceHistoryService.GetProduct(keyProductId, keyStartDate));
        }

        [EnableQuery]
        public IQueryable<ProductListPriceHistory> Get()
        {
            return _productListPriceHistoryService.GetProductListPriceHistories();
        }

        [EnableQuery]
        public IQueryable<ProductListPriceHistory> Get([FromODataUri] int keyProductId, DateTime keyStartDate)
        {
            return _productListPriceHistoryService.GetProductListPriceHistories(keyProductId, keyStartDate);
        }
    }
}
