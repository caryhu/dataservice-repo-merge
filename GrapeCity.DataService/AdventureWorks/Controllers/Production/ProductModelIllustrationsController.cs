﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductModelIllustrationsController : ODataController
    {
        private readonly IProductModelIllustrationService _productModelIllustrationService;

        public ProductModelIllustrationsController(IProductModelIllustrationService productModelIllustrationService)
        {
            _productModelIllustrationService = productModelIllustrationService;
        }

        [EnableQuery]
        public SingleResult<Illustration> GetIllustration([FromODataUri] int keyIllustrationId,[FromODataUri] int keyProductModelId)
        {
            return SingleResult.Create(_productModelIllustrationService.GetIllustration(keyProductModelId, keyIllustrationId));
        }

        [EnableQuery]
        public SingleResult<ProductModel> GetProductModel([FromODataUri] int keyIllustrationId, [FromODataUri] int keyProductModelId)
        {
            return SingleResult.Create(_productModelIllustrationService.GetProductModel(keyProductModelId, keyIllustrationId));
        }

        [EnableQuery]
        public IQueryable<ProductModelIllustration> Get()
        {
            return _productModelIllustrationService.GetProductModelIllustrations();
        }
    }
}
