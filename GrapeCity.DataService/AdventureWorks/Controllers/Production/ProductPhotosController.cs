﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductPhotosController : ODataController
    {
        private readonly IProductPhotoService _productPhotoService;

        public ProductPhotosController(IProductPhotoService productPhotoService)
        {
            _productPhotoService = productPhotoService;
        }

        [EnableQuery]
        public SingleResult<ProductPhoto> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_productPhotoService.GetProductPhoto(key));
        }

        [EnableQuery]
        public IQueryable<ProductPhoto> Get()
        {
            return _productPhotoService.GetProductPhotos();
        }
    }
}
