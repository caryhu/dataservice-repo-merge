﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class UnitMeasuresController : ODataController
    {
        private readonly IUnitMeasureService _unitMeasureService;

        public UnitMeasuresController(IUnitMeasureService unitMeasureService)
        {
            _unitMeasureService = unitMeasureService;
        }

        [EnableQuery]
        public IQueryable<BillOfMaterial> GetBillOfMaterials([FromODataUri] string key)
        {
            return _unitMeasureService.GetBillOfMaterials(key);
        }

        [EnableQuery]
        public IQueryable<ProductVendor> GetProductVendors([FromODataUri] string key)
        {
            return _unitMeasureService.GetProductVendors(key);
        }

        [EnableQuery]
        public SingleResult<UnitMeasure> Get([FromODataUri] string key)
        {
            return SingleResult.Create(_unitMeasureService.GetUnitMeasure(key));
        }

        [EnableQuery]
        public IQueryable<UnitMeasure> Get()
        {
            return _unitMeasureService.GetUnitMeasures();
        }
    }
}
