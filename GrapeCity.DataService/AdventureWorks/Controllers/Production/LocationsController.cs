﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class LocationsController : ODataController
    {
        private readonly ILocationService _locationService;

        public LocationsController(ILocationService locationService)
        {
            _locationService = locationService;
        }

        [EnableQuery]
        public IQueryable<Location> Get()
        {
            return _locationService.GetLocations();
        }

        [EnableQuery]
        public SingleResult<Location> Get([FromODataUri] short key)
        {
            return SingleResult.Create(_locationService.GetLocation(key));
        }

        [EnableQuery]
        public IQueryable<ProductInventory> GetProductInventories([FromODataUri] short key)
        {
            return _locationService.GetProductInventories(key);
        }

        [EnableQuery]
        public IQueryable<WorkOrderRouting> GetWorkOrderRoutings([FromODataUri] short key)
        {
            return _locationService.GetWorkOrderRoutings(key);
        }
    }
}
