﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductCategoriesController : ODataController
    {
        private readonly IProductCategoryService _productCategoryService;

        public ProductCategoriesController(IProductCategoryService productCategoryService)
        {
            _productCategoryService = productCategoryService;
        }

        [EnableQuery]
        public IQueryable<ProductCategory> Get()
        {
            return _productCategoryService.GetProductCategories();
        }

        [EnableQuery]
        public SingleResult<ProductCategory> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_productCategoryService.GetProductCategory(key));
        }

        [EnableQuery]
        public IQueryable<ProductSubcategory> GetProductSubcategories([FromODataUri] int key)
        {
            return _productCategoryService.GetProductSubcategories(key);
        }
    }
}
