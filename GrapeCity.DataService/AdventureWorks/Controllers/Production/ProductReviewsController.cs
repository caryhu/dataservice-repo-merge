﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductReviewsController : ODataController
    {
        private readonly IProductReviewService _productReviewService;

        public ProductReviewsController(IProductReviewService productReviewService) 
        {
            _productReviewService = productReviewService;
        }

        [EnableQuery]
        public IQueryable<ProductReview> Get()
        {
            return _productReviewService.GetProductReviews();
        }

        [EnableQuery]
        public SingleResult<ProductReview> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_productReviewService.GetProductReview(key));
        }

        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] int key)
        {
            return SingleResult.Create(_productReviewService.GetProduct(key));
        }
    }
}
