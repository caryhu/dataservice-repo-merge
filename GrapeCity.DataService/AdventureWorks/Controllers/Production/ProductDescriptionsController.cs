﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductDescriptionsController : ODataController
    {
        private readonly IProductDescriptionService _productDescriptionService;

        public ProductDescriptionsController(IProductDescriptionService productDescriptionService)
        {
            _productDescriptionService = productDescriptionService;
        }

        [EnableQuery]
        public SingleResult<ProductDescription> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_productDescriptionService.GetProductDescription(key));
        }

        [EnableQuery]
        public IQueryable<ProductDescription> Get()
        {
            return _productDescriptionService.GetProductDescriptions();
        }
    }
}