﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductSubcategoriesController : ODataController
    {
        private readonly IProductSubcategoryService _productSubcategoryService;

        public ProductSubcategoriesController(IProductSubcategoryService productSubcategoryService)
        {
            _productSubcategoryService = productSubcategoryService;
        }

        [EnableQuery]
        public SingleResult<ProductCategory> GetProductCategory([FromODataUri] int key)
        {
            return SingleResult.Create(_productSubcategoryService.GetCategory(key));
        }

        [EnableQuery]
        public IQueryable<Product> GetProducts([FromODataUri] int key)
        {
            return _productSubcategoryService.GetProducts(key);
        }

        [EnableQuery]
        public IQueryable<ProductSubcategory> Get()
        {
            return _productSubcategoryService.GetProductSubcategories();
        }

        [EnableQuery]
        public SingleResult<ProductSubcategory> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_productSubcategoryService.GetProductSubcategory(key));
        }
    }
}
