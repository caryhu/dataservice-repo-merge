﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductCostHistoriesController : ODataController
    {
        private readonly IProductCostHistoryService _productCostHistoryService;

        public ProductCostHistoriesController(IProductCostHistoryService productCostHistoryService)
        {
            _productCostHistoryService = productCostHistoryService;
        }

        [EnableQuery]
        public IQueryable<ProductCostHistory> Get()
        {
            return _productCostHistoryService.GetProductCostHistories();
        }

        [EnableQuery]
        public IQueryable<ProductCostHistory> Get([FromODataUri] int keyProductId, DateTime keyStartDate)
        {
            return _productCostHistoryService.GetProductCostHistories(keyProductId, keyStartDate);
        }

        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] int keyProductId, DateTime keyStartDate)
        {
            return SingleResult.Create(_productCostHistoryService.GetProduct(keyProductId, keyStartDate));
        }
    }
}
