﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class TransactionHistoryArchivesController : ODataController
    {
        private readonly ITransactionHistoryArchiveService _transactionHistoryArchiveService;

        public TransactionHistoryArchivesController(ITransactionHistoryArchiveService transactionHistoryArchiveService)
        {
            _transactionHistoryArchiveService = transactionHistoryArchiveService;
        }

        [EnableQuery]
        public SingleResult<TransactionHistoryArchive> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_transactionHistoryArchiveService.GetTransactionHistoryArchive(key));
        }

        [EnableQuery]
        public IQueryable<TransactionHistoryArchive> Get()
        {
            return _transactionHistoryArchiveService.GetTransactionHistoryArchives();
        }
    }
}
