﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductModelsController : ODataController
    {
        private readonly IProductModelService _productModelService;

        public ProductModelsController(IProductModelService productModelService)
        {
            _productModelService = productModelService;
        }

        [EnableQuery]
        public SingleResult<ProductModel> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_productModelService.GetProductModel(key));
        }

        [EnableQuery]
        public IQueryable<ProductModel> Get()
        {
            return _productModelService.GetProductModels();
        }
    }
}
