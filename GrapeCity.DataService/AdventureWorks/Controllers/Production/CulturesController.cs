﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class CulturesController : ODataController
    {
        private readonly ICultureService _cultureService;

        public CulturesController(ICultureService cultureService)
        {
            _cultureService = cultureService;
        }

        [EnableQuery]
        public IQueryable<Culture> Get()
        {
            return _cultureService.GetCultures();
        }

        [EnableQuery]
        public SingleResult<Culture> Get([FromODataUri] string key)
        {
            return SingleResult.Create(_cultureService.GetCulture(key));
        }

        [EnableQuery]
        public IQueryable<ProductDescription> GetProductModelProductDescriptionCultures([FromODataUri] string key)
        {
            return _cultureService.GetProductModelDescriptions(key);
        }
    }
}
