﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ScrapReasonsController : ODataController
    {
        private readonly IScrapReasonService _scrapReasonService;

        public ScrapReasonsController(IScrapReasonService scrapReasonService)
        {
            _scrapReasonService = scrapReasonService;
        }

        [EnableQuery]
        public SingleResult<ScrapReason> Get([FromODataUri] short key)
        {
            return SingleResult.Create(_scrapReasonService.GetScrapReason(key));
        }

        [EnableQuery]
        public IQueryable<ScrapReason> Get()
        {
            return _scrapReasonService.GetScrapReasons();
        }

        [EnableQuery]
        public IQueryable<WorkOrder> GetWorkOrders([FromODataUri] short key)
        {
            return _scrapReasonService.GetWorkOrders(key);
        }
    }
}
