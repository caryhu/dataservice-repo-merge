﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ProductsController : ODataController
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [EnableQuery]
        public SingleResult<Product> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_productService.GetProduct(key));
        }

        [EnableQuery]
        public IQueryable<ProductCostHistory> GetProductCostHistories(int key)
        {
            return _productService.GetProductCostHistories(key);
        }

        [EnableQuery]
        public IQueryable<ProductInventory> GetProductInventories(int key)
        {
            return _productService.GetProductInventories(key);
        }

        [EnableQuery]
        public SingleResult<ProductModel> GetProductModel(int key)
        {
            return SingleResult.Create(_productService.GetProductModel(key));
        }

        [EnableQuery]
        public IQueryable<ProductListPriceHistory> GetProductListPriceHistories(int key)
        {
            return _productService.GetProductPriceHistories(key);
        }

        [EnableQuery]
        public IQueryable<ProductReview> GetProductReviews(int key)
        {
            return _productService.GetProductReviews(key);
        }

        [EnableQuery]
        public IQueryable<Product> Get()
        {
            return _productService.GetProducts();
        }

        [EnableQuery]
        public SingleResult<UnitMeasure> GetUnitOfSize(int key)
        {
            return SingleResult.Create(_productService.GetProductSizeUnit(key));
        }

        [EnableQuery]
        public SingleResult<ProductSubcategory> GetProductSubcategory(int key)
        {
            return SingleResult.Create(_productService.GetProductSubcategory(key));
        }

        [EnableQuery]
        public IQueryable<TransactionHistory> GetTransactionHistories(int key)
        {
            return _productService.GetProductTransactions(key);
        }

        [EnableQuery]
        public IQueryable<ProductVendor> GetProductVendors(int key)
        {
            return _productService.GetProductVendors(key);
        }

        [EnableQuery]
        public SingleResult<UnitMeasure> GetUnitOfWeight(int key)
        {
            return SingleResult.Create(_productService.GetProductWeightUnit(key));
        }

        [EnableQuery]
        public IQueryable<WorkOrder> GetWorkOrders(int key)
        {
            return _productService.GetWorkOrders(key);
        }
    }
}
