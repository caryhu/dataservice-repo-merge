﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class ProductVendorsController : ODataController
    {
        IProductVendorService _productVendorService;
        public ProductVendorsController(IProductVendorService productVendorService)
        {
            _productVendorService = productVendorService;
        }

        [EnableQuery]
        public IQueryable<ProductVendor> Get()
        {
            return _productVendorService.GetProductVendors(null, null);
        }

        [EnableQuery]
        public SingleResult<ProductVendor> Get([FromODataUri] int keyBusinessEntityId, [FromODataUri] int keyProductId)
        {
            return SingleResult.Create(_productVendorService.GetProductVendors(keyProductId, keyBusinessEntityId));
        }


        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] int keyBusinessEntityId, [FromODataUri] int keyProductId)
        {
            return SingleResult.Create(_productVendorService.GetProduct(keyProductId, keyBusinessEntityId));
        }

        [EnableQuery]
        public SingleResult<Vendor> GetBusinessEntity([FromODataUri] int keyBusinessEntityId, [FromODataUri] int keyProductId)
        {
            return SingleResult.Create(_productVendorService.GetVendor(keyProductId, keyBusinessEntityId));
        }

        [EnableQuery]
        public SingleResult<UnitMeasure> GetUnitOfMeasure([FromODataUri] int keyBusinessEntityId, [FromODataUri] int keyProductId)
        {
            return SingleResult.Create(_productVendorService.GetProductVendorUnitMeasure(keyProductId, keyBusinessEntityId));
        }
    }
}