﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class PurchaseOrderDetailsController : ODataController
    {
        IPurchaseOrderDetailService _purchaseOrderService;
        public PurchaseOrderDetailsController(IPurchaseOrderDetailService purchaseOrderService)
        {
            _purchaseOrderService = purchaseOrderService;
        }

        [EnableQuery]
        public IQueryable<PurchaseOrderDetail> Get()
        {
            return _purchaseOrderService.GetPurchaseOrderDetails();
        }

        [EnableQuery]
        public SingleResult<PurchaseOrderDetail> Get(int keyPurchaseOrderDetailId, int keyPurchaseOrderId)
        {
            return SingleResult.Create(_purchaseOrderService.GetPurchaseOrderDetail(keyPurchaseOrderId, keyPurchaseOrderDetailId));
        }

        [EnableQuery]
        public SingleResult<PurchaseOrderHeader> GetPurchaseOrder(int keyPurchaseOrderDetailId, int keyPurchaseOrderId)
        {
            return SingleResult.Create(_purchaseOrderService.GetPurchaseOrder(keyPurchaseOrderId, keyPurchaseOrderDetailId));
        }

        [EnableQuery]
        public SingleResult<Product> GetProduct(int keyPurchaseOrderDetailId, int keyPurchaseOrderId)
        {
            return SingleResult.Create(_purchaseOrderService.GetOrderedProduct(keyPurchaseOrderId, keyPurchaseOrderDetailId));
        }
    }
}