﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class VendorsController : ODataController
    {
        IVendorService _vendorService;
        public VendorsController(IVendorService vendorService)
        {
            _vendorService = vendorService;
        }

        [EnableQuery]
        public IQueryable<Vendor> Get()
        {
            return _vendorService.GetVendors();
        }

        [EnableQuery]
        public SingleResult<Vendor> Get(int key)
        {
            return SingleResult.Create(_vendorService.GetVendor(key));
        }

        [EnableQuery]
        public SingleResult<BusinessEntity> GetBusinessEntity(int key)
        {
            return SingleResult.Create(_vendorService.GetBusinessEntity(key));
        }

        [EnableQuery]
        public IQueryable<ProductVendor> GetProductVendors(int key)
        {
            return _vendorService.GetProductVendors(key);
        }

        [EnableQuery]
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrderHeaders(int key)
        {
            return _vendorService.GetPurchaseOrders(key);
        }
    }
}