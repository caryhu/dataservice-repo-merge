﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class PurchaseOrderHeadersController : ODataController
    {
        IPurchaseOrderHeaderService _purchaseOrderHeaderService;
        public PurchaseOrderHeadersController(IPurchaseOrderHeaderService purchaseOrderHeaderService)
        {
            _purchaseOrderHeaderService = purchaseOrderHeaderService;
        }

        [EnableQuery]
        public IQueryable<PurchaseOrderHeader> Get()
        {
            return _purchaseOrderHeaderService.GetPurchaseOrderHeaders();
        }

        [EnableQuery]
        public SingleResult<PurchaseOrderHeader> Get(int key)
        {
            return SingleResult.Create(_purchaseOrderHeaderService.GetPurchaseOrderHeader(key));
        }

        [EnableQuery]
        public SingleResult<Employee> GetEmployee(int key)
        {
            return SingleResult.Create(_purchaseOrderHeaderService.GetPurchaseOrderCreator(key));
        }

        [EnableQuery]
        public SingleResult<ShipMethod> GetShipMethod(int key)
        {
            return SingleResult.Create(_purchaseOrderHeaderService.GetPurchaseOrderShippingDetail(key));
        }

        [EnableQuery]
        public SingleResult<Vendor> GetVendor(int key)
        {
            return SingleResult.Create(_purchaseOrderHeaderService.GetPurchaseOrderVendor(key));
        }

        [EnableQuery]
        public IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetails(int key)
        {
            return _purchaseOrderHeaderService.GetPurchaseOrderDetails(key);
        }
    }
}