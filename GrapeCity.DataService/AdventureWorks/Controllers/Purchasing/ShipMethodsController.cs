﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
 
    public class ShipMethodsController : ODataController
    {
        IShipMethodService _shipMethodService;
        public ShipMethodsController(IShipMethodService shipMethodService)
        {
            _shipMethodService = shipMethodService;
        }

        [EnableQuery]
        public IQueryable<ShipMethod> Get()
        {
            return _shipMethodService.GetShipMethods();
        }

        [EnableQuery]
        public SingleResult<ShipMethod> Get(int key)
        {
            return SingleResult.Create(_shipMethodService.GetShipMethod(key));
        }

        [EnableQuery]
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrderHeaders(int key)
        {
            return _shipMethodService.GetPurchaseOrders(key);
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrderHeaders(int key)
        {
            return _shipMethodService.GetSalesOrders(key);
        }
    }
}