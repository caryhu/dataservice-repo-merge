﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class CurrenciesController : ODataController
    {
        ICurrencyService _currencyService;
        public CurrenciesController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        [EnableQuery]
        public IQueryable<Currency> Get()
        {
            return _currencyService.GetCurrencies();
        }

        public ActionResult<string> Get(string key)
        {
            var culture = _currencyService.GetCurrency(key).FirstOrDefault();
            if (culture == null)
                return NotFound();
            return culture.Name;
        }

        [EnableQuery]
        public IQueryable<CurrencyRate> GetCurrencyRatesFrom(string key)
        {
            return _currencyService.GetCurrnecyConversionRates(key);
        }
    }
}