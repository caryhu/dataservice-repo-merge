﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class StoresController : ODataController
    {
        IStoreService _storeService;
        public StoresController(IStoreService storeService)
        {
            _storeService = storeService;
        }

        [EnableQuery]
        public IQueryable<Store> Get()
        {
            return _storeService.GetStores();
        }

        [EnableQuery]
        public SingleResult<Store> Get(int key)
        {
            return SingleResult.Create(_storeService.GetStore(key));
        }

        [EnableQuery]
        public SingleResult<SalesPerson> GetSalesPerson(int key)
        {
            return SingleResult.Create(_storeService.GetSalesPerson(key));
        }

        [EnableQuery]
        public SingleResult<BusinessEntity> GetBusinessEntity(int key)
        {
            return SingleResult.Create(_storeService.GetBusinessEntity(key));
        }

        [EnableQuery]
        public IQueryable<Customer> GetCustomers(int key)
        {
            return _storeService.GetCustomers(key);
        }
    }
}