﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SalesOrderHeadersController : ODataController
    {
        ISalesOrderHeaderService _salesOrderHeaderService;
        public SalesOrderHeadersController(ISalesOrderHeaderService salesOrderHeaderService)
        {
            _salesOrderHeaderService = salesOrderHeaderService;
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> Get()
        {
            return _salesOrderHeaderService.GetSalesOrders();
        }

        [EnableQuery]
        public SingleResult<SalesOrderHeader> Get(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetSalesOrder(key));
        }

        [EnableQuery]
        public SingleResult<Address> GetBillToAddress(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetBillingAddress(key));
        }

        [EnableQuery]
        public SingleResult<Address> GetShipToAddress(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetShippingAddress(key));
        }

        [EnableQuery]
        public SingleResult<CreditCard> GetCreditCard(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetCreditCardUsedForOrder(key));
        }

        [EnableQuery]
        public SingleResult<CurrencyRate> GetCurrencyRate(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetCurrencyRateAppliedOnOrder(key));
        }

        [EnableQuery]
        public SingleResult<Customer> GetCustomer(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetCustomerOfOrder(key));
        }

        [EnableQuery]
        public SingleResult<SalesPerson> GetSalesPerson(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetSalesPerson(key));
        }

        [EnableQuery]
        public SingleResult<ShipMethod> GetShipMethod(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetShippingMethod(key));
        }

        [EnableQuery]
        public SingleResult<SalesTerritory> GetTerritory(int key)
        {
            return SingleResult.Create(_salesOrderHeaderService.GetSalesOrderTerritory(key));
        }

        [EnableQuery]
        public IQueryable<SalesOrderDetail> GetSalesOrderDetails(int key)
        {
            return _salesOrderHeaderService.GetSalesOrderDetails(key);
        }
    }
}