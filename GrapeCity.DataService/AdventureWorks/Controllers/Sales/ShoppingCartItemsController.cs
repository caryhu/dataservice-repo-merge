﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class ShoppingCartItemsController : ODataController
    {
        IShoppingCartItemService _shottpingCartService;
        public ShoppingCartItemsController(IShoppingCartItemService shoppingCartService)
        {
            _shottpingCartService = shoppingCartService;
        }

        [EnableQuery]
        public IQueryable<ShoppingCartItem> Get(string shoppingCartId)
        {
            if (!string.IsNullOrEmpty(shoppingCartId))
                return _shottpingCartService.GetShoppingCartItemByCartId(shoppingCartId);
            return _shottpingCartService.GetShoppingCartItems();
        }

        [EnableQuery]
        public SingleResult<ShoppingCartItem> Get(int key)
        {
            return SingleResult.Create(_shottpingCartService.GetShoppingCartItemByItemId(key));
        }

        [EnableQuery]
        public SingleResult<Product> GetProduct(int key)
        {
            return SingleResult.Create(_shottpingCartService.GetProduct(key));
        }
    }
}