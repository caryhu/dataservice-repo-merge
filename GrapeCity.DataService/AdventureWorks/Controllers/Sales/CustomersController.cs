﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class CustomersController : ODataController
    {
        ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [EnableQuery]
        public IQueryable<Customer> Get()
        {
            return _customerService.GetCustomers();
        }

        [EnableQuery]
        public SingleResult<Customer> Get(int key)
        {
            return SingleResult.Create(_customerService.GetCustomer(key));
        }

        [EnableQuery]
        public SingleResult<Person> GetPerson(int key)
        {
            return SingleResult.Create(_customerService.GetCustomerPersonDetails(key));
        }

        [EnableQuery]
        public SingleResult<Store> GetStore(int key)
        {
            return SingleResult.Create(_customerService.GetCustomerStore(key));
        }

        [EnableQuery]
        public SingleResult<SalesTerritory> GetTerritory(int key)
        {
            return SingleResult.Create(_customerService.GetCustomerTerritory(key));
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrderHeaders(int key)
        {
            return _customerService.GetSalesOrdersPlacedByCustomer(key);
        }
    }
}