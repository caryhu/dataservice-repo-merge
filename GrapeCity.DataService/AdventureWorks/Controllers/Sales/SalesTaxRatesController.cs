﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SalesTaxRatesController : ODataController
    {
        ISalesTaxRateService _salesTaxRateService;
        public SalesTaxRatesController(ISalesTaxRateService salesTaxRateService)
        {
            _salesTaxRateService = salesTaxRateService;
        }

        [EnableQuery]
        public IQueryable<SalesTaxRate> Get()
        {
            return _salesTaxRateService.GetSalesTaxRates();
        }

        [EnableQuery]
        public SingleResult<SalesTaxRate> Get(int key)
        {
            return SingleResult.Create(_salesTaxRateService.GetSalesTaxRate(key));
        }

        [EnableQuery]
        public SingleResult<StateProvince> GetStateProvince(int key)
        {
            return SingleResult.Create(_salesTaxRateService.GetStateProvince(key));
        }
    }
}