﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SalesOrderDetailsController : ODataController
    {
        ISalesOrderDetailService _salesOrderDetailService;
        public SalesOrderDetailsController(ISalesOrderDetailService salesOrderDetailService)
        {
            _salesOrderDetailService = salesOrderDetailService;
        }

        [EnableQuery]
        public IQueryable<SalesOrderDetail> Get()
        {
            return _salesOrderDetailService.GetSalesOrderDetails();
        }

        [EnableQuery]
        public SingleResult<SalesOrderDetail> Get(int keySalesOrderDetailId, int keySalesOrderId)
        {
            return SingleResult.Create(_salesOrderDetailService.GetSalesOrderDetail(keySalesOrderId, keySalesOrderDetailId));
        }

        [EnableQuery]
        public SingleResult<SalesOrderHeader> GetSalesOrder(int keySalesOrderDetailId, int keySalesOrderId)
        {
            return SingleResult.Create(_salesOrderDetailService.GetSalesOrder(keySalesOrderId, keySalesOrderDetailId));
        }
    }
}