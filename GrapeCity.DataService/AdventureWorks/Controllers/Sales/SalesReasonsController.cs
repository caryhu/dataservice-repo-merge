﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SalesReasonsController : ODataController
    {
        ISalesReasonService _salesReasonService;
        public SalesReasonsController(ISalesReasonService salesReasonService)
        {
            _salesReasonService = salesReasonService;
        }

        [EnableQuery]
        public IQueryable<SalesReason> Get()
        {
            return _salesReasonService.GetSalesReasons();
        }

        [EnableQuery]
        public SingleResult<SalesReason> Get(int key)
        {
            return SingleResult.Create(_salesReasonService.GetSalesReason(key));
        }

    }
}