﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
     
    public class SalesTerritoriesController : ODataController
    {
        ISalesTerritoryService _salesTerritoryService;
        public SalesTerritoriesController(ISalesTerritoryService salesTerritoryService)
        {
            _salesTerritoryService = salesTerritoryService;
        }

        [EnableQuery]
        public IQueryable<SalesTerritory> Get()
        {
            return _salesTerritoryService.GetSalesTerritories();
        }

        [EnableQuery]
        public SingleResult<SalesTerritory> Get(int key)
        {
            return SingleResult.Create(_salesTerritoryService.GetSalesTerritory(key));
        }

        [EnableQuery]
        public SingleResult<CountryRegion> GetCountry(int key)
        {
            return SingleResult.Create(_salesTerritoryService.GetCountry(key));
        }

        [EnableQuery]
        public IQueryable<Customer> GetCustomers(int key)
        {
            return _salesTerritoryService.GetCustomers(key);
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrderHeaders(int key)
        {
            return _salesTerritoryService.GetSalesOrders(key);
        }

        [EnableQuery]
        public IQueryable<SalesPerson> GetSalesPeople(int key)
        {
            return _salesTerritoryService.GetSalesPeople(key);
        }

        [EnableQuery]
        public IQueryable<SalesTerritoryHistory> GetSalesterritoryHistories(int key)
        {
            return _salesTerritoryService.GetSalesTerritoryHistories(key);
        }

        [EnableQuery]
        public IQueryable<StateProvince> GetStateProvinces(int key)
        {
            return _salesTerritoryService.GetStateProvinces(key);
        }
    }
}