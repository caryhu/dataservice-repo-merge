﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SalesPersonsController : ODataController
    {
        ISalesPersonService _salesPersonService;
        public SalesPersonsController(ISalesPersonService salesPersonService)
        {
            _salesPersonService = salesPersonService;
        }

        [EnableQuery]
        public IQueryable<SalesPerson> Get()
        {
            return _salesPersonService.GetSalesPersons();
        }

        [EnableQuery]
        public SingleResult<SalesPerson> Get(int key)
        {
            return SingleResult.Create(_salesPersonService.GetSalesPerson(key));
        }

        [EnableQuery]
        public SingleResult<Employee> GetBusinessEntity(int key)
        {
            return SingleResult.Create(_salesPersonService.GetEmployee(key));
        }

        [EnableQuery]
        public SingleResult<SalesTerritory> GetTerritory(int key)
        {
            return SingleResult.Create(_salesPersonService.GetSalesTerritory(key));
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrderHeaders(int key)
        {
            return _salesPersonService.GetSalesOrders(key);
        }

        [EnableQuery]
        public IQueryable<SalesPersonQuotaHistory> GetSalesPersonQuotaHistories(int key)
        {
            return _salesPersonService.GetQuotaHistories(key);
        }

        [EnableQuery]
        public IQueryable<SalesTerritoryHistory> GetSalesTerritoryHistories(int key)
        {
            return _salesPersonService.GetSalesTerritoryHistories(key);
        }

        [EnableQuery]
        public IQueryable<Store> GetStores(int key)
        {
            return _salesPersonService.GetStores(key);
        }
    }
}