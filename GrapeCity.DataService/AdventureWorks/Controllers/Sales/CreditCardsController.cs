﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class CreditCardsController : ODataController
    {
        ICreditCardService _creditCardService;
        public CreditCardsController(ICreditCardService creditCardService)
        {
            _creditCardService = creditCardService;
        }

        [EnableQuery]
        public IQueryable<CreditCard> Get()
        {
            return _creditCardService.GetCreditCards();
        }

        [EnableQuery]
        public SingleResult<CreditCard> Get(int key)
        {
            return SingleResult.Create(_creditCardService.GetCreditCard(key));
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrderHeaders(int key)
        {
            return _creditCardService.GetSalesOrdersPlacedWithCreditCard(key);
        }
    }
}