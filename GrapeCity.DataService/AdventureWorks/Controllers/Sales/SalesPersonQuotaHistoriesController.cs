﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OData.Edm;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SalesPersonQuotaHistoriesController : ODataController
    {
        ISalesPersonQuotaHistoryService _salesPersonQuotaHistory;
        public SalesPersonQuotaHistoriesController(ISalesPersonQuotaHistoryService salesPersonQuotaHistoryService)
        {
            _salesPersonQuotaHistory = salesPersonQuotaHistoryService;
        }

        [EnableQuery]
        public IQueryable<SalesPersonQuotaHistory> Get(int? salesPersonId, DateTime? dateOfQuota)
        {
            return _salesPersonQuotaHistory.GetSalesPersonQuotaHistories(salesPersonId, dateOfQuota);
        }
    }
}