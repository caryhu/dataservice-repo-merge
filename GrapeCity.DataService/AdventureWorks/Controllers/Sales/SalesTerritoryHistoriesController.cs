﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SalesTerritoryHistoriesController : ODataController
    {
        ISalesTerritoryHistoryService _salesTerritoryHistoryService;

        public SalesTerritoryHistoriesController(ISalesTerritoryHistoryService salesTerritoryHistoryService)
        {
            _salesTerritoryHistoryService = salesTerritoryHistoryService;
        }

        [EnableQuery]
        public IQueryable<SalesTerritoryHistory> Get(int? salesPersonId, int? salesTerritoryId, DateTime? startingDate)
        {
            return _salesTerritoryHistoryService.GetSalesTerritoryHistories(salesPersonId, salesTerritoryId, startingDate);
        }
    }
}