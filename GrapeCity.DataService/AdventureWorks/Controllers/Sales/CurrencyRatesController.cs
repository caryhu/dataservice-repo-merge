﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class CurrencyRatesController : ODataController
    {
        ICurrencyRateService _currencyRateService;
        public CurrencyRatesController(ICurrencyRateService currencyRateService)
        {
            _currencyRateService = currencyRateService;
        }

        [EnableQuery]
        public IQueryable<CurrencyRate> Get(string fromCode, string toCode)
        {
            return _currencyRateService.GetCurrencyRates(fromCode, toCode);
        }

        [EnableQuery]
        public SingleResult<CurrencyRate> Get(int key)
        {
            return SingleResult.Create(_currencyRateService.GetCurrencyRate(key));
        }

        [EnableQuery]
        public IQueryable<SalesOrderHeader> GetSalesOrderHeaders(int key)
        {
            return _currencyRateService.GetSalesOrdersWithCurrencyRate(key);
        } 
    }
}