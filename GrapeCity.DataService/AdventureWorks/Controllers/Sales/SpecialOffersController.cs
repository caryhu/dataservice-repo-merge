﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapeCity.DataService.AdventureWorks.Controllers.V2
{
    
    public class SpecialOffersController : ODataController
    {
        public ISpecialOfferService _specialOfferService;
        public SpecialOffersController(ISpecialOfferService specialOfferService)
        {
            _specialOfferService = specialOfferService;
        }

        [EnableQuery]
        public IQueryable<SpecialOffer> Get()
        {
            return _specialOfferService.GetSpecialOffers();
        }

        [EnableQuery]
        public SingleResult<SpecialOffer> Get(int key)
        {
            return SingleResult.Create(_specialOfferService.GetSpecialOffer(key));
        }

        [EnableQuery]
        public IQueryable<Product> GetSpecialOfferProducts(int key)
        {
            return _specialOfferService.GetProducts(key);
        } 
    }
}