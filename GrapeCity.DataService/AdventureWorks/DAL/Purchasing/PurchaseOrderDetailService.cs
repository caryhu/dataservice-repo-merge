﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IPurchaseOrderDetailService
    {
        IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetails();
        IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetail(int orderDetailId);
        IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetail(int orderId, int orderDetailId);
        IQueryable<PurchaseOrderHeader> GetPurchaseOrder(int orderDetailId);
        IQueryable<PurchaseOrderHeader> GetPurchaseOrder(int orderId, int orderDetailId);
        IQueryable<Product> GetOrderedProduct(int orderDetailId);
        IQueryable<Product> GetOrderedProduct(int orderId, int orderDetailId);
    }
    public class PurchaseOrderDetailService : IPurchaseOrderDetailService
    {
        readonly AdventureWorksContext _context;
        public PurchaseOrderDetailService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetails()
        {
            return _context.PurchaseOrderDetails.ToList().AsQueryable();
        }
        public IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetail(int orderDetailId)
        {
            return _context.PurchaseOrderDetails.Where(x => x.PurchaseOrderDetailId == orderDetailId);
        }
        public IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetail(int orderId, int orderDetailId)
        {
            return _context.PurchaseOrderDetails.Where(x => x.PurchaseOrderId == orderId && x.PurchaseOrderDetailId == orderDetailId);
        }
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrder(int orderDetailId)
        {
            return _context.PurchaseOrderDetails.Where(x => x.PurchaseOrderDetailId == orderDetailId).
                Select(x => x.PurchaseOrder);
        }
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrder(int orderId, int orderDetailId)
        {
            return GetPurchaseOrderDetail(orderId, orderDetailId).
                Select(x => x.PurchaseOrder);
        }
        public IQueryable<Product> GetOrderedProduct(int orderDetailId)
        {
            return _context.PurchaseOrderDetails.Where(x => x.PurchaseOrderDetailId == orderDetailId).
                Select(x => x.Product);
        }
        public IQueryable<Product> GetOrderedProduct(int orderId, int orderDetailId)
        {
            return GetPurchaseOrderDetail(orderId, orderDetailId).
                Select(x => x.Product);
        }
    }
}
