﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IShipMethodService
    {
        IQueryable<ShipMethod> GetShipMethods();
        IQueryable<ShipMethod> GetShipMethod(int shipMethodId);
        IQueryable<PurchaseOrderHeader> GetPurchaseOrders(int shipMethodId);
        IQueryable<SalesOrderHeader> GetSalesOrders(int shipMethodId);
    }
    public class ShipMethodService: IShipMethodService
    {
        readonly AdventureWorksContext _context;
        public ShipMethodService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<ShipMethod> GetShipMethods()
        {
            return _context.ShipMethods;
        }
        public IQueryable<ShipMethod> GetShipMethod(int shipMethodId)
        {
            return _context.ShipMethods.Where(x => x.ShipMethodId == shipMethodId);
        }
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrders(int shipMethodId)
        {
            return _context.ShipMethods.Where(x => x.ShipMethodId == shipMethodId)
                .SelectMany(x => x.PurchaseOrderHeaders.AsQueryable());
        }
        public IQueryable<SalesOrderHeader> GetSalesOrders(int shipMethodId)
        {
            return _context.ShipMethods
                .Where(x => x.ShipMethodId == shipMethodId)
                .SelectMany(x => x.SalesOrderHeaders)
                .Include(s=>s.ShipMethod)
                .ToList()
                .AsQueryable();
        }
    }
}
