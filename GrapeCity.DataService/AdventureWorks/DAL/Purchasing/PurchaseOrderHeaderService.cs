﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IPurchaseOrderHeaderService
    {
        IQueryable<PurchaseOrderHeader> GetPurchaseOrderHeaders();
        IQueryable<PurchaseOrderHeader> GetPurchaseOrderHeader(int orderId);
        IQueryable<Employee> GetPurchaseOrderCreator(int orderId);
        IQueryable<ShipMethod> GetPurchaseOrderShippingDetail(int orderId);
        IQueryable<Vendor> GetPurchaseOrderVendor(int orderId);
        IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetails(int orderId);
    }
    public class PurchaseOrderHeaderService : IPurchaseOrderHeaderService
    {
        readonly AdventureWorksContext _context;
        public PurchaseOrderHeaderService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrderHeaders()
        {
            return _context.PurchaseOrderHeaders;
        }
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrderHeader(int orderId)
        {
            return _context.PurchaseOrderHeaders.Where(x => x.PurchaseOrderId == orderId);
        }
        public IQueryable<Employee> GetPurchaseOrderCreator(int orderId)
        {
            return _context.PurchaseOrderHeaders.Where(x => x.PurchaseOrderId == orderId).
                Select(x => x.Employee);
        }
        public IQueryable<ShipMethod> GetPurchaseOrderShippingDetail(int orderId)
        {
            return _context.PurchaseOrderHeaders.Where(x => x.PurchaseOrderId == orderId).
                Select(x => x.ShipMethod);
        }
        public IQueryable<Vendor> GetPurchaseOrderVendor(int orderId)
        {
            return _context.PurchaseOrderHeaders.Where(x => x.PurchaseOrderId == orderId).
                Select(x => x.Vendor);
        }
        public IQueryable<PurchaseOrderDetail> GetPurchaseOrderDetails(int orderId)
        {
            return _context.PurchaseOrderHeaders.Where(x => x.PurchaseOrderId == orderId).
                SelectMany(x => x.PurchaseOrderDetails.AsQueryable());
        }
    }
}
