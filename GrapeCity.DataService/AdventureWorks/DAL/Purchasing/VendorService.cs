﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IVendorService
    {
        IQueryable<Vendor> GetVendors();
        IQueryable<Vendor> GetVendor(int vendorId);
        IQueryable<BusinessEntity> GetBusinessEntity(int vendorId);
        IQueryable<ProductVendor> GetProductVendors(int vendorId);
        IQueryable<PurchaseOrderHeader> GetPurchaseOrders(int vendorId);
    }
    public class VendorService : IVendorService
    {
        readonly AdventureWorksContext _context;
        public VendorService(AdventureWorksContext context)
        {
            _context = context;
        }
        
        public IQueryable<Vendor> GetVendors()
        {
            return _context.Vendors;
        }
        public IQueryable<Vendor> GetVendor(int vendorId)
        {
            return _context.Vendors.Where(x => x.BusinessEntityId == vendorId);
        }
        public IQueryable<PurchaseOrderHeader> GetPurchaseOrders(int vendorId)
        {
            return _context.Vendors.Where(x => x.BusinessEntityId == vendorId).
                SelectMany(x => x.PurchaseOrderHeaders);
        }
        public IQueryable<ProductVendor> GetProductVendors(int vendorId)
        {
            return _context.Vendors.Where(x => x.BusinessEntityId == vendorId).
                SelectMany(x => x.ProductVendors);
        }
        public IQueryable<BusinessEntity> GetBusinessEntity(int vendorId)
        {
            return _context.Vendors.Where(x => x.BusinessEntityId == vendorId).Select(x => x.BusinessEntity);
        }
    }
}
