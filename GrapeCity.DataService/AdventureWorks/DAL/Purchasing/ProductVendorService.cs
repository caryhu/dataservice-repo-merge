﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductVendorService
    {
        IQueryable<ProductVendor> GetProductVendors(int? productId, int? vendorId);
        IQueryable<Product> GetProduct(int productId, int vendorId);
        IQueryable<Vendor> GetVendor(int productId, int vendorId);
        IQueryable<Product> GetProductsFromVendor(int vendorId);
        IQueryable<Vendor> GetVendorsForProduct(int productId);
        IQueryable<UnitMeasure> GetProductVendorUnitMeasure(int productId, int vendorId);
    }
    public class ProductVendorService : IProductVendorService
    {
        private readonly AdventureWorksContext _context;
        public ProductVendorService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<ProductVendor> GetProductVendors(int? productId, int? vendorId)
        {
            var result = _context.ProductVendors.AsQueryable();
            if (productId.HasValue)
                result = result.Where(x => x.ProductId == productId.Value);
            if (vendorId.HasValue)
                result = result.Where(x => x.BusinessEntityId == vendorId);
            return result;
        }
        public IQueryable<Product> GetProduct(int productId, int vendorId)
        {
            return _context.ProductVendors.Where(x => x.ProductId == productId && x.BusinessEntityId == vendorId).
                Select(x => x.Product);
        }

        public IQueryable<Product> GetProductsFromVendor(int vendorId)
        {
            return _context.ProductVendors.Where(x => x.BusinessEntityId == vendorId).Select(x => x.Product);
        }
        public IQueryable<Vendor> GetVendor(int productId, int vendorId)
        {
            return _context.ProductVendors.Where(x => x.ProductId == productId && x.BusinessEntityId == vendorId).
                Select(x => x.BusinessEntity);
        }

        public IQueryable<Vendor> GetVendorsForProduct(int productId)
        {
            return _context.ProductVendors.Where(x => x.ProductId == productId).Select(x => x.BusinessEntity);
        }
        public IQueryable<UnitMeasure> GetProductVendorUnitMeasure(int productId, int vendorId)
        {
            return _context.ProductVendors.Where(x => x.ProductId == productId && x.BusinessEntityId == vendorId).
                Select(x => x.UnitOfMeasure);
        }
    }
}
