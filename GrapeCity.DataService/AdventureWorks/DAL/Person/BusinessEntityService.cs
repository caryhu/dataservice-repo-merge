﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IBusinessEntityService
    {
        IQueryable<Person> GetPerson(int entityId);

        IQueryable<Store> GetStore(int entityId);

        IQueryable<Vendor> GetVendor(int entityId);

        IQueryable<BusinessEntityAddress> GetAddresses(int entityId);

        IQueryable<BusinessEntityContact> GetContacts(int entityId);

        IQueryable<BusinessEntityAddress> GetAddresses();

        IQueryable<BusinessEntityContact> GetContacts();
    }

    public class BusinessEntityService : IBusinessEntityService
    {
        private readonly AdventureWorksContext _context;

        public BusinessEntityService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<BusinessEntityAddress> GetAddresses(int entityId)
        {
            return _context.BusinessEntityAddresses.Where(b => b.BusinessEntity.BusinessEntityId == entityId).ToList().AsQueryable();
        }

        public IQueryable<BusinessEntityAddress> GetAddresses()
        {
            return _context.BusinessEntityAddresses
                .Include(a=>a.Address)
                .Include(a=>a.AddressType)
                .ToList().AsQueryable();
        }

        public IQueryable<BusinessEntityContact> GetContacts(int entityId)
        {
            return _context.BusinessEntityContacts.Where(b => b.BusinessEntity.BusinessEntityId == entityId).ToList().AsQueryable();
        }

        public IQueryable<BusinessEntityContact> GetContacts()
        {
            return _context.BusinessEntityContacts.ToList().AsQueryable();
        }

        public IQueryable<Person> GetPerson(int entityId)
        {
            return _context.BusinessEntities.Where(b => b.BusinessEntityId == entityId).Select(b => b.Person);
        }

        public IQueryable<Store> GetStore(int entityId)
        {
            return _context.BusinessEntities.Where(b => b.BusinessEntityId == entityId).Select(b => b.Store);
        }

        public IQueryable<Vendor> GetVendor(int entityId)
        {
            return _context.BusinessEntities.Where(b => b.BusinessEntityId == entityId).Select(b => b.Vendor);
        }
    }
}
