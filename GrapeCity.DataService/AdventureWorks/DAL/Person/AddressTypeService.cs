﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IAddressTypeService
    {
        IQueryable<AddressType> GetAddressTypes();

        IQueryable<AddressType> GetAddressType(short addressTypeId);

        IQueryable<BusinessEntityAddress> GetBusinessEntityAddresses(short addressTypeId);
    }

    public class AddressTypeService : IAddressTypeService
    {
        private readonly AdventureWorksContext _context;

        public AddressTypeService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<AddressType> GetAddressType(short addressTypeId)
        {
            return _context.AddressTypes.Where(a => a.AddressTypeId == addressTypeId).AsQueryable();
        }

        public IQueryable<AddressType> GetAddressTypes()
        {
            return _context.AddressTypes;
        }

        public IQueryable<BusinessEntityAddress> GetBusinessEntityAddresses(short addressTypeId)
        {
            return _context.BusinessEntityAddresses.Where(b => b.AddressTypeId == addressTypeId).
                Include(p => p.Address).Include(p=>p.AddressType).ToList().AsQueryable();
        }
    }
}
