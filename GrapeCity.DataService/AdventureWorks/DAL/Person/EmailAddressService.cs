﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IEmailAddressService
    {
        IQueryable<EmailAddress> GetEmailAddresses();

        IQueryable<Person> GetPerson(string email);

        IQueryable<Person> GetPerson(int businessEntityId, int emailAddressId);
    }

    public class EmailAddressService : IEmailAddressService
    {
        private readonly AdventureWorksContext _context;

        public EmailAddressService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<EmailAddress> GetEmailAddresses()
        {
            return _context.EmailAddresses.ToList().AsQueryable();
        }

        public IQueryable<Person> GetPerson(string email)
        {
            return _context.EmailAddresses.Where(e => e.Email == email).Select(e => e.Person).AsQueryable();
        }

        public IQueryable<Person> GetPerson(int businessEntityId, int emailAddressId)
        {
            return _context.EmailAddresses.Where(e => e.EmailAddressId == emailAddressId && e.BusinessEntityId == businessEntityId).Select(e => e.Person).AsQueryable();
        }
    }
}
