﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ICountryRegionService
    {
        IQueryable<CountryRegion> GetCountries();

        IQueryable<Currency> GetCurrencies(string countryCode);

        IQueryable<SalesTerritory> GetSalesTerritories(string countryCode);

        IQueryable<StateProvince> GetStateProvinces(string countryCode);

        IQueryable<string> GetCountryNames();

        IQueryable<string> GetCountryName(string countryCode);

        IQueryable<CountryRegion> GetCountryDetails(string countryCode);
    }

    public class CountryRegionService : ICountryRegionService
    {
        private readonly AdventureWorksContext _context;

        public CountryRegionService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<CountryRegion> GetCountries()
        {
            return _context.CountryRegions;
        }

        public IQueryable<CountryRegion> GetCountryDetails(string countryCode)
        {
            return _context.CountryRegions.Where(c => c.CountryRegionCode == countryCode).AsQueryable();
        }

        public IQueryable<string> GetCountryName(string countryCode)
        {
            return _context.CountryRegions.Where(c => c.CountryRegionCode == countryCode).Select(c => c.Name).AsQueryable();
        }

        public IQueryable<string> GetCountryNames()
        {
            return _context.CountryRegions.Select(c => c.Name);
        }

        public IQueryable<Currency> GetCurrencies(string countryCode)
        {
            return _context.CountryRegionCurrencies
                 .Where(c => c.CountryRegionCode == countryCode)
                 .Select(c => c.Currency).ToList().AsQueryable();
        }

        public IQueryable<SalesTerritory> GetSalesTerritories(string countryCode)
        {
            return _context.SalesTerritories.Where(s => s.CountryRegionCode == countryCode).ToList().AsQueryable();
        }

        public IQueryable<StateProvince> GetStateProvinces(string countryCode)
        {
            return _context.StateProvinces.Where(s => s.CountryRegionCode == countryCode).ToList().AsQueryable();
        }
    }
}
