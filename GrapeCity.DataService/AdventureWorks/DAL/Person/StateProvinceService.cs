﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IStateProvinceService
    {
        IQueryable<StateProvince> GetStateProvinces();

        IQueryable<string> GetStateProvinceNames();

        IQueryable<StateProvince> GetStateProvince(int provinceId);

        IQueryable<StateProvince> GetStateProvince(string provinceCode);

        IQueryable<CountryRegion> GetCountry(string provinceCode);

        IQueryable<CountryRegion> GetCountry(int provinceId);

        IQueryable<SalesTerritory> GetTerritory(string provinceCode);

        IQueryable<SalesTerritory> GetTerritory(int provinceId);

        IQueryable<Address> GetAddresses(string provinceCode);

        IQueryable<Address> GetAddresses(int provinceId);
    }

    public class StateProvinceService : IStateProvinceService
    {
        private readonly AdventureWorksContext _context;

        public StateProvinceService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Address> GetAddresses(string provinceCode)
        {
            return _context.Addresses.Where(a => a.StateProvince.StateProvinceCode == provinceCode).ToList().AsQueryable();
        }

        public IQueryable<Address> GetAddresses(int provinceId)
        {
            return _context.Addresses.Where(a => a.StateProvinceId == provinceId).ToList().AsQueryable();
        }

        public IQueryable<CountryRegion> GetCountry(string provinceCode)
        {
            return _context.StateProvinces.Where(s => s.StateProvinceCode == provinceCode).Select(s => s.Country).AsQueryable();
        }

        public IQueryable<CountryRegion> GetCountry(int provinceId)
        {
            return _context.StateProvinces.Where(s => s.StateProvinceId == provinceId).Select(s => s.Country).AsQueryable();
        }

        public IQueryable<StateProvince> GetStateProvince(int provinceId)
        {
            return _context.StateProvinces.Where(s => s.StateProvinceId == provinceId).AsQueryable();
        }

        public IQueryable<StateProvince> GetStateProvince(string provinceCode)
        {
            return _context.StateProvinces.Where(s => s.StateProvinceCode == provinceCode).AsQueryable();
        }

        public IQueryable<string> GetStateProvinceNames()
        {
            return _context.StateProvinces.Select(s => s.Name).AsQueryable();
        }

        public IQueryable<StateProvince> GetStateProvinces()
        {
            return _context.StateProvinces.ToList().AsQueryable();
        }

        public IQueryable<SalesTerritory> GetTerritory(string provinceCode)
        {
            return _context.StateProvinces.Where(s => s.StateProvinceCode == provinceCode).Select(s => s.Territory).AsQueryable();
        }

        public IQueryable<SalesTerritory> GetTerritory(int provinceId)
        {
            return _context.StateProvinces.Where(s => s.StateProvinceId == provinceId).Select(s => s.Territory).AsQueryable();
        }
    }
}
