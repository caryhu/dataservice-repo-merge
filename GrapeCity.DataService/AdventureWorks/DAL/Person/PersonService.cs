﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IPersonService
    {
        IQueryable<Person> GetPersons();

        IQueryable<Person> GetPerson(int personId);

        IQueryable<Employee> GetEmployee(int personId);

        IQueryable<Password> GetPassword(int personId);

        IQueryable<BusinessEntityContact> GetBusinessEntityContacts(int personId);

        IQueryable<Customer> GetCustomers(int personId);

        IQueryable<EmailAddress> GetEmails(int personId);

        IQueryable<CreditCard> GetCreditCards(int personId);

        IQueryable<PersonPhone> GetPhones(int personId);

        IQueryable<Dictionary<string, string>> GetDemographicsData(int personId);

        IQueryable<string> GetPersonTypes();
    }

    public class PersonService : IPersonService
    {
        private readonly AdventureWorksContext _context;

        public PersonService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<BusinessEntityContact> GetBusinessEntityContacts(int personId)
        {
            return _context.BusinessEntityContacts.Where(c => c.PersonId == personId).ToList().AsQueryable();
        }

        public IQueryable<CreditCard> GetCreditCards(int personId)
        {
            return _context.PersonCreditCards.Where(c => c.BusinessEntity.BusinessEntityId == personId)
                .Select(c => c.CreditCard).ToList().AsQueryable();
        }

        public IQueryable<Customer> GetCustomers(int personId)
        {
            return _context.Customers.Where(c => c.PersonId == personId).ToList().AsQueryable();
        }

        public IQueryable<Dictionary<string, string>> GetDemographicsData(int personId)
        {
            return _context.People.Where(p => p.BusinessEntityId == personId)
                .Select(p => DataCompletionSource.Demographics(p.Demographics)).AsQueryable();
        }

        public IQueryable<EmailAddress> GetEmails(int personId)
        {
            return _context.EmailAddresses.Where(e => e.Person.BusinessEntityId == personId).ToList().AsQueryable();
        }

        public IQueryable<Employee> GetEmployee(int personId)
        {
            return _context.People.Where(p => p.BusinessEntityId == personId).Select(p => p.Employee).AsQueryable();
        }

        public IQueryable<Password> GetPassword(int personId)
        {
            return _context.People.Where(p => p.BusinessEntityId == personId).Select(p => p.Password).AsQueryable();
        }

        public IQueryable<Person> GetPerson(int personId)
        {
            return _context.People.Where(p => p.BusinessEntityId == personId).AsQueryable();
        }

        public IQueryable<Person> GetPersons()
        {
            return _context.People.ToList().AsQueryable();
        }

        public IQueryable<PersonPhone> GetPhones(int personId)
        {
            return _context.PersonPhones.Where(p => p.Person.BusinessEntityId == personId).ToList().AsQueryable();
        }

        public IQueryable<string> GetPersonTypes()
        {
            var personTypes = _context.People.Select(p => p.PersonType).Distinct();
            return personTypes.Select(p => DataCompletionSource.PersonType(p));
        }
    }
}
