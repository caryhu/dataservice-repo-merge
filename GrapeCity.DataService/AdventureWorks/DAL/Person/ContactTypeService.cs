﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IContactTypeService
    {
        IQueryable<ContactType> GetContactTypes();

        IQueryable<ContactType> GetContactType(short typeId);

        IQueryable<BusinessEntityContact> GetBusinessEntityContacts(short typeId);
    }

    public class ContactTypeService : IContactTypeService
    {
        private readonly AdventureWorksContext _context;

        public ContactTypeService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<BusinessEntityContact> GetBusinessEntityContacts(short typeId)
        {
            return _context.BusinessEntityContacts.Where(c => c.ContactTypeId == typeId).ToList().AsQueryable();
        }

        public IQueryable<ContactType> GetContactType(short typeId)
        {
            return _context.ContactTypes.Where(c => c.ContactTypeId == typeId).AsQueryable();
        }

        public IQueryable<ContactType> GetContactTypes()
        {
            return _context.ContactTypes;
        }
    }
}
