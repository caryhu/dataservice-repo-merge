﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IAddressService
    {
        IQueryable<Address> GetAddresses();

        IQueryable<Address> GetAddress(int addressId);

        IQueryable<StateProvince> GetProvince(int addressId);

        IQueryable<BusinessEntityAddress> GetBusinessEntityAddresses(int addressId);

        IQueryable<SalesOrderHeader> GetSalesOrdersBilledToAddress(int addressId);

        IQueryable<SalesOrderHeader> GetSalesOrdersShippedToAddress(int addressId);
    }

    public class AddressService : IAddressService
    {
        private readonly AdventureWorksContext _context;

        public AddressService(AdventureWorksContext context)
        {
            _context = context;    
        }

        public IQueryable<Address> GetAddress(int addressId)
        {
            return _context.Addresses.Where(a => a.AddressId == addressId).ToList().AsQueryable();
        }

        public IQueryable<Address> GetAddresses()
        {
            return _context.Addresses.ToList().AsQueryable();
        }

        public IQueryable<BusinessEntityAddress> GetBusinessEntityAddresses(int addressId)
        {
            return _context.BusinessEntityAddresses.Where(b => b.AddressId == addressId).ToList().AsQueryable();
        }

        public IQueryable<StateProvince> GetProvince(int addressId)
        {
            return _context.StateProvinces.Where(s => s.Addresses
                .Where(a => a.AddressId == addressId)
                .FirstOrDefault().StateProvinceId == s.StateProvinceId).ToList().AsQueryable();
        }

        public IQueryable<SalesOrderHeader> GetSalesOrdersBilledToAddress(int addressId)
        {
            return _context.SalesOrderHeaders.Where(s => s.BillToAddressId == addressId).ToList().AsQueryable();
        }

        public IQueryable<SalesOrderHeader> GetSalesOrdersShippedToAddress(int addressId)
        {
            return _context.SalesOrderHeaders.Where(s => s.ShipToAddressId == addressId).ToList().AsQueryable();
        }
    }
}
