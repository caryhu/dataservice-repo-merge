﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IPersonPhoneService
    {
        IQueryable<PersonPhone> GetPhoneNumbers();

        IQueryable<Person> GetPerson(string phoneNumber);

        IQueryable<Person> GetPerson(int businessEntityId, string phoneNumber, int phoneNumberTypeId);

        IQueryable<PhoneNumberType> GetPhoneNumberType(string phoneNumber);

        IQueryable<PhoneNumberType> GetPhoneNumberType(int businessEntityId, string phoneNumber, int phoneNumberTypeId);

        IQueryable<PhoneNumberType> GetPhoneNumberTypes();

        IQueryable<PhoneNumberType> GetPhoneNumberType(short typeId);

        IQueryable<PersonPhone> GetPersonPhones(short typeId);
    }

    public class PersonPhoneService : IPersonPhoneService
    {
        private readonly AdventureWorksContext _context;
        private List<char> _charsToRemove = new List<char>() { '-', '(', ')', ' ' };

        public PersonPhoneService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Person> GetPerson(string phoneNumber)
        {
            return _context.PersonPhones.ToList()
                .Where(p => p.PhoneNumber.RemoveAllChars(_charsToRemove) == phoneNumber.RemoveAllChars(_charsToRemove)).Select(p => p.Person).AsQueryable();
        }

        public IQueryable<Person> GetPerson(int businessEntityId, string phoneNumber, int phoneNumberTypeId)
        {
            return _context.PersonPhones.ToList()
                .Where(p => p.BusinessEntityId == businessEntityId && p.PhoneNumber.RemoveAllChars(_charsToRemove) == phoneNumber.RemoveAllChars(_charsToRemove) && p.PhoneNumberTypeId == phoneNumberTypeId)
                .Select(p => p.Person).AsQueryable();
        }

        public IQueryable<PersonPhone> GetPersonPhones(short typeId)
        {
            return _context.PersonPhones.Where(p => p.PhoneNumberTypeId == typeId)
                .Include(p => p.Person)
                .Include(p => p.PhoneNumberType)
                .ToList().AsQueryable();
        }

        public IQueryable<PersonPhone> GetPhoneNumbers()
        {
            return _context.PersonPhones
                .Include(p => p.Person)
                .Include(p => p.PhoneNumberType)
                .ToList().AsQueryable();
        }

        public IQueryable<PhoneNumberType> GetPhoneNumberType(string phoneNumber)
        {
            return _context.PersonPhones.ToList()
                .Where(p => p.PhoneNumber.RemoveAllChars(_charsToRemove) == phoneNumber.RemoveAllChars(_charsToRemove))
                .Select(p => p.PhoneNumberType).AsQueryable();
        }

        public IQueryable<PhoneNumberType> GetPhoneNumberType(short typeId)
        {
            return _context.PhoneNumberTypes.Where(p => p.PhoneNumberTypeId == typeId).AsQueryable();
        }

        public IQueryable<PhoneNumberType> GetPhoneNumberType(int businessEntityId, string phoneNumber, int phoneNumberTypeId)
        {
            return _context.PersonPhones.ToList()
                .Where(p => p.BusinessEntityId == businessEntityId && p.PhoneNumber.RemoveAllChars(_charsToRemove) == phoneNumber.RemoveAllChars(_charsToRemove) && p.PhoneNumberTypeId == phoneNumberTypeId)
                .Select(p => p.PhoneNumberType).AsQueryable();
        }

        public IQueryable<PhoneNumberType> GetPhoneNumberTypes()
        {
            return _context.PhoneNumberTypes;
        }
    }
}