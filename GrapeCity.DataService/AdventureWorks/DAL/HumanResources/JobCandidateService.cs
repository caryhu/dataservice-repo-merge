﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IJobCandidateService
    {
        IQueryable<JobCandidate> GetJobCandidates();

        IQueryable<JobCandidate> GetJobCandidate(int candidateId);

        IQueryable<Employee> GetEmployee(int candidateId); 
    }

    public class JobCandidateService : IJobCandidateService
    {
        private readonly AdventureWorksContext _context;

        public JobCandidateService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Employee> GetEmployee(int candidateId)
        {
            return _context.JobCandidates.Where(j => j.JobCandidateId == candidateId).Select(j => j.BusinessEntity).AsQueryable();
        }

        public IQueryable<JobCandidate> GetJobCandidate(int candidateId)
        {
            return _context.JobCandidates.Where(j => j.JobCandidateId == candidateId).ToList().AsQueryable();
        }

        public IQueryable<JobCandidate> GetJobCandidates()
        {
            return _context.JobCandidates.ToList().AsQueryable();
        }
    }
}
