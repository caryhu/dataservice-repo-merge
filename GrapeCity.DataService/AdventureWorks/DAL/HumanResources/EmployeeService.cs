﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IEmployeeService
    {
        IQueryable<Employee> GetEmployees();

        IQueryable<Employee> GetEmployee(int employeeId);

        IQueryable<Person> GetEmployeePersonalDetails(int employeeId);

        IQueryable<SalesOrderHeader> GetSales(int employeeId);

        IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories(int employeeId);

        IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistory(int employeeId, short departmentId);

        IQueryable<EmployeePayHistory> GetEmployeePayHistories(int employeeId);

        IQueryable<EmployeePayHistory> GetEmployeePayHistories(int employeeId, DateTime rateChangeDate);

        IQueryable<Employee> GetEmployeeFromPayHistories(int employeeId, DateTime rateChangeDate);

        IQueryable<EmployeePayHistory> GetEmployeePayHistories();

        IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories();

        IQueryable<JobCandidate> GetEmployeeJobApplications(int employeeId);

        IQueryable<PurchaseOrderHeader> GetEmployeePurchaseOrders(int employeeId);

        IQueryable<Employee> GetEmployeeFromDepartmentHistories(int businessEntityId, short departmentId, byte shiftId, DateTime startDate);

        IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistory(int businessEntityId, short departmentId, byte shiftId, DateTime startDate);

        IQueryable<Department> GetDepartmentFromDepartmentHistories(int businessEntityId, short departmentId, byte shiftId, DateTime startDate);

        IQueryable<Shift> GetShiftFromDepartmentHistories(int businessEntityId, short departmentId, byte shiftId, DateTime startDate);
    }

    public class EmployeeService : IEmployeeService
    {
        private readonly AdventureWorksContext _context;

        public EmployeeService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Employee> GetEmployee(int employeeId)
        {
            return _context.Employees.Where(e => e.BusinessEntityId == employeeId).AsQueryable();
        }

        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistory(int employeeId, short departmentId)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.BusinessEntityId == employeeId && e.DepartmentId == departmentId).AsQueryable();
        }

        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories(int employeeId)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.BusinessEntityId == employeeId).AsQueryable();
        }

        public IQueryable<EmployeePayHistory> GetEmployeePayHistories(int employeeId)
        {
            return _context.EmployeePayHistories.Where(e => e.BusinessEntity.BusinessEntityId == employeeId).AsQueryable();
        }

        public IQueryable<Person> GetEmployeePersonalDetails(int employeeId)
        {
            return _context.People.Where(p => p.Employee.BusinessEntityId == employeeId).AsQueryable();
        }

        public IQueryable<Employee> GetEmployees()
        {
            return _context.Employees.ToList().AsQueryable();
        }

        public IQueryable<SalesOrderHeader> GetSales(int employeeId)
        {
            return _context.SalesOrderHeaders.Where(s => s.SalesPerson.BusinessEntity.BusinessEntityId == employeeId).ToList().AsQueryable();
        }

        public IQueryable<JobCandidate> GetEmployeeJobApplications(int employeeId)
        {
            return _context.JobCandidates.Where(j => j.BusinessEntityId == employeeId).ToList().AsQueryable();
        }

        public IQueryable<PurchaseOrderHeader> GetEmployeePurchaseOrders(int employeeId)
        {
            return _context.PurchaseOrderHeaders.Where(p => p.EmployeeId == employeeId).ToList().AsQueryable();
        }

        public IQueryable<EmployeePayHistory> GetEmployeePayHistories()
        {
            return _context.EmployeePayHistories.ToList().AsQueryable();
        }

        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories()
        {
            return _context.EmployeeDepartmentHistories.ToList().AsQueryable();
        }

        public IQueryable<EmployeePayHistory> GetEmployeePayHistories(int employeeId, DateTime rateChangeDate)
        {
            return _context.EmployeePayHistories.Where(e => e.BusinessEntityId == employeeId && e.RateChangeDate == rateChangeDate).AsQueryable();
        }

        public IQueryable<Employee> GetEmployeeFromPayHistories(int employeeId, DateTime rateChangeDate)
        {
            return _context.EmployeePayHistories.Where(e => e.BusinessEntityId == employeeId && e.RateChangeDate == rateChangeDate).Select(e => e.BusinessEntity).AsQueryable();
        }

        public IQueryable<Employee> GetEmployeeFromDepartmentHistories(int businessEntityId, short departmentId, byte shiftId, DateTime startDate)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.BusinessEntityId == businessEntityId && e.DepartmentId == departmentId && e.ShiftId == shiftId && e.StartDate == startDate)
                .Select(e => e.BusinessEntity).AsQueryable();
        }

        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistory(int businessEntityId, short departmentId, byte shiftId, DateTime startDate)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.BusinessEntityId == businessEntityId && e.DepartmentId == departmentId && e.ShiftId == shiftId && e.StartDate == startDate)
                .AsQueryable();
        }

        public IQueryable<Department> GetDepartmentFromDepartmentHistories(int businessEntityId, short departmentId, byte shiftId, DateTime startDate)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.BusinessEntityId == businessEntityId && e.DepartmentId == departmentId && e.ShiftId == shiftId && e.StartDate == startDate)
                .Select(e => e.Department).AsQueryable();
        }

        public IQueryable<Shift> GetShiftFromDepartmentHistories(int businessEntityId, short departmentId, byte shiftId, DateTime startDate)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.BusinessEntityId == businessEntityId && e.DepartmentId == departmentId && e.ShiftId == shiftId && e.StartDate == startDate)
                .Select(e => e.Shift).AsQueryable();
        }
    }
}
