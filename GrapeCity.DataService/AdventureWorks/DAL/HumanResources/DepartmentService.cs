﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IDepartmentService
    {
        IQueryable<Department> GetDepartments();

        IQueryable<Department> GetDepartment(short departmentId);

        IQueryable<Employee> GetEmployees(short departmentId);

        IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories(short departmentId);
    }

    public class DeptartmentService : IDepartmentService
    {
        private readonly AdventureWorksContext _context;
        public DeptartmentService(AdventureWorksContext context)
        {
            this._context = context;
        }

        public IQueryable<Department> GetDepartments()
        {
            return _context.Departments;
        }

        public IQueryable<Department> GetDepartment(short departmentId)
        {
            return _context.Departments.Where(d=>d.DepartmentId == departmentId).AsQueryable();
        }

        public IQueryable<Employee> GetEmployees(short departmentId)
        {
            return _context.Employees.Where(x => x.EmployeeDepartmentHistories.Any(d => d.DepartmentId == departmentId));
        }

        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories(short departmentId)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.DepartmentId == departmentId).ToList().AsQueryable();
        }
    }
}
