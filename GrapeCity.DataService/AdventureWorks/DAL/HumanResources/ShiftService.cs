﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IShiftService
    {
        IQueryable<Shift> GetShifts();

        IQueryable<Shift> GetShift(byte shiftId);

        IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories(byte shiftId);
    }

    public class ShiftService : IShiftService
    {
        private readonly AdventureWorksContext _context;

        public ShiftService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories(byte shiftId)
        {
            return _context.EmployeeDepartmentHistories.Where(e => e.ShiftId == shiftId).ToList().AsQueryable();
        }

        public IQueryable<Shift> GetShift(byte shiftId)
        {
            return _context.Shifts.Where(s => s.ShiftId == shiftId).AsQueryable();
        }

        public IQueryable<Shift> GetShifts()
        {
            return _context.Shifts.ToList().AsQueryable();
        }
    }
}
