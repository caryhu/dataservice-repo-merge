﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IIllustrationService
    {
        IQueryable<Illustration> GetIllustrations();

        IQueryable<Illustration> GetIllustration(int illustrationId);

        IQueryable<ProductModelIllustration> GetProductModelIllustrations(int illustrationId);
    }

    public class IllustrationService : IIllustrationService
    {
        private readonly AdventureWorksContext _context;

        public IllustrationService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Illustration> GetIllustration(int illustrationId)
        {
            return _context.Illustrations.Where(i => i.IllustrationId == illustrationId).AsQueryable();
        }

        public IQueryable<Illustration> GetIllustrations()
        {
            return _context.Illustrations.ToList().AsQueryable();
        }

        public IQueryable<ProductModelIllustration> GetProductModelIllustrations(int illustrationId)
        {
            return _context.ProductModelIllustrations.Where(p => p.IllustrationId == illustrationId).ToList().AsQueryable();
        }
    }
}
