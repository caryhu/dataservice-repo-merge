﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IWorkOrderService
    {
        IQueryable<WorkOrder> GetWorkOrders();

        IQueryable<WorkOrder> GetWorkOrder(int workOrderId);

        IQueryable<Product> GetProduct(int workOrderId);

        IQueryable<ScrapReason> GetScrapReason(int workOrderId);

        IQueryable<WorkOrderRouting> GetWorkOrderRoutings(int workOrderId);
    }

    public class WorkOrderService : IWorkOrderService
    {
        private readonly AdventureWorksContext _context;

        public WorkOrderService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Product> GetProduct(int workOrderId)
        {
            return _context.WorkOrders.Where(w => w.WorkOrderId == workOrderId).Select(w => w.Product).AsQueryable();
        }

        public IQueryable<ScrapReason> GetScrapReason(int workOrderId)
        {
            return _context.WorkOrders.Where(w => w.WorkOrderId == workOrderId).Select(w => w.ScrapReason).AsQueryable();
        }

        public IQueryable<WorkOrder> GetWorkOrder(int workOrderId)
        {
            return _context.WorkOrders.Where(w => w.WorkOrderId == workOrderId).AsQueryable();
        }

        public IQueryable<WorkOrderRouting> GetWorkOrderRoutings(int workOrderId)
        {
            return _context.WorkOrderRoutings.Where(w => w.WorkOrderId == workOrderId).AsQueryable();
        }

        public IQueryable<WorkOrder> GetWorkOrders()
        {
            return _context.WorkOrders
                .Include(x => x.ScrapReason)
                .ToList()
                .AsQueryable();                 
        }
    }
}
