﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IScrapReasonService
    {
        IQueryable<ScrapReason> GetScrapReasons();

        IQueryable<ScrapReason> GetScrapReason(short scrapReasonId);

        IQueryable<WorkOrder> GetWorkOrders(short scrapReasonId);
    }

    public class ScrapReasonService : IScrapReasonService
    {
        private readonly AdventureWorksContext _context;

        public ScrapReasonService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<ScrapReason> GetScrapReason(short scrapReasonId)
        {
            return _context.ScrapReasons.Where(s => s.ScrapReasonId == scrapReasonId).AsQueryable();
        }

        public IQueryable<ScrapReason> GetScrapReasons()
        {
            return _context.ScrapReasons.ToList().AsQueryable();
        }

        public IQueryable<WorkOrder> GetWorkOrders(short scrapReasonId)
        {
            return _context.WorkOrders.Where(w => w.ScrapReasonId == scrapReasonId).AsQueryable();
        }
    }
}
