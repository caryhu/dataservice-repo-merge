﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductService
    {
        IQueryable<Product> GetProducts();

        IQueryable<Product> GetProduct(int productId);

        IQueryable<Product> GetProduct(string productNumber);

        IQueryable<Product> GetProductDetails(int productId);

        IQueryable<ProductModel> GetProductModel(int productId);

        IQueryable<ProductSubcategory> GetProductSubcategory(int productId);

        IQueryable<UnitMeasure> GetProductSizeUnit(int productId);

        IQueryable<UnitMeasure> GetProductWeightUnit(int productId);

        IQueryable<Product> GetProductComponents(int productId);

        IQueryable<Product> GetProductAssemblies(int productId);

        IQueryable<ProductCostHistory> GetProductCostHistories(int productId);

        IQueryable<ProductInventory> GetProductInventories(int productId);

        IQueryable<ProductListPriceHistory> GetProductPriceHistories(int productId);

        IQueryable<ProductPhoto> GetProductPhotos(int productId);

        IQueryable<ProductReview> GetProductReviews(int productId);

        IQueryable<ProductVendor> GetProductVendors(int productId);

        IQueryable<SpecialOffer> GetProductOffers(int productId);

        IQueryable<TransactionHistory> GetProductTransactions(int productId);

        IQueryable<WorkOrder> GetWorkOrders(int productId);
    }

    public class ProductService : IProductService
    {
        private readonly AdventureWorksContext _context;

        public ProductService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Product> GetProduct(int productId)
        {
            return _context.Products.Where(p => p.ProductId == productId).AsQueryable();
        }

        public IQueryable<Product> GetProduct(string productNumber)
        {
            return _context.Products.Where(p => p.ProductNumber == productNumber).AsQueryable();
        }

        public IQueryable<Product> GetProductAssemblies(int productId)
        {
            return _context.BillOfMaterials.Where(p => p.ProductAssembly.ProductId == productId)
                .Select(p => p.ProductAssembly).ToList().AsQueryable();
        }

        public IQueryable<Product> GetProductComponents(int productId)
        {
            return _context.BillOfMaterials.Where(p => p.ProductAssembly.ProductId == productId)
                .Select(p => p.Component).ToList().AsQueryable();
        }

        public IQueryable<ProductCostHistory> GetProductCostHistories(int productId)
        {
            return _context.ProductCostHistories.Where(p => p.ProductId == productId).ToList().AsQueryable();
        }

        public IQueryable<Product> GetProductDetails(int productId)
        {
            return GetProduct(productId);
        }

        public IQueryable<ProductInventory> GetProductInventories(int productId)
        {
            return _context.ProductInventories.Where(p => p.ProductId == productId).ToList().AsQueryable();
        }

        public IQueryable<ProductModel> GetProductModel(int productId)
        {
            return _context.Products.Where(p => p.ProductId == productId).Select(p => p.ProductModel).AsQueryable();
        }

        public IQueryable<SpecialOffer> GetProductOffers(int productId)
        {
            return _context.SpecialOfferProducts.Where(p => p.ProductId == productId).Select(p => p.SpecialOffer).AsQueryable();
        }

        public IQueryable<ProductPhoto> GetProductPhotos(int productId)
        {
            return _context.ProductProductPhotos.Where(p => p.ProductId == productId).Select(p => p.ProductPhoto).AsQueryable();
        }

        public IQueryable<ProductListPriceHistory> GetProductPriceHistories(int productId)
        {
            return _context.ProductListPriceHistories.Where(p => p.ProductId == productId).ToList().AsQueryable();
        }

        public IQueryable<ProductReview> GetProductReviews(int productId)
        {
            return _context.ProductReviews.Where(p => p.ProductId == productId).ToList().AsQueryable();
        }

        public IQueryable<Product> GetProducts()
        {
            return _context.Products.ToList().AsQueryable();
        }

        public IQueryable<UnitMeasure> GetProductSizeUnit(int productId)
        {
            return _context.Products.Where(p => p.ProductId == productId).Select(p => p.UnitOfSize).AsQueryable();
        }

        public IQueryable<ProductSubcategory> GetProductSubcategory(int productId)
        {
            return _context.Products.Where(p => p.ProductId == productId).Select(p => p.ProductSubcategory).AsQueryable();
        }

        public IQueryable<TransactionHistory> GetProductTransactions(int productId)
        {
            return _context.TransactionHistories.Where(p => p.ProductId == productId).ToList().AsQueryable();
        }

        public IQueryable<ProductVendor> GetProductVendors(int productId)
        {
            return _context.ProductVendors.Where(p => p.ProductId == productId).ToList().AsQueryable();
        }

        public IQueryable<UnitMeasure> GetProductWeightUnit(int productId)
        {
            return _context.Products.Where(p => p.ProductId == productId).Select(p => p.UnitOfWeight).AsQueryable();
        }

        public IQueryable<WorkOrder> GetWorkOrders(int productId)
        {
            return _context.WorkOrders.Where(p => p.ProductId == productId).ToList().AsQueryable();
        }
    }
}
