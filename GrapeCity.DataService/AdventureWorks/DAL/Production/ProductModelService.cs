﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductModelService
    {
        IQueryable<ProductModel> GetProductModels();

        IQueryable<ProductModel> GetProductModel(int modelId);

        IQueryable<ProductDescription> GetProductModelDescriptions(int modelId);

        IQueryable<ProductDescription> GetProductModelDescription(int modelId, string cultureId);
    }

    public class ProductModelService : IProductModelService
    {
        private readonly AdventureWorksContext _context;

        public ProductModelService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<ProductModel> GetProductModel(int modelId)
        {
            return _context.ProductModels.Where(m => m.ProductModelId == modelId).AsQueryable();
        }

        public IQueryable<ProductDescription> GetProductModelDescriptions(int modelId)
        {
            return _context.ProductModelProductDescriptionCultures.Where(p => p.ProductModelId == modelId)
                .Select(p => p.ProductDescription).AsQueryable();
        }

        public IQueryable<ProductDescription> GetProductModelDescription(int modelId, string cultureId)
        {
            return _context.ProductModelProductDescriptionCultures.Where(p => p.ProductModelId == modelId && p.CultureId == cultureId)
                .Select(p => p.ProductDescription).AsQueryable();
        }

        public IQueryable<ProductModel> GetProductModels()
        {
            return _context.ProductModels.ToList().AsQueryable();
        }
    }
}
