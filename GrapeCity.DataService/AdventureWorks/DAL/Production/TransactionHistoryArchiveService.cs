﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ITransactionHistoryArchiveService
    {
        IQueryable<TransactionHistoryArchive> GetTransactionHistoryArchives();

        IQueryable<TransactionHistoryArchive> GetTransactionHistoryArchive(int transactionId);
    }

    public class TransactionHistoryArchiveService : ITransactionHistoryArchiveService
    {
        private readonly AdventureWorksContext _context;

        public TransactionHistoryArchiveService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<TransactionHistoryArchive> GetTransactionHistoryArchive(int transactionId)
        {
            return _context.TransactionHistoryArchives.Where(t => t.TransactionId == transactionId).AsQueryable();
        }

        public IQueryable<TransactionHistoryArchive> GetTransactionHistoryArchives()
        {
            return _context.TransactionHistoryArchives.ToList().AsQueryable();
        }
    }
}
