﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IUnitMeasureService
    {
        IQueryable<UnitMeasure> GetUnitMeasures();

        IQueryable<UnitMeasure> GetUnitMeasure(string measureCode);

        IQueryable<BillOfMaterial> GetBillOfMaterials(string measureCode);

        IQueryable<UnitMeasure> GetProductSizeUnits(string measureCode);

        IQueryable<UnitMeasure> GetProductWeightUnits(string measureCode);

        IQueryable<ProductVendor> GetProductVendors(string measureCode);
    }

    public class UnitMeasureService : IUnitMeasureService
    {
        private readonly AdventureWorksContext _context;

        public UnitMeasureService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<BillOfMaterial> GetBillOfMaterials(string measureCode)
        {
            return _context.BillOfMaterials.Where(b => b.UnitMeasureCode == measureCode).AsQueryable();
        }

        public IQueryable<UnitMeasure> GetProductSizeUnits(string measureCode)
        {
            return _context.Products.Select(p => p.UnitOfSize).Where(p => p.UnitMeasureCode == measureCode).AsQueryable();
        }

        public IQueryable<ProductVendor> GetProductVendors(string measureCode)
        {
            return _context.ProductVendors.Where(v => v.UnitMeasureCode == measureCode).AsQueryable();
        }

        public IQueryable<UnitMeasure> GetProductWeightUnits(string measureCode)
        {
            return _context.Products.Select(p => p.UnitOfWeight).Where(p => p.UnitMeasureCode == measureCode).AsQueryable();
        }

        public IQueryable<UnitMeasure> GetUnitMeasure(string measureCode)
        {
            return _context.UnitMeasures.Where(u => u.UnitMeasureCode == measureCode).AsQueryable();
        }

        public IQueryable<UnitMeasure> GetUnitMeasures()
        {
            return _context.UnitMeasures.ToList().AsQueryable();
        }
    }
}
