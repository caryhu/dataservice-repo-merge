﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ICultureService
    {
        IQueryable<Culture> GetCultures();

        IQueryable<Culture> GetCulture(string cultureId);

        IQueryable<Culture> GetInvariantCulture();

        IQueryable<ProductDescription> GetProductModelDescriptions(string cultureId);
    }

    public class CultureService : ICultureService
    {
        private readonly AdventureWorksContext _context;

        public CultureService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Culture> GetCulture(string cultureId)
        {
            return _context.Cultures.Where(c => c.CultureId == cultureId).AsQueryable();
        }

        public IQueryable<Culture> GetCultures()
        {
            return _context.Cultures;
        }

        public IQueryable<Culture> GetInvariantCulture()
        {
            return _context.Cultures.Where(c => string.IsNullOrEmpty(c.CultureId)).AsQueryable();
        }

        public IQueryable<ProductDescription> GetProductModelDescriptions(string cultureId)
        {
            return _context.ProductModelProductDescriptionCultures.Where(p => p.CultureId == cultureId).Select(p => p.ProductDescription).ToList().AsQueryable();
        }
    }
}
