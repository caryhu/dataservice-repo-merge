﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IBillOfMaterialService
    {
        IQueryable<BillOfMaterial> GetBillOfMaterials();

        IQueryable<BillOfMaterial> GetBillOfMaterial(int billId);

        IQueryable<Product> GetParentProduct(int billId);

        IQueryable<Product> GetComponent(int billId);

        IQueryable<UnitMeasure> GetUnitMeasure(int billId);
    }

    public class BillOfMaterialService : IBillOfMaterialService
    {
        private readonly AdventureWorksContext _context;

        public BillOfMaterialService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<BillOfMaterial> GetBillOfMaterial(int billId)
        {
            return _context.BillOfMaterials.Where(b => b.BillOfMaterialsId == billId).AsQueryable();
        }

        public IQueryable<BillOfMaterial> GetBillOfMaterials()
        {
            return _context.BillOfMaterials.ToList().AsQueryable();
        }

        public IQueryable<Product> GetComponent(int billId)
        {
            return _context.BillOfMaterials.Where(b => b.BillOfMaterialsId == billId).Select(b => b.Component).AsQueryable();
        }

        public IQueryable<Product> GetParentProduct(int billId)
        {
           return _context.BillOfMaterials.Where(b => b.BillOfMaterialsId == billId).Select(b => b.ProductAssembly).AsQueryable();
        }

        public IQueryable<UnitMeasure> GetUnitMeasure(int billId)
        {
            return _context.BillOfMaterials.Where(b => b.BillOfMaterialsId == billId).Select(b => b.QuantityUnit).AsQueryable();
        }
    }
}
