﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ITransactionHistoryService
    {
        IQueryable<TransactionHistory> GetTransactionHistories();

        IQueryable<TransactionHistory> GetTransactionHistory(int transactionId);

        IQueryable<Product> GetProduct(int transactionId);
    }

    public class TransactionHistoryService : ITransactionHistoryService
    {
        private readonly AdventureWorksContext _context;

        public TransactionHistoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Product> GetProduct(int transactionId)
        {
            return _context.TransactionHistories.Where(t => t.TransactionId == transactionId).Select(t => t.Product).AsQueryable();
        }

        public IQueryable<TransactionHistory> GetTransactionHistories()
        {
            return _context.TransactionHistories.ToList().AsQueryable();
        }

        public IQueryable<TransactionHistory> GetTransactionHistory(int transactionId)
        {
            return _context.TransactionHistories.Where(t => t.TransactionId == transactionId).AsQueryable();
        }
    }
}
