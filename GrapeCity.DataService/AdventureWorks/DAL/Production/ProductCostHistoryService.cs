﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductCostHistoryService
    {
        IQueryable<ProductCostHistory> GetProductCostHistories();

        IQueryable<ProductCostHistory> GetProductCostHistories(int productId, DateTime? startDate);

        IQueryable<Product> GetProduct(int productId);

        IQueryable<Product> GetProduct(int productId, DateTime startDate);
    }

    public class ProductCostHistoryService : IProductCostHistoryService
    {
        private readonly AdventureWorksContext _context;

        public ProductCostHistoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<ProductCostHistory> GetProductCostHistories()
        {
            return _context.ProductCostHistories.ToList().AsQueryable();
        }

        public IQueryable<ProductCostHistory> GetProductCostHistories(int productId, DateTime? startDate)
        {
            var result = _context.ProductCostHistories.Where(p => p.ProductId == productId).ToList().AsQueryable();

            if(startDate.HasValue)
            {
                result = result.Where(p => p.StartDate >= startDate);
            }

            return result.AsQueryable();
        }

        public IQueryable<Product> GetProduct(int productId)
        {
            return _context.ProductCostHistories.Where(p => p.ProductId == productId).Select(p => p.Product).AsQueryable();
        }

        public IQueryable<Product> GetProduct(int productId, DateTime startDate)
        {
            return _context.ProductCostHistories.Where(p => p.ProductId == productId &&  p.StartDate == startDate).Select(p => p.Product).AsQueryable();
        }
    }
}
