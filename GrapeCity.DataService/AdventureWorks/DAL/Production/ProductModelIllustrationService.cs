﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductModelIllustrationService
    {
        IQueryable<ProductModelIllustration> GetProductModelIllustrations();

        IQueryable<Illustration> GetIllustration(int modelId, int illustrationId);

        IQueryable<ProductModel> GetProductModel(int modelId, int illustrationId);
    }

    public class ProductModelIllustrationService : IProductModelIllustrationService
    {
        private readonly AdventureWorksContext _context;

        public ProductModelIllustrationService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Illustration> GetIllustration(int modelId, int illustrationId)
        {
            return _context.ProductModelIllustrations.Where(p => p.ProductModelId == modelId && p.IllustrationId == illustrationId).Select(p => p.Illustration).AsQueryable();
        }

        public IQueryable<ProductModel> GetProductModel(int modelId, int illustrationId)
        {
            return _context.ProductModelIllustrations.Where(p => p.ProductModelId == modelId && p.IllustrationId == illustrationId).Select(p => p.ProductModel).AsQueryable();
        }

        public IQueryable<ProductModelIllustration> GetProductModelIllustrations()
        {
            return _context.ProductModelIllustrations.ToList().AsQueryable();
        }
    }
}
