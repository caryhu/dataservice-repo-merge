﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductListPriceHistoryService
    {
        IQueryable<ProductListPriceHistory> GetProductListPriceHistories();

        IQueryable<ProductListPriceHistory> GetProductListPriceHistories(int productId, DateTime? startDate);

        IQueryable<Product> GetProduct(int productId);

        IQueryable<Product> GetProduct(int productId, DateTime startDate);
    }

    public class ProductListPriceHistoryService : IProductListPriceHistoryService
    {
        private readonly AdventureWorksContext _context;

        public ProductListPriceHistoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Product> GetProduct(int productId)
        {
            return _context.ProductListPriceHistories.Where(p => p.ProductId == productId).Select(p => p.Product).AsQueryable();
        }

        public IQueryable<Product> GetProduct(int productId, DateTime startDate)
        {
            return _context.ProductListPriceHistories.Where(p => p.ProductId == productId && p.StartDate == startDate).Select(p => p.Product).AsQueryable();
        }

        public IQueryable<ProductListPriceHistory> GetProductListPriceHistories()
        {
            return _context.ProductListPriceHistories.ToList().AsQueryable();
        }

        public IQueryable<ProductListPriceHistory> GetProductListPriceHistories(int productId, DateTime? startDate)
        {
            var result = _context.ProductListPriceHistories.Where(p => p.ProductId == productId);

            if(startDate.HasValue)
            {
                result = result.Where(p => p.StartDate >= startDate);
            }

            return result;
        }
    }
}
