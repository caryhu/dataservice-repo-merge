﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductSubcategoryService
    {
        IQueryable<ProductSubcategory> GetProductSubcategories();

        IQueryable<ProductSubcategory> GetProductSubcategory(int subcategoryId);

        IQueryable<Product> GetProducts(int subcategoryId);

        IQueryable<ProductCategory> GetCategory(int subcategoryId);
    }

    public class ProductSubcategoryService : IProductSubcategoryService
    {
        private readonly AdventureWorksContext _context;

        public ProductSubcategoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<ProductCategory> GetCategory(int subcategoryId)
        {
            return _context.ProductSubcategories.Where(s => s.ProductSubcategoryId == subcategoryId).Select(s => s.ProductCategory).AsQueryable();
        }

        public IQueryable<Product> GetProducts(int subcategoryId)
        {
            return _context.Products.Where(p => p.ProductSubcategoryId == subcategoryId).AsQueryable();
        }

        public IQueryable<ProductSubcategory> GetProductSubcategories()
        {
            return _context.ProductSubcategories.ToList().AsQueryable();
        }

        public IQueryable<ProductSubcategory> GetProductSubcategory(int subcategoryId)
        {
            return _context.ProductSubcategories.Where(s => s.ProductSubcategoryId == subcategoryId).AsQueryable();
        }
    }
}
