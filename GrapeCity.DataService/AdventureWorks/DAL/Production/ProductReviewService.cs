﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductReviewService
    {
        IQueryable<ProductReview> GetProductReviews();

        IQueryable<ProductReview> GetProductReview(int reviewId);

        IQueryable<Product> GetProduct(int reviewId);
    }

    public class ProductReviewService : IProductReviewService
    {
        private readonly AdventureWorksContext _context;

        public ProductReviewService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Product> GetProduct(int reviewId)
        {
            return _context.ProductReviews.Where(r => r.ProductReviewId == reviewId).Select(r => r.Product).AsQueryable();
        }

        public IQueryable<ProductReview> GetProductReview(int reviewId)
        {
            return _context.ProductReviews.Where(r => r.ProductReviewId == reviewId).AsQueryable();
        }

        public IQueryable<ProductReview> GetProductReviews()
        {
            return _context.ProductReviews.ToList().AsQueryable();
        }
    }
}
