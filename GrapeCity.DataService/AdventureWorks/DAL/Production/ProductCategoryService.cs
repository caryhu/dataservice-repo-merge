﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductCategoryService
    {
        IQueryable<ProductCategory> GetProductCategories();

        IQueryable<ProductCategory> GetProductCategory(int categoryId);

        IQueryable<ProductSubcategory> GetProductSubcategories(int categoryId);

        IQueryable<Product> GetProducts(int categoryId);

        IQueryable<Product> GetSubcategoriesProduct(int categoryId, int subcategoryId);
    }

    public class ProductCategoryService : IProductCategoryService
    {
        private readonly AdventureWorksContext _context;

        public ProductCategoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<ProductCategory> GetProductCategories()
        {
            return _context.ProductCategories.ToList().AsQueryable();
        }

        public IQueryable<ProductCategory> GetProductCategory(int categoryId)
        {
            return _context.ProductCategories.Where(p => p.ProductCategoryId == categoryId).AsQueryable();
        }

        public IQueryable<Product> GetProducts(int categoryId)
        {
            return _context.Products.Where(p => p.ProductSubcategory.ProductCategoryId == categoryId).ToList().AsQueryable();
        }

        public IQueryable<ProductSubcategory> GetProductSubcategories(int categoryId)
        {
            return _context.ProductSubcategories.Where(p => p.ProductCategoryId == categoryId).ToList().AsQueryable();
        }

        public IQueryable<Product> GetSubcategoriesProduct(int categoryId, int subcategoryId)
        {
           return _context.Products.Where(p => p.ProductSubcategoryId == subcategoryId && p.ProductSubcategory.ProductCategoryId == categoryId).ToList().AsQueryable();
        }
    }
}
