﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ILocationService
    {
        IQueryable<Location> GetLocations();

        IQueryable<Location> GetLocation(short locationId);

        IQueryable<ProductInventory> GetProductInventories(short locationId);

        IQueryable<WorkOrderRouting> GetWorkOrderRoutings(short locationId);
    }

    public class LocationService : ILocationService
    {
        private readonly AdventureWorksContext _context;

        public LocationService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Location> GetLocation(short locationId)
        {
            return _context.Locations.Where(l => l.LocationId == locationId).AsQueryable();
        }

        public IQueryable<Location> GetLocations()
        {
            return _context.Locations.ToList().AsQueryable();
        }

        public IQueryable<ProductInventory> GetProductInventories(short locationId)
        {
            return _context.ProductInventories.Where(p => p.LocationId == locationId).ToList().AsQueryable();
        }

        public IQueryable<WorkOrderRouting> GetWorkOrderRoutings(short locationId)
        {
            return _context.WorkOrderRoutings.Where(w => w.LocationId == locationId).ToList().AsQueryable();
        }
    }
}
