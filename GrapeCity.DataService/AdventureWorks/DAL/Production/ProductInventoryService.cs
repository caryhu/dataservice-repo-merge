﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductInventoryService
    {
        IQueryable<ProductInventory> GetProductInventories(int? productId, short? locationId);

        IQueryable<Product> GetProduct(int productId, short locationId);

        IQueryable<Location> GetLocation(int productId, short locationId);
    }

    public class ProductInventoryService : IProductInventoryService
    {
        private readonly AdventureWorksContext _context;

        public ProductInventoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Location> GetLocation(int productId, short locationId)
        {
            return _context.ProductInventories.Where(p => p.ProductId == productId && p.LocationId == locationId).Select(p => p.Location).AsQueryable();
        }

        public IQueryable<Product> GetProduct(int productId, short locationId)
        {
            return _context.ProductInventories.Where(p => p.ProductId == productId && p.LocationId == locationId).Select(p => p.Product).AsQueryable();
        }

        public IQueryable<ProductInventory> GetProductInventories(int? productId, short? locationId)
        {
            var result = _context.ProductInventories.ToList().AsQueryable();

            if(productId.HasValue)
            {
                result = result.Where(p => p.ProductId == productId);
            }

            if(locationId.HasValue)
            {
                result = result.Where(p => p.LocationId == locationId);
            }

            return result;
        }
    }
}
