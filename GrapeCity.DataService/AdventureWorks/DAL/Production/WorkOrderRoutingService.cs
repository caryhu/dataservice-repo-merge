﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IWorkOrderRoutingService
    {
        IQueryable<WorkOrderRouting> GetWorkOrderRoutings(int? workOrderId, int? productId, short? operationSequence);

        IQueryable<Location> GetLocation(int workOrderId, int productId, short operationSequence);

        IQueryable<WorkOrder> GetWorkOrder(int workOrderId, int productId, short operationSequence);
    }

    public class WorkOrderRoutingService : IWorkOrderRoutingService
    {
        private readonly AdventureWorksContext _context;

        public WorkOrderRoutingService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Location> GetLocation(int workOrderId, int productId, short operationSequence)
        {
            return _context.WorkOrderRoutings.Where(w => w.WorkOrderId == workOrderId && w.ProductId == productId && w.OperationSequence == operationSequence)
                .Select(w => w.Location).AsQueryable();
        }

        public IQueryable<WorkOrder> GetWorkOrder(int workOrderId, int productId, short operationSequence)
        {
            return _context.WorkOrderRoutings.Where(w => w.WorkOrderId == workOrderId && w.ProductId == productId && w.OperationSequence == operationSequence)
                .Select(w => w.WorkOrder).AsQueryable();
        }

        public IQueryable<WorkOrderRouting> GetWorkOrderRoutings(int? workOrderId, int? productId, short? operationSequence)
        {
            var result = _context.WorkOrderRoutings
                        .Include(w=>w.Location)
                        .ToList().AsQueryable();

            if(workOrderId.HasValue && productId.HasValue && operationSequence.HasValue)
            {
                result = result.Where(w => w.WorkOrderId == workOrderId && w.ProductId == productId && w.OperationSequence == operationSequence).AsQueryable();
            }

            return result;
        }
    }
}
