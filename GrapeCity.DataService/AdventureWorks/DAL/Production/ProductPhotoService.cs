﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductPhotoService
    {
        IQueryable<ProductPhoto> GetProductPhotos();

        IQueryable<ProductPhoto> GetProductPhoto(int photoId);
    }

    public class ProductPhotoService : IProductPhotoService
    {
        private readonly AdventureWorksContext _context;

        public ProductPhotoService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<ProductPhoto> GetProductPhoto(int photoId)
        {
            return _context.ProductPhotos.Where(p => p.ProductPhotoId == photoId).AsQueryable();
        }

        public IQueryable<ProductPhoto> GetProductPhotos()
        {
            return _context.ProductPhotos.ToList().AsQueryable();
        }
    }
}
