﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IProductDescriptionService
    {
        IQueryable<ProductDescription> GetProductDescriptions();

        IQueryable<ProductDescription> GetProductDescription(int descriptionId);
    }

    public class ProductDescriptionService : IProductDescriptionService
    {
        private readonly AdventureWorksContext _context;

        public ProductDescriptionService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<ProductDescription> GetProductDescription(int descriptionId)
        {
            return _context.ProductDescriptions.Where(p => p.ProductDescriptionId == descriptionId).AsQueryable();
        }

        public IQueryable<ProductDescription> GetProductDescriptions()
        {
            return _context.ProductDescriptions.ToList().AsQueryable();
        }
    }
}
