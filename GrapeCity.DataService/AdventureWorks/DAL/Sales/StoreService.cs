﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IStoreService
    {
        IQueryable<Store> GetStores();

        IQueryable<Store> GetStore(int storeId);

        IQueryable<SalesPerson> GetSalesPerson(int storeId);

        IQueryable<Customer> GetCustomers(int storeId);

        IQueryable<BusinessEntity> GetBusinessEntity(int storeId);
    }

    public class StoreService : IStoreService
    {
        private readonly AdventureWorksContext _context;

        public StoreService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<BusinessEntity> GetBusinessEntity(int storeId)
        {
            return _context.Stores.Where(s => s.BusinessEntityId == storeId).Select(s => s.BusinessEntity).AsQueryable();
        }

        public IQueryable<Customer> GetCustomers(int storeId)
        {
            return _context.Customers.Where(c => c.StoreId == storeId).ToList().AsQueryable();
        }

        public IQueryable<SalesPerson> GetSalesPerson(int storeId)
        {
            return _context.Stores.Where(s => s.BusinessEntityId == storeId).Select(s => s.SalesPerson).AsQueryable();
        }

        public IQueryable<Store> GetStore(int storeId)
        {
            return _context.Stores.Where(s => s.BusinessEntityId == storeId).AsQueryable();
        }

        public IQueryable<Store> GetStores()
        {
            return _context.Stores.ToList().AsQueryable();
        }
    }
}
