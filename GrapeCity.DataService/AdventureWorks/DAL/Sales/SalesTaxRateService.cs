﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesTaxRateService
    {
        IQueryable<SalesTaxRate> GetSalesTaxRates();

        IQueryable<SalesTaxRate> GetSalesTaxRate(int taxRateId);

        IQueryable<StateProvince> GetStateProvince(int taxRateId);
    }

    public class SalesTaxRateService : ISalesTaxRateService
    {
        private readonly AdventureWorksContext _context;

        public SalesTaxRateService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<SalesTaxRate> GetSalesTaxRate(int taxRateId)
        {
            return _context.SalesTaxRates.Where(s => s.SalesTaxRateId == taxRateId).AsQueryable();
        }

        public IQueryable<SalesTaxRate> GetSalesTaxRates()
        {
            return _context.SalesTaxRates.ToList().AsQueryable();
        }

        public IQueryable<StateProvince> GetStateProvince(int taxRateId)
        {
            return _context.SalesTaxRates.Where(s => s.SalesTaxRateId == taxRateId).Select(s => s.StateProvince).AsQueryable();
        }
    }
}
