﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesTerritoryService
    {
        IQueryable<SalesTerritory> GetSalesTerritories();

        IQueryable<SalesTerritory> GetSalesTerritory(int territoryId);

        IQueryable<CountryRegion> GetCountry(int territoryId);

        IQueryable<Customer> GetCustomers(int territoryId);

        IQueryable<SalesOrderHeader> GetSalesOrders(int territoryId);

        IQueryable<SalesPerson> GetSalesPeople(int territoryId);

        IQueryable<SalesTerritoryHistory> GetSalesTerritoryHistories(int territoryId);

        IQueryable<StateProvince> GetStateProvinces(int territoryId);
    }

    public class SalesTerritoryService : ISalesTerritoryService
    {
        private readonly AdventureWorksContext _context;

        public SalesTerritoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<CountryRegion> GetCountry(int territoryId)
        {
            return _context.SalesTerritories.Where(s => s.TerritoryId == territoryId).Select(s => s.Country).AsQueryable();
        }

        public IQueryable<Customer> GetCustomers(int territoryId)
        {
            return _context.Customers.Where(c => c.TerritoryId == territoryId).ToList().AsQueryable();
        }

        public IQueryable<SalesOrderHeader> GetSalesOrders(int territoryId)
        {
            return _context.SalesOrderHeaders.Where(s => s.TerritoryId == territoryId).ToList().AsQueryable();
        }

        public IQueryable<SalesPerson> GetSalesPeople(int territoryId)
        {
            return _context.SalesPeople.Where(s => s.TerritoryId == territoryId).ToList().AsQueryable();
        }

        public IQueryable<SalesTerritory> GetSalesTerritories()
        {
            return _context.SalesTerritories.ToList().AsQueryable();
        }

        public IQueryable<SalesTerritory> GetSalesTerritory(int territoryId)
        {
            return _context.SalesTerritories.Where(s => s.TerritoryId == territoryId).AsQueryable();
        }

        public IQueryable<SalesTerritoryHistory> GetSalesTerritoryHistories(int territoryId)
        {
            return _context.SalesTerritoryHistories.Where(s => s.TerritoryId == territoryId).ToList().AsQueryable();
        }

        public IQueryable<StateProvince> GetStateProvinces(int territoryId)
        {
            return _context.StateProvinces.Where(s => s.TerritoryId == territoryId).ToList().AsQueryable();
        }
    }
}
