﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ICurrencyService
    {
        IQueryable<Currency> GetCurrencies();
        IQueryable<Currency> GetCurrency(string code);
        IQueryable<CountryRegion> GetCountriesWithCourrency(string currencyCode);
        IQueryable<CurrencyRate> GetCurrnecyConversionRates(string fromCurrencyCode);
    }
    public class CurrencyService : ICurrencyService
    {
        readonly AdventureWorksContext _context;
        public CurrencyService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Currency> GetCurrencies()
        {
            return _context.Currencies;
        }

        public IQueryable<Currency> GetCurrency(string code)
        {
            code = code?.ToUpperInvariant();
            return _context.Currencies.Where(x => x.CurrencyCode == code);
        }

        public IQueryable<CurrencyRate> GetCurrnecyConversionRates(string fromCurrencyCode)
        {
            fromCurrencyCode = fromCurrencyCode?.ToUpperInvariant();
            return _context.CurrencyRates.Where(x => x.FromCurrencyCode == fromCurrencyCode).ToList().AsQueryable();
        }

        public IQueryable<CountryRegion> GetCountriesWithCourrency(string currencyCode)
        {
            currencyCode = currencyCode?.ToUpperInvariant();
            return _context.CountryRegionCurrencies.Where(x => x.CurrencyCode == currencyCode)
                .Select(x=>x.Country);
        }
    }
}
