﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface IShoppingCartItemService
    {
        IQueryable<ShoppingCartItem> GetShoppingCartItems();

        IQueryable<ShoppingCartItem> GetShoppingCartItemByItemId(int cartItemId);

        IQueryable<ShoppingCartItem> GetShoppingCartItemByCartId(string cartId);

        IQueryable<Product> GetProduct(int cartItemId);
    }

    public class ShoppingCartItemService : IShoppingCartItemService
    {
        private readonly AdventureWorksContext _context;

        public ShoppingCartItemService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Product> GetProduct(int cartItemId)
        {
            return _context.ShoppingCartItems.Where(s => s.ShoppingCartItemId == cartItemId).Select(s => s.Product).AsQueryable();
        }

        public IQueryable<ShoppingCartItem> GetShoppingCartItemByCartId(string cartId)
        {
            return _context.ShoppingCartItems.Where(s => s.ShoppingCartId == cartId).AsQueryable();
        }

        public IQueryable<ShoppingCartItem> GetShoppingCartItemByItemId(int cartItemId)
        {
            return _context.ShoppingCartItems.Where(s => s.ShoppingCartItemId == cartItemId).AsQueryable();
        }

        public IQueryable<ShoppingCartItem> GetShoppingCartItems()
        {
            return _context.ShoppingCartItems.ToList().AsQueryable();
        }
    }
}
