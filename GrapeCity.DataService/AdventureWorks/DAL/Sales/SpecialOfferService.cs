﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISpecialOfferService
    {
        IQueryable<SpecialOffer> GetSpecialOffers();

        IQueryable<SpecialOffer> GetSpecialOffer(int offerId);

        IQueryable<Product> GetProducts(int offerId);
    }

    public class SpecialOfferService : ISpecialOfferService
    {
        private readonly AdventureWorksContext _context;

        public SpecialOfferService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<Product> GetProducts(int offerId)
        {
            return _context.SpecialOfferProducts.Where(s => s.SpecialOfferId == offerId)
                .Select(s => s.Product).ToList().AsQueryable();
        }

        public IQueryable<SpecialOffer> GetSpecialOffer(int offerId)
        {
            return _context.SpecialOffers.Where(s => s.SpecialOfferId == offerId).AsQueryable();
        }

        public IQueryable<SpecialOffer> GetSpecialOffers()
        {
            return _context.SpecialOffers.ToList().AsQueryable();
        }
    }
}
