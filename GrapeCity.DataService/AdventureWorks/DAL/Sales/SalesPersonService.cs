﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesPersonService
    {
        IQueryable<SalesPerson> GetSalesPersons();
        IQueryable<SalesPerson> GetSalesPerson(int id);
        IQueryable<Employee> GetEmployee(int salesPersonId);
        IQueryable<SalesTerritory> GetSalesTerritory(int salesPersonId);
        IQueryable<SalesOrderHeader> GetSalesOrders(int salesPersonId);
        IQueryable<SalesPersonQuotaHistory> GetQuotaHistories(int salesPersonId);
        IQueryable<SalesTerritoryHistory> GetSalesTerritoryHistories(int salesPersonId);
        IQueryable<Store> GetStores(int salesPersonId);
    }

    public class SalesPersonService : ISalesPersonService
    {
        private readonly AdventureWorksContext _context;

        public SalesPersonService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<SalesPerson> GetSalesPersons()
        {
            return _context.SalesPeople.ToList().AsQueryable();
        }
        public IQueryable<SalesPerson> GetSalesPerson(int id)
        {
            return _context.SalesPeople.Where(x => x.BusinessEntityId == id);
        }
        public IQueryable<Employee> GetEmployee(int salesPersonId)
        {
            return _context.SalesPeople.Where(x => x.BusinessEntityId == salesPersonId).
                Select(x => x.BusinessEntity);
        }
        public IQueryable<SalesTerritory> GetSalesTerritory(int salesPersonId)
        {
            return _context.SalesPeople.Where(x => x.BusinessEntityId == salesPersonId).
                Select(x => x.Territory);
        }
        public IQueryable<SalesOrderHeader> GetSalesOrders(int salesPersonId)
        {
            return _context.SalesPeople.Where(x => x.BusinessEntityId == salesPersonId).
                SelectMany(x => x.SalesOrderHeaders);
        }
        public IQueryable<SalesPersonQuotaHistory> GetQuotaHistories(int salesPersonId)
        {
            return _context.SalesPersonQuotaHistories.Where(x => x.BusinessEntityId == salesPersonId);
        }
        public IQueryable<SalesTerritoryHistory> GetSalesTerritoryHistories(int salesPersonId)
        {
            return _context.SalesPeople.Where(x => x.BusinessEntityId == salesPersonId).
                SelectMany(x => x.SalesTerritoryHistories);
        }
        public IQueryable<Store> GetStores(int salesPersonId)
        {
            return _context.SalesPeople.Where(x => x.BusinessEntityId == salesPersonId).
                SelectMany(x => x.Stores);
        }
    }
}
