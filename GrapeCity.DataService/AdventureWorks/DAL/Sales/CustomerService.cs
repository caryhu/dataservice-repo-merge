﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ICustomerService
    {
        IQueryable<Customer> GetCustomers();
        IQueryable<Customer> GetCustomer(int id);
        IQueryable<Person> GetCustomerPersonDetails(int customerId);
        IQueryable<Store> GetCustomerStore(int customerId);
        IQueryable<SalesTerritory> GetCustomerTerritory(int customerId);
        IQueryable<SalesOrderHeader> GetSalesOrdersPlacedByCustomer(int customerId);
    }
    public class CustomerService : ICustomerService
    {
        readonly AdventureWorksContext _context;
        public CustomerService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<Customer> GetCustomers()
        {
            return _context.Customers.ToList().AsQueryable();
        }
        public IQueryable<Customer> GetCustomer(int id)
        {
            return _context.Customers.Where(x => x.CustomerId == id);
        }
        public IQueryable<Person> GetCustomerPersonDetails(int customerId)
        {
            return _context.Customers.Where(x => x.CustomerId == customerId).Select(x => x.Person);
        }

        public IQueryable<Store> GetCustomerStore(int customerId)
        {
            return _context.Customers.Where(x => x.CustomerId == customerId).Select(x => x.Store);
        }

        public IQueryable<SalesTerritory> GetCustomerTerritory(int customerId)
        {
            return _context.Customers.Where(x => x.CustomerId == customerId).Select(x => x.Territory);
        }
        public IQueryable<SalesOrderHeader> GetSalesOrdersPlacedByCustomer(int customerId)
        {
            return _context.Customers.Where(x => x.CustomerId == customerId).SelectMany(x => x.SalesOrderHeaders);
        }
    }
}
