﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesOrderHeaderService
    {
        IQueryable<SalesOrderHeader> GetSalesOrders();
        IQueryable<SalesOrderHeader> GetSalesOrder(int id);
        IQueryable<Address> GetBillingAddress(int orderId);
        IQueryable<Address> GetShippingAddress(int orderId);
        IQueryable<CreditCard> GetCreditCardUsedForOrder(int orderId);
        IQueryable<CurrencyRate> GetCurrencyRateAppliedOnOrder(int orderId);
        IQueryable<Customer> GetCustomerOfOrder(int orderId);
        IQueryable<SalesPerson> GetSalesPerson(int orderId);
        IQueryable<ShipMethod> GetShippingMethod(int orderId);
        IQueryable<SalesTerritory> GetSalesOrderTerritory(int orderId);
        IQueryable<SalesOrderDetail> GetSalesOrderDetails(int orderId);
        IQueryable<SalesReason> GetSalesReasons(int orderId);
    }

    public class SalesOrderHeaderService : ISalesOrderHeaderService
    {
        private readonly AdventureWorksContext _context;

        public SalesOrderHeaderService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<SalesOrderHeader> GetSalesOrders()
        {
            return _context.SalesOrderHeaders
                           .Include(s=>s.BillToAddress)
                           .Include(s=>s.ShipToAddress)
                           .Include(s=>s.Territory)
                           .Include(s=>s.ShipMethod)
                           .ToList().AsQueryable();
        }
        public IQueryable<SalesOrderHeader> GetSalesOrder(int id)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == id);
        }
        public IQueryable<Address> GetBillingAddress(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.BillToAddress);
        }
        public IQueryable<Address> GetShippingAddress(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.ShipToAddress);

        }
        public IQueryable<CreditCard> GetCreditCardUsedForOrder(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.CreditCard);

        }
        public IQueryable<CurrencyRate> GetCurrencyRateAppliedOnOrder(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.CurrencyRate);
        }
        public IQueryable<Customer> GetCustomerOfOrder(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.Customer);
        }
        public IQueryable<SalesPerson> GetSalesPerson(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.SalesPerson);
        }
        public IQueryable<ShipMethod> GetShippingMethod(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.ShipMethod);
        }
        public IQueryable<SalesTerritory> GetSalesOrderTerritory(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                Select(x => x.Territory);
        }
        public IQueryable<SalesOrderDetail> GetSalesOrderDetails(int orderId)
        {
            return _context.SalesOrderHeaders.Where(x => x.SalesOrderId == orderId).
                SelectMany(x => x.SalesOrderDetails);
        }
        public IQueryable<SalesReason> GetSalesReasons(int orderId)
        {
            return _context.SalesOrderHeaderSalesReasons.Where(x => x.SalesOrderId == orderId).
                Select(x => x.SalesReason);
        }
    }
}
