﻿using GrapeCity.DataService.AdventureWorks.Models;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesReasonService
    {
        IQueryable<SalesReason> GetSalesReasons();

        IQueryable<SalesReason> GetSalesReason(int salesReasonId);

        IQueryable<SalesOrderHeader> GetSalesOrders(int salesReasonId);
    }

    public class SalesReasonService : ISalesReasonService
    {
        private readonly AdventureWorksContext _context;

        public SalesReasonService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<SalesOrderHeader> GetSalesOrders(int salesReasonId)
        {
            return _context.SalesOrderHeaderSalesReasons.Where(s => s.SalesReasonId == salesReasonId).Select(s => s.SalesOrder).ToList().AsQueryable();
        }

        public IQueryable<SalesReason> GetSalesReason(int salesReasonId)
        {
            return _context.SalesReasons.Where(s => s.SalesReasonId == salesReasonId).AsQueryable();
        }

        public IQueryable<SalesReason> GetSalesReasons()
        {
            return _context.SalesReasons.ToList().AsQueryable();
        }
    }
}
