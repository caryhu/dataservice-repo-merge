﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesPersonQuotaHistoryService
    {
        IQueryable<SalesPersonQuotaHistory> GetSalesPersonQuotaHistories(int? salesPersonId, DateTime? quotaDate);
    }

    public class SalesPersonQuotaHistoryService : ISalesPersonQuotaHistoryService
    {
        private readonly AdventureWorksContext _context;

        public SalesPersonQuotaHistoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<SalesPersonQuotaHistory> GetSalesPersonQuotaHistories(int? salesPersonId, DateTime? quotaDate)
        {
            var result = _context.SalesPersonQuotaHistories.ToList().AsQueryable();

            if (salesPersonId.HasValue)
                result = result.Where(s => s.BusinessEntityId == salesPersonId);
            if (quotaDate.HasValue)
                result = result.Where(s => s.QuotaDate <= quotaDate);

            return result;
        }
    }
}
