﻿using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesOrderDetailService
    {
        IQueryable<SalesOrderDetail> GetSalesOrderDetails();
        IQueryable<SalesOrderDetail> GetSalesOrderDetail(int id);
        IQueryable<SalesOrderDetail> GetSalesOrderDetail(int orderId, int orderDetailId);
        IQueryable<SalesOrderHeader> GetSalesOrder(int orderDetailId);
        IQueryable<SalesOrderHeader> GetSalesOrder(int orderId, int orderDetailId);
        IQueryable<SpecialOffer> GetSpecialOfferWithOrder(int orderDetailId);
    }

    public class SalesOrderDetailService : ISalesOrderDetailService
    {
        private readonly AdventureWorksContext _context;

        public SalesOrderDetailService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<SalesOrderDetail> GetSalesOrderDetails()
        {
            return _context.SalesOrderDetails.Include(x=>x.SpecialOfferProduct.Product).ToList().AsQueryable();
        }
        public IQueryable<SalesOrderDetail> GetSalesOrderDetail(int id)
        {
            return _context.SalesOrderDetails.Where(x => x.SalesOrderDetailId == id);
        }
        public IQueryable<SalesOrderDetail> GetSalesOrderDetail(int orderId, int orderDetailId)
        {
            return _context.SalesOrderDetails.Where(x => x.SalesOrderId == orderId && x.SalesOrderDetailId == orderDetailId);
        }
        public IQueryable<SalesOrderHeader> GetSalesOrder(int orderDetailId)
        {
            return _context.SalesOrderDetails.Where(x => x.SalesOrderDetailId == orderDetailId).
                Select(x => x.SalesOrder);
        }
        public IQueryable<SalesOrderHeader> GetSalesOrder(int orderId, int orderDetailId)
        {
            return GetSalesOrderDetail(orderId, orderDetailId).Select(x => x.SalesOrder);
        }
        public IQueryable<SpecialOffer> GetSpecialOfferWithOrder(int orderDetailId)
        {
            return _context.SalesOrderDetails.Where(x => x.SalesOrderDetailId == orderDetailId).
                Select(x => x.SpecialOfferProduct.SpecialOffer);
        }
    }
}
