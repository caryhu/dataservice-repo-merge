﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Linq;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ISalesTerritoryHistoryService
    {
        IQueryable<SalesTerritoryHistory> GetSalesTerritoryHistories(int? salesPersonid, int? territoryId, DateTime? startDate);
    }

    public class SalesTerritoryHistoryService : ISalesTerritoryHistoryService
    {
        private readonly AdventureWorksContext _context;

        public SalesTerritoryHistoryService(AdventureWorksContext context)
        {
            _context = context;
        }

        public IQueryable<SalesTerritoryHistory> GetSalesTerritoryHistories(int? salesPersonid, int? territoryId, DateTime? startDate)
        {
            var result = _context.SalesTerritoryHistories.ToList().AsQueryable();

            if (salesPersonid.HasValue)
                result = result.Where(s => s.BusinessEntityId == salesPersonid);
            if (territoryId.HasValue)
                result = result.Where(s => s.TerritoryId == territoryId);
            if (startDate.HasValue)
                result = result.Where(s => s.StartDate >= startDate);
            
            return result;
        }
    }
}
