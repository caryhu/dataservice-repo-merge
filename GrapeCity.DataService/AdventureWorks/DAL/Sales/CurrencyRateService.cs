﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ICurrencyRateService
    {
        IQueryable<CurrencyRate> GetCurrencyRates(string fromCurrencyCode, string toCurrencyCode);
        IQueryable<CurrencyRate> GetCurrencyRate(int id);
        IQueryable<SalesOrderHeader> GetSalesOrdersWithCurrencyRate(int currencyRateId);
    }
    public class CurrencyRateService: ICurrencyRateService
    {
        readonly AdventureWorksContext _context;
        public CurrencyRateService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<CurrencyRate> GetCurrencyRates(string fromCurrencyCode, string toCurrencyCode)
        {
            var result = _context.CurrencyRates.ToList();
            fromCurrencyCode = fromCurrencyCode?.ToUpperInvariant();
            toCurrencyCode = toCurrencyCode?.ToUpperInvariant();

            if (!string.IsNullOrEmpty(fromCurrencyCode))
                result = result.Where(x => x.FromCurrencyCode == fromCurrencyCode).ToList();

            if (!string.IsNullOrEmpty(toCurrencyCode))
                result = result.Where(x => x.ToCurrencyCode == toCurrencyCode).ToList();

            return result.AsQueryable();
        }
        public IQueryable<CurrencyRate> GetCurrencyRate(int id)
        {
            return _context.CurrencyRates.Where(x => x.CurrencyRateId == id);
        }
        public IQueryable<SalesOrderHeader> GetSalesOrdersWithCurrencyRate(int currencyRateId)
        {
            return _context.CurrencyRates.Where(x => x.CurrencyRateId == currencyRateId).
                SelectMany(x => x.SalesOrderHeaders);
        }
    }
}
