﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.DAL
{
    public interface ICreditCardService
    {
        IQueryable<CreditCard> GetCreditCards();
        IQueryable<CreditCard> GetCreditCard(int cardId);
        IQueryable<Person> GetPersonsHavingCreditCard(int cardId);
        IQueryable<SalesOrderHeader> GetSalesOrdersPlacedWithCreditCard(int cardId);
    }
    public class CreditCardService : ICreditCardService
    {
        readonly AdventureWorksContext _context;
        public CreditCardService(AdventureWorksContext context)
        {
            _context = context;
        }
        public IQueryable<CreditCard> GetCreditCards()
        {
            return _context.CreditCards.ToList().AsQueryable();
        }
        public IQueryable<CreditCard> GetCreditCard(int cardId)
        {
            return _context.CreditCards.Where(x => x.CreditCardId == cardId);
        }
        public IQueryable<Person> GetPersonsHavingCreditCard(int cardId)
        {
            return _context.PersonCreditCards.Where(x => x.CreditCardId == cardId)
                .Select(x => x.BusinessEntity);
        }

        public IQueryable<SalesOrderHeader> GetSalesOrdersPlacedWithCreditCard(int cardId)
        {
            return _context.CreditCards.Where(x => x.CreditCardId == cardId)
                .SelectMany(x => x.SalesOrderHeaders);
        }
    }
}
