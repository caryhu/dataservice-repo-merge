﻿using GrapeCity.DataService.AdventureWorks.DAL;
using GrapeCity.DataService.AdventureWorks.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks
{
    public static class ServiceConfig
    {
        public static IServiceCollection AddAdventureWorkServices(this IServiceCollection services)
        {
            services.AddScoped<IDepartmentService, DeptartmentService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IJobCandidateService, JobCandidateService>();
            services.AddScoped<IShiftService, ShiftService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IAddressTypeService, AddressTypeService>();
            services.AddScoped<IBusinessEntityService, BusinessEntityService>();
            services.AddScoped<IContactTypeService, ContactTypeService>();
            services.AddScoped<ICountryRegionService, CountryRegionService>();
            services.AddScoped<IProductVendorService, ProductVendorService>();
            services.AddScoped<IEmailAddressService, EmailAddressService>();
            services.AddScoped<IPersonService, PersonService>();
            services.AddScoped<IPersonPhoneService, PersonPhoneService>();
            services.AddScoped<IStateProvinceService, StateProvinceService>();
            services.AddScoped<IPurchaseOrderDetailService, PurchaseOrderDetailService>();
            services.AddScoped<IPurchaseOrderHeaderService, PurchaseOrderHeaderService>();
            services.AddScoped<IShipMethodService, ShipMethodService>();
            services.AddScoped<IBillOfMaterialService, BillOfMaterialService>();
            services.AddScoped<ICultureService, CultureService>();
            services.AddScoped<IVendorService, VendorService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ILocationService, LocationService>();
            services.AddScoped<IIllustrationService, IllustrationService>();
            services.AddScoped<IProductCategoryService, ProductCategoryService>();
            services.AddScoped<IProductCostHistoryService, ProductCostHistoryService>();
            services.AddScoped<IProductModelService, ProductModelService>();
            services.AddScoped<IProductInventoryService, ProductInventoryService>();
            services.AddScoped<IProductDescriptionService, ProductDescriptionService>();
            services.AddScoped<IProductModelIllustrationService, ProductModelIllustrationService>();
            services.AddScoped<IProductListPriceHistoryService, ProductListPriceHistoryService>();
            services.AddScoped<IProductPhotoService, ProductPhotoService>();
            services.AddScoped<IProductReviewService, ProductReviewService>();
            services.AddScoped<IProductSubcategoryService, ProductSubcategoryService>();
            services.AddScoped<IUnitMeasureService, UnitMeasureService>();
            services.AddScoped<IWorkOrderRoutingService, WorkOrderRoutingService>();
            services.AddScoped<IWorkOrderService, WorkOrderService>();
            services.AddScoped<ITransactionHistoryArchiveService, TransactionHistoryArchiveService>();
            services.AddScoped<ITransactionHistoryService, TransactionHistoryService>();
            services.AddScoped<IScrapReasonService, ScrapReasonService>();
            services.AddScoped<ICurrencyService, CurrencyService>();
            services.AddScoped<ICurrencyRateService, CurrencyRateService>();
            services.AddScoped<ICreditCardService, CreditCardService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ISalesOrderDetailService, SalesOrderDetailService>();
            services.AddScoped<ISalesOrderHeaderService, SalesOrderHeaderService>();
            services.AddScoped<ISalesPersonQuotaHistoryService, SalesPersonQuotaHistoryService>();
            services.AddScoped<ISalesPersonService, SalesPersonService>();
            services.AddScoped<ISalesReasonService, SalesReasonService>();
            services.AddScoped<ISalesTaxRateService, SalesTaxRateService>();
            services.AddScoped<ISalesTerritoryHistoryService, SalesTerritoryHistoryService>();
            services.AddScoped<ISalesTerritoryService, SalesTerritoryService>();
            services.AddScoped<IShoppingCartItemService, ShoppingCartItemService>();
            services.AddScoped<ISpecialOfferService, SpecialOfferService>();
            services.AddScoped<IStoreService, StoreService>();
            return services;
        }
    }
}
