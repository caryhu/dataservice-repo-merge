﻿using GrapeCity.DataService.AdventureWorks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GrapeCity.DataService.AdventureWorks
{
    public static class DataCompletionSource
    {
        public static string Gender(string str)
        {
            switch (str)
            {
                case "M": return "Male";
                case "F": return "Female";
                default: return str;
            }
        }

        public static string MaritalStatus(string str)
        {
            switch (str)
            {
                case "M": return "Married";
                case "S": return "Single";
                default: return str;
            }
        }

        public static string PayFrequency(byte frequency)
        {
            switch (frequency)
            {
                case 1: return "Monthly";
                case 2: return "BiWeekly";
                default:  return frequency.ToString();
            }
        }
        public static string Address(Address addr)
        {
            if (addr == null)
                return string.Empty;
            var addrLine2 = string.IsNullOrEmpty(addr.AddressLine2) ? string.Empty : $"/{addr.AddressLine2}";
            return $"{addr.AddressLine1}{addrLine2}-{addr.City}, {addr.StateProvince.Name}, {addr.StateProvince.Country.Name}";
        }
        public static string Territory(SalesTerritory territory)
        {
            return territory == null ? string.Empty : $"{territory.Name} ({territory.Country.Name})";
        }
        public static string PersonType(string type)
        {
            switch (type)
            {
                case "SC": return "Store Contact";
                case "IN": return "Individual (retail) customer";
                case "SP": return "Sales person";
                case "EM": return "Employee (non-sales)";
                case "VC": return "Vendor contact";
                case "GC": return "General contact";
                default: return type;
            }
        }
        public static string NameStyle(bool style)
        {
            return style ? "Western Style(first name, last name)" : "Eastern Style(last name, first name)";
        }
        public static string EmailPromotion(int promoteFlag)
        {
            switch (promoteFlag)
            {
                case 0: return "Doesn't receive email promotions";
                case 1: return "Receive email promotions from Adventure Works";
                case 2: return "Receive email promotions from Adventure Works and selected partners";
                default: return promoteFlag.ToString();
            }
        }
        public static string TransactionType(string type)
        {
            switch (type)
            {
                case "W": return "WorkOrder";
                case "S":return "SalesOrder";
                case "P": return "PurchaseOrder";
                default: return type;
            }
        }
        public static string ProductLine(string line)
        {
            switch (line)
            {
                case "R": return "Road";
                case "M": return "Mountain";
                case "S": return "Standard";
                default: return line;
            }
        }
        public static string ProductClass(string prodClass)
        {
            switch (prodClass)
            {
                case "H": return "High";
                case "M": return "Medium";
                case "L": return "Low";
                default:return prodClass;
            }
        }

        public static string ProductStyle(string style)
        {
            switch (style)
            {
                case "W": return "Womens";
                case "M": return "Mens";
                case "U": return "Universal";
                default:  return style;
            }
        }
        public static string PurchaseOrderStatus(byte status)
        {
            switch (status)
            {
                case 1: return "Pending";
                case 2: return "Approved";
                case 3: return "Rejected";
                case 4: return "Complete";
                default:  return status.ToString();
            }
        }
        public static string Rating(byte rating)
        {
            switch (rating)
            {
                case 1: return "Superior";
                case 2: return "Excellent";
                case 3: return "Above average";
                case 4: return "Average";
                case 5: return "Below average";
                default: return rating.ToString();
            }
        }
        public static string SalesOrderStatus(byte status)
        {
            switch (status)
            {
                case 1: return "In process";
                case 2: return "Approved";
                case 3: return "Backordered";
                case 4: return "Rejected";
                case 5: return "Shipped";
                case 6: return "Cancelled";
                default: return status.ToString();
            }
        }
        public static string TaxType(byte type)
        {
            switch (type)
            {
                case 1: return "Retail transactions";
                case 2: return "Wholesale transactions";
                case 3: return "All sales transactions";
                default: return type.ToString();
            }
        }
        public static Dictionary<string, string> Demographics(string xmlDemographics)
        {
            var output = new Dictionary<string, string>();
            var xmlReader = new XmlDocument();
            xmlReader.LoadXml(xmlDemographics);
            foreach(XmlNode node in xmlReader.DocumentElement.ChildNodes)
            {
                output[node.Name] = node.InnerText;
            }
            return output;
        }
    }
}
