﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks
{
    public static class HelperExtensions
    {
        public static string RemoveAllChars(this string sourceString, IEnumerable<char> chars)
        {
            if (sourceString == null)
                return sourceString;

            foreach (char c in sourceString)
            {
                if (chars.Contains(c))
                {
                    sourceString = sourceString.Replace(c.ToString(), string.Empty);
                }
            }

            return sourceString;
        }
    }
}
