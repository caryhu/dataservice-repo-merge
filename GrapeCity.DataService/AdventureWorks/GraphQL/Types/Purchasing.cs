﻿using GrapeCity.DataService.AdventureWorks.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.GraphQL
{

    public class ProductVendorGraphType : ObjectGraphType<ProductVendor>
    {
        public ProductVendorGraphType()
        {
            Field(x => x.ProductId);
            Field(x => x.BusinessEntityId);
            Field(x => x.AverageLeadTime);
            Field(x => x.StandardPrice);
            Field(x => x.LastReceiptCost, true);
            Field(x => x.LastReceiptDate, true);
            Field(x => x.MinOrderQty);
            Field(x => x.MaxOrderQty);
            Field(x => x.OnOrderQty, true);
            Field(x => x.UnitMeasureCode);
            Field(x => x.ModifiedDate);
            Field<VendorGraphType>("BusinessEntity");
            Field<ProductGraphType>("Product");
            Field<UnitMeasureGraphType>("UnitOfMeasure");
        }
    }

    public class PurchaseOrderDetailGraphType : ObjectGraphType<PurchaseOrderDetail>
    {
        public PurchaseOrderDetailGraphType()
        {
            Field(x => x.PurchaseOrderId);
            Field(x => x.PurchaseOrderDetailId);
            Field(x => x.DueDate);
            Field(x => x.OrderQty, type: typeof(ShortGraphType));
            Field(x => x.ProductId);
            Field(x => x.UnitPrice);
            Field(x => x.LineTotal);
            Field(x => x.ReceivedQty);
            Field(x => x.StockedQty);
            Field(x => x.ModifiedDate);
            Field<PurchaseOrderHeaderGraphType>("PurchaseOrder");
            Field<ProductGraphType>("Product");
        }
    }

    public class PurchaseOrderHeaderGraphType : ObjectGraphType<PurchaseOrderHeader>
    {
        public PurchaseOrderHeaderGraphType()
        {
            Field(x => x.PurchaseOrderId);
            Field("RevisionNumber", x =>Convert.ToInt16(x.RevisionNumber), type: typeof(ShortGraphType));
            Field("Status", x => Convert.ToInt16(x.Status), type: typeof(ShortGraphType));
            Field(x => x.EmployeeId);
            Field(x => x.VendorId);
            Field(x => x.ShipMethodId);
            Field(x => x.OrderDate);
            Field(x => x.ShipDate, true);
            Field(x => x.SubTotal);
            Field(x => x.TaxAmt);
            Field(x => x.Freight);
            Field(x => x.TotalDue);
            Field(x => x.ModifiedDate);
            Field<EmployeeGraphType>("Employee");
            Field<ShipMethodGraphType>("ShipMethod");
            Field<VendorGraphType>("Vendor");
            Field<ListGraphType<PurchaseOrderDetailGraphType>>("PurchaseOrderDetails");
        }
    }

    public class ShipMethodGraphType : ObjectGraphType<ShipMethod>
    {
        public ShipMethodGraphType()
        {
            Field(x => x.ShipMethodId);
            Field(x => x.Name);
            Field(x => x.ShipBase);
            Field(x => x.ShipRate);
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<ListGraphType<PurchaseOrderHeaderGraphType>>("PurchaseOrderHeaders");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrderHeaders");
        }
    }

    public class VendorGraphType : ObjectGraphType<Vendor>
    {
        public VendorGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.PreferredVendorStatus, true);
            Field(x => x.AccountNumber);
            Field(x => x.Name);
            Field("CreditRating", x => Convert.ToInt16(x.CreditRating), type: typeof(ShortGraphType));
            Field(x => x.ActiveFlag, true);
            Field(x => x.PurchasingWebServiceUrl, true);
            Field(x => x.ModifiedDate);
            Field<BusinessEntityGraphType>("BusinessEntity");
            Field<ListGraphType<ProductVendorGraphType>>("ProductVendors");
            Field<ListGraphType<PurchaseOrderHeaderGraphType>>("PurchaseOrderHeaders");
        }
    }
}
