﻿using GrapeCity.DataService.AdventureWorks.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.GraphQL
{
    public class AddressGraphType : ObjectGraphType<Address>
    {
        public AddressGraphType()
        {
            Field(x => x.AddressId);
            Field(x => x.AddressLine1);
            Field(x => x.AddressLine2,true);
            Field(x => x.City);
            Field(x => x.StateProvinceId);
            Field(x => x.PostalCode);
            Field(x => x.ModifiedDate);
            Field<StateProvinceGraphType>("StateProvince");
            Field<ListGraphType<BusinessEntityAddressGraphType>>("BusinessEntityAddresses");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrdersBilledTo");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrdersShippedTo");
        }
    }

    public class AddressTypeGraphType : ObjectGraphType<AddressType>
    {
        public AddressTypeGraphType()
        {
            Field(x => x.AddressTypeId);
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<BusinessEntityAddressGraphType>>("BusinessEntityAddresses");
        }
    }

    public class BusinessEntityGraphType : ObjectGraphType<BusinessEntity>
    {
        public BusinessEntityGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.ModifiedDate);
            Field<PersonGraphType>("Person");
            Field<StoreGraphType>("Store");
            Field<VendorGraphType>("Vendor");
            Field<ListGraphType<BusinessEntityAddressGraphType>>("BusinessEntityAddresses");
            Field<ListGraphType<BusinessEntityContactGraphType>>("BusinessEntityContacts");
        }
    }

    public class BusinessEntityAddressGraphType : ObjectGraphType<BusinessEntityAddress>
    {
        public BusinessEntityAddressGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.AddressId);
            Field(x => x.AddressTypeId);
            Field(x => x.ModifiedDate);
            Field<AddressGraphType>("Address");
            Field<AddressTypeGraphType>("AddressType");
            Field<BusinessEntityGraphType>("BusinessEntity");
        }
    }

    public class BusinessEntityContactGraphType : ObjectGraphType<BusinessEntityContact>
    {
        public BusinessEntityContactGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.PersonId);
            Field(x => x.ContactTypeId);
            Field(x => x.ModifiedDate);
            Field<BusinessEntityGraphType>("BusinessEntity");
            Field<ContactTypeGraphType>("ContactType");
            Field<PersonGraphType>("Person");
        }
    }

    public class ContactTypeGraphType : ObjectGraphType<ContactType>
    {
        public ContactTypeGraphType()
        {
            Field(x => x.ContactTypeId);
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<BusinessEntityContactGraphType>>("BusinessEntityContacts");
        }
    }

    public class CountryRegionGraphType : ObjectGraphType<CountryRegion>
    {
        public CountryRegionGraphType()
        {
            Field(x => x.CountryRegionCode);
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<CountryRegionCurrencyGraphType>>("CountryRegionCurrencies");
            Field<ListGraphType<SalesTerritoryGraphType>>("SalesTerritories");
            Field<ListGraphType<StateProvinceGraphType>>("StateProvinces");
        }
    }

    public class EmailAddressGraphType : ObjectGraphType<EmailAddress>
    {
        public EmailAddressGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.EmailAddressId);
            Field(x => x.Email,true);
            Field(x => x.ModifiedDate);
            Field<PersonGraphType>("Person");
        }
    }

    public class PasswordGraphType : ObjectGraphType<Password>
    {
        public PasswordGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.PasswordHash);
            Field(x => x.PasswordSalt);
            Field(x => x.ModifiedDate);
            Field<PersonGraphType>("Person");
        }
    }

    public class PersonGraphType : ObjectGraphType<Person>
    {
        public PersonGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.PersonType);
            Field(x => x.NameStyle);
            Field(x => x.Title,true);
            Field(x => x.FirstName);
            Field(x => x.MiddleName, true);
            Field(x => x.LastName);
            Field(x => x.Suffix, true);
            Field(x => x.EmailPromotion);
            Field(x => x.AdditionalContactInfo, true);
            Field(x => x.Demographics,true);
            Field(x => x.ModifiedDate);
            Field<BusinessEntityGraphType>("BusinessEntity");
            Field<EmployeeGraphType>("Employee");
            Field<PasswordGraphType>("Password");
            Field<ListGraphType<BusinessEntityContactGraphType>>("BusinessEntityContacts");
            Field<ListGraphType<CustomerGraphType>>("Customers");
            Field<ListGraphType<EmailAddressGraphType>>("EmailAddresses");
            Field<ListGraphType<PersonCreditCardGraphType>>("PersonCreditCards");
            Field<ListGraphType<PersonPhoneGraphType>>("PersonPhones");
        }
    }

    public class PersonPhoneGraphType : ObjectGraphType<PersonPhone>
    {
        public PersonPhoneGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.PhoneNumber);
            Field(x => x.PhoneNumberTypeId);
            Field(x => x.ModifiedDate);
            Field<PersonGraphType>("Person");
            Field<PhoneNumberTypeGraphType>("PhoneNumberType");
        }
    }

    public class PhoneNumberTypeGraphType : ObjectGraphType<PhoneNumberType>
    {
        public PhoneNumberTypeGraphType()
        {
            Field(x => x.PhoneNumberTypeId);
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<PersonPhoneGraphType>>("PersonPhones");
        }
    }

    public class StateProvinceGraphType : ObjectGraphType<StateProvince>
    {
        public StateProvinceGraphType()
        {
            Field(x => x.StateProvinceId);
            Field(x => x.StateProvinceCode);
            Field(x => x.CountryRegionCode);
            Field(x => x.IsOnlyStateProvinceFlag,true);
            Field(x => x.Name);
            Field(x => x.TerritoryId);
            Field(x => x.ModifiedDate);
            Field<CountryRegionGraphType>("Country");
            Field<SalesTerritoryGraphType>("Territory");
            Field<ListGraphType<AddressGraphType>>("Addresses");
            Field<ListGraphType<SalesTaxRateGraphType>>("SalesTaxRates");
        }
    }
}
