﻿using GrapeCity.DataService.AdventureWorks.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.GraphQL
{
    public class CountryRegionCurrencyGraphType : ObjectGraphType<CountryRegionCurrency>
    {
        public CountryRegionCurrencyGraphType()
        {
            Field(x => x.CountryRegionCode);
            Field(x => x.CurrencyCode);
            Field(x => x.ModifiedDate);
            Field<CountryRegionGraphType>("Country");
            Field<CurrencyGraphType>("Currency");
        }
    }

    public class CreditCardGraphType : ObjectGraphType<CreditCard>
    {
        public CreditCardGraphType()
        {
            Field(x => x.CreditCardId);
            Field(x => x.CardType);
            Field(x => x.CardNumber);
            Field(x => x.ExpYear, type: typeof(ShortGraphType));
            Field("ExpMonth", x =>Convert.ToInt16(x.ExpMonth), type: typeof(ShortGraphType));
            Field(x => x.ModifiedDate);
            Field<ListGraphType<PersonCreditCardGraphType>>("PersonCreditCards");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrderHeaders");
        }
    }

    public class CurrencyGraphType : ObjectGraphType<Currency>
    {
        public CurrencyGraphType()
        {
            Field(x => x.CurrencyCode);
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<CountryRegionCurrencyGraphType>>("CountryRegionCurrencies");
            Field<ListGraphType<CurrencyRateGraphType>>("CurrencyRatesFrom");
            Field<ListGraphType<CurrencyRateGraphType>>("CurrencyRatesTo");
        }
    }

    public class CurrencyRateGraphType : ObjectGraphType<CurrencyRate>
    {
        public CurrencyRateGraphType()
        {
            Field(x => x.CurrencyRateId);
            Field(x => x.CurrencyRateDate);
            Field(x => x.FromCurrencyCode);
            Field(x => x.ToCurrencyCode);
            Field(x => x.AverageRate);
            Field(x => x.EndOfDayRate);
            Field(x => x.ModifiedDate);
            Field<CurrencyGraphType>("CurrencyFrom");
            Field<CurrencyGraphType>("CurrencyTo");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrderHeaders");
        }
    }

    public class CustomerGraphType : ObjectGraphType<Customer>
    {
        public CustomerGraphType()
        {
            Field(x => x.CustomerId);
            Field(x => x.AccountNumber);
            Field(x => x.PersonId, true);
            Field(x => x.StoreId, true);
            Field(x => x.TerritoryId, true);
            Field(x => x.ModifiedDate);
            Field<PersonGraphType>("Person");
            Field<StoreGraphType>("Store");
            Field<SalesTerritoryGraphType>("Territory");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrderHeaders");
        }
    }

    public class PersonCreditCardGraphType : ObjectGraphType<PersonCreditCard>
    {
        public PersonCreditCardGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.CreditCardId);
            Field(x => x.ModifiedDate);
            Field<PersonGraphType>("BusinessEntity");
            Field<CreditCardGraphType>("CreditCard");
        }
    }

    public class SalesOrderDetailGraphType : ObjectGraphType<SalesOrderDetail>
    {
        public SalesOrderDetailGraphType()
        {
            Field(x => x.SalesOrderDetailId);
            Field(x => x.SalesOrderId);
            Field(x => x.CarrierTrackingNumber, true);
            Field(x => x.ProductId);
            Field(x => x.OrderQty, type: typeof(ShortGraphType));
            Field(x => x.SpecialOfferId);
            Field(x => x.UnitPrice);
            Field(x => x.UnitPriceDiscount);
            Field(x => x.LineTotal);
            Field(x => x.ModifiedDate);
            Field<SalesOrderHeaderGraphType>("SalesOrder");
            Field<SpecialOfferProductGraphType>("SpecialOfferProduct");
        }
    }

    public class SalesOrderHeaderGraphType : ObjectGraphType<SalesOrderHeader>
    {
        public SalesOrderHeaderGraphType()
        {
            Field(x => x.SalesOrderId);
            Field("RevisionNumber", x => Convert.ToInt16(x.RevisionNumber), type: typeof(ShortGraphType));
            Field(x => x.OrderDate);
            Field(x => x.DueDate);
            Field(x => x.ShipDate, true);
            Field("Status", x => DataCompletionSource.SalesOrderStatus(x.Status));
            Field(x => x.OnlineOrderFlag, true);
            Field(x => x.SalesOrderNumber);
            Field(x => x.PurchaseOrderNumber, true);
            Field(x => x.AccountNumber, true);
            Field(x => x.CustomerId);
            Field(x => x.SalesPersonId, true);
            Field(x => x.TerritoryId, true);
            Field(x => x.BillToAddressId);
            Field(x => x.ShipToAddressId);
            Field(x => x.ShipMethodId);
            Field(x => x.CreditCardApprovalCode, true);
            Field(x => x.CreditCardId, true);
            Field(x => x.CurrencyRateId, true);
            Field(x => x.SubTotal);
            Field(x => x.TaxAmt);
            Field(x => x.Freight);
            Field(x => x.TotalDue);
            Field(x => x.Comment, true);
            Field(x => x.ModifiedDate);
            Field<AddressGraphType>("BillToAddress");
            Field<CreditCardGraphType>("CreditCard");
            Field<CurrencyRateGraphType>("CurrencyRate");
            Field<CustomerGraphType>("Customer");
            Field<SalesPersonGraphType>("SalesPerson");
            Field<ShipMethodGraphType>("ShipMethod");
            Field<AddressGraphType>("ShipToAddress");
            Field<SalesTerritoryGraphType>("Territory");
            Field<ListGraphType<SalesOrderDetailGraphType>>("SalesOrderDetails");
            Field<ListGraphType<SalesOrderHeaderSalesReasonGraphType>>("SalesOrderHeaderSalesReasons");
        }
    }

    public class SalesOrderHeaderSalesReasonGraphType : ObjectGraphType<SalesOrderHeaderSalesReason>
    {
        public SalesOrderHeaderSalesReasonGraphType()
        {
            Field(x => x.SalesOrderId);
            Field(x => x.SalesReasonId);
            Field(x => x.ModifiedDate);
            Field<SalesOrderHeaderGraphType>("SalesOrder");
            Field<SalesReasonGraphType>("SalesReason");
        }
    }

    public class SalesPersonGraphType : ObjectGraphType<SalesPerson>
    {
        public SalesPersonGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.TerritoryId, true);
            Field(x => x.SalesQuota, true);
            Field(x => x.Bonus);
            Field(x => x.CommissionPct);
            Field(x => x.SalesYtd);
            Field(x => x.SalesLastYear);
            Field(x => x.ModifiedDate);
            Field<EmployeeGraphType>("BusinessEntity");
            Field<SalesTerritoryGraphType>("Territory");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrderHeaders");
            Field<ListGraphType<SalesPersonQuotaHistoryGraphType>>("SalesPersonQuotaHistories");
            Field<ListGraphType<SalesTerritoryHistoryGraphType>>("SalesTerritoryHistories");
            Field<ListGraphType<StoreGraphType>>("Stores");
        }
    }

    public class SalesPersonQuotaHistoryGraphType : ObjectGraphType<SalesPersonQuotaHistory>
    {
        public SalesPersonQuotaHistoryGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.QuotaDate);
            Field(x => x.SalesQuota);
            Field(x => x.ModifiedDate);
            Field<SalesPersonGraphType>("SalesPerson");
        }
    }

    public class SalesReasonGraphType : ObjectGraphType<SalesReason>
    {
        public SalesReasonGraphType()
        {
            Field(x => x.SalesReasonId);
            Field(x => x.Name);
            Field(x => x.ReasonType);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<SalesOrderHeaderSalesReasonGraphType>>("SalesOrderHeaderSalesReasons");
        }
    }

    public class SalesTaxRateGraphType : ObjectGraphType<SalesTaxRate>
    {
        public SalesTaxRateGraphType()
        {
            Field(x => x.SalesTaxRateId);
            Field(x => x.Name);
            Field("TaxType", x => DataCompletionSource.TaxType(x.TaxType));
            Field(x => x.TaxRate);
            Field(x => x.StateProvinceId);
            Field(x => x.ModifiedDate);
            Field<StateProvinceGraphType>("StateProvince");
        }
    }

    public class SalesTerritoryGraphType : ObjectGraphType<SalesTerritory>
    {
        public SalesTerritoryGraphType()
        {
            Field(x => x.TerritoryId);
            Field(x => x.Name);
            Field(x => x.CountryRegionCode);
            Field(x => x.Group);
            Field(x => x.SalesYtd);
            Field(x => x.SalesLastYear);
            Field(x => x.CostYtd);
            Field(x => x.CostLastYear);
            Field(x => x.ModifiedDate);
            Field<CountryRegionGraphType>("Country");
            Field<ListGraphType<CustomerGraphType>>("Customers");
            Field<ListGraphType<SalesOrderHeaderGraphType>>("SalesOrderHeaders");
            Field<ListGraphType<SalesPersonGraphType>>("SalesPeople");
            Field<ListGraphType<SalesTerritoryHistoryGraphType>>("SalesTerritoryHistories");
            Field<ListGraphType<StateProvinceGraphType>>("StateProvinces");
        }
    }

    public class SalesTerritoryHistoryGraphType : ObjectGraphType<SalesTerritoryHistory>
    {
        public SalesTerritoryHistoryGraphType()
        {
            Field(x => x.TerritoryId);
            Field(x => x.BusinessEntityId);
            Field(x => x.StartDate);
            Field(x => x.EndDate, true);
            Field(x => x.ModifiedDate);
            Field<SalesPersonGraphType>("SalesPerson");
            Field<SalesTerritoryGraphType>("Territory");
        }
    }

    public class ShoppingCartItemGraphType : ObjectGraphType<ShoppingCartItem>
    {
        public ShoppingCartItemGraphType()
        {
            Field(x => x.ShoppingCartItemId);
            Field(x => x.ShoppingCartId);
            Field(x => x.Quantity);
            Field(x => x.ProductId);
            Field(x => x.DateCreated);
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Product");
        }
    }

    public class SpecialOfferGraphType : ObjectGraphType<SpecialOffer>
    {
        public SpecialOfferGraphType()
        {
            Field(x => x.SpecialOfferId);
            Field(x => x.Description);
            Field(x => x.Category);
            Field(x => x.DiscountPercentage);
            Field(x => x.StartDate);
            Field(x => x.EndDate);
            Field(x => x.MinimumQuantity);
            Field(x => x.MaximumQuantity, true);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<SpecialOfferProductGraphType>>("SpecialOfferProducts");
        }
    }

    public class SpecialOfferProductGraphType : ObjectGraphType<SpecialOfferProduct>
    {
        public SpecialOfferProductGraphType()
        {
            Field(x => x.SpecialOfferId);
            Field(x => x.ProductId);
            Field(x => x.ModifiedDate);
            Field<SpecialOfferGraphType>("SpecialOffer");
            Field<ProductGraphType>("Product");
            Field<ListGraphType<SalesOrderDetailGraphType>>("SalesOrderDetails");
        }
    }

    public class StoreGraphType : ObjectGraphType<Store>
    {
        public StoreGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.Name);
            Field(x => x.SalesPersonId, true);
            Field(x => x.Demographics, true);
            Field(x => x.ModifiedDate);
            Field<BusinessEntityGraphType>("BusinessEntity");
            Field<SalesPersonGraphType>("SalesPerson");
            Field<ListGraphType<CustomerGraphType>>("Customers");
        }
    }
}
