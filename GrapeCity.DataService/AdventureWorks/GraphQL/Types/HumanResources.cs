﻿using GrapeCity.DataService.AdventureWorks.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.GraphQL
{

    public class DepartmentGraphType : ObjectGraphType<Department>
    {
        public DepartmentGraphType()
        {
            Field(x => x.DepartmentId, type: typeof(ShortGraphType));
            Field(x => x.Name);
            Field(x => x.GroupName);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<EmployeeDepartmentHistoryGraphType>>("EmployeeDepartmentHistories");
        }
    }

    public class EmployeeGraphType : ObjectGraphType<Employee>
    {
        public EmployeeGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.NationalIdnumber);
            Field(x => x.LoginId);
            Field(x => x.OrganizationLevel, true, type: typeof(ShortGraphType));
            Field(x => x.JobTitle);
            Field(x => x.BirthDate);
            Field(x => x.MaritalStatus);
            Field(x => x.Gender);
            Field(x => x.HireDate);
            Field(x => x.SalariedFlag,true);
            Field(x => x.VacationHours, type: typeof(ShortGraphType));
            Field(x => x.SickLeaveHours, type: typeof(ShortGraphType));
            Field(x => x.CurrentFlag,true);
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<PersonGraphType>("BusinessEntity");
            Field<SalesPersonGraphType>("SalesPerson");
            Field<ListGraphType<EmployeeDepartmentHistoryGraphType>>("EmployeeDepartmentHistories");
            Field<ListGraphType<EmployeePayHistoryGraphType>>("EmployeePayHistories");
            Field<ListGraphType<JobCandidateGraphType>>("JobCandidates");
            Field<ListGraphType<PurchaseOrderHeaderGraphType>>("PurchaseOrderHeaders");
        }
    }

    public class EmployeeDepartmentHistoryGraphType : ObjectGraphType<EmployeeDepartmentHistory>
    {
        public EmployeeDepartmentHistoryGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.DepartmentId, type: typeof(ShortGraphType));
            Field("ShiftId", x => Convert.ToInt16(x.ShiftId), type: typeof(ShortGraphType));
            Field(x => x.StartDate);
            Field(x => x.EndDate,true);
            Field(x => x.ModifiedDate);
            Field<EmployeeGraphType>("BusinessEntity");
            Field<DepartmentGraphType>("Department");
            Field<ShiftGraphType>("Shift");
        }
    }

    public class EmployeePayHistoryGraphType : ObjectGraphType<EmployeePayHistory>
    {
        public EmployeePayHistoryGraphType()
        {
            Field(x => x.BusinessEntityId);
            Field(x => x.RateChangeDate);
            Field(x => x.Rate);
            Field("PayFrequency", x => Convert.ToInt16(x.PayFrequency), type: typeof(ShortGraphType));
            Field(x => x.ModifiedDate);
            Field<EmployeeGraphType>("BusinessEntity");
        }
    }


    public class JobCandidateGraphType : ObjectGraphType<JobCandidate>
    {
        public JobCandidateGraphType()
        {
            Field(x => x.JobCandidateId);
            Field(x => x.BusinessEntityId,true);
            Field(x => x.Resume,true);
            Field(x => x.ModifiedDate);
            Field<EmployeeGraphType>("BusinessEntity");
        }
    }

    public class ShiftGraphType : ObjectGraphType<Shift>
    {
        public ShiftGraphType()
        {
            Field("ShiftId", x => Convert.ToInt16(x.ShiftId), type: typeof(ShortGraphType));
            Field(x => x.Name);
            Field(x => x.StartTime);
            Field(x => x.EndTime);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<EmployeeDepartmentHistoryGraphType>>("EmployeeDepartmentHistories");
        }
    }
}
