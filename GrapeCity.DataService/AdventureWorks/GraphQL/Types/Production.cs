﻿using GrapeCity.DataService.AdventureWorks.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.GraphQL
{
    public class BillOfMaterialGraphType : ObjectGraphType<BillOfMaterial>
    {
        public BillOfMaterialGraphType()
        {
            Field(x => x.BillOfMaterialsId);
            Field(x => x.ProductAssemblyId, true);
            Field(x => x.ComponentId);
            Field(x => x.StartDate);
            Field(x => x.EndDate, true);
            Field(x => x.UnitMeasureCode);
            Field(x => x.Bomlevel, type: typeof(ShortGraphType));
            Field(x => x.PerAssemblyQty);
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Component");
            Field<ProductGraphType>("ProductAssembly");
            Field<UnitMeasureGraphType>("QuantityUnit");
        }
    }

    public class CultureGraphType : ObjectGraphType<Culture>
    {
        public CultureGraphType()
        {
            Field(x => x.CultureId);
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<ProductModelProductDescriptionCultureGraphType>>("ProductModelProductDescriptionCultures");
        }
    }

    public class IllustrationGraphType : ObjectGraphType<Illustration>
    {
        public IllustrationGraphType()
        {
            Field(x => x.IllustrationId);
            Field(x => x.Diagram,true);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<ProductModelIllustrationGraphType>>("ProductModelIllustrations");
        }
    }

    public class LocationGraphType : ObjectGraphType<Location>
    {
        public LocationGraphType()
        {
            Field(x => x.LocationId, type: typeof(ShortGraphType));
            Field(x => x.Name);
            Field(x => x.CostRate);
            Field(x => x.Availability);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<ProductInventoryGraphType>>("ProductInventories");
            Field<ListGraphType<WorkOrderRoutingGraphType>>("WorkOrderRoutings");
        }
    }

    public class ProductGraphType : ObjectGraphType<Product>
    {
        public ProductGraphType()
        {
            Field(x => x.ProductId);
            Field(x => x.Name);
            Field(x => x.ProductNumber);
            Field(x => x.MakeFlag, true);
            Field(x => x.FinishedGoodsFlag, true);
            Field(x => x.Color,true);
            Field(x => x.SafetyStockLevel, type: typeof(ShortGraphType));
            Field(x => x.ReorderPoint, type: typeof(ShortGraphType));
            Field(x => x.StandardCost);
            Field(x => x.ListPrice);
            Field(x => x.Size,true);
            Field(x => x.SizeUnitMeasureCode, true);
            Field(x => x.WeightUnitMeasureCode, true);
            Field(x => x.Weight, true);
            Field(x => x.DaysToManufacture);
            Field(x => x.ProductLine, true);
            Field(x => x.Class, true);
            Field(x => x.Style, true);
            Field(x => x.ProductSubcategoryId, true);
            Field(x => x.ProductModelId, true);
            Field(x => x.SellStartDate);
            Field(x => x.SellEndDate, true);
            Field(x => x.DiscontinuedDate, true);
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<ProductModelGraphType>("ProductModel");
            Field<ProductSubcategoryGraphType>("ProductSubcategory");
            Field<UnitMeasureGraphType>("UnitOfSize");
            Field<UnitMeasureGraphType>("UnitOfWeight");
            Field<ListGraphType<BillOfMaterialGraphType>>("BillOfMaterialComponents");
            Field<ListGraphType<BillOfMaterialGraphType>>("BillOfMaterialProductAssemblies");
            Field<ListGraphType<ProductCostHistoryGraphType>>("ProductCostHistories");
            Field<ListGraphType<ProductInventoryGraphType>>("ProductInventories");
            Field<ListGraphType<ProductListPriceHistoryGraphType>>("ProductListPriceHistories");
            Field<ListGraphType<ProductProductPhotoGraphType>>("ProductProductPhotos");
            Field<ListGraphType<ProductReviewGraphType>>("ProductReviews");
            Field<ListGraphType<ProductVendorGraphType>>("ProductVendors");
            Field<ListGraphType<PurchaseOrderDetailGraphType>>("PurchaseOrderDetails");
            Field<ListGraphType<ShoppingCartItemGraphType>>("ShoppingCartItems");
            Field<ListGraphType<SpecialOfferProductGraphType>>("SpecialOfferProducts");
            Field<ListGraphType<TransactionHistoryGraphType>>("TransactionHistories");
            Field<ListGraphType<WorkOrderGraphType>>("WorkOrders");
        }
    }

    public class ProductCategoryGraphType : ObjectGraphType<ProductCategory>
    {
        public ProductCategoryGraphType()
        {
            Field(x => x.ProductCategoryId);
            Field(x => x.Name);
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<ListGraphType<ProductSubcategoryGraphType>>("ProductSubcategories");
        }
    }

    public class ProductCostHistoryGraphType : ObjectGraphType<ProductCostHistory>
    {
        public ProductCostHistoryGraphType()
        {
            Field(x => x.ProductId);
            Field(x => x.StandardCost);
            Field(x => x.EndDate, true);
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Product");
        }
    }

    public class ProductDescriptionGraphType : ObjectGraphType<ProductDescription>
    {
        public ProductDescriptionGraphType()
        {
            Field(x => x.ProductDescriptionId);
            Field(x => x.Description);
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<ListGraphType<ProductModelProductDescriptionCultureGraphType>>("ProductModelProductDescriptionCultures");
        }
    }

    public class ProductInventoryGraphType : ObjectGraphType<ProductInventory>
    {
        public ProductInventoryGraphType()
        {
            Field(x => x.ProductId);
            Field(x => x.LocationId, type: typeof(ShortGraphType));
            Field(x => x.Shelf);
            Field("Bin", x => Convert.ToInt16(x.Bin), type: typeof(ShortGraphType));
            Field(x => x.Quantity, type: typeof(ShortGraphType));
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<LocationGraphType>("Location");
            Field<ProductGraphType>("Product");
        }
    }

    public class ProductListPriceHistoryGraphType : ObjectGraphType<ProductListPriceHistory>
    {
        public ProductListPriceHistoryGraphType()
        {
            Field(x => x.ProductId);
            Field(x => x.StartDate);
            Field(x => x.EndDate, true);
            Field(x => x.ListPrice);
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Product");
        }
    }

    public class ProductModelGraphType : ObjectGraphType<ProductModel>
    {
        public ProductModelGraphType()
        {
            Field(x => x.ProductModelId);
            Field(x => x.Name);
            Field(x => x.CatalogDescription, true);
            Field(x => x.Instructions, true);
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<ListGraphType<ProductModelIllustrationGraphType>>("ProductModelIllustrations");
            Field<ListGraphType<ProductModelProductDescriptionCultureGraphType>>("ProductModelProductDescriptionCultures");
            Field<ListGraphType<ProductGraphType>>("Products");
        }
    }

    public class ProductModelIllustrationGraphType : ObjectGraphType<ProductModelIllustration>
    {
        public ProductModelIllustrationGraphType()
        {
            Field(x => x.ProductModelId);
            Field(x => x.IllustrationId);
            Field(x => x.ModifiedDate);
            Field<IllustrationGraphType>("Illustration");
            Field<ProductModelGraphType>("ProductModel");
        }
    }

    public class ProductModelProductDescriptionCultureGraphType : ObjectGraphType<ProductModelProductDescriptionCulture>
    {
        public ProductModelProductDescriptionCultureGraphType()
        {
            Field(x => x.ProductModelId);
            Field(x => x.ProductDescriptionId);
            Field(x => x.CultureId);
            Field(x => x.ModifiedDate);
            Field<CultureGraphType>("Culture");
            Field<ProductDescriptionGraphType>("ProductDescription");
            Field<ProductModelGraphType>("ProductModel");
        }
    }

    public class ProductPhotoGraphType : ObjectGraphType<ProductPhoto>
    {
        public ProductPhotoGraphType()
        {
            Field(x => x.ProductPhotoId);
            Field("ThumbnailPhoto", x => Convert.ToBase64String(x.ThumbNailPhoto), true);
            Field(x => x.ThumbnailPhotoFileName, true);
            Field("LargePhoto", x => Convert.ToBase64String(x.LargePhoto), true);
            Field(x => x.LargePhotoFileName, true);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<ProductProductPhotoGraphType>>("ProductProductPhotos");
        }
    }

    public class ProductProductPhotoGraphType : ObjectGraphType<ProductProductPhoto>
    {
        public ProductProductPhotoGraphType()
        {
            Field(x => x.ProductId);
            Field(x => x.ProductPhotoId);
            Field(x => x.Primary);
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Product");
            Field<ProductPhotoGraphType>("ProductPhoto");
        }
    }

    public class ProductReviewGraphType : ObjectGraphType<ProductReview>
    {
        public ProductReviewGraphType()
        {
            Field(x => x.ProductReviewId);
            Field(x => x.ProductId);
            Field(x => x.ReviewerName);
            Field(x => x.ReviewDate);
            Field(x => x.EmailAddress);
            Field(x => x.Rating);
            Field(x => x.Comments, true);
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Product");
        }
    }

    public class ProductSubcategoryGraphType : ObjectGraphType<ProductSubcategory>
    {
        public ProductSubcategoryGraphType()
        {
            Field(x => x.ProductSubcategoryId);
            Field(x => x.ProductCategoryId);
            Field(x => x.Name);
            Field(x => x.Rowguid, type: typeof(GuidGraphType));
            Field(x => x.ModifiedDate);
            Field<ProductCategoryGraphType>("ProductCategory");
            Field<ListGraphType<ProductGraphType>>("Products");
        }
    }

    public class ScrapReasonGraphType : ObjectGraphType<ScrapReason>
    {
        public ScrapReasonGraphType()
        {
            Field(x => x.ScrapReasonId, type: typeof(ShortGraphType));
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<WorkOrderGraphType>>("WorkOrders");
        }
    }

    public class TransactionHistoryGraphType : ObjectGraphType<TransactionHistory>
    {
        public TransactionHistoryGraphType()
        {
            Field(x => x.TransactionId);
            Field(x => x.ProductId);
            Field(x => x.ReferenceOrderId);
            Field(x => x.ReferenceOrderLineId);
            Field(x => x.TransactionDate);
            Field(x => x.TransactionType);
            Field(x => x.Quantity);
            Field(x => x.ActualCost);
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Product");
        }
    }

    public class TransactionHistoryArchiveGraphType : ObjectGraphType<TransactionHistoryArchive>
    {
        public TransactionHistoryArchiveGraphType()
        {
            Field(x => x.TransactionId);
            Field(x => x.ProductId);
            Field(x => x.ReferenceOrderId);
            Field(x => x.ReferenceOrderLineId);
            Field(x => x.TransactionDate);
            Field(x => x.TransactionType);
            Field(x => x.Quantity);
            Field(x => x.ActualCost);
            Field(x => x.ModifiedDate);
        }
    }

    public class UnitMeasureGraphType : ObjectGraphType<UnitMeasure>
    {
        public UnitMeasureGraphType()
        {
            Field(x => x.UnitMeasureCode);
            Field(x => x.Name);
            Field(x => x.ModifiedDate);
            Field<ListGraphType<BillOfMaterialGraphType>>("BillOfMaterials");
            Field<ListGraphType<ProductGraphType>>("SizeUnitProducts");
            Field<ListGraphType<ProductVendorGraphType>>("ProductVendors");
            Field<ListGraphType<ProductGraphType>>("WeightUnitProducts");
        }
    }

    public class WorkOrderGraphType : ObjectGraphType<WorkOrder>
    {
        public WorkOrderGraphType()
        {
            Field(x => x.WorkOrderId);
            Field(x => x.ProductId);
            Field(x => x.OrderQty);
            Field(x => x.StockedQty);
            Field(x => x.ScrappedQty, type: typeof(ShortGraphType));
            Field(x => x.StartDate);
            Field(x => x.EndDate, true);
            Field(x => x.DueDate);
            Field(x => x.ScrapReasonId, true, type: typeof(ShortGraphType));
            Field(x => x.ModifiedDate);
            Field<ProductGraphType>("Product");
            Field<ScrapReasonGraphType>("ScrapReason");
            Field<ListGraphType<WorkOrderRoutingGraphType>>("WorkOrderRoutings");
        }
    }

    public class WorkOrderRoutingGraphType : ObjectGraphType<WorkOrderRouting>
    {
        public WorkOrderRoutingGraphType()
        {
            Field(x => x.WorkOrderId);
            Field(x => x.ProductId);
            Field(x => x.OperationSequence, type: typeof(ShortGraphType));
            Field(x => x.LocationId, type: typeof(ShortGraphType));
            Field(x => x.ScheduledStartDate);
            Field(x => x.ScheduledEndDate);
            Field(x => x.ActualStartDate, true);
            Field(x => x.ActualResourceHrs, true);
            Field(x => x.ActualEndDate, true);
            Field(x => x.PlannedCost);
            Field(x => x.ActualCost, true);
            Field(x => x.ModifiedDate);
            Field<LocationGraphType>("Location");
            Field<WorkOrderGraphType>("WorkOrder");
        }
    }
}
