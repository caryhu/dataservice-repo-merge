﻿using GrapeCity.DataService.AdventureWorks.Models;
using GraphQL;
using GraphQL.Types;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Runtime.Intrinsics.X86;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.GraphQL
{
    public class GraphQLQuery
    {
        public string OperationName { get; set; }
        public string NamedQuery { get; set; }
        public string Query { get; set; }
        public JObject Variables { get; }
    }

    public class AdventureWorksQuery : ObjectGraphType
    {
        private readonly AdventureWorksContext _context;

        public AdventureWorksQuery(AdventureWorksContext context)
        {
            this._context = context;

            #region Human Resource
            // Department
            Field<ListGraphType<DepartmentGraphType>>("departments",
                resolve: context => this.GetDepartments());

            Field<DepartmentGraphType>("department",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "departmentId" }),
                resolve: context => this.GetDepartment(context.GetArgument<int>("departmentId")));

            // Employee
            Field<ListGraphType<EmployeeGraphType>>("employees",
                resolve: context => this.GetEmployees());

            Field<EmployeeGraphType>("employee",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "employeeId" }),
                resolve: context => this.GetEmployee(context.GetArgument<int>("employeeId")));

            // Employee Department History
            Field<ListGraphType<EmployeeDepartmentHistoryGraphType>>("employeeDepartmentHistories",
                resolve: context => this.GetEmployeeDepartmentHistories());

            Field<ListGraphType<EmployeeDepartmentHistoryGraphType>>("employeeDepartmentHistory",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "businessEntityId" },
                    new QueryArgument<IntGraphType> { Name = "departmentId" },
                    new QueryArgument<IntGraphType> { Name = "shiftId" },
                    new QueryArgument<DateTimeGraphType> { Name = "startDate" }
                    ),
                resolve: context => this.GetEmployeeDepartmentHistory(
                    context.GetArgument<int>("businessEntityId"),
                    context.GetArgument<int>("departmentId"),
                    context.GetArgument<int>("shiftId"),
                    context.GetArgument<DateTime>("startDate")
                    ));

            // Employee Pay History
            Field<ListGraphType<EmployeePayHistoryGraphType>>("employeePayHistories",
                resolve: context => this.GetEmployeePayHistories());

            Field<EmployeePayHistoryGraphType>("employeePayHistory",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "businessEntityId" },
                    new QueryArgument<DateTimeGraphType> { Name = "rateChangeDate" }
                    ),
                resolve: context => this.GetEmployeePayHistory(
                    context.GetArgument<int>("businessEntityId"),
                    context.GetArgument<DateTime>("rateChangeDate")
                    ));

            // Job Candidate
            Field<ListGraphType<JobCandidateGraphType>>("jobCandidates",
                resolve: context => this.GetJobCandidates());

            Field<JobCandidateGraphType>("jobCandidate",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "jobCandidateId" }),
                resolve: context => this.GetJobCandidate(context.GetArgument<int>("jobCandidateId")));

            // Shift
            Field<ListGraphType<ShiftGraphType>>("shifts",
                resolve: context => this.GetShifts());

            Field<ShiftGraphType>("shift",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "shiftId" }),
                resolve: context => this.GetShift(context.GetArgument<int>("shiftId")));
            #endregion

            #region Person
            // Address
            Field<ListGraphType<AddressGraphType>>("addresses",
                resolve: context => this.GetAddresses());

            Field<AddressGraphType>("address",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "addressId" }),
                resolve: context => this.GetAddress(context.GetArgument<int>("addressId")));

            // Address Type
            Field<ListGraphType<AddressTypeGraphType>>("addressTypes",
                resolve: context => this.GetAddressTypes());

            Field<AddressTypeGraphType>("addressType",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "addressTypeId" }),
                resolve: context => this.GetAddressType(context.GetArgument<int>("addressTypeId")));

            // Business Entity
            Field<ListGraphType<BusinessEntityGraphType>>("businessEntities",
                resolve: context => this.GetBusinessEntities());

            Field<BusinessEntityGraphType>("businessEntity",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "businessEntityId" }),
                resolve: context => this.GetBusinessEntity(context.GetArgument<int>("businessEntityId")));

            // Business Entity Address
            Field<ListGraphType<BusinessEntityAddressGraphType>>("businessEntityAddresses",
                resolve: context => this.GetBusinessEntityAddresses());

            Field<BusinessEntityAddressGraphType>("businessEntityAddress",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "businessEntityId" },
                    new QueryArgument<IntGraphType> { Name = "addressId" },
                    new QueryArgument<IntGraphType> { Name = "addressTypeId" }
                    ),
                resolve: context => this.GetBusinessEntityAddress(
                    context.GetArgument<int>("businessEntityId"),
                    context.GetArgument<int>("addressId"),
                    context.GetArgument<int>("addressTypeId")
                    ));

            // Business Entity Contacts
            Field<ListGraphType<BusinessEntityContactGraphType>>("businessEntityContacts",
                resolve: context => this.GetBusinessEntityContacts());

            Field<BusinessEntityContactGraphType>("businessEntityContact",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "businessEntityId" },
                    new QueryArgument<IntGraphType> { Name = "personId" },
                    new QueryArgument<IntGraphType> { Name = "contactTypeId" }
                    ),
                resolve: context => this.GetBusinessEntityContact(
                    context.GetArgument<int>("businessEntityId"),
                    context.GetArgument<int>("personId"),
                    context.GetArgument<int>("contactTypeId")
                    ));

            // Contact Type
            Field<ListGraphType<ContactTypeGraphType>>("contactTypes",
                resolve: context => this.GetContactTypes());

            Field<ContactTypeGraphType>("contactType",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "contactTypeId" }),
                resolve: context => this.GetContactType(context.GetArgument<int>("contactTypeId")));

            // Country Region
            Field<ListGraphType<CountryRegionGraphType>>("countryRegions",
                resolve: context => this.GetCountryRegions());

            Field<CountryRegionGraphType>("countryRegion",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "countryRegionCode" }),
                resolve: context => this.GetCountryRegion(context.GetArgument<string>("countryRegionCode")));

            // Email Address
            Field<ListGraphType<EmailAddressGraphType>>("emailAddresses",
                resolve: context => this.GetEmailAddresses());

            Field<EmailAddressGraphType>("emailAddress",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "personId" },
                    new QueryArgument<IntGraphType> { Name = "emailAddressId" }
                    ),
                resolve: context => this.GetEmailAddress(
                    context.GetArgument<int>("personId"),
                    context.GetArgument<int>("emailAddressId")
                    ));

            // Password
            Field<ListGraphType<PasswordGraphType>>("passwords",
                resolve: context => this.GetPasswords());

            Field<PasswordGraphType>("password",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "businessEntityId" }),
                resolve: context => this.GetPassword(context.GetArgument<int>("businessEntityId")));

            // Person
            Field<ListGraphType<PersonGraphType>>("persons",
                resolve: context => this.GetPersons());

            Field<PersonGraphType>("person",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "businessEntityId" }),
                resolve: context => this.GetPerson(context.GetArgument<int>("businessEntityId")));

            // Person Phone
            Field<ListGraphType<PersonPhoneGraphType>>("personPhones",
                resolve: context => this.GetPersonPhones());

            Field<PersonPhoneGraphType>("personPhone",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "businessEntityId" },
                    new QueryArgument<StringGraphType> { Name = "phoneNumber" },
                    new QueryArgument<IntGraphType> { Name = "phoneNumberTypeId" }
                    ),
                resolve: context => this.GetPersonPhone(
                    context.GetArgument<int>("businessEntityId"),
                    context.GetArgument<string>("phoneNumber"),
                    context.GetArgument<int>("phoneNumberTypeId")
                    ));

            // Phone Number Type
            Field<ListGraphType<PhoneNumberTypeGraphType>>("phoneNumberTypes",
                resolve: context => this.GetPhoneNumberTypes());

            Field<PhoneNumberTypeGraphType>("phoneNumberType",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "phoneNumberTypeId" }),
                resolve: context => this.GetPhoneNumberType(context.GetArgument<int>("phoneNumberTypeId")));

            // State Province
            Field<ListGraphType<StateProvinceGraphType>>("stateProvinces",
                resolve: context => this.GetStateProvinces());

            Field<StateProvinceGraphType>("stateProvince",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "stateProvinceId" }),
                resolve: context => this.GetStateProvince(context.GetArgument<int>("stateProvinceId")));
            #endregion

            #region Production
            // Bill of Material
            Field<ListGraphType<BillOfMaterialGraphType>>("billOfMaterials",
                resolve: context => this.GetBillOfMaterials());

            Field<BillOfMaterialGraphType>("billOfMaterial",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "billOfMaterialsId" }),
                resolve: context => this.GetBillOfMaterial(context.GetArgument<int>("billOfMaterialsId")));

            // Culture
            Field<ListGraphType<CultureGraphType>>("cultures",
                resolve: context => this.GetCultures());

            Field<CultureGraphType>("culture",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cultureId" }),
                resolve: context => this.GetCulture(context.GetArgument<string>("cultureId")));

            // Illustration
            Field<ListGraphType<IllustrationGraphType>>("illustrations",
                resolve: context => this.GetIllustrations());

            Field<IllustrationGraphType>("illustration",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "illustrationId" }),
                resolve: context => this.GetIllustration(context.GetArgument<int>("illustrationId")));

            // Location
            Field<ListGraphType<LocationGraphType>>("locations",
               resolve: context => this.GetLocations());

            Field<LocationGraphType>("location",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "locationId" }),
                resolve: context => this.GetLocation(context.GetArgument<int>("locationId")));

            // Product
            Field<ListGraphType<ProductGraphType>>("products",
                resolve: context => this.GetProducts());

            Field<ProductGraphType>("product",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "productId" }),
                resolve: context => this.GetProduct(context.GetArgument<int>("productId")));

            // Product Category
            Field<ListGraphType<ProductCategoryGraphType>>("productCategories",
                resolve: context => this.GetProductCategories());

            Field<ProductCategoryGraphType>("productCategory",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "productCategoryId" }),
                resolve: context => this.GetProductCategory(context.GetArgument<int>("productCategoryId")));

            // Product Cost History
            Field<ListGraphType<ProductCostHistoryGraphType>>("productCostHistories",
                resolve: context => this.GetProductCostHistories());

            Field<ListGraphType<ProductCostHistoryGraphType>>("productCostHistory",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "productId" },
                    new QueryArgument<DateTimeGraphType> { Name = "startDate" }
                    ),
                resolve: context => this.GetProductCostHistory(
                    context.GetArgument<int>("productId"),
                    context.GetArgument<DateTime>("startDate")
                    ));

            // Product Description
            Field<ListGraphType<ProductDescriptionGraphType>>("productDescriptions",
                resolve: context => this.GetProductDescriptions());

            Field<ProductDescriptionGraphType>("productDescription",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "productDescriptionId" }),
                resolve: context => this.GetProductDescription(context.GetArgument<int>("productDescriptionId")));

            // Product Inventory
            Field<ListGraphType<ProductInventoryGraphType>>("productInventories",
                resolve: context => this.GetProductInventories());

            Field<ProductInventoryGraphType>("productInventory",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "productId" },
                    new QueryArgument<IntGraphType> { Name = "locationId" }
                    ),
                resolve: context => this.GetProductInventory(
                    context.GetArgument<int>("productId"),
                    context.GetArgument<int>("locationId")
                    ));

            // Product List Price History
            Field<ListGraphType<ProductListPriceHistoryGraphType>>("productListPriceHistories",
                resolve: context => this.GetProductListPriceHistories());

            Field<ListGraphType<ProductListPriceHistoryGraphType>>("productListPriceHistory",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "productId" },
                    new QueryArgument<DateTimeGraphType> { Name = "startDate" }
                    ),
                resolve: context => this.GetProductListPriceHistory(
                    context.GetArgument<int>("productId"),
                    context.GetArgument<DateTime>("startDate")
                    ));

            // Product Model
            Field<ListGraphType<ProductModelGraphType>>("productModels",
                resolve: context => this.GetProductModels());

            Field<ProductModelGraphType>("productModel",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "productModelId" }),
                resolve: context => this.GetProductModel(context.GetArgument<int>("productModelId")));

            // Product Model Illustration
            Field<ListGraphType<ProductModelIllustrationGraphType>>("productModelIllustrations",
                resolve: context => this.GetProductModelIllustrations());

            Field<ProductModelIllustrationGraphType>("productModelIllustration",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "productModelId" },
                    new QueryArgument<IntGraphType> { Name = "illustrationId" }
                    ),
                resolve: context => this.GetProductModelIllustration(
                    context.GetArgument<int>("productModelId"),
                    context.GetArgument<int>("illustrationId")
                    ));

            // Product Model Product Description Culture
            Field<ListGraphType<ProductModelProductDescriptionCultureGraphType>>("productModelProductDescriptionCultures",
                resolve: context => this.GetProductModelProductDescriptionCultures());

            Field<ProductModelProductDescriptionCultureGraphType>("productModelProductDescriptionCulture",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "productModelId" },
                    new QueryArgument<IntGraphType> { Name = "descriptionId" },
                    new QueryArgument<StringGraphType> { Name = "cultureId" }
                    ),
                resolve: context => this.GetProductModelProductDescriptionCulture(
                    context.GetArgument<int>("productModelId"),
                    context.GetArgument<int>("descriptionId"),
                    context.GetArgument<string>("cultureId")
                    ));

            // Product Photo
            Field<ListGraphType<ProductPhotoGraphType>>("productPhotos",
                resolve: context => this.GetProductPhotos());

            Field<ProductPhotoGraphType>("productPhoto",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "productPhotoId" }),
                resolve: context => this.GetProductPhoto(context.GetArgument<int>("productPhotoId")));

            // Product Product Photo
            Field<ListGraphType<ProductProductPhotoGraphType>>("productProductPhotos",
                resolve: context => this.GetProductProductPhotos());

            Field<ProductProductPhotoGraphType>("productProductPhoto",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "productId" },
                    new QueryArgument<IntGraphType> { Name = "productPhotoId" }
                    ),
                resolve: context => this.GetProductProductPhoto(
                    context.GetArgument<int>("productId"),
                    context.GetArgument<int>("productPhotoId")
                    ));

            // Product Review
            Field<ListGraphType<ProductReviewGraphType>>("productReviews",
                resolve: context => this.GetProductReviews());

            Field<ProductReviewGraphType>("productReview",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "productReviewId" }),
                resolve: context => this.GetProductReview(context.GetArgument<int>("productReviewId")));

            // Product Subcategory
            Field<ListGraphType<ProductSubcategoryGraphType>>("productSubcategories",
                resolve: context => this.GetProductSubcategories());

            Field<ProductSubcategoryGraphType>("productSubcategory",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "productSubcategoryId" }),
                resolve: context => this.GetProductSubcategory(context.GetArgument<int>("productSubcategoryId")));

            // Scrap Reason
            Field<ListGraphType<ScrapReasonGraphType>>("scrapReasons",
                resolve: context => this.GetScrapReasons());

            Field<ScrapReasonGraphType>("scrapReason",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "scrapReasonId" }),
                resolve: context => this.GetScrapReason(context.GetArgument<int>("scrapReasonId")));

            // Transaction History
            Field<ListGraphType<TransactionHistoryGraphType>>("transactionHistories",
                resolve: context => this.GetTransactionHistories());

            Field<TransactionHistoryGraphType>("transactionHistory",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "transactionId" }),
                resolve: context => this.GetTransactionHistory(context.GetArgument<int>("transactionId")));

            // Transaction History Archive
            Field<ListGraphType<TransactionHistoryArchiveGraphType>>("transactionHistoryArchives",
                resolve: context => this.GetTransactionHistoryArchives());

            Field<TransactionHistoryArchiveGraphType>("transactionHistoryArchive",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "transactionId" }),
                resolve: context => this.GetTransactionHistoryArchive(context.GetArgument<int>("transactionId")));

            // Unit Measure
            Field<ListGraphType<UnitMeasureGraphType>>("unitMeasures",
                resolve: context => this.GetUnitMeasures());

            Field<UnitMeasureGraphType>("unitMeasure",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "unitMeasureCode" }),
                resolve: context => this.GetUnitMeasure(context.GetArgument<string>("unitMeasureCode")));

            // Work Order
            Field<ListGraphType<WorkOrderGraphType>>("workOrders",
                resolve: context => this.GetWorkOrders());

            Field<WorkOrderGraphType>("workOrder",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "workOrderId" }),
                resolve: context => this.GetWorkOrder(context.GetArgument<int>("workOrderId")));

            // Work Order Routing
            Field<ListGraphType<WorkOrderRoutingGraphType>>("workOrderRoutings",
                resolve: context => this.GetWorkOrderRoutings());

            Field<WorkOrderRoutingGraphType>("workOrderRouting",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "workOrderId" },
                    new QueryArgument<IntGraphType> { Name = "productId" },
                    new QueryArgument<IntGraphType> { Name = "operationSequence" }
                    ),
                resolve: context => this.GetWorkOrderRouting(
                    context.GetArgument<int>("workOrderId"),
                    context.GetArgument<int>("productId"),
                    context.GetArgument<int>("operationSequence")
                    ));
            #endregion

            #region Purchasing
            // Product Vendor
            Field<ListGraphType<ProductVendorGraphType>>("productVendors",
                resolve: context => this.GetProductVendors());

            Field<ProductVendorGraphType>("productVendor",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "productId" },
                    new QueryArgument<IntGraphType> { Name = "businessEntityId" }
                    ),
                resolve: context => this.GetProductVendor(
                    context.GetArgument<int>("productId"),
                    context.GetArgument<int>("businessEntityId")
                    ));

            // Purchase Order Detail
            Field<ListGraphType<PurchaseOrderDetailGraphType>>("purchaseOrderDetails",
                resolve: context => this.GetPurchaseOrderDetails());

            Field<PurchaseOrderDetailGraphType>("purchaseOrderDetail",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "purchaseOrderId" },
                    new QueryArgument<IntGraphType> { Name = "purchaseOrderDetailId" }
                    ),
                resolve: context => this.GetPurchaseOrderDetail(
                    context.GetArgument<int>("purchaseOrderId"),
                    context.GetArgument<int>("purchaseOrderDetailId")
                    ));

            // Purchase Order Header
            Field<ListGraphType<PurchaseOrderHeaderGraphType>>("purchaseOrderHeaders",
                resolve: context => this.GetPurchaseOrderHeaders());

            Field<PurchaseOrderHeaderGraphType>("purchaseOrderHeader",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "purchaseOrderId" }),
                resolve: context => this.GetPurchaseOrderHeader(context.GetArgument<int>("purchaseOrderId")));

            // Ship Method
            Field<ListGraphType<ShipMethodGraphType>>("shipMethods",
                resolve: context => this.GetShipMethods());

            Field<ShipMethodGraphType>("shipMethod",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "shipMethodId" }),
                resolve: context => this.GetShipMethod(context.GetArgument<int>("shipMethodId")));

            // Vendor
            Field<ListGraphType<VendorGraphType>>("vendors",
                resolve: context => this.GetVendors());

            Field<VendorGraphType>("vendor",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "businessEntityId" }),
                resolve: context => this.GetVendor(context.GetArgument<int>("businessEntityId")));
            #endregion

            #region Sales
            // Country Region Currency
            Field<ListGraphType<CountryRegionCurrencyGraphType>>("countryRegionCurrencies",
               resolve: context => this.GetCountryRegionCurrencies());

            Field<CountryRegionCurrencyGraphType>("countryRegionCurrency",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType> { Name = "countryRegionCode" },
                    new QueryArgument<StringGraphType> { Name = "currencyCode" }
                    ),
                resolve: context => this.GetCountryRegionCurrency(
                    context.GetArgument<string>("countryRegionCode"),
                    context.GetArgument<string>("currencyCode")
                    ));

            // Credit Card
            Field<ListGraphType<CreditCardGraphType>>("creditCards",
               resolve: context => this.GetCreditCards());

            Field<CreditCardGraphType>("creditCard",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "creditCardId" }),
                resolve: context => this.GetCreditCard(context.GetArgument<int>("creditCardId")));

            // Currency
            Field<ListGraphType<CurrencyGraphType>>("currencies",
              resolve: context => this.GetCurrencies());

            Field<CurrencyGraphType>("currency",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "currencyCode" }),
                resolve: context => this.GetCurrency(context.GetArgument<string>("currencyCode")));

            // Currency Rate
            Field<ListGraphType<CurrencyRateGraphType>>("currencyRates",
              resolve: context => this.GetCurrencyRates());

            Field<CurrencyRateGraphType>("currencyRate",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "currencyRateId" }),
                resolve: context => this.GetCurrencyRate(context.GetArgument<int>("currencyRateId")));

            // Customer
            Field<ListGraphType<CustomerGraphType>>("customers",
              resolve: context => this.GetCustomers());

            Field<CustomerGraphType>("customer",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "customerId" }),
                resolve: context => this.GetCustomer(context.GetArgument<int>("customerId")));

            // Person Credit Card
            Field<ListGraphType<PersonCreditCardGraphType>>("personCreditCards",
              resolve: context => this.GetPersonCreditCards());

            Field<PersonCreditCardGraphType>("personCreditCard",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "personId" },
                    new QueryArgument<IntGraphType> { Name = "creditCardId" }
                    ),
                resolve: context => this.GetPersonCreditCard(
                    context.GetArgument<int>("personId"),
                    context.GetArgument<int>("creditCardId")
                    ));

            // Sales Order Detail
            Field<ListGraphType<SalesOrderDetailGraphType>>("salesOrderDetails",
              resolve: context => this.GetSalesOrderDetails());

            Field<SalesOrderDetailGraphType>("salesOrderDetail",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "salesOrderId" },
                    new QueryArgument<IntGraphType> { Name = "salesOrderDetailId" }
                    ),
                resolve: context => this.GetSalesOrderDetail(
                    context.GetArgument<int>("salesOrderId"),
                    context.GetArgument<int>("salesOrderDetailId")
                    ));

            // Sales Order Header
            Field<ListGraphType<SalesOrderHeaderGraphType>>("salesOrderHeaders",
              resolve: context => this.GetSalesOrderHeaders());

            Field<SalesOrderHeaderGraphType>("salesOrderHeader",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "salesOrderId" }),
                resolve: context => this.GetSalesOrderHeader(context.GetArgument<int>("salesOrderId")));

            // Sales Order Header Sales Reason
            Field<ListGraphType<SalesOrderHeaderSalesReasonGraphType>>("salesOrderHeaderSalesReasons",
              resolve: context => this.GetSalesOrderHeaderSalesReasons());

            Field<SalesOrderHeaderSalesReasonGraphType>("salesOrderHeaderSalesReason",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "salesOrderId" },
                    new QueryArgument<IntGraphType> { Name = "salesReasonId" }
                    ),
                resolve: context => this.GetSalesOrderHeaderSalesReason(
                    context.GetArgument<int>("salesOrderId"),
                    context.GetArgument<int>("salesReasonId")
                    ));

            // Sales Person
            Field<ListGraphType<SalesPersonGraphType>>("salesPersons",
              resolve: context => this.GetSalesPersons());

            Field<SalesPersonGraphType>("salesPerson",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "businessEntityId" }),
                resolve: context => this.GetSalesPerson(context.GetArgument<int>("businessEntityId")));

            // Sales Person Quota History
            Field<ListGraphType<SalesPersonQuotaHistoryGraphType>>("salesPersonQuotaHistories",
              resolve: context => this.GetSalesPersonQuotaHistories());;

            Field<SalesPersonQuotaHistoryGraphType>("salesPersonQuotaHistory",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "businessEntityId" },
                    new QueryArgument<DateTimeGraphType> { Name = "quotaDate" }
                    ),
                resolve: context => this.GetSalesPersonQuotaHistory(
                    context.GetArgument<int>("salesOrderId"),
                    context.GetArgument<DateTime>("quotaDate")
                    ));

            // Sales Reason
            Field<ListGraphType<SalesReasonGraphType>>("salesReasons",
              resolve: context => this.GetSalesReasons());

            Field<SalesReasonGraphType>("salesReason",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "salesReasonId" }),
                resolve: context => this.GetSalesReason(context.GetArgument<int>("salesReasonId")));

            // Sales Tax Rate
            Field<ListGraphType<SalesTaxRateGraphType>>("salesTaxRates",
              resolve: context => this.GetSalesTaxRates());

            Field<SalesTaxRateGraphType>("salesTaxRate",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "salesTaxRateId" }),
                resolve: context => this.GetSalesTaxRate(context.GetArgument<int>("salesTaxRateId")));

            // Sales Territory
            Field<ListGraphType<SalesTerritoryGraphType>>("salesTerritories",
              resolve: context => this.GetSalesTerritories());

            Field<SalesTerritoryGraphType>("salesTerritory",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "territoryId" }),
                resolve: context => this.GetSalesTerritory(context.GetArgument<int>("territoryId")));

            // Sales Territory History
            Field<ListGraphType<SalesTerritoryHistoryGraphType>>("salesTerritoryHistories",
              resolve: context => this.GetSalesTerritoryHistories()); ;

            Field<SalesTerritoryHistoryGraphType>("salesTerritoryHistory",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "salesPersonId" },
                    new QueryArgument<IntGraphType> { Name = "territoryId" },
                    new QueryArgument<DateTimeGraphType> { Name = "startDate" }
                    ),
                resolve: context => this.GetSalesTerritoryHistory(
                    context.GetArgument<int>("salesPersonId"),
                    context.GetArgument<int>("territoryId"),
                    context.GetArgument<DateTime>("startDate")
                    ));

            // Shopping Cart Item
            Field<ListGraphType<ShoppingCartItemGraphType>>("shoppingCartItems",
              resolve: context => this.GetShoppingCartItems());

            Field<ShoppingCartItemGraphType>("shoppingCartItem",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "shoppingCartItemId" }),
                resolve: context => this.GetShoppingCartItem(context.GetArgument<int>("shoppingCartItemId")));

            // Special Offer
            Field<ListGraphType<SpecialOfferGraphType>>("specialOffers",
              resolve: context => this.GetSpecialOffers());

            Field<SpecialOfferGraphType>("specialOffer",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "specialOfferId" }),
                resolve: context => this.GetSpecialOffer(context.GetArgument<int>("specialOfferId")));

            // Special Offer Product
            Field<ListGraphType<SpecialOfferProductGraphType>>("specialOfferProducts",
              resolve: context => this.GetSpecialOfferProducts()); ;

            Field<SpecialOfferProductGraphType>("specialOfferProduct",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType> { Name = "specialOfferId" },
                    new QueryArgument<IntGraphType> { Name = "productId" }
                    ),
                resolve: context => this.GetSpecialOfferProduct(
                    context.GetArgument<int>("specialOfferId"),
                    context.GetArgument<int>("productId")
                    ));

            // Store
            Field<ListGraphType<StoreGraphType>>("stores",
              resolve: context => this.GetStores());

            Field<StoreGraphType>("store",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "businessEntityId" }),
                resolve: context => this.GetStore(context.GetArgument<int>("businessEntityId")));

            #endregion
        }

        #region Human Resource
        [GraphQLMetadata("department")]
        public Department GetDepartment(int departmentId)
        {
            return _context.Departments.FirstOrDefault(d => d.DepartmentId == departmentId);
        }

        [GraphQLMetadata("departments")]
        public IEnumerable<Department> GetDepartments()
        {
            return _context.Departments.ToList();
        }

        [GraphQLMetadata("employee")]
        public Employee GetEmployee(int employeeId)
        {
            return _context.Employees.FirstOrDefault(e => e.BusinessEntityId == employeeId);
        }

        [GraphQLMetadata("employees")]
        public IEnumerable<Employee> GetEmployees()
        {
            return _context.Employees.ToList();
        }

        [GraphQLMetadata("employeeDepartmentHistory")]
        public IEnumerable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistory(int businessEntityId, int departmentId, int shiftId, DateTime startDate)
        {
            return _context.EmployeeDepartmentHistories.Where(
                e => e.BusinessEntityId == businessEntityId && 
                e.DepartmentId == departmentId && 
                e.ShiftId == shiftId && 
                e.StartDate >= startDate).ToList();
        }

        [GraphQLMetadata("employeeDepartmentHistories")]
        public IEnumerable<EmployeeDepartmentHistory> GetEmployeeDepartmentHistories()
        {
            return _context.EmployeeDepartmentHistories.ToList();
        }

        [GraphQLMetadata("employeePayHistory")]
        public EmployeePayHistory GetEmployeePayHistory(int businessEntityId, DateTime rateChangeDate)
        {
            return _context.EmployeePayHistories.FirstOrDefault(
                e => e.BusinessEntityId == businessEntityId && 
                e.RateChangeDate == rateChangeDate);
        }

        [GraphQLMetadata("employeePayHistories")]
        public IEnumerable<EmployeePayHistory> GetEmployeePayHistories()
        {
            return _context.EmployeePayHistories.ToList();
        }

        [GraphQLMetadata("jobCandidate")]
        public JobCandidate GetJobCandidate(int jobCandidateId)
        {
            return _context.JobCandidates.FirstOrDefault(j => j.JobCandidateId == jobCandidateId);
        }

        [GraphQLMetadata("jobCandidates")]
        public IEnumerable<JobCandidate> GetJobCandidates()
        {
            return _context.JobCandidates.ToList();
        }

        [GraphQLMetadata("shift")]
        public Shift GetShift(int shiftId)
        {
            return _context.Shifts.FirstOrDefault(s => s.ShiftId == shiftId);
        }

        [GraphQLMetadata("shifts")]
        public IEnumerable<Shift> GetShifts()
        {
            return _context.Shifts.ToList();
        }
        #endregion

        #region Person
        [GraphQLMetadata("address")]
        public Address GetAddress(int addressId)
        {
            return _context.Addresses.FirstOrDefault(a => a.AddressId == addressId);
        }

        [GraphQLMetadata("addresses")]
        public IEnumerable<Address> GetAddresses()
        {
            return _context.Addresses.ToList();
        }

        [GraphQLMetadata("addressType")]
        public AddressType GetAddressType(int addressTypeId)
        {
            return _context.AddressTypes.FirstOrDefault(a => a.AddressTypeId == addressTypeId);
        }

        [GraphQLMetadata("addressTypes")]
        public IEnumerable<AddressType> GetAddressTypes()
        {
            return _context.AddressTypes.ToList();
        }

        [GraphQLMetadata("businessEntity")]
        public BusinessEntity GetBusinessEntity(int businessEntityId)
        {
            return _context.BusinessEntities.FirstOrDefault(b => b.BusinessEntityId == businessEntityId);
        }

        [GraphQLMetadata("businessEntities")]
        public IEnumerable<BusinessEntity> GetBusinessEntities()
        {
            return _context.BusinessEntities.ToList();
        }

        [GraphQLMetadata("businessEntityAddress")]
        public BusinessEntityAddress GetBusinessEntityAddress(int businessEntityId, int addressId, int addressTypeId)
        {
            return _context.BusinessEntityAddresses.FirstOrDefault(
                b => b.BusinessEntityId == businessEntityId && 
                b.AddressId == addressId && 
                b.AddressTypeId == addressTypeId);
        }

        [GraphQLMetadata("businessEntityAddresses")]
        public IEnumerable<BusinessEntityAddress> GetBusinessEntityAddresses()
        {
            return _context.BusinessEntityAddresses.ToList();
        }

        [GraphQLMetadata("businessEntityContact")]
        public BusinessEntityContact GetBusinessEntityContact(int businessEntityId, int personId, int contactTypeId)
        {
            return _context.BusinessEntityContacts.FirstOrDefault(
                b => b.BusinessEntityId == businessEntityId &&
                b.PersonId == personId &&
                b.ContactTypeId == contactTypeId);
        }

        [GraphQLMetadata("businessEntityContacts")]
        public IEnumerable<BusinessEntityContact> GetBusinessEntityContacts()
        {
            return _context.BusinessEntityContacts.ToList();
        }

        [GraphQLMetadata("contactType")]
        public ContactType GetContactType(int contactTypeId)
        {
            return _context.ContactTypes.FirstOrDefault(c => c.ContactTypeId == contactTypeId);
        }

        [GraphQLMetadata("contactTypes")]
        public IEnumerable<ContactType> GetContactTypes()
        {
            return _context.ContactTypes.ToList();
        }

        [GraphQLMetadata("countryRegion")]
        public CountryRegion GetCountryRegion(string countryRegionCode)
        {
            return _context.CountryRegions.FirstOrDefault(c => c.CountryRegionCode == countryRegionCode);
        }

        [GraphQLMetadata("countryRegions")]
        public IEnumerable<CountryRegion> GetCountryRegions()
        {
            return _context.CountryRegions.ToList();
        }

        [GraphQLMetadata("emailAddress")]
        public EmailAddress GetEmailAddress(int personId, int emailAddressId)
        {
            return _context.EmailAddresses.FirstOrDefault(e => e.BusinessEntityId == personId && e.EmailAddressId == emailAddressId);
        }

        [GraphQLMetadata("emailAddresses")]
        public IEnumerable<EmailAddress> GetEmailAddresses()
        {
            return _context.EmailAddresses.ToList();
        }

        [GraphQLMetadata("password")]
        public Password GetPassword(int businessEntityId)
        {
            return _context.Passwords.FirstOrDefault(p => p.BusinessEntityId == businessEntityId);
        }

        [GraphQLMetadata("passwords")]
        public IEnumerable<Password> GetPasswords()
        {
            return _context.Passwords.ToList();
        }

        [GraphQLMetadata("person")]
        public Person GetPerson(int businessEntityId)
        {
            return _context.People.FirstOrDefault(p => p.BusinessEntityId == businessEntityId);
        }

        [GraphQLMetadata("persons")]
        public IEnumerable<Person> GetPersons()
        {
            return _context.People.ToList();
        }

        [GraphQLMetadata("personPhone")]
        public PersonPhone GetPersonPhone(int businessEntityId, string phoneNumber, int phoneNumberTypeId)
        {
            return _context.PersonPhones.FirstOrDefault(
                p => p.BusinessEntityId == businessEntityId &&
                p.PhoneNumber == phoneNumber &&
                p.PhoneNumberTypeId == phoneNumberTypeId);
        }

        [GraphQLMetadata("personPhones")]
        public IEnumerable<PersonPhone> GetPersonPhones()
        {
            return _context.PersonPhones.ToList();
        }

        [GraphQLMetadata("phoneNumberType")]
        public PhoneNumberType GetPhoneNumberType(int phoneNumberTypeId)
        {
            return _context.PhoneNumberTypes.FirstOrDefault(p => p.PhoneNumberTypeId == phoneNumberTypeId);
        }

        [GraphQLMetadata("phoneNumberTypes")]
        public IEnumerable<PhoneNumberType> GetPhoneNumberTypes()
        {
            return _context.PhoneNumberTypes.ToList();
        }

        [GraphQLMetadata("stateProvince")]
        public StateProvince GetStateProvince(int stateProvinceId)
        {
            return _context.StateProvinces.FirstOrDefault(s => s.StateProvinceId == stateProvinceId);
        }

        [GraphQLMetadata("stateProvinces")]
        public IEnumerable<StateProvince> GetStateProvinces()
        {
            return _context.StateProvinces.ToList();
        }
        #endregion

        #region Production
        [GraphQLMetadata("billOfMaterial")]
        public BillOfMaterial GetBillOfMaterial(int billOfMaterialsId)
        {
            return _context.BillOfMaterials.FirstOrDefault(b => b.BillOfMaterialsId == billOfMaterialsId);
        }

        [GraphQLMetadata("billOfMaterials")]
        public IEnumerable<BillOfMaterial> GetBillOfMaterials()
        {
            return _context.BillOfMaterials.ToList();
        }

        [GraphQLMetadata("culture")]
        public Culture GetCulture(string cultureId)
        {
            return _context.Cultures.FirstOrDefault(c => c.CultureId == cultureId);
        }

        [GraphQLMetadata("cultures")]
        public IEnumerable<Culture> GetCultures()
        {
            return _context.Cultures.ToList();
        }

        [GraphQLMetadata("illustration")]
        public Illustration GetIllustration(int illustrationId)
        {
            return _context.Illustrations.FirstOrDefault(i => i.IllustrationId == illustrationId);
        }

        [GraphQLMetadata("illustrations")]
        public IEnumerable<Illustration> GetIllustrations()
        {
            return _context.Illustrations.ToList();
        }

        [GraphQLMetadata("location")]
        public Location GetLocation(int locationId)
        {
            return _context.Locations.FirstOrDefault(l => l.LocationId == locationId);
        }

        [GraphQLMetadata("locations")]
        public IEnumerable<Location> GetLocations()
        {
            return _context.Locations.ToList();
        }

        [GraphQLMetadata("product")]
        public Product GetProduct(int productId)
        {
            return _context.Products.FirstOrDefault(p => p.ProductId == productId);
        }

        [GraphQLMetadata("products")]
        public IEnumerable<Product> GetProducts()
        {
            return _context.Products.ToList();
        }

        [GraphQLMetadata("productCategory")]
        public ProductCategory GetProductCategory(int productCategoryId)
        {
            return _context.ProductCategories.FirstOrDefault(p => p.ProductCategoryId == productCategoryId);
        }

        [GraphQLMetadata("productCategories")]
        public IEnumerable<ProductCategory> GetProductCategories()
        {
            return _context.ProductCategories.ToList();
        }

        [GraphQLMetadata("productCostHistory")]
        public IEnumerable<ProductCostHistory> GetProductCostHistory(int productId, DateTime startDate)
        {
            return _context.ProductCostHistories.Where(
                p => p.ProductId == productId &&
                p.StartDate >= startDate).ToList();
        }

        [GraphQLMetadata("productCostHistories")]
        public IEnumerable<ProductCostHistory> GetProductCostHistories()
        {
            return _context.ProductCostHistories.ToList();
        }

        [GraphQLMetadata("productDescription")]
        public ProductDescription GetProductDescription(int productDescriptionId)
        {
            return _context.ProductDescriptions.FirstOrDefault(p => p.ProductDescriptionId == productDescriptionId);
        }

        [GraphQLMetadata("productDescriptions")]
        public IEnumerable<ProductDescription> GetProductDescriptions()
        {
            return _context.ProductDescriptions.ToList();
        }

        [GraphQLMetadata("productInventory")]
        public ProductInventory GetProductInventory(int productId, int locationId)
        {
            return _context.ProductInventories.FirstOrDefault(
                p => p.ProductId == productId &&
                p.LocationId == locationId);
        }

        [GraphQLMetadata("productInventories")]
        public IEnumerable<ProductInventory> GetProductInventories()
        {
            return _context.ProductInventories.ToList();
        }

        [GraphQLMetadata("productListPriceHistory")]
        public IEnumerable<ProductListPriceHistory> GetProductListPriceHistory(int productId, DateTime startDate)
        {
            return _context.ProductListPriceHistories.Where(
                p => p.ProductId == productId &&
                p.StartDate >= startDate).ToList();
        }

        [GraphQLMetadata("productListPriceHistories")]
        public IEnumerable<ProductListPriceHistory> GetProductListPriceHistories()
        {
            return _context.ProductListPriceHistories.ToList();
        }

        [GraphQLMetadata("productModel")]
        public ProductModel GetProductModel(int productModelId)
        {
            return _context.ProductModels.FirstOrDefault(p => p.ProductModelId == productModelId);
        }

        [GraphQLMetadata("productModels")]
        public IEnumerable<ProductModel> GetProductModels()
        {
            return _context.ProductModels.ToList();
        }

        [GraphQLMetadata("productModelIllustration")]
        public ProductModelIllustration GetProductModelIllustration(int productModelId, int illustrationId)
        {
            return _context.ProductModelIllustrations.FirstOrDefault(
                p => p.ProductModelId == productModelId &&
                p.IllustrationId == illustrationId);
        }

        [GraphQLMetadata("productModelIllustrations")]
        public IEnumerable<ProductModelIllustration> GetProductModelIllustrations()
        {
            return _context.ProductModelIllustrations.ToList();
        }

        [GraphQLMetadata("productModelProductDescriptionCulture")]
        public ProductModelProductDescriptionCulture GetProductModelProductDescriptionCulture(int productModelId, int descriptionId, string cultureId)
        {
            return _context.ProductModelProductDescriptionCultures.FirstOrDefault(
                p => p.ProductModelId == productModelId &&
                p.ProductDescriptionId == descriptionId &&
                p.CultureId == cultureId);
        }

        [GraphQLMetadata("productModelProductDescriptionCultures")]
        public IEnumerable<ProductModelProductDescriptionCulture> GetProductModelProductDescriptionCultures()
        {
            return _context.ProductModelProductDescriptionCultures.ToList();
        }

        [GraphQLMetadata("productPhoto")]
        public ProductPhoto GetProductPhoto(int productPhotoId)
        {
            return _context.ProductPhotos.FirstOrDefault(p => p.ProductPhotoId == productPhotoId);
        }

        [GraphQLMetadata("productPhotos")]
        public IEnumerable<ProductPhoto> GetProductPhotos()
        {
            return _context.ProductPhotos.ToList();
        }

        [GraphQLMetadata("productProductPhoto")]
        public ProductProductPhoto GetProductProductPhoto(int productId, int productPhotoId)
        {
            return _context.ProductProductPhotos.FirstOrDefault(
                p => p.ProductId == productId &&
                p.ProductPhotoId == productPhotoId);
        }

        [GraphQLMetadata("productProductPhotos")]
        public IEnumerable<ProductProductPhoto> GetProductProductPhotos()
        {
            return _context.ProductProductPhotos.ToList();
        }

        [GraphQLMetadata("productReview")]
        public ProductReview GetProductReview(int productReviewId)
        {
            return _context.ProductReviews.FirstOrDefault(p => p.ProductReviewId == productReviewId);
        }

        [GraphQLMetadata("productReviews")]
        public IEnumerable<ProductReview> GetProductReviews()
        {
            return _context.ProductReviews.ToList();
        }

        [GraphQLMetadata("productSubcategory")]
        public ProductSubcategory GetProductSubcategory(int productSubcategoryId)
        {
            return _context.ProductSubcategories.FirstOrDefault(p => p.ProductSubcategoryId == productSubcategoryId);
        }

        [GraphQLMetadata("productSubcategories")]
        public IEnumerable<ProductSubcategory> GetProductSubcategories()
        {
            return _context.ProductSubcategories.ToList();
        }

        [GraphQLMetadata("scrapReason")]
        public ScrapReason GetScrapReason(int scrapReasonId)
        {
            return _context.ScrapReasons.FirstOrDefault(s => s.ScrapReasonId == scrapReasonId);
        }

        [GraphQLMetadata("scrapReasons")]
        public IEnumerable<ScrapReason> GetScrapReasons()
        {
            return _context.ScrapReasons.ToList();
        }

        [GraphQLMetadata("transactionHistory")]
        public TransactionHistory GetTransactionHistory(int transactionId)
        {
            return _context.TransactionHistories.FirstOrDefault(t => t.TransactionId == transactionId);
        }

        [GraphQLMetadata("transactionHistories")]
        public IEnumerable<TransactionHistory> GetTransactionHistories()
        {
            return _context.TransactionHistories.ToList();
        }

        [GraphQLMetadata("transactionHistoryArchive")]
        public TransactionHistoryArchive GetTransactionHistoryArchive(int transactionId)
        {
            return _context.TransactionHistoryArchives.FirstOrDefault(t => t.TransactionId == transactionId);
        }

        [GraphQLMetadata("transactionHistoryArchives")]
        public IEnumerable<TransactionHistoryArchive> GetTransactionHistoryArchives()
        {
            return _context.TransactionHistoryArchives.ToList();
        }

        [GraphQLMetadata("unitMeasure")]
        public UnitMeasure GetUnitMeasure(string unitMeasureCode)
        {
            return _context.UnitMeasures.FirstOrDefault(u => u.UnitMeasureCode == unitMeasureCode);
        }

        [GraphQLMetadata("unitMeasures")]
        public IEnumerable<UnitMeasure> GetUnitMeasures()
        {
            return _context.UnitMeasures.ToList();
        }

        [GraphQLMetadata("workOrder")]
        public WorkOrder GetWorkOrder(int workOrderId)
        {
            return _context.WorkOrders.FirstOrDefault(w => w.WorkOrderId == workOrderId);
        }

        [GraphQLMetadata("workOrders")]
        public IEnumerable<WorkOrder> GetWorkOrders()
        {
            return _context.WorkOrders.ToList();
        }

        [GraphQLMetadata("workOrderRouting")]
        public WorkOrderRouting GetWorkOrderRouting(int workOrderId, int productId, int operationSequence)
        {
            return _context.WorkOrderRoutings.FirstOrDefault(
                w => w.WorkOrderId == workOrderId &&
                w.ProductId == productId &&
                w.OperationSequence == operationSequence);
        }

        [GraphQLMetadata("workOrderRoutings")]
        public IEnumerable<WorkOrderRouting> GetWorkOrderRoutings()
        {
            return _context.WorkOrderRoutings.ToList();
        }
        #endregion

        #region Purchasing
        [GraphQLMetadata("productVendor")]
        public ProductVendor GetProductVendor(int productId, int businessEntityId)
        {
            return _context.ProductVendors.FirstOrDefault(
                p => p.ProductId == productId && 
                p.BusinessEntityId == businessEntityId);
        }

        [GraphQLMetadata("productVendors")]
        public IEnumerable<ProductVendor> GetProductVendors()
        {
            return _context.ProductVendors.ToList();
        }

        [GraphQLMetadata("purchaseOrderDetail")]
        public PurchaseOrderDetail GetPurchaseOrderDetail(int purchaseOrderId, int purchaseOrderDetailId)
        {
            return _context.PurchaseOrderDetails.FirstOrDefault(
                p => p.PurchaseOrderId == purchaseOrderId && 
                p.PurchaseOrderDetailId == purchaseOrderDetailId);
        }

        [GraphQLMetadata("purchaseOrderDetails")]
        public IEnumerable<PurchaseOrderDetail> GetPurchaseOrderDetails()
        {
            return _context.PurchaseOrderDetails.ToList();
        }

        [GraphQLMetadata("purchaseOrderHeader")]
        public PurchaseOrderHeader GetPurchaseOrderHeader(int purchaseOrderId)
        {
            return _context.PurchaseOrderHeaders.FirstOrDefault(p => p.PurchaseOrderId == purchaseOrderId);
        }

        [GraphQLMetadata("purchaseOrderHeaders")]
        public IEnumerable<PurchaseOrderHeader> GetPurchaseOrderHeaders()
        {
            return _context.PurchaseOrderHeaders.ToList();
        }

        [GraphQLMetadata("shipMethod")]
        public ShipMethod GetShipMethod(int shipMethodId)
        {
            return _context.ShipMethods.FirstOrDefault(s => s.ShipMethodId == shipMethodId);
        }

        [GraphQLMetadata("shipMethods")]
        public IEnumerable<ShipMethod> GetShipMethods()
        {
            return _context.ShipMethods.ToList();
        }

        [GraphQLMetadata("vendor")]
        public Vendor GetVendor(int businessEntityId)
        {
            return _context.Vendors.FirstOrDefault(v => v.BusinessEntityId == businessEntityId);
        }

        [GraphQLMetadata("vendors")]
        public IEnumerable<Vendor> GetVendors()
        {
            return _context.Vendors.ToList();
        }
        #endregion

        #region Sales
        [GraphQLMetadata("store")]
        public Store GetStore(int businessEntityId)
        {
            return _context.Stores.FirstOrDefault(s => s.BusinessEntityId == businessEntityId);
        }

        [GraphQLMetadata("stores")]
        public IEnumerable<Store> GetStores()
        {
            return _context.Stores.ToList();


        }
        [GraphQLMetadata("specialOfferProduct")]
        public SpecialOfferProduct GetSpecialOfferProduct(int specialOfferId, int productId)
        {
            return _context.SpecialOfferProducts.FirstOrDefault(
                s => s.SpecialOfferId == specialOfferId &&
                s.ProductId == productId);
        }

        [GraphQLMetadata("specialOfferProducts")]
        public IEnumerable<SpecialOfferProduct> GetSpecialOfferProducts()
        {
            return _context.SpecialOfferProducts.ToList();

        }

        [GraphQLMetadata("specialOffer")]
        public SpecialOffer GetSpecialOffer(int specialOfferId)
        {
            return _context.SpecialOffers.FirstOrDefault(s => s.SpecialOfferId == specialOfferId);
        }

        [GraphQLMetadata("specialOffers")]
        public IEnumerable<SpecialOffer> GetSpecialOffers()
        {
            return _context.SpecialOffers.ToList();

        }

        [GraphQLMetadata("shoppingCartItem")]
        public ShoppingCartItem GetShoppingCartItem(int shoppingCartItemId)
        {
            return _context.ShoppingCartItems.FirstOrDefault(s => s.ShoppingCartItemId == shoppingCartItemId);
        }

        [GraphQLMetadata("shoppingCartItems")]
        public IEnumerable<ShoppingCartItem> GetShoppingCartItems()
        {
            return _context.ShoppingCartItems.ToList();

        }

        [GraphQLMetadata("salesTerritoryHistory")]
        public SalesTerritoryHistory GetSalesTerritoryHistory(int salesPersonId, int territoryId, DateTime startDate)
        {
            return _context.SalesTerritoryHistories.FirstOrDefault(
                s => s.TerritoryId == territoryId &&
                s.BusinessEntityId == salesPersonId &&
                s.StartDate >= startDate);
        }

        [GraphQLMetadata("salesTerritoryHistories")]
        public IEnumerable<SalesTerritoryHistory> GetSalesTerritoryHistories()
        {
            return _context.SalesTerritoryHistories.ToList();

        }

        [GraphQLMetadata("salesTerritory")]
        public SalesTerritory GetSalesTerritory(int territoryId)
        {
            return _context.SalesTerritories.FirstOrDefault(s => s.TerritoryId == territoryId);
        }

        [GraphQLMetadata("salesTerritories")]
        public IEnumerable<SalesTerritory> GetSalesTerritories()
        {
            return _context.SalesTerritories.ToList();

        }

        [GraphQLMetadata("salesTaxRate")]
        public SalesTaxRate GetSalesTaxRate(int salesTaxRateId)
        {
            return _context.SalesTaxRates.FirstOrDefault(s => s.SalesTaxRateId == salesTaxRateId);
        }

        [GraphQLMetadata("salesTaxRates")]
        public IEnumerable<SalesTaxRate> GetSalesTaxRates()
        {
            return _context.SalesTaxRates.ToList();

        }

        [GraphQLMetadata("salesReason")]
        public SalesReason GetSalesReason(int salesReasonId)
        {
            return _context.SalesReasons.FirstOrDefault(s => s.SalesReasonId == salesReasonId);
        }

        [GraphQLMetadata("salesReasons")]
        public IEnumerable<SalesReason> GetSalesReasons()
        {
            return _context.SalesReasons.ToList();
        }

        [GraphQLMetadata("salesPersonQuotaHistory")]
        public SalesPersonQuotaHistory GetSalesPersonQuotaHistory(int businessEntityId, DateTime quotaDate)
        {
            return _context.SalesPersonQuotaHistories.FirstOrDefault(
                s => s.BusinessEntityId == businessEntityId &&
                s.QuotaDate <= quotaDate);
        }

        [GraphQLMetadata("salesPersonQuotaHistories")]
        public IEnumerable<SalesPersonQuotaHistory> GetSalesPersonQuotaHistories()
        {
            return _context.SalesPersonQuotaHistories.ToList();
        }

        [GraphQLMetadata("salesPerson")]
        public SalesPerson GetSalesPerson(int businessEntityId)
        {
            return _context.SalesPeople.FirstOrDefault(s => s.BusinessEntityId == businessEntityId);
        }

        [GraphQLMetadata("salesPersons")]
        public IEnumerable<SalesPerson> GetSalesPersons()
        {
            return _context.SalesPeople.ToList();
        }

        [GraphQLMetadata("salesOrderHeaderSalesReason")]
        public SalesOrderHeaderSalesReason GetSalesOrderHeaderSalesReason(int salesOrderId, int salesReasonId)
        {
            return _context.SalesOrderHeaderSalesReasons.FirstOrDefault(
                s => s.SalesOrderId == salesOrderId &&
                s.SalesReasonId == salesReasonId);
        }

        [GraphQLMetadata("salesOrderHeaderSalesReasons")]
        public IEnumerable<SalesOrderHeaderSalesReason> GetSalesOrderHeaderSalesReasons()
        {
            return _context.SalesOrderHeaderSalesReasons.ToList();
        }

        [GraphQLMetadata("salesOrderHeader")]
        public SalesOrderHeader GetSalesOrderHeader(int salesOrderId)
        {
            return _context.SalesOrderHeaders.FirstOrDefault(s => s.SalesOrderId == salesOrderId);
        }

        [GraphQLMetadata("salesOrderHeaders")]
        public IEnumerable<SalesOrderHeader> GetSalesOrderHeaders()
        {
            return _context.SalesOrderHeaders.ToList();
        }

        [GraphQLMetadata("salesOrderDetail")]
        public SalesOrderDetail GetSalesOrderDetail(int salesOrderId, int salesOrderDetailId)
        {
            return _context.SalesOrderDetails.FirstOrDefault(
                s => s.SalesOrderId == salesOrderId &&
                s.SalesOrderDetailId == salesOrderDetailId);
        }

        [GraphQLMetadata("salesOrderDetails")]
        public IEnumerable<SalesOrderDetail> GetSalesOrderDetails()
        {
            return _context.SalesOrderDetails.ToList();
        }

        [GraphQLMetadata("personCreditCard")]
        public PersonCreditCard GetPersonCreditCard(int personId, int creditCardId)
        {
            return _context.PersonCreditCards.FirstOrDefault(
                p => p.BusinessEntityId == personId && 
                p.CreditCardId == creditCardId);
        }

        [GraphQLMetadata("personCreditCards")]
        public IEnumerable<PersonCreditCard> GetPersonCreditCards()
        {
            return _context.PersonCreditCards.ToList();
        }

        [GraphQLMetadata("customer")]
        public Customer GetCustomer(int customerId)
        {
            return _context.Customers.FirstOrDefault(c => c.CustomerId == customerId);
        }

        [GraphQLMetadata("customers")]
        public IEnumerable<Customer> GetCustomers()
        {
            return _context.Customers.ToList();
        }

        [GraphQLMetadata("currencyRate")]
        public CurrencyRate GetCurrencyRate(int currencyRateId)
        {
            return _context.CurrencyRates.FirstOrDefault(c => c.CurrencyRateId == currencyRateId);
        }

        [GraphQLMetadata("currencyRates")]
        public IEnumerable<CurrencyRate> GetCurrencyRates()
        {
            return _context.CurrencyRates.ToList();
        }

        [GraphQLMetadata("currency")]
        public Currency GetCurrency(string currencyCode)
        {
            return _context.Currencies.FirstOrDefault(c => c.CurrencyCode == currencyCode);
        }

        [GraphQLMetadata("currencies")]
        public IEnumerable<Currency> GetCurrencies()
        {
            return _context.Currencies.ToList();
        }

        [GraphQLMetadata("creditCard")]
        public CreditCard GetCreditCard(int creditCardId)
        {
            return _context.CreditCards.FirstOrDefault(c => c.CreditCardId == creditCardId);
        }

        [GraphQLMetadata("creditCards")]
        public IEnumerable<CreditCard> GetCreditCards()
        {
            return _context.CreditCards.ToList();
        }

        [GraphQLMetadata("countryRegionCurrency")]
        public CountryRegionCurrency GetCountryRegionCurrency(string countryRegionCode, string currencyCode)
        {
            return _context.CountryRegionCurrencies.FirstOrDefault(
                c => c.CountryRegionCode == countryRegionCode && 
                c.CurrencyCode == currencyCode);
        }

        [GraphQLMetadata("countryRegionCurrencies")]
        public IEnumerable<CountryRegionCurrency> GetCountryRegionCurrencies()
        {
            return _context.CountryRegionCurrencies.ToList();
        }
        #endregion
    }
}
