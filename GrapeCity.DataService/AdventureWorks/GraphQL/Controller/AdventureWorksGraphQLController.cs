﻿using GrapeCity.DataService.AdventureWorks.Models;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.GraphQL
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiVersionNeutral]
    [Route("adventureworks/graphql")]
    [ApiController]
    public class AdventureWorksGraphQLController : ControllerBase
    {
        private readonly AdventureWorksContext _context;

        public AdventureWorksGraphQLController(AdventureWorksContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            if (query == null)
            {
                return BadRequest();
            }
            using (var schema = new Schema { Query = new AdventureWorksQuery(_context) })
            {
                var inputs = query.Variables.ToInputs();
                var result = await new DocumentExecuter().ExecuteAsync(_ =>
                {
                    _.Inputs = inputs;
                    _.Schema = schema;
                    _.OperationName = query.OperationName;
                    _.Query = query.Query;
                    _.ExposeExceptions = true;

                }).ConfigureAwait(false);

                if (result.Errors?.Count > 0)
                {
                    return BadRequest();
                }

                return Ok(new { result.Data });  // TODO: return Ok(result) is not like .Net framework version, so only return the data part 
            }
        }
    }
}
