﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Infra
{

    public class PagedResponse<T> 
    {
        public PageMetaData MetaData { get; private set; }

        public IQueryable<T> Source { get; private set; }

        public PagedResponse(IQueryable<T> source, int count, PagingParameters parameters)
        {
            this.Source = source;
            this.MetaData = new PageMetaData(count, parameters);
        }
    }
}
