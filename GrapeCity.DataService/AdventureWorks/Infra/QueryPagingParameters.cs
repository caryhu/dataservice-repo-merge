﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Infra
{
    public class PagingParameters
    {
		const int maxPageSize = 500;
		/// <summary>
		/// Gets or sets the Page number to return records. Default = 1.
		/// </summary>
		public int PageNumber { get; set; } = 1;

		private int _pageSize = 100;
		/// <summary>
		/// Gets or Sets the page size to return records. Default = 100, Max = 500
		/// </summary>
		public int PageSize
		{
			get
			{
				return _pageSize;
			}
			set
			{
				_pageSize = (value > maxPageSize) ? maxPageSize : value;
			}
		}
	}

	public class PageMetaData
    {
		public int CurrentPage { get; private set; }
		public int TotalPages { get; private set; }
		public int PageSize { get; private set; }
		public int TotalCount { get; private set; }

		public bool HasPrevious => CurrentPage > 1;
		public bool HasNext => CurrentPage < TotalPages;

		public PageMetaData(int count, PagingParameters parameters)
        {
			TotalCount = count;
			PageSize = parameters.PageSize;
			CurrentPage = parameters.PageNumber;
			TotalPages = (int)Math.Ceiling(count / (double)parameters.PageSize);
		}
	}
}
