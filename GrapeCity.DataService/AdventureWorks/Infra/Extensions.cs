﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeCity.DataService.AdventureWorks.Infra
{
    public static class Extensions
    {

        public static IQueryable<T> GetPagedData<T>(this IQueryable<T> source,int pageNumber,int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return items;
        }

        public static IQueryable<T> GetPagedData<T>(this IQueryable<T> source, PagingParameters parameters  )
        {
            return GetPagedData(source, parameters.PageNumber, parameters.PageSize);
        }

        public static PagedResponse<T2> CreatePagedResponse<T1,T2>(this IQueryable<T1> source,PagingParameters pagingParameters, Expression<Func<T1,T2>> converter)
        {
            var count = source.Count();
            var pagedCol = source.GetPagedData(pagingParameters).Select(converter);
            var response = new PagedResponse<T2>(pagedCol, count, pagingParameters);
            return response;
        }
	}
}
