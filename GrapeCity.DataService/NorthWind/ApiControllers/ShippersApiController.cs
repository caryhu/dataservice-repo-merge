﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GrapeCity.DataService.NorthWind.Models;
using GrapeCity.DataService.NorthWind.DTO;

namespace GrapeCity.DataService.NorthWind.ApiControllers
{
    
    [Route("northwind/api/Shippers")]
    [ApiController]
    public class ShippersApiController : ControllerBase
    {
        private readonly NorthwindContext _context;

        public ShippersApiController(NorthwindContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IQueryable<ShipperDto> GetShippers()
        {
            return _context.Shippers.Select(DtoConverter.AsShipperDto);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ShipperDto>> GetShipper(int id)
        {
            var shipper = await _context.Shippers.FindAsync(id);

            if (shipper == null)
            {
                return NotFound();
            }

            return DtoConverter.ConvertToShipperDto(shipper);
        }

        [HttpGet("{id:int}/Orders")]
        public IQueryable<OrderDto> GetOrders(int id)
        {
            return _context.Shippers.Where(s => s.ShipperId == id).SelectMany(s => s.Orders).Select(DtoConverter.AsOrderDto);
        }

    }
}
