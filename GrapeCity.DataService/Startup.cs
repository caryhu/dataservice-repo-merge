using System;
using System.Linq;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OData.Edm;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using GraphQL.Server.Ui.GraphiQL;
using GrapeCity.DataService.AdventureWorks;
using GrapeCity.DataService.AdventureWorks.Models;
using GrapeCity.DataService.NorthWind.Models;

namespace GrapeCity.DataService
{
    public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
    {
        readonly IApiVersionDescriptionProvider provider;

        public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider) =>
          this.provider = provider;

        public void Configure(SwaggerGenOptions options)
        {
            foreach (var description in provider.ApiVersionDescriptions)
            {
                options.SwaggerDoc(
                  description.GroupName,
                    new OpenApiInfo()
                    {
                        Title = $"Sample API ",
                        Version = "0.0.1",
                    });
            }
        }
    }
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if(services==null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            services.AddMvcCore(options =>
            {
                // options.EnableEndpointRouting = false;
                options.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
            }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.MaxDepth = int.MaxValue;
            });

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedHost | ForwardedHeaders.XForwardedProto;
            });

            services.AddDbContext<AdventureWorksContext>(options =>
                options
                .UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("Context")));

            services.AddDbContext<NorthwindContext>(options =>
                options
                .UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("Context")));

            services.AddAdventureWorkServices();
            services.AddHttpContextAccessor();
            services.AddControllers();
            services.AddOData();

            var selector = services.First(s => s.ServiceType == typeof(IActionSelector) && s.ImplementationFactory != null);
            services.Remove(selector);
            services.Add(new ServiceDescriptor(selector.ServiceType, selector.ImplementationFactory, ServiceLifetime.Singleton));
            services.AddSingleton<IActionSelector>(s =>
            {
                return new NamespaceSelector((IActionSelector)selector.ImplementationFactory(s));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
#pragma warning disable CA1822 // Mark members as static
        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
#pragma warning restore CA1822 // Mark members as static
        {
            // app.UseForwardedHeaders();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseDefaultFiles();
            app.UseStaticFiles();


            var graphiQLAWOptions = new GraphiQLOptions();
            graphiQLAWOptions.GraphQLEndPoint = "/adventureworks/graphql";
            graphiQLAWOptions.GraphiQLPath = "/adventurework/ui/graphql";
            app.UseGraphiQLServer(graphiQLAWOptions);

            var graphiQLNWOptions = new GraphiQLOptions();
            graphiQLNWOptions.GraphQLEndPoint = "/northwind/graphql";
            graphiQLNWOptions.GraphiQLPath = "/northwind/ui/graphql";
            app.UseGraphiQLServer(graphiQLNWOptions);

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.EnableDependencyInjection();
                endpoints.Select().Expand().Filter().OrderBy().MaxTop(100).Count();
                endpoints.MapODataRoute("northwind", "northwind/v1", GenerateNorthWindEdmModel());
                endpoints.MapODataRoute("northwind", "northwind/v2", GenerateNorthWindEdmModel());
                endpoints.MapODataRoute("adventureworks", "adventureworks/v1", GenerateAdventureWorksEdmModel());
                endpoints.MapODataRoute("adventureworks", "adventureworks/v2", GenerateAdventureWorksEdmModel());
            });

            app.UseSwaggerUI(
                options =>
                {
                    options.SwaggerEndpoint(
                        $"/docs/AdventureWorks/restful_swagger.json",
                        "Restful AdventureWorks");
                    options.SwaggerEndpoint(
                        $"/docs/AdventureWorks/odata_swagger.json",
                        "OData AdventureWorks");
                    options.SwaggerEndpoint(
                            $"/docs/NorthWind/restful_swagger.json",
                            "Restful NorthWind");
                    options.SwaggerEndpoint(
                        $"/docs/NorthWind/V2/odata_swagger.json",
                        "OData NorthWind V2");
                    options.SwaggerEndpoint(
                        $"/docs/NorthWind/V1/odata_swagger.json",
                        "OData NorthWind V1");
                });
        }
        private static IEdmModel GenerateAdventureWorksEdmModel()
        {
            var builder = new ODataConventionModelBuilder();
            #region Human Resources
            builder.EntitySet<Department>("Departments");
            builder.EntitySet<AdventureWorks.Models.Employee>("Employees");
            builder.EntitySet<EmployeePayHistory>("EmployeePayHistories");
            builder.EntityType<EmployeePayHistory>().HasKey(t => new
            {
                t.BusinessEntityId,
                t.RateChangeDate
            });
            builder.EntitySet<EmployeeDepartmentHistory>("EmployeeDepartmentHistories");
            builder.EntityType<EmployeeDepartmentHistory>().HasKey(t => new
            {
                t.BusinessEntityId,
                t.DepartmentId,
                t.ShiftId,
                t.StartDate
            });
            builder.EntitySet<JobCandidate>("JobCandidates");
            builder.EntitySet<Shift>("Shifts");
            #endregion

            #region Person
            builder.EntitySet<Address>("Addresses");
            builder.EntitySet<AddressType>("AddressTypes");
            builder.EntitySet<BusinessEntity>("BusinessEntities");
            builder.EntitySet<BusinessEntityAddress>("BusinessEntityAddresses");
            builder.EntitySet<BusinessEntityContact>("BusinessEntityContacts");
            builder.EntitySet<ContactType>("ContactTypes");
            builder.EntitySet<CountryRegion>("CountryRegions");
            builder.EntitySet<EmailAddress>("EmailAddresses");
            builder.EntitySet<PersonPhone>("PersonPhones");
            builder.EntitySet<PhoneNumberType>("PhoneNumberTypes");
            builder.EntitySet<Person>("Persons");
            builder.EntitySet<StateProvince>("StateProvinces");
            #endregion

            #region Production
            builder.EntitySet<BillOfMaterial>("BillOfMaterials");
            builder.EntitySet<Culture>("Cultures");
            builder.EntitySet<Illustration>("Illustrations");
            builder.EntitySet<Location>("Locations");
            builder.EntitySet<ProductCategory>("ProductCategories");
            builder.EntitySet<ProductCostHistory>("ProductCostHistories");
            builder.EntityType<ProductCostHistory>().HasKey(t => new
            {
                t.ProductId,
                t.StartDate
            });
            builder.EntitySet<ProductDescription>("ProductDescriptions");
            builder.EntitySet<ProductInventory>("ProductInventories");
            builder.EntitySet<ProductListPriceHistory>("ProductListPriceHistories");
            builder.EntityType<ProductListPriceHistory>().HasKey(t => new
            {
                t.ProductId,
                t.StartDate
            });
            builder.EntitySet<ProductModelIllustration>("ProductModelIllustrations");
            builder.EntitySet<ProductModel>("ProductModels");
            builder.EntitySet<ProductPhoto>("ProductPhotos");
            builder.EntitySet<ProductReview>("ProductReviews");
            builder.EntitySet<AdventureWorks.Models.Product>("Products");
            builder.EntitySet<ProductSubcategory>("ProductSubcategories");
            builder.EntitySet<ScrapReason>("ScrapReasons");
            builder.EntitySet<TransactionHistory>("TransactionHistories");
            builder.EntitySet<TransactionHistoryArchive>("TransactionHistoryArchives");
            builder.EntitySet<UnitMeasure>("UnitMeasures");
            builder.EntitySet<WorkOrder>("WorkOrders");
            builder.EntitySet<WorkOrderRouting>("WorkOrderRoutings");
            #endregion

            #region Purchasing
            builder.EntitySet<ProductVendor>("ProductVendors");
            builder.EntitySet<PurchaseOrderDetail>("PurchaseOrderDetails");
            builder.EntitySet<PurchaseOrderHeader>("PurchaseOrderHeaders");
            builder.EntitySet<ShipMethod>("ShipMethods");
            builder.EntitySet<Vendor>("Vendors");
            #endregion

            #region Sales
            builder.EntitySet<CreditCard>("CreditCards");
            builder.EntitySet<Currency>("Currencies");
            builder.EntitySet<CurrencyRate>("CurrencyRates");
            builder.EntitySet<AdventureWorks.Models.Customer>("Customers");
            builder.EntitySet<SalesOrderHeader>("SalesOrderHeaders");
            builder.EntitySet<SalesOrderDetail>("SalesOrderDetails");
            builder.EntitySet<SalesPerson>("SalesPersons");
            builder.EntitySet<SalesPersonQuotaHistory>("SalesPersonQuotaHistories");
            builder.EntitySet<SalesReason>("SalesReasons");
            builder.EntitySet<SalesTaxRate>("SalesTaxRates");
            builder.EntitySet<SalesTerritory>("SalesTerritories");
            builder.EntitySet<SalesTerritoryHistory>("SalesTerritoryHistories");
            builder.EntitySet<ShoppingCartItem>("ShoppingCartItems");
            builder.EntitySet<SpecialOffer>("SpecialOffers");
            builder.EntitySet<Store>("Stores");
            #endregion
            return builder.GetEdmModel();
        }
        private static IEdmModel GenerateNorthWindEdmModel()
        {
            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<Category>("Categories");
            builder.EntitySet<NorthWind.Models.Customer>("Customers");
            builder.EntitySet<NorthWind.Models.Product>("Products");
            builder.EntitySet<NorthWind.Models.Employee>("Employees");
            builder.EntitySet<Order>("Orders");
            builder.EntitySet<Territory>("Territories");
            builder.EntityType<OrderDetail>().HasKey(t => new { t.OrderId, t.ProductId });
            builder.EntitySet<OrderDetail>("OrderDetails");
            builder.EntitySet<Shipper>("Shippers");
            builder.EntitySet<Supplier>("Suppliers");
            builder.EntitySet<Region>("Regions");
            return builder.GetEdmModel();
        }

    }
}
