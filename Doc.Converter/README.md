# Doc.Converter

This is used to help modify JSON file for swagger UI to fit API.

You need provide the orignal file created by Doc.* as source files.
Then call convert***Document along with the file name in relative path, for OData provide the version in v* format to fit API path.
The output file can copy to GrapeCity.DataService as static file for swagger UI.
