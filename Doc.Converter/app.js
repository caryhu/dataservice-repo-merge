'use strict';

const fs = require('fs');
const path = require('path');

function convertRestfulDocument(srcFile, targetFile) {
    let rawdata = fs.readFileSync(srcFile);
    let data = JSON.parse(rawdata);

    const paths = data.paths;

    for (const key in paths) {
        let item = paths[key];
        let getNode = item["get"];
        getNode["tags"] = getNode["tags"].map((n) => n.replace(/Api$/, "")); // update tags - remove Api suffix

        let parameters = getNode["parameters"];
        if (parameters) {
            for (let i = 0, count = parameters.length; i < count; i++) {
                if (parameters[i].name === "api-version") { // remove api-version parameter
                    parameters.splice(i, 1);
                    break;
                }
            }
            if (parameters.length === 0) {
                delete getNode["parameters"];
            }
        }

        let schemas = data.components.schemas;
        let schemas2 = {};
        for (const typeName in schemas) {
            if (typeName.endsWith("Dto")) { // only keep types with Dto suffix for Api
                schemas2[typeName] = schemas[typeName];
            }
        }
        data.components.schemas = schemas2;
    }

    fs.writeFileSync(targetFile, JSON.stringify(data, null, 2));
}

function convertODataDocument(srcFile, targetFile, version) {
    let rawdata = fs.readFileSync(srcFile);
    let data = JSON.parse(rawdata);

    const paths = data.paths;
    let paths2 = {};
    for (const key in paths) {
        let item = paths2[key.replace(/\/odata\//, "/odata/" + version + "/")] = paths[key];  // add vertion to path
        let getNode = item["get"];

        let parameters = getNode["parameters"];
        if (parameters) {
            for (let i = 0, count = parameters.length; i < count; i++) {
                if (parameters[i].name === "api-version") { // remove api-version parameter
                    parameters.splice(i, 1);
                    break;
                }
            }
            if (parameters.length === 0) {
                delete getNode["parameters"];
            }
        }

    }
    data.paths = paths2;

    fs.writeFileSync(targetFile, JSON.stringify(data, null, 2));
}

const sourceDirectory = ".\\Src\\V1\\";
const targetDirectory = "..\\GrapeCity.DataService\\wwwroot\\docs\\v1\\";

let fileName = "restful_swagger.json";
convertRestfulDocument(sourceDirectory + fileName, targetDirectory + fileName);

fileName = "odata_swagger.json";
convertODataDocument(sourceDirectory + fileName, targetDirectory + fileName, "v1");

console.log("Done!");