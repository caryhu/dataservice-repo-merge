# Data Service Introduction (.Net Core Version)

## Summary
Provide a .Net core version of Dataservice with web API for OData, GraphQL and RESTful, use Northwind database and AdventureWorks database as data source. 

## Notes
1. Doc.Converter use to change the api tag name, remove the api-version parameter, and filter the schemas type(only hold which one endwith Dto).
2. You shuold take the original swagger.json from Doc.OData.AWorks, Doc.OData.Northwind, Doc.Restful.AWorks, Doc.Restful.Northwind. when the web running, you can find the swagger.json, and you shuold copy the original json to Doc.Converter/Src.
3. In GrapCity.DataService folder, NamespaceSelector.cs used to distinguish OData API and RESTful API, you can achieve version control in this file yet.
4. You should keep all the project debug in different iis port, otherwise, different project will work in same workfolder!
5. You can find the database script in database scripts folder, and you should prepare a empty database and ssms, MUST run AdventureWorks script at first, then run NorthWind script.
6. SSMS provide a feature to push local database to Azure, you can right click the database you want transfer, and then choose Tasks, then Deploy Databse to Microsoft Azure SQL Databse...
